import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

/**
 * Servicio de configuración de los paths de los endpoints
 * 
 * @author Miguel Romero
 */
@Injectable()
export class AppEndPoints {

    // API
    public host: string = environment.host;
    public api_name: string = "/inventario-afg-api/api";
    public api_version: string = "/v1";
    public api: string = this.host + this.api_name + this.api_version;

    // Auth    
    public login: string = this.api + "/auth/login";
    public logout: string = this.api + "/auth/logout";

    // Parámetros
    public parametros: string = this.api + "/parametros";

    // Usuarios
    public usuarios: string = this.api + "/usuarios";

    // Roles
    public roles: string = this.api + "/roles";

    // Clientes
    public clientes: string = this.api + "/clientes";

    // Clientes Direcciones
    public clientes_direcciones: string = this.api + "/clientes-direcciones";

    // Servicios
    public servicios: string = this.api + "/servicios";

    // Servicios x Cotización
    public servicios_x_cotizacion: string = this.api + "/servicios-x-cotizacion";

    // Cotizaciones
    public cotizaciones: string = this.api + "/cotizaciones";

    // Productos
    public productos: string = this.api + "/productos";

    // Productos Categorias
    public productos_categorias: string = this.api + "/productos-categorias";

    // Remisiones
    public remisiones: string = this.api + "/remisiones";

    // Productos x Remision
    public producto_x_remision: string = this.api + "/productos-x-remision"

    // Facturas
    public facturas: string = this.api + "/facturas";
    public facturas_estado: string = this.api + "/facturas-estados";
    public facturas_tipos_pago: string = this.api + "/facturas-tipos-pago";

    // Productos x Factura
    public producto_x_factura: string = this.api + "/productos-x-factura"

    // Inversiones
    public inversiones: string = this.api + "/inversiones"; 

    // Citas
    public citas: string = this.api + "/citas";

    // Citas Estados
    public citas_estados: string = this.api + "/citas-estados";

}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TemplateComponent } from './common/template/template.component';
import { SecurityService } from './common/services/security/security.service';

const routes: Routes = [
    { path: '', component: LoginComponent },
    {
        path: '', component: TemplateComponent,
        children: [
            { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule), canActivate: [SecurityService] },
            // Change password
            { path: 'change-password', loadChildren: () => import('./change-password/change-password.module').then(m => m.ChangePasswordModule), canActivate: [SecurityService] },
            // Administración
            { path: 'admin/usuarios', loadChildren: () => import('./admin/usuarios/usuarios.module').then(m => m.UsuariosModule), canActivate: [SecurityService] },
            { path: 'admin/productos', loadChildren: () => import('./admin/productos/productos.module').then(m => m.ProductosModule), canActivate: [SecurityService] },
            { path: 'admin/clientes', loadChildren: () => import('./admin/clientes/clientes.module').then(m => m.ClientesModule), canActivate: [SecurityService] },
            // Citas
            { path: 'citas', loadChildren: () => import('./citas/citas.module').then(m => m.CitasModule), canActivate: [SecurityService] },
            // Facturación
            { path: 'facturacion/remisiones', loadChildren: () => import('./facturacion/remisiones/remisiones.module').then(m => m.RemisionesModule), canActivate: [SecurityService] },
            { path: 'facturacion/facturas', loadChildren: () => import('./facturacion/facturas/facturas.module').then(m => m.FacturasModule), canActivate: [SecurityService] },
            { path: 'facturacion/inversiones', loadChildren: () => import('./facturacion/inversiones/inversiones.module').then(m => m.InversionesModule), canActivate: [SecurityService] }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UsuariosService } from './usuarios.service';
import { UsuariosListComponent } from './usuarios-list/usuarios-list.component';
import { UsuariosFormComponent } from './usuarios-form/usuarios-form.component';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { RolesService } from '../roles/roles.service';
import { SharedModule } from '../../common/modules/shared/shared.module';
import { NoWhitespaceDirective } from '../../common/directives/no-whitespace/no-whitespace.directive';

import { BlockUIModule } from 'ng-block-ui';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

/**
* Modulo de administración de Usuarios
*
* @author Miguel Romero
*/
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        UsuariosRoutingModule,
        BlockUIModule,
        TableModule,
        TooltipModule.forRoot()
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [UsuariosListComponent, UsuariosFormComponent,
        NoWhitespaceDirective],
    providers: [UsuariosService, RolesService]
})
export class UsuariosModule { }

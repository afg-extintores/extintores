import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../common/model/usuario';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { RolesService } from '../../roles/roles.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuariosService } from '../usuarios.service';
import { Rol } from 'src/app/common/model/rol';
import { MessagesService } from 'src/app/common/components/messages/messages.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

/**
* Componente del formulario de Usuarios
*
* @author Miguel Romero
*/
@Component({
    selector: 'afg-usuarios-form',
    templateUrl: './usuarios-form.component.html',
    styleUrls: ['./usuarios-form.component.css']
})
export class UsuariosFormComponent implements OnInit {

    usuario: Usuario;
    usuarioForm: FormGroup;
    submitted: boolean;

    isNew: boolean;
    isEdit: boolean;
    idUsuario: number;

    validEmail: boolean;
    validDocument: boolean;

    roles: Rol[];

    @BlockUI() blockUI: NgBlockUI;

    constructor(private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute,
        private rolService: RolesService, private usuariosService: UsuariosService, private msgService: MessagesService) {
        this.initForm();
        this.loadRoles();
    }

    ngOnInit() {
        this.submitted = false;

        this.usuario = new Usuario();

        this.isNew = true;

        this.validEmail = false;
        this.validDocument = false;

        this.actRoute.params.subscribe(params => {
            if (params['id'] != undefined) {
                this.idUsuario = params['id'];
                this.usuariosService.getUsuarioById(this.idUsuario).subscribe(
                    data => {

                        if (params['edit'] != undefined) {
                            this.isEdit = (params['edit'] == 'true');
                        }

                        this.usuario = data;
                        this.usuario.id = this.idUsuario;

                        this.usuarioForm.get('nombres').setValue(this.usuario.nombres, { onlySelf: true });
                        this.usuarioForm.get('apellidos').setValue(this.usuario.apellidos, { onlySelf: true });
                        this.usuarioForm.get('correo').setValue(this.usuario.correo, { onlySelf: true });
                        this.usuarioForm.get('documento').setValue(this.usuario.documento, { onlySelf: true });
                        this.usuarioForm.get('rol').setValue(this.usuario.rol, { onlySelf: true });
                        this.usuarioForm.get('acceso').setValue(this.usuario.acceso, { onlySelf: true });
                        this.usuarioForm.get('activo').setValue(this.usuario.activo, { onlySelf: true });
                        this.usuarioForm.get('vendedor').setValue(this.usuario.vendedor, { onlySelf: true });

                        if (!this.isEdit) {
                            this.usuarioForm.disable();
                        } else {
                            this.usuarioForm.enable();
                        }

                        this.isNew = false;

                    },
                    error => {
                        console.log(error);
                    }
                )
            }
        });
    }

    private initForm() {
        this.usuarioForm = this.fb.group({
            nombres: new FormControl(null, Validators.required),
            apellidos: new FormControl(null, Validators.required),
            correo: new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
            documento: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(5)]),
            rol: new FormControl(null, Validators.required),
            acceso: new FormControl(false),
            activo: new FormControl(false),
            vendedor: new FormControl(false)
        });
    }

    private loadRoles() {
        this.roles = [];
        this.rolService.getAllRoles().subscribe(
            roles => {
                this.roles = roles;
            },
            error => {
                console.log(error);
            }
        )
    }

    compareFn(c1: Usuario, c2: Usuario): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Valida que el Email ingresado para el usuario no exista en el sistema
     */
    validateEmail() {
        if (!this.usuarioForm.get('correo').pristine) {
            const value = this.usuarioForm.get('correo').value;
            if (value != null && value != '') {
                this.usuariosService.getUsuarioByCorreo(value).subscribe(
                    usuario => {
                        if (usuario != null) {
                            this.validEmail = true
                        } else {
                            this.validEmail = false
                        }
                    },
                    error => {
                        console.log(error)
                    }
                )
            }
        }
    }

    /**
     * Valida que el documento ingresado para el usuario no exista aún en el sistema
     */
    validateDocumento() {
        if (!this.usuarioForm.get('documento').pristine) {
            const value = this.usuarioForm.get('documento').value;
            if (value != null && value != '') {
                this.usuariosService.getUsuarioByDocumento(value).subscribe(
                    usuario => {
                        if (usuario != null) {
                            this.validDocument = true;
                        } else {
                            this.validDocument = false;
                        }
                    },
                    error => {
                        console.log(error);
                    }
                );
            }
        }
    }

    cancelar() {
        this.router.navigate(['/admin/usuarios']);
    }

    saveUsuario(value: any, valid: boolean) {
        this.submitted = true;

        if (valid && !this.validEmail) {

            if (this.isNew) {

                this.blockUI.start('Guardando Usuario');

                let user: Usuario = new Usuario();
                user.nombres = value.nombres;
                user.apellidos = value.apellidos;
                user.correo = value.correo;
                user.documento = value.documento;
                user.rol = value.rol;
                user.acceso = value.acceso;
                user.activo = value.activo;
                user.vendedor = value.vendedor;
                user.createdBy = sessionStorage.getItem('user');

                this.usuariosService.saveUsuario(user).subscribe(
                    usuario => {
                        this.isEdit = true;
                        this.isNew = false;
                        this.usuario = usuario;
                        this.msgService.addMessage("Usuario Agregado Exitosamente.");
                        if (this.usuario.acceso) {
                            this.msgService.addMessage("Usuario Agregado Exitosamente. - Sus datos de acceso son el correo o documento y password - 12345");
                        }
                        this.blockUI.stop();
                    },
                    error => {
                        this.msgService.addError("Error al registrar el Usuario.");
                        this.blockUI.stop();
                    }
                )

            } else {

                this.blockUI.start('Actualizando Usuario');

                this.usuario.nombres = value.nombres;
                this.usuario.apellidos = value.apellidos;
                this.usuario.correo = value.correo;
                this.usuario.documento = value.documento;
                this.usuario.rol = value.rol;
                this.usuario.acceso = value.acceso;
                this.usuario.activo = value.activo;
                this.usuario.vendedor = value.vendedor;
                this.usuario.modifiedBy = sessionStorage.getItem("user");

                this.usuariosService.updateUsuario(this.usuario).subscribe(
                    usuario => {
                        this.isEdit = true;
                        this.isNew = false;
                        this.usuario = usuario;
                        this.msgService.addMessage("Usuario Actualizado Exitosamente.");
                        if (value.acceso) {
                            this.msgService.addMessage("Usuario Editado Exitosamente. - Sus datos de acceso son el correo o documento y password - 12345");
                        }
                        this.blockUI.stop();
                    },
                    error => {
                        this.msgService.addError("Error al actualizar el Usuario.");
                        this.blockUI.stop();
                    }
                );

            }
        }
    }

    cleanForm() {
        this.submitted = false;
        this.usuario = new Usuario();
        this.initForm();
        this.loadRoles();
        this.isEdit = false;
        this.isNew = true;
        this.msgService.close();
        this.validEmail = false
    }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Usuario } from '../../../common/model/usuario';
import { UsuariosService } from '../usuarios.service';
import { Page } from 'src/app/common/model/page';

/**
* Componente del listado de usuarios
*
* @author Miguel Romero
*/
@Component({
    selector: 'afg-usuarios-list',
    templateUrl: './usuarios-list.component.html',
    styleUrls: ['./usuarios-list.component.css']
})
export class UsuariosListComponent implements OnInit {

    usuarios: Usuario[]; // Listado de Usuarios
    idUsuario: number; // Id de Usuario

    cols: any[]; // Columnas de la tabla

    constructor(private router: Router, private usuariosService: UsuariosService) {
    }

    ngOnInit() {
        this.initCols();
        this.getAllUsuarios();
    }

    private initCols() {
        this.cols = [
            { field: 'nombres', header: 'Nombres', width: '15%' },
            { field: 'apellidos', header: 'Apellidos', width: '15%' },
            { field: 'correo', header: 'Correo' },
            { field: 'documento', header: 'Documento' },
            { field: 'rol.nombre', header: 'Rol', width: '10%' },
            { field: 'activo', header: 'Estado', width: '8%' }
        ];
    }

    /**
     * Obtiene el listado de usuarios inicial
     */
    private getAllUsuarios() {
        this.usuariosService.getAllUsuarios().subscribe(
            usuarios => {
                this.usuarios = usuarios;
            },
            error => {
                console.log(error);
            }
        )
    }

    goForm(item, edit) {
        this.router.navigate(['/admin/usuarios/usuario', item.id, edit]);
    }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppEndPoints } from '../../app.endpoints';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Usuario } from '../../common/model/usuario';
import { StringResponse } from '../../common/model/string-response';
import { Page } from '../../common/model/page';

/**
 * Service para el manejo de data de Usuarios
 * 
 * @author Miguel Romero
 */
@Injectable()
export class UsuariosService {

    /**
     * Constructor del Service de Usuarios
     * 
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Usuarios
     */
    public getAllUsuarios(): Observable<Usuario[]> {
        return this.http.get<Usuario[]>(this.endpoints.usuarios);
    }

    /**
     * Obtiene el listado de Usuarios paginados
     */
    public getAllUsuariosPaginados(values: Map<string, string>): Observable<Page> {
        let params = new HttpParams()
            .set('page', values.get('page'))
            .set('size', values.get('size'))
            .set('sort', values.get('sort'))
            ;
        return this.http.get<Page>(this.endpoints.usuarios + '/pageables', { params: params });
    }

    /**
     * Registrav un Usuario
     * @param usuario Usuario a registrar
     */
    public saveUsuario(usuario: Usuario): Observable<Usuario> {
        return this.http.post<Usuario>(this.endpoints.usuarios, usuario);
    }

    /**
     * Actualiza un Usuario
     * @param usuario Usuario a actualizar
     */
    public updateUsuario(usuario: Usuario): Observable<Usuario> {
        return this.http.put<Usuario>(this.endpoints.usuarios, usuario);
    }

    /**
     * Elimina un usuario por Id
     * @param id Id del Usuario
     */
    public deleteUsuario(id: number): Observable<StringResponse> {
        return this.http.delete<StringResponse>(this.endpoints.usuarios + '/' + id);
    }

    /**
     * Obtiene un Usuario por Id
     * @param id Id del Usuario
     */
    public getUsuarioById(id: number): Observable<Usuario> {
        return this.http.get<Usuario>(this.endpoints.usuarios + '/' + id);
    }

    /**
     * Obtiene el listado de Usuarios Vendedores Activos
     */
    public getAllUsuariosVendedoresActivos(): Observable<Usuario[]> {
        return this.http.get<Usuario[]>(this.endpoints.usuarios + '/vendedores-activos');
    }

    /**
     * Obtiene el listado de Usuarios Activos
     */
    public getAllUsuariosActivos(): Observable<Usuario[]> {
        return this.http.get<Usuario[]>(this.endpoints.usuarios + '/activos');
    }

    /**
     * Obtiene un Usuario por correo
     * @param correo Correo del Usuario
     */
    public getUsuarioByCorreo(correo: number): Observable<Usuario> {
        return this.http.get<Usuario>(this.endpoints.usuarios + '/by-correo/' + correo);
    }

    /**
     * Obtiene un Usuario por documento
     * @param documento Documento del Usuario
     */
    public getUsuarioByDocumento(documento: number): Observable<Usuario> {
        return this.http.get<Usuario>(this.endpoints.usuarios + '/by-documento/' + documento);
    }

    /**
     * Obtiene un listado de Usuarios por nombre
     * @param nombre Nombre del Usuario
     */
    public getUsuarioLikeNombre(nombre: string): Observable<Usuario[]> {
        return this.http.get<Usuario[]>(this.endpoints.usuarios + '/find-like-nombre/' + nombre);
    }

}

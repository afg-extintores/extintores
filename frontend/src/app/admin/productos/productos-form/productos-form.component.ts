import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/common/model/producto';
import { FormGroup, FormBuilder, FormControl, Validators, FormControlName } from '@angular/forms';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductosService } from '../productos.service';
import { MessagesService } from 'src/app/common/components/messages/messages.service';

@Component({
    selector: 'afg-productos-form',
    templateUrl: './productos-form.component.html',
    styleUrls: ['./productos-form.component.css']
})
export class ProductosFormComponent implements OnInit {

    producto: Producto;
    productoForm: FormGroup;
    submitted: boolean;

    isNew: boolean;
    isEdit: boolean;
    idProducto: number;

    validCode: boolean;
    errorMaxPrice: boolean;

    @BlockUI() blockUI: NgBlockUI;

    constructor(private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute,
        private msgService: MessagesService, private productosService: ProductosService) {
        this.initForm();
    }

    ngOnInit() {
        this.submitted = false;
        this.producto = new Producto();
        this.isNew = true;
        this.validCode = true;
        this.errorMaxPrice = false;
        this.actRoute.params.subscribe(params => {
            if (params['id'] != undefined) {
                this.idProducto = params['id'];
                this.productosService.getProductoById(this.idProducto).subscribe(
                    data => {

                        if (params['edit'] != undefined) {
                            this.isEdit = (params['edit'] == 'true');
                        }

                        this.producto = data;
                        this.producto.id = this.idProducto;

                        this.productoForm.get('nombre').setValue(this.producto.nombre, { onlySelf: true });
                        this.productoForm.get('descripcion').setValue(this.producto.descripcion, { onlySelf: true });
                        this.productoForm.get('activo').setValue(this.producto.activo, { onlySelf: true });
                        this.productoForm.get('codigo').setValue(this.producto.codigo, { onlySelf: true });
                        this.productoForm.get('precioUnidad').setValue(this.producto.precioUnidad, { onlySelf: true });

                        if (!this.isEdit) {
                            this.productoForm.disable();
                        } else {
                            this.productoForm.enable();
                        }

                        this.isNew = false;
                    },
                    error => {
                        console.log(error);
                    }
                );
            }
        });
        this.subscribeFormChanges();
    }

    private initForm() {
        this.productoForm = this.fb.group({
            codigo: new FormControl(null, Validators.required),
            nombre: new FormControl(null, Validators.required),
            descripcion: new FormControl(null, Validators.required),
            precioUnidad: new FormControl(null, [Validators.required, Validators.min(1), Validators.maxLength(11)]),
            activo: new FormControl(false)
        });
    }

    subscribeFormChanges() {
        this.productoForm.get('precioUnidad').valueChanges.subscribe(
            value => {
                if (String(value).length > 11) {
                    this.errorMaxPrice = true;
                } else {
                    this.errorMaxPrice = false;
                }
            }
        );
    }

    cancelar() {
        this.router.navigate(['/admin/productos']);
    }

    validateProductCode(code) {
        if (code != '') {
            this.productosService.getProductoByCodigo(code).subscribe(
                product => {
                    if (product) {
                        this.validCode = false;
                    } else {
                        this.validCode = true;
                    }
                }
            );
        }
    }

    saveProducto(value: any, valid: boolean) {
        this.submitted = true;
        if (valid) {
            if (this.isNew) {
                this.blockUI.start('Guardando Producto');

                let prod: Producto = new Producto();
                prod.activo = value.activo;
                prod.nombre = value.nombre;
                prod.precioUnidad = value.precioUnidad;
                prod.descripcion = value.descripcion;
                prod.codigo = value.codigo;
                prod.createdBy = sessionStorage.getItem('user');

                this.productosService.saveProducto(prod).subscribe(
                    producto => {
                        this.isEdit = true;
                        this.isNew = false;
                        this.producto = producto;
                        this.msgService.addMessage("Producto Agregado Exitosamente.");
                        this.blockUI.stop();
                    },
                    error => {
                        this.msgService.addError("Error al registrar el producto.");
                        this.blockUI.stop();
                    }
                );
            } else {
                this.blockUI.start('Actualizando Producto');

                this.producto.activo = value.activo;
                this.producto.nombre = value.nombre;
                this.producto.precioUnidad = value.precioUnidad;
                this.producto.codigo = value.codigo;
                this.producto.createdBy = sessionStorage.getItem('user');

                this.productosService.updateProducto(this.producto).subscribe(
                    producto => {
                        this.isEdit = true;
                        this.isNew = false;
                        this.producto = producto;
                        this.msgService.addMessage("Producto Actualizado Exitosamente.");
                        this.blockUI.stop();
                    },
                    error => {
                        this.msgService.addError("Error al actualizar el producto.");
                        this.blockUI.stop();
                    }
                );
            }
        }
    }

    cleanForm() {
        this.submitted = false;
        this.producto = new Producto();
        this.initForm();
        this.isEdit = false;
        this.isNew = true;
        this.submitted = false;
        this.msgService.close();
    }

}

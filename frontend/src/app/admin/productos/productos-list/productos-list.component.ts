import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/common/model/producto';
import { Router } from '@angular/router';
import { ProductosService } from '../productos.service';

@Component({
    selector: 'afg-productos-list',
    templateUrl: './productos-list.component.html',
    styleUrls: ['./productos-list.component.css']
})
export class ProductosListComponent implements OnInit {

    productos: Producto[]; // Listado de Productos
    idProducto: number; // Id de Producto

    cols: any[]; // Columnas de la tabla

    constructor(private router: Router, private productosService: ProductosService) {
        this.getAllProducts();
    }

    ngOnInit() {
        this.cols = [
            { field: 'codigo', header: 'Código' },
            { field: 'nombre', header: 'Nombre' },
            { field: 'precioUnidad', header: 'Precio Unidad' },
            { field: 'activo', header: 'Estado' }
        ];
    }

    private getAllProducts() {
        this.productosService.getAllProductos().subscribe(
            productos => {
                this.productos = productos;
            },
            error => {
                console.log(error);
            }
        );
    }

    goForm(item, edit) {
        this.router.navigate(['/admin/productos/producto', item.id, edit]);
    }

}

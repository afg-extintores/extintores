import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductosListComponent } from './productos-list/productos-list.component';
import { ProductosFormComponent } from './productos-form/productos-form.component';
import { ProductosService } from './productos.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/common/modules/shared/shared.module';
import { ProductosRoutingModule } from './productos-routing.module';
import { BlockUIModule } from 'ng-block-ui';
import { TableModule } from 'primeng/table';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

/**
* Modulo de administración de Productos
*
* @author Miguel Romero
*/
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        BlockUIModule,
        CurrencyMaskModule,
        ProductosRoutingModule,
        TableModule,
        TooltipModule.forRoot()
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [ProductosListComponent, ProductosFormComponent],
    providers: [ProductosService]
})
export class ProductosModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppEndPoints } from 'src/app/app.endpoints';
import { Observable } from 'rxjs';
import { Producto } from 'src/app/common/model/producto';
import { StringResponse } from 'src/app/common/model/string-response';
import { Page } from 'src/app/common/model/page';

@Injectable()
export class ProductosService {

    /**
    * Constructor del Service de Clientes
    * 
    * @param http 
    * @param endpoints 
    */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Productos
     */
    public getAllProductos(): Observable<Producto[]> {
        return this.http.get<Producto[]>(this.endpoints.productos);
    }

    /**
     * Obtiene el listado de Productos paginados
     */
    public getAllProductosPaginados(values: Map<string, string>): Observable<Page> {
        let params = new HttpParams()
            .set('page', values.get('page'))
            .set('size', values.get('size'))
            .set('sort', values.get('sort'))
            ;
        return this.http.get<Page>(this.endpoints.productos + '/pageables', { params: params });
    }


    /**
     * Retorna el listado de productos activos
     */
    public getProductosActivos(): Observable<Producto[]> {
        return this.http.get<Producto[]>(this.endpoints.productos + '/activos');
    }

    /**
     * Obtiene un producto por su código
     * 
     * @param codigo Código del producto
     */
    public getProductoByCodigo(codigo: string): Observable<Producto> {
        return this.http.get<Producto>(this.endpoints.productos + '/by-codigo/' + codigo);
    }

    /**
     * Registra un Producto
     * @param producto Producto a registrar
     */
    public saveProducto(producto: Producto): Observable<Producto> {
        return this.http.post<Producto>(this.endpoints.productos, producto);
    }

    /**
     * Actualiza un Producto
     * @param producto Producto a actualizar
     */
    public updateProducto(producto: Producto): Observable<Producto> {
        return this.http.put<Producto>(this.endpoints.productos, producto);
    }

    /**
     * Elimina un producto por Id
     * @param id Id del Producto
     */
    public deleteProducto(id: number): Observable<StringResponse> {
        return this.http.delete<StringResponse>(this.endpoints.productos + '/' + id);
    }


    /**
     * Obtiene un Producto por Id
     * @param id Id del Producto
     */
    public getProductoById(id: number): Observable<Producto> {
        return this.http.get<Producto>(this.endpoints.productos + '/' + id);
    }

}

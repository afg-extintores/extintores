import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductosListComponent } from './productos-list/productos-list.component';
import { ProductosFormComponent } from './productos-form/productos-form.component';

const routes: Routes = [
    { path: '', component: ProductosListComponent },
    { path: 'producto', component: ProductosFormComponent },
    { path: 'producto/:id/:edit', component: ProductosFormComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductosRoutingModule { }
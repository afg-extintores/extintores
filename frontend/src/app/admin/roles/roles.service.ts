import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppEndPoints } from '../../app.endpoints';
import { HttpClient } from '@angular/common/http';
import { Rol } from '../../common/model/rol';

/**
 * Service para el manejo de data de Roles
 * 
 * @author Miguel Romero
 */
@Injectable()
export class RolesService {

    /**
     * Constructor del Service de Roles
     * 
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Roles
     */
    public getAllRoles(): Observable<Rol[]> {
        return this.http.get<Rol[]>(this.endpoints.roles);
    }

    /**
     * Registrar un Rol
     * @param Rol Rol a registrar
     */
    public saveRol(Rol: Rol): Observable<Rol> {
        return this.http.post<Rol>(this.endpoints.roles, Rol);
    }

    /**
     * Actualiza un Rol
     * @param Rol Rol a actualizar
     */
    public updateRol(Rol: Rol): Observable<Rol> {
        return this.http.put<Rol>(this.endpoints.roles, Rol);
    }

    /**
     * Elimina un Rol por Id
     * @param id Id del Rol
     */
    public deleteRol(id: number): Observable<Rol> {
        return this.http.delete<Rol>(this.endpoints.roles + '/' + id);
    }

    /**
     * Obtiene un Rol por Id
     * @param id Id del Rol
     */
    public getRolById(id: number): Observable<Rol> {
        return this.http.get<Rol>(this.endpoints.roles + '/' + id);
    }
}

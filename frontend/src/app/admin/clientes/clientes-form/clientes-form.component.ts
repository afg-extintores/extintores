import { Component, OnInit, ViewChild } from '@angular/core';
import { Cliente } from 'src/app/common/model/cliente';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute } from '@angular/router';
import { MessagesService } from 'src/app/common/components/messages/messages.service';
import { ClientesService } from '../clientes.service';
import { ClienteDireccion } from 'src/app/common/model/cliente-direccion';
import { SecurityService } from 'src/app/common/services/security/security.service';
import { Usuario } from 'src/app/common/model/usuario';
import { UsuariosService } from '../../usuarios/usuarios.service';

@Component({
    selector: 'afg-clientes-form',
    templateUrl: './clientes-form.component.html',
    styleUrls: ['./clientes-form.component.css']
})
export class ClientesFormComponent implements OnInit {

    cliente: Cliente;
    clienteForm: FormGroup;
    submitted: boolean;

    isNew: boolean;
    isEdit: boolean;
    idCliente: number;

    errorNombre: boolean;
    errorNIT: boolean;
    errorEmail: boolean;

    usuarios: Usuario[];
    direcciones: ClienteDireccion[];
    @ViewChild('direccion', {static: false}) inputDireccion;
    editDireccion: boolean;
    editableDireccion: string;
    direccionRadio;

    @BlockUI() blockUI: NgBlockUI;

    constructor(private fb: FormBuilder, private router: Router, private msgService: MessagesService,
        private securityService: SecurityService, private actRoute: ActivatedRoute, private clientesService: ClientesService,
        private usuariosService: UsuariosService) {
        this.initForm();
        this.loadUsuarios();
    }

    ngOnInit() {
        this.submitted = false;
        this.cliente = new Cliente();
        this.isNew = true;
        this.errorNombre = false;
        this.errorNIT = false;
        this.errorEmail = false;
        this.editDireccion = false;

        this.actRoute.params.subscribe(params => {
            if (params['id'] != undefined) {
                this.idCliente = params['id'];

                this.clientesService.getClienteById(this.idCliente).subscribe(
                    data => {
                        if (params['edit'] != undefined) {
                            this.isEdit = (params['edit'] == 'true');
                        }

                        this.cliente = data;
                        this.cliente.id = this.idCliente;

                        this.clienteForm.get('nombre').setValue(this.cliente.nombre, { onlySelf: true });
                        this.clienteForm.get('nombreContacto').setValue(this.cliente.nombreContacto, { onlySelf: true });
                        this.clienteForm.get('nit').setValue(this.cliente.nit, { onlySelf: true });
                        this.clienteForm.get('correo').setValue(this.cliente.correo, { onlySelf: true });
                        this.clienteForm.get('telefono').setValue(this.cliente.telefono, { onlySelf: true });
                        this.clienteForm.get('usuario').setValue(this.cliente.usuario, { onlySelf: true });
                        this.clienteForm.get('activo').setValue(this.cliente.activo, { onlySelf: true });

                        this.cliente.clientesDirecciones.forEach(dir => {
                            this.direcciones = [];
                            this.direcciones.push(dir);
                            this.addDireccionRowToForm(dir.direccion);
                        });

                        this.isNew = false;
                    }
                );
            }
        });
    }

    private initForm() {
        this.clienteForm = this.fb.group({
            nombre: new FormControl(null, Validators.required),
            nombreContacto: new FormControl(null, Validators.required),
            nit: new FormControl(null, [Validators.required, Validators.minLength(8)]),
            telefono: new FormControl(null, Validators.required),
            correo: new FormControl(null, [Validators.email]),
            activo: new FormControl(false),
            usuario: new FormControl(null, [Validators.required]),
            direcciones: this.fb.array([])
        });
    }

    /**
     * Carga el listado de Usuarios Vendedores Activos
     */
    private loadUsuarios() {
        this.usuarios = [];
        this.usuariosService.getAllUsuariosVendedoresActivos().subscribe(
            usuarios => {
                this.usuarios = usuarios;
            },
            error => {
                console.log(error);
            }
        )
    }

    /**
     * Compare del select de Usuarios
     * @param c1 
     * @param c2 
     */
    compareFnUsuarios(c1: Usuario, c2: Usuario): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    validateClientName(name: string) {
        if (!this.clienteForm.get('nombre').pristine) {
            if (name != '') {
                this.clientesService.getClienteByNombre(name).subscribe(
                    cliente => {
                        if (cliente != undefined) {
                            this.errorNombre = true;
                        } else {
                            this.errorNombre = false;
                        }
                    },
                    error => {
                        this.errorNombre = true;
                    }
                );
            }
        }
    }

    validateClientNit(nit: string) {
        if (!this.clienteForm.get('nit').pristine) {
            if (nit != '') {
                this.clientesService.getClienteByNit(nit).subscribe(
                    cliente => {
                        if (cliente != undefined) {
                            this.errorNIT = true;
                        } else {
                            this.errorNIT = false;
                        }
                    },
                    error => {
                        this.errorNIT = true;
                    }
                );
            }
        }
    }

    validateInputNIT(event) {
        const keyCode = event.keyCode;
        if ((keyCode == 45) || (keyCode >= 48 && keyCode <= 57)) {
            return true;
        } else {
            return false;
        }
    }

    validateInputPhone(event) {
        const keyCode = event.keyCode;
        if (keyCode >= 48 && keyCode <= 57) {
            return true;
        } else {
            return false;
        }
    }

    validateClientCorreo(correo: string) {
        if (!this.clienteForm.get('correo').pristine) {
            if (correo != '') {
                this.clientesService.getClienteByCorreo(correo).subscribe(
                    cliente => {
                        if (cliente != undefined) {
                            this.errorEmail = true;
                        } else {
                            this.errorEmail = false;
                        }
                    },
                    error => {
                        this.errorEmail = true;
                    }
                );
            }
        }
    }

    addDireccionRowToForm(direccion) {
        let control = <FormArray>this.clienteForm.get('direcciones');
        if (this.editDireccion) {
            for (var i = 0; i < control.length; i++) {
                if (this.editableDireccion === control.value[i].direccion) {
                    control.removeAt(i);
                }
            }
            this.direcciones.forEach(dir => {
                if (dir.direccion === this.editableDireccion) {
                    dir.direccion = direccion;
                }
                dir.modifiedBy = this.securityService.getItem('usuario');
            });
        }
        control.push(this.fb.group({
            direccion: new FormControl(direccion, Validators.required)
        }));
        this.inputDireccion.nativeElement.value = '';
    }

    loadEditDireccion(direccion) {
        this.editDireccion = true;
        this.inputDireccion.nativeElement.value = direccion;
        this.editableDireccion = direccion;
    }

    saveCliente(value: any, valid: boolean) {
        this.submitted = true;
        if (valid && !this.errorNombre && !this.errorNIT && !this.errorEmail) {
            if (this.isNew) {

                this.blockUI.start('Guardando Cliente');

                let client: Cliente = new Cliente();
                client.nombre = value.nombre;
                client.nombreContacto = value.nombreContacto;
                client.nit = value.nit;
                client.correo = value.correo;
                client.telefono = value.telefono;
                client.activo = value.activo;
                client.usuario = value.usuario;
                client.createdBy = this.securityService.getItem('user');

                let direcciones = <FormArray>this.clienteForm.get('direcciones');
                let direccionesXCliente: ClienteDireccion[] = [];

                for (var i = 0; i < direcciones.length; i++) {
                    let dirXCliente: ClienteDireccion = new ClienteDireccion();
                    dirXCliente.cliente = this.cliente;
                    dirXCliente.direccion = direcciones.value[i].direccion;
                    dirXCliente.createdBy = this.securityService.getItem('user');
                    direccionesXCliente.push(dirXCliente);
                }

                client.clientesDirecciones = direccionesXCliente;

                this.clientesService.saveCliente(client).subscribe(
                    cliente => {
                        this.cliente = cliente;
                        this.idCliente = this.cliente.id;
                        this.msgService.addMessage("Cliente registrado exitosamente");
                        this.isNew = false;
                        this.isEdit = true;
                        this.blockUI.stop();
                    },
                    error => {
                        console.log(error);
                        this.msgService.addError("Error al guardar el cliente.");
                        this.blockUI.stop();
                    }
                );

            } else {
                this.blockUI.start('Actualizando Cliente');

                let client: Cliente = new Cliente();

                client.id = this.idCliente;
                client.nombre = value.nombre;
                client.nombreContacto = value.nombreContacto;
                client.nit = value.nit;
                client.correo = value.correo;
                client.telefono = value.telefono;
                client.activo = value.activo;
                client.usuario = value.usuario;
                client.modifiedBy = this.securityService.getItem('user');

                let direccionesForm = <FormArray>this.clienteForm.get('direcciones');

                for (var i = 0; i < direccionesForm.length; i++) {
                    let d = this.direcciones.find(dir => dir.direccion === direccionesForm.value[i].direccion);
                    if (d == undefined) {
                        this.direcciones.push({
                            direccion: direccionesForm.value[i].direccion,
                            createdBy: this.securityService.getItem('user')
                        });
                    }
                }

                client.clientesDirecciones = this.direcciones;

                this.clientesService.updateCliente(client).subscribe(
                    cliente => {
                        this.cliente = cliente;
                        this.msgService.addMessage("Cliente actualizado exitosamente");
                        this.isNew = false;
                        this.isEdit = true;
                        this.blockUI.stop();
                    },
                    error => {
                        console.log(JSON.stringify(error));
                        this.msgService.addError("Error al actualizar el cliente.");
                        this.blockUI.stop();
                    }
                );
            }
        }
    }

    cancelar() {
        this.router.navigate(['/admin/clientes']);
    }

    cleanForm() {
        this.submitted = false;
        this.cliente = new Cliente();
        this.initForm();
        this.isEdit = false;
        this.isNew = true;
        this.submitted = false;
        this.msgService.close();
    }

}

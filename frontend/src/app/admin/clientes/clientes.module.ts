import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/common/modules/shared/shared.module';
import { BlockUIModule } from 'ng-block-ui';
import { TableModule } from 'primeng/table';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ClientesRoutingModule } from './clientes-routing.module';
import { ClientesService } from './clientes.service';
import { ClientesListComponent } from './clientes-list/clientes-list.component';
import { ClientesFormComponent } from './clientes-form/clientes-form.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { UsuariosService } from '../usuarios/usuarios.service';

/**
* Modulo de administración de Clientes
*
* @author Miguel Romero
*/
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        BlockUIModule,
        CurrencyMaskModule,
        ClientesRoutingModule,
        TableModule,
        TooltipModule.forRoot()
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [ClientesListComponent, ClientesFormComponent],
    providers: [ClientesService, UsuariosService]
})
export class ClientesModule { }

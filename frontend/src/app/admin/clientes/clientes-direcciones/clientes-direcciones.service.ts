import { Injectable } from '@angular/core';
import { AppEndPoints } from '../../../app.endpoints';
import { HttpClient } from '@angular/common/http';
import { ClienteDireccion } from 'src/app/common/model/cliente-direccion';
import { Observable } from 'rxjs';

@Injectable()
export class ClientesDireccionesService {

    /**
       * Constructor del Service de Clientes
       * 
       * @param http 
       * @param endpoints 
       */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el lilstado de direcciones por cliente
     * 
     * @param cliente_id Id del cliente
     */
    public getDireccionesByCliente(cliente_id: number): Observable<ClienteDireccion[]> {
        return this.http.get<ClienteDireccion[]>(this.endpoints.clientes_direcciones + '/by-client/' + cliente_id);
    }
}

import { TestBed } from '@angular/core/testing';

import { ClientesDireccionesService } from './clientes-direcciones.service';

describe('ClientesDireccionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientesDireccionesService = TestBed.get(ClientesDireccionesService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/common/model/cliente';
import { Router } from '@angular/router';
import { ClientesService } from '../clientes.service';

@Component({
    selector: 'afg-clientes-list',
    templateUrl: './clientes-list.component.html',
    styleUrls: ['./clientes-list.component.css']
})
export class ClientesListComponent implements OnInit {

    clientes: Cliente[]; // Listado de Clientes
    idCliente: number; // Id de Cliente

    cols: any[]; // Columnas de la tabla

    constructor(private router: Router, private clientesService: ClientesService) {
        this.getAllClientes();
    }

    ngOnInit() {
        this.cols = [
            { field: 'nombre', header: 'Nombre', width: '45%' },
            { field: 'nit', header: 'NIT' },
            { field: 'telefono', header: 'Teléfono' },
            { field: 'usuario.nombres', header: 'Usuario' },
            { field: 'activo', header: 'Estado', width: '8%' }
        ];
    }

    private getAllClientes() {
        this.clientesService.getAllClientes().subscribe(
            clientes => {
                this.clientes = clientes;
            },
            error => {
                console.log(error);
            }
        );
    }

    goForm(item, edit) {
        this.router.navigate(['/admin/clientes/cliente', item.id, edit]);
    }

}

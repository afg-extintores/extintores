import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Cliente } from '../../common/model/cliente';

import { AppEndPoints } from '../../app.endpoints';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Page } from '../../common/model/page';
import { StringResponse } from 'src/app/common/model/string-response';

@Injectable()
export class ClientesService {

    /**
       * Constructor del Service de Clientes
       * 
       * @param http 
       * @param endpoints 
       */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Clientes
     */
    public getAllClientes(): Observable<Cliente[]> {
        return this.http.get<Cliente[]>(this.endpoints.clientes);
    }

    /**
     * Obtiene el listado de Clientes paginados
     */
    public getAllClientesPaginados(values: Map<string, string>): Observable<Page> {
        let params = new HttpParams()
            .set('page', values.get('page'))
            .set('size', values.get('size'))
            .set('sort', values.get('sort'))
            ;
        return this.http.get<Page>(this.endpoints.clientes + '/pageables', { params: params });
    }

    /**
     * Registrav un Cliente
     * @param cliente Cliente a registrar
     */
    public saveCliente(cliente: Cliente): Observable<Cliente> {
        return this.http.post<Cliente>(this.endpoints.clientes, cliente);
    }

    /**
     * Actualiza un Cliente
     * @param cliente Cliente a actualizar
     */
    public updateCliente(cliente: Cliente): Observable<Cliente> {
        return this.http.put<Cliente>(this.endpoints.clientes, cliente);
    }

    /**
     * Elimina un cliente por Id
     * @param id Id del Cliente
     */
    public deleteCliente(id: number): Observable<StringResponse> {
        return this.http.delete<StringResponse>(this.endpoints.clientes + '/' + id);
    }

    /**
     * Obtiene un Cliente por Id
     * @param id Id del Cliente
     */
    public getClienteById(id: number): Observable<Cliente> {
        return this.http.get<Cliente>(this.endpoints.clientes + '/' + id);
    }

    /**
     * Obtiene el listado de Clientes Vendedores Activos
     */
    public getAllClientesActivos(): Observable<Cliente[]> {
        return this.http.get<Cliente[]>(this.endpoints.clientes + '/activos');
    }

    /**
     * Obtiene un cliente por el nombre exacto
     * @param nombre Nombre del cliente
     */
    public getClienteByNombre(nombre: string): Observable<Cliente> {
        return this.http.get<Cliente>(this.endpoints.clientes + '/find-by-name/' + nombre);
    }

    /**
     * Obtiene un cliente por el NIT
     * @param nit NIT del cliente
     */
    public getClienteByNit(nit: string): Observable<Cliente> {
        return this.http.get<Cliente>(this.endpoints.clientes + '/find-by-nit/' + nit);
    }

    /**
     * Obtiene un cliente por el Correo
     * @param correo Correo del cliente
     */
    public getClienteByCorreo(correo: string): Observable<Cliente> {
        return this.http.get<Cliente>(this.endpoints.clientes + '/find-by-correo/' + correo);
    }


}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordService } from './change-password.service';
import { ChangePasswordComponent } from './change-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../common/modules/shared/shared.module';
import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { BlockUIModule } from 'ng-block-ui';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedModule,
        ChangePasswordRoutingModule,
        BlockUIModule,
        TooltipModule.forRoot()
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [ChangePasswordComponent],
    providers: [ChangePasswordService]
})
export class ChangePasswordModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppEndPoints } from '../app.endpoints';
import { Usuario } from '../common/model/usuario';
import { Observable } from 'rxjs';

/**
 * Service para el manejo de Cambio de password
 * 
 * @author Miguel Romero
 */
@Injectable()
export class ChangePasswordService {

    /**
     * Constructor del Service de Cambio de password
     * 
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Actualiza el password del usuario
     * 
     * @param usuario Usuario a actualiar el password
     */
    public updatePasswordUsuario(usuario: Usuario): Observable<boolean> {
        return this.http.post<boolean>(this.endpoints.usuarios + '/change-pass', usuario);
    }
}

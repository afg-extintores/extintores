import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ChangePasswordService } from './change-password.service';
import { Router } from '@angular/router';
import { MessagesService } from '../common/components/messages/messages.service';
import { Usuario } from '../common/model/usuario';
import { SecurityService } from '../common/services/security/security.service';

/**
* Componente para el cambio de password de usuario
*
* @author Miguel Romero
*/
@Component({
    selector: 'afg-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

    changePasswordForm: FormGroup;
    submitted: boolean;

    constructor(private fb: FormBuilder, private router: Router, private msgService: MessagesService,
        private changePasswordService: ChangePasswordService, private securityService: SecurityService) {
        this.initForm();
    }

    ngOnInit() {
        this.subscribeFormChanges();
    }

    private subscribeFormChanges() {
        this.changePasswordForm.get('confirmNewPassword').valueChanges.subscribe(
            confirmValue => {
                const newValue = this.changePasswordForm.get('newPassword').value;
                if (newValue != confirmValue) {
                    this.msgService.addError("Las claves no coinciden, deben ser iguales");
                } else {
                    this.msgService.close();
                }
            }
        )
    }

    private initForm() {
        this.changePasswordForm = this.fb.group({
            newPassword: new FormControl(null, [Validators.required]),
            confirmNewPassword: new FormControl(null, [Validators.required])
        });
    }

    changePass(value: any, valid: boolean) {
        this.submitted = true;
        if (valid) {
            let user = new Usuario();
            user.id = Number(this.securityService.getItem('userId'));
            user.password = value.newPassword;
            this.changePasswordService.updatePasswordUsuario(user).subscribe(
                result => {
                    if (result) {
                        this.msgService.addMessage("Contraseña actualizada correctamente.");
                    } else {
                        this.msgService.addError("Ocurrio un error al actualizar la contraseña.");
                    }
                },
                error => {
                    console.log(error);
                    this.msgService.addError("Ocurrio un error al actualizar la contraseña.");
                }
            )
        }
    }

    cancelar() {
        this.router.navigateByUrl('/home');
    }

}

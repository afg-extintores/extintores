import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppEndPoints } from '../../app.endpoints';
import { Observable } from 'rxjs';
import { ProductoXRemision } from '../../common/model/producto_x_remision';
import { StringResponse } from 'src/app/common/model/string-response';

/**
 * Service para el manejo de data de Productos x Remision
 * 
 * @author Miguel Romero
 */
@Injectable()
export class ProductoXRemisionService {

    /**
     * Constructor del Service de Productos x Remision
     * 
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Productos x Remision
     */
    public getAllProductosXRemision(): Observable<ProductoXRemision[]> {
        return this.http.get<ProductoXRemision[]>(this.endpoints.producto_x_remision);
    }

    /**
     * Registrar un ProductoXRemision
     * @param ProductoXRemision ProductoXRemision a registrar
     */
    public saveProductoXRemision(productoXRemision: ProductoXRemision): Observable<ProductoXRemision> {
        return this.http.post<ProductoXRemision>(this.endpoints.producto_x_remision, productoXRemision);
    }

    /**
     * Asocia un listado de productos a una remisión
     * 
     * @param list Listado de productos que se asocian a la remisión
     */
    public saveListProductoXRemision(list: ProductoXRemision[]): Observable<StringResponse> {
        console.log(list[0]);
        return this.http.post<StringResponse>(this.endpoints.producto_x_remision + '/list', list);
    }

    /**
     * Actualiza un ProductoXRemision
     * @param ProductoXRemision ProductoXRemision a actualizar
     */
    public updateProductoXRemision(productoXRemision: ProductoXRemision): Observable<ProductoXRemision> {
        return this.http.put<ProductoXRemision>(this.endpoints.producto_x_remision, productoXRemision);
    }

    /**
     * Elimina un ProductoXRemision por Id
     * @param id Id del ProductoXRemision
     */
    public deleteProductoXRemision(id: number): Observable<StringResponse> {
        return this.http.delete<StringResponse>(this.endpoints.producto_x_remision + '/' + id);
    }

    /**
     * Obtiene un ProductoXRemision por Id
     * @param id Id del ProductoXRemision
     */
    public getProductoXRemisionById(id: number): Observable<ProductoXRemision> {
        return this.http.get<ProductoXRemision>(this.endpoints.producto_x_remision + '/' + id);
    }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RemisionesListComponent } from './remisiones-list/remisiones-list.component';
import { RemisionesFormComponent } from './remisiones-form/remisiones-form.component';

const routes: Routes = [
    { path: '', component: RemisionesListComponent },
    { path: 'remision', component: RemisionesFormComponent },
    { path: 'remision/:id/:edit', component: RemisionesFormComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RemisionesRoutingModule { }
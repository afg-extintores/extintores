import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppEndPoints } from '../../app.endpoints';
import { Observable } from 'rxjs';
import { Page } from '../../common/model/page';
import { Remision } from '../../common/model/remision';
import { StringResponse } from 'src/app/common/model/string-response';

/**
 * Service para el manejo de data de Remisiones
 * 
 * @author Miguel Romero
 */
@Injectable()
export class RemisionesService {

    /**
     * Constructor del Service de Remisiones
     * 
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Remisiones paginadas
     */
    public getAllRemisiones(): Observable<Remision[]> {
        return this.http.get<Remision[]>(this.endpoints.remisiones);
    }

    /**
     * Obtiene el listado de Remisiones no facturadas
     */
    public getAllRemisionesNoFactura(): Observable<Remision[]> {
        return this.http.get<Remision[]>(this.endpoints.remisiones + '/no-factura');
    }

    /**
     * Obtiene el listado de Remisiones paginadas
     */
    public getAllRemisionesPaginadas(values: Map<string, string>): Observable<Page> {
        let params = new HttpParams()
            .set('page', values.get('page'))
            .set('size', values.get('size'))
            .set('sort', values.get('sort'))
            ;
        return this.http.get<Page>(this.endpoints.remisiones + '/pageables', { params: params });
    }

    /**
     * Registra un Remision
     * @param remision Remision a registrar
     */
    public saveRemision(remision: Remision): Observable<Remision> {
        return this.http.post<Remision>(this.endpoints.remisiones, remision);
    }

    /**
     * Actualiza una Remision
     * @param remision Remision a actualizar
     */
    public updateRemision(remision: Remision): Observable<Remision> {
        return this.http.put<Remision>(this.endpoints.remisiones, remision);
    }

    /**
     * Elimina una remision por Id
     * @param id Id de la Remision
     */
    public deleteRemision(id: number): Observable<StringResponse> {
        return this.http.delete<StringResponse>(this.endpoints.remisiones + '/' + id);
    }

    /**
     * Obtiene un Remision por Id
     * @param id Id del Remision
     */
    public getRemisionById(id: number): Observable<Remision> {
        return this.http.get<Remision>(this.endpoints.remisiones + '/' + id);
    }

    public getMaxConsecutive(): Observable<number> {
        return this.http.get<number>(this.endpoints.remisiones + '/max-consecutive');
    }

    /**
     * Carga a S3 la imagen de la remisión
     * @param image 
     */
    public uploadImage(image: File, remisionId: number): Observable<StringResponse> {
        const formData: FormData = new FormData();
        formData.append('image', image);
        formData.append('remisionId', String(remisionId));
        return this.http.post<StringResponse>(this.endpoints.remisiones + '/upload', formData);
    }
}

import { Component, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Remision } from '../../../common/model/remision';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray, NgControlStatus } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RemisionesService } from '../remisiones.service';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Cliente } from '../../../common/model/cliente';
import { ClientesService } from '../../../admin/clientes/clientes.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/public_api';
import { ClienteDireccion } from '../../../common/model/cliente-direccion';
import { ClientesDireccionesService } from '../../../admin/clientes/clientes-direcciones/clientes-direcciones.service';
import { UsuariosService } from '../../../admin/usuarios/usuarios.service';
import { Usuario } from '../../../../app/common/model/usuario';
import { SecurityService } from '../../../common/services/security/security.service';
import { MessagesService } from 'src/app/common/components/messages/messages.service';
import { ParametrosService } from 'src/app/common/services/util/parametros.service';
import { Producto } from 'src/app/common/model/producto';
import { ProductosService } from 'src/app/admin/productos/productos.service';
import { Parametro } from 'src/app/common/model/parametro';
import { ProductoTable } from 'src/app/common/model/producto-table';
import { ProductoXRemision } from 'src/app/common/model/producto_x_remision';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
    selector: 'afg-remisiones-form',
    templateUrl: './remisiones-form.component.html',
    styleUrls: ['./remisiones-form.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class RemisionesFormComponent implements OnInit {

    /** Object binding to info form */
    remision: Remision;
    /** Remission information form */
    remisionForm: FormGroup;
    /** Flag to validate if the form has been submitted  */
    submitted: boolean;
    /** Id of the remission */
    idRemision: number;

    isNew: boolean;
    isEdit: boolean;

    cliente: Cliente;
    clientesDS: Cliente[];

    direcciones: ClienteDireccion[];

    usuarios: Usuario[];

    iva: number;

    @BlockUI() blockUI: NgBlockUI;
    modalRef: BsModalRef;

    productosDisponibles: Producto[];
    productosSeleccionados: Producto[];

    prodsXRemision: ProductoXRemision[];

    imageRemision: File;

    constructor(private fb: FormBuilder, private router: Router, private localService: BsLocaleService,
        private securityService: SecurityService, private clientesService: ClientesService, private paramsService: ParametrosService,
        private clientesDireccionesService: ClientesDireccionesService, private productosService: ProductosService, private usuariosService: UsuariosService,
        private remisionesService: RemisionesService, private msgService: MessagesService, private actRoute: ActivatedRoute,
        private modalService: BsModalService) {

        this.initForm();
        this.setLocale();
        this.loadIva();
        this.loadConsecutive();
        this.loadClientes();
        this.loadUsuarios();
        this.loadProductosDisponibles();

    }

    /**
     * Init del Componente
     */
    ngOnInit() {
        this.submitted = false;
        this.remision = new Remision();
        this.cliente = new Cliente();
        this.isNew = true;
        this.isEdit = false;

        this.productosDisponibles = [];
        this.productosSeleccionados = [];

        this.actRoute.params.subscribe(params => {
            if (params['id'] != undefined) {
                this.idRemision = params['id'];

                this.remisionesService.getRemisionById(this.idRemision).subscribe(
                    async remision => {
                        if (params['edit'] != undefined) {
                            this.isEdit = (params['edit'] == 'true');
                        }

                        this.remision = remision;

                        this.cliente = this.remision.cliente;
                        await this.getDireccionesByCliente(this.cliente.id);

                        this.remisionForm.get('consecutivo').setValue(this.remision.consecutivo, { onlySelf: true });
                        this.remisionForm.get('fechaRemision').setValue(this.remision.fechaRemision, { onlySelf: true });
                        this.remisionForm.get('cliente').setValue(this.cliente.nombre, { onlySelf: true });
                        this.remisionForm.get('clientesDirecciones').setValue(this.remision.clientesDirecciones, { onlySelf: true });
                        this.remisionForm.get('usuario').setValue(this.remision.usuario, { onlySelf: true });
                        this.remisionForm.get('observaciones').setValue(this.remision.observaciones, { onlySelf: true });

                        let control = <FormArray>this.remisionForm.get('productosXRemision');

                        this.remision.productosXRemisions.forEach(pr => {
                            let proSel = this.productosDisponibles.find(p => p.codigo == pr.producto.codigo);
                            this.productosSeleccionados.push(proSel);
                            this.productosDisponibles.splice(this.productosDisponibles.indexOf(proSel), 1);
                            control.push(this.fb.group({
                                codigo: new FormControl(pr.producto.codigo),
                                nombre: new FormControl(pr.producto.nombre),
                                precioUnidad: new FormControl(pr.precioUnidadVenta, [Validators.required, Validators.min(1)]),
                                cantidad: new FormControl(pr.cantidad, [Validators.required, Validators.min(1)]),
                                valorVenta: new FormControl(pr.valorVenta)
                            }));
                            this.prodsXRemision = [];
                            this.prodsXRemision.push(pr);
                            this.calculateValoresRemision();
                        });

                        if (!this.isEdit) {
                            this.remisionForm.disable();
                        } else {
                            this.remisionForm.enable();
                            this.remisionForm.get('consecutivo').disable();
                        }

                        this.isNew = false;

                        this.subscribeFormChanges();
                    }
                );
            }
        });

        this.subscribeFormChanges();
    }

    /**
     * Inicializa el formulario
     */
    private initForm() {
        this.remisionForm = this.fb.group({
            consecutivo: new FormControl(null, [Validators.required]),
            fechaRemision: new FormControl(new Date(), [Validators.required]),
            cliente: new FormControl(null, [Validators.required]),
            clientesDirecciones: new FormControl(null, [Validators.required]),
            usuario: new FormControl(null, [Validators.required]),
            observaciones: new FormControl(null),
            productosXRemision: this.fb.array([])
        });
    }

    /**
     * Obsevable para cambios en el formulario
     */
    subscribeFormChanges() {
        let control = <FormArray>this.remisionForm.get('productosXRemision');
        if (control.controls.length > 0) {
            for (var i = 0; i < control.controls.length; i++) {
                control.controls[i].get('precioUnidad').valueChanges.subscribe(
                    val => {
                        this.calculateValoresRemision();
                    }
                );
                control.controls[i].get('cantidad').valueChanges.subscribe(
                    val => {
                        this.calculateValoresRemision();
                    }
                );
            }
        }
    }

    /**
     * Configura el Locale para el calendar
     */
    private setLocale() {
        this.localService.use('es');
    }

    /**
     * Carga el valor del parámetro del IVA
     */
    private loadIva() {
        this.paramsService.getValueParametroByName('IVA').subscribe(
            param => {
                this.iva = Number(param.value);
            }
        );
    }

    /**
     * Carga el valor del siguiente Consecutivo
     */
    private loadConsecutive() {
        this.paramsService.getValueParametroByName('REM_CONS').subscribe(
            value => {
                this.remisionForm.get('consecutivo').setValue(value.value, { onlySelf: true });
            }
        );
    }

    /**
     * Actualiza el valor del consecutivo de Remisiones
     * @param value Valor nuevo
     */
    async updateConsecutive(value: string) {
        let param = new Parametro();
        param.nombre = 'REM_CONS';
        param.value = value;
        await this.paramsService.updateParametro(param).toPromise();
    }

    /**
     * Carga el listado de Clientes Activos
     */
    private loadClientes() {
        this.clientesDS = [];
        this.clientesService.getAllClientesActivos().subscribe(
            clientes => {
                this.clientesDS = clientes;
            }
        )
    }

    /**
     * Carga el listado de Usuarios Vendedores Activos
     */
    private loadUsuarios() {
        this.usuarios = [];
        this.usuariosService.getAllUsuariosVendedoresActivos().subscribe(
            usuarios => {
                this.usuarios = usuarios;
            },
            error => {
                console.log(error);
            }
        )
    }

    /**
     * Carga el listado de Productos Activos
     */
    private loadProductosDisponibles() {
        this.productosService.getProductosActivos().subscribe(
            productos => {
                this.productosDisponibles = productos;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Retorna el listado de direcciones por Cliente
     * @param idCliente Id del Cliente
     */
    private getDireccionesByCliente(idCliente: number) {
        this.direcciones = [];
        this.clientesDireccionesService.getDireccionesByCliente(idCliente).subscribe(
            direcciones => {
                this.direcciones = direcciones;
            },
            error => {
                console.log(error);
            }
        )
    }

    /**
     * Compare del select de Direcciones
     * @param c1 
     * @param c2 
     */
    compareFnDirecciones(c1: ClienteDireccion, c2: ClienteDireccion): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Compare del select de Usuarios
     * @param c1 
     * @param c2 
     */
    compareFnUsuarios(c1: Usuario, c2: Usuario): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Selecciona un cliente
     * @param event 
     */
    onSelectCliente(event: TypeaheadMatch): void {
        this.cliente = event.item;
        this.getDireccionesByCliente(this.cliente.id);
    }

    calculateValoresRemision() {
        this.remision.subtotal = 0;
        this.remision.iva = 0;
        this.remision.total = 0;
        let control = <FormArray>this.remisionForm.get('productosXRemision');
        if (control.controls.length > 0) {
            for (var i = 0; i < control.controls.length; i++) {
                let newTotal = control.controls[i].get('precioUnidad').value * control.controls[i].get('cantidad').value;
                control.controls[i].get('valorVenta').setValue(newTotal);
                this.remision.subtotal += control.controls[i].get('valorVenta').value;
            }
            this.remision.iva = (this.remision.subtotal * this.iva) / 100;
            this.remision.total = this.remision.subtotal + this.remision.iva;
        }
    }

    /**
     * Agrega un row al Form de la tabla de productos de la Remisión
     * @param row 
     */
    private addRowToFormTotales(row: ProductoTable) {
        let control = <FormArray>this.remisionForm.get('productosXRemision');
        control.push(this.fb.group({
            codigo: new FormControl(row.codigo),
            nombre: new FormControl(row.producto),
            precioUnidad: new FormControl(row.precioUnidad, [Validators.required, Validators.min(1)]),
            cantidad: new FormControl(row.cantidad, [Validators.required, Validators.min(1)]),
            valorVenta: new FormControl(row.valorVenta)
        }));
        this.subscribeFormChanges();
    }

    /**
     * Agrega un Producto a la Remisión
     * @param event 
     */
    addProduct(event) {
        if (event.items.length > 0) {
            for (var i = 0; i < event.items.length; i++) {
                let prodFacTable: ProductoTable = new ProductoTable();
                prodFacTable.codigo = event.items[i].codigo;
                prodFacTable.producto = event.items[i].nombre;
                prodFacTable.precioUnidad = event.items[i].precioUnidad;
                prodFacTable.valorVenta = prodFacTable.cantidad * prodFacTable.precioUnidad;
                this.addRowToFormTotales(prodFacTable);
                this.calculateValoresRemision();
            }
        }
    }

    /**
     * Elimina la relación de un producto a la Remisión
     * @param event 
     */
    removeProduct(event) {
        let control = <FormArray>this.remisionForm.get('productosXRemision');
        for (var i = 0; i < event.items.length; i++) {
            for (var j = 0; j < control.controls.length; j++) {
                if (event.items[i].codigo == control.controls[j].value.codigo) {
                    control.removeAt(j);
                    continue;
                }
            }
        }
        this.calculateValoresRemision();
    }

    /**
     * Guarda o Actualiza una remisión y sus productos
     * @param value Value del formulario
     * @param valid Status del formulario
     */
    saveRemision(value: any, valid: boolean) {
        this.submitted = true;
        if (valid) {
            if (this.isNew) {
                this.blockUI.start('Guardando Remisión');

                let remision: Remision = new Remision();
                remision.consecutivo = value.consecutivo;
                remision.fechaRemision = value.fechaRemision;
                remision.cliente = this.cliente;
                remision.clientesDirecciones = value.clientesDirecciones;
                remision.usuario = value.usuario;
                remision.observaciones = value.observaciones;
                remision.total = this.remision.total;
                remision.subtotal = this.remision.subtotal;
                remision.iva = this.remision.iva;
                remision.createdBy = this.securityService.getItem('user');

                let productos = <FormArray>this.remisionForm.get('productosXRemision');
                let productosXRemision: ProductoXRemision[] = [];

                for (var i = 0; i < productos.length; i++) {
                    let proXRem: ProductoXRemision = new ProductoXRemision();
                    proXRem.cantidad = productos.value[i].cantidad;
                    proXRem.precioUnidadVenta = productos.value[i].precioUnidad;
                    proXRem.producto = this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo);
                    proXRem.valorVenta = productos.value[i].valorVenta;
                    proXRem.createdBy = this.securityService.getItem('usuario');
                    productosXRemision.push(proXRem);
                }

                remision.productosXRemisions = productosXRemision;

                this.remisionesService.saveRemision(remision).subscribe(
                    remision => {
                        this.updateConsecutive(String(remision.consecutivo + 1));
                        this.remision = remision;
                        this.msgService.addMessage('La remisión fue guardada con éxito.');
                        this.isNew = false;
                        this.isEdit = true;
                        this.blockUI.stop();
                    },
                    error => {
                        this.msgService.addError("Error al guardar la remisión");
                        this.blockUI.stop();
                    }
                );
            } else {
                this.blockUI.start('Actualizando Remisión');

                this.remision.fechaRemision = value.fechaRemision;
                this.remision.cliente = this.cliente;
                this.remision.clientesDirecciones = value.clientesDirecciones;
                this.remision.usuario = value.usuario;
                this.remision.observaciones = value.observaciones;
                this.remision.total = this.remision.total;
                this.remision.subtotal = this.remision.subtotal;
                this.remision.iva = this.remision.iva;
                this.remision.modifiedBy = this.securityService.getItem('usuario');

                let productos = <FormArray>this.remisionForm.get('productosXRemision');

                for (var i = 0; i < productos.length; i++) {
                    let p = this.prodsXRemision.find(p => p.producto.codigo == productos.value[i].codigo);
                    if (p == undefined) {
                        this.prodsXRemision.push({
                            cantidad: productos.value[i].cantidad,
                            precioUnidadVenta: productos.value[i].precioUnidad,
                            producto: this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo),
                            valorVenta: productos.value[i].valorVenta,
                            createdBy: this.securityService.getItem('usuario')
                        });
                    }else{
                        this.prodsXRemision = this.prodsXRemision.map(pr => {

                            if (p.id == pr.id) {
                                pr.cantidad = productos.value[i].cantidad
                                pr.precioUnidadVenta = productos.value[i].precioUnidad
                                pr.producto = this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo)
                                pr.valorVenta = productos.value[i].valorVenta
                                pr.modifiedBy = this.securityService.getItem('user')
                            }

                            return pr
                        })
                    }
                }

                this.remision.productosXRemisions = this.prodsXRemision;

                this.remisionesService.updateRemision(this.remision).subscribe(
                    remision => {
                        this.remision = remision;
                        this.msgService.addMessage('La remisión fue actualizada con éxito.');
                        this.isNew = false;
                        this.isEdit = true;
                        this.blockUI.stop();
                    },
                    error => {
                        console.log(error);
                        this.msgService.addError("Error al actualizar la remisión");
                        this.blockUI.stop();
                    }
                );
            }
        }
    }

    /**
     * Retorna al listado de Remisiones
     */
    cancelar() {
        this.router.navigate(['/facturacion/remisiones']);
    }

    cleanForm() {
        this.submitted = false;
        this.remision = new Remision();
        this.initForm();
        this.setLocale();
        this.loadIva();
        this.loadConsecutive();
        this.loadClientes();
        this.loadUsuarios();
        this.loadProductosDisponibles();
        this.productosSeleccionados = [];
        this.msgService.close();
    }

    showModalUploadImage(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    onFileChanged(imageInput: any) {
        this.imageRemision = new File([imageInput.files[0]], this.remision.cliente.nombre + '.' + imageInput.files[0].name.split('.')[1], { type: imageInput.files[0].type });
    }

    uploadFile() {
        this.remisionesService.uploadImage(this.imageRemision, this.remision.id).subscribe(
            res => {
                this.imageRemision = null;
                this.modalRef.hide();
                this.msgService.addMessage("Imagen remisión cargada exitosamente");
            },
            error => {
                console.log(error);
                this.modalRef.hide();
                this.msgService.addError("Error al cargar la imagen de la remisión");
            }
        );
    }

}
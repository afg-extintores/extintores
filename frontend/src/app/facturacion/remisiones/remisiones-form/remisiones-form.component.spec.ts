import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemisionesFormComponent } from './remisiones-form.component';

describe('RemisionesFormComponent', () => {
  let component: RemisionesFormComponent;
  let fixture: ComponentFixture<RemisionesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemisionesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemisionesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

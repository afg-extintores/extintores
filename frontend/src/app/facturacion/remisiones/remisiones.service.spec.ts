import { TestBed } from '@angular/core/testing';

import { RemisionesService } from './remisiones.service';

describe('RemisionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemisionesService = TestBed.get(RemisionesService);
    expect(service).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemisionesListComponent } from './remisiones-list.component';

describe('RemisionesListComponent', () => {
  let component: RemisionesListComponent;
  let fixture: ComponentFixture<RemisionesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemisionesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemisionesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

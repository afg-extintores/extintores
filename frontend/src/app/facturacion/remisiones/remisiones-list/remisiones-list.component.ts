import { Component, OnInit, TemplateRef } from '@angular/core';
import { Page } from '../../../common/model/page';
import { Remision } from '../../../common/model/remision';
import { Router } from '@angular/router';
import { RemisionesService } from '../remisiones.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

/**
* Componente del listado de remisiones
*
* @author Miguel Romero
*/
@Component({
    selector: 'afg-remisiones-list',
    templateUrl: './remisiones-list.component.html',
    styleUrls: ['./remisiones-list.component.css']
})
export class RemisionesListComponent implements OnInit {

    remisiones: Remision[]; // Listado de Remisiones
    idRemision: number; // Id de Remision
    cols: any[]; // Columnas de la tabla

    modalRef: BsModalRef;

    urlImageRemision: string;

    constructor(private router: Router, private remisionesService: RemisionesService, private modalService: BsModalService) { }

    ngOnInit() {
        this.initCols();
        this.getAllRemisiones();
        this.urlImageRemision = '';
    }

    private initCols() {
        this.cols = [
            { field: 'consecutivo', header: 'Consecutivo', width: '7%' },
            { field: 'fechaRemision', header: 'Fecha', width: '15%' },
            { field: 'cliente.nombre', header: 'Cliente', width: '15%' },
            { field: 'usuario.nombres', header: 'Usuario', width: '15%' },
            { field: 'total', header: 'Total', width: '10%' }
        ];
    }

    private getAllRemisiones() {
        this.remisionesService.getAllRemisiones().subscribe(
            remisiones => {
                this.remisiones = remisiones;
            },
            error => {
                console.log(error);
            }
        );
    }

    goForm(item, edit) {
        this.router.navigate(['/facturacion/remisiones/remision', item.id, edit]);
    }

    showImage(template: TemplateRef<any>, url: string){
        this.modalRef = this.modalService.show(template);
        this.urlImageRemision = url;
    }

}

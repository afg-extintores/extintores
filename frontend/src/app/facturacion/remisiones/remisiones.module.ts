import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/common/modules/shared/shared.module';
import { BlockUIModule } from 'ng-block-ui';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { RemisionesRoutingModule } from './remisiones-routing.module';
import { RemisionesListComponent } from './remisiones-list/remisiones-list.component';
import { RemisionesFormComponent } from './remisiones-form/remisiones-form.component';
import { RemisionesService } from './remisiones.service';

import { TableModule } from 'primeng/table';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { ClientesService } from 'src/app/admin/clientes/clientes.service';
import { ClientesDireccionesService } from 'src/app/admin/clientes/clientes-direcciones/clientes-direcciones.service';
import { UsuariosService } from 'src/app/admin/usuarios/usuarios.service';
import { ProductosService } from 'src/app/admin/productos/productos.service';
import { UtilService } from 'src/app/common/services/util/util.service';
import { ProductoXRemisionService } from './producto-x-remision.service';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { PickListModule } from 'primeng/picklist';
import { NgxDocViewerModule } from 'ngx-doc-viewer';

/**
 * Módulo de administración de Remisiones
 */
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        RemisionesRoutingModule,
        BlockUIModule,
        BsDatepickerModule,
        TableModule,
        TypeaheadModule,
        CurrencyMaskModule,
        ModalModule,
        TooltipModule.forRoot(),
        PickListModule,
        NgxDocViewerModule
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [RemisionesListComponent, RemisionesFormComponent],
    providers: [
        UtilService,
        RemisionesService,
        ClientesService,
        ClientesDireccionesService,
        UsuariosService,
        ProductosService,
        ProductoXRemisionService,
        BsModalService
    ]
})
export class RemisionesModule { }

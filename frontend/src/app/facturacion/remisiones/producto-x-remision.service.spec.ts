import { TestBed } from '@angular/core/testing';

import { ProductoXRemisionService } from './producto-x-remision.service';

describe('ProductoXRemisionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductoXRemisionService = TestBed.get(ProductoXRemisionService);
    expect(service).toBeTruthy();
  });
});

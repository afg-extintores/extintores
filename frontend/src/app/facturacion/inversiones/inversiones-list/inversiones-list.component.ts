import { Component, OnInit } from '@angular/core';
import { Inversion } from 'src/app/common/model/inversion';
import { Router } from '@angular/router';
import { InversionesService } from '../inversiones.service';

@Component({
    selector: 'afg-inversiones-list',
    templateUrl: './inversiones-list.component.html',
    styleUrls: ['./inversiones-list.component.css']
})
export class InversionesListComponent implements OnInit {

    inversiones: Inversion[]; // Listado de Inversiones
    idInversion: number; // Id de Inversion
    cols: any[]; // Columnas de la tabla

    constructor(private router: Router, private inversionesService: InversionesService) { }

    ngOnInit() {
        this.initCols();
        this.getAllInversiones();
    }

    private initCols() {
        this.cols = [
            { field: 'consecutivo', header: 'Consecutivo Factura', width: '7%' },
            { field: 'fecha', header: 'Fecha', width: '15%' },
            { field: 'cliente.nombre', header: 'Cliente', width: '15%' },
            { field: 'usuario.nombres', header: 'Usuario', width: '15%' },
            { field: 'total', header: 'Total', width: '10%' }
        ];
    }

    private getAllInversiones() {
        this.inversionesService.getAllInversiones().subscribe(
            inversiones => {
                this.inversiones = inversiones;
            },
            error => {
                console.log(error);
            }
        );
    }

    goForm(item, edit) {
        this.router.navigate(['/facturacion/inversiones/inversion', item.id, edit]);
    }

}

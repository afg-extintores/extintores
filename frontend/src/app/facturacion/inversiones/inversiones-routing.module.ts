import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InversionesListComponent } from './inversiones-list/inversiones-list.component';
import { InversionesFormComponent } from './inversiones-form/inversiones-form.component';

const routes: Routes = [
    { path: '', component: InversionesListComponent },
    { path: 'inversion', component: InversionesFormComponent },
    { path: 'inversion/:id/:edit', component: InversionesFormComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InversionesRoutingModule { }
import { Component, OnInit } from '@angular/core';
import { Inversion } from 'src/app/common/model/inversion';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { Factura } from 'src/app/common/model/factura';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/common/model/usuario';
import { UsuariosService } from 'src/app/admin/usuarios/usuarios.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/public_api';
import { Producto } from 'src/app/common/model/producto';
import { ProductoTable } from 'src/app/common/model/producto-table';
import { ProductosService } from 'src/app/admin/productos/productos.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { FacturasService } from '../../facturas/facturas.service';
import { SecurityService } from 'src/app/common/services/security/security.service';
import { ProductoXInversion } from 'src/app/common/model/producto-x-inversion';
import { InversionesService } from '../inversiones.service';
import { MessagesService } from 'src/app/common/components/messages/messages.service';

@Component({
    selector: 'afg-inversiones-form',
    templateUrl: './inversiones-form.component.html',
    styleUrls: ['./inversiones-form.component.css']
})
export class InversionesFormComponent implements OnInit {

    inversion: Inversion
    inversionForm: FormGroup
    submitted: boolean
    idInversion: number
    isNew: boolean
    isEdit: boolean

    factura: Factura
    facturas: Factura[]
    facturasDS: Factura[]

    usuario: Usuario
    usuarios: Usuario[]

    productosDisponibles: Producto[]
    productosSeleccionados: Producto[]

    prodsXInversion: ProductoXInversion[]

    @BlockUI() blockUI: NgBlockUI

    constructor(private fb: FormBuilder, private router: Router, private usuariosService: UsuariosService,
        private productosService: ProductosService, private facturasService: FacturasService, private securityService: SecurityService,
        private inversionesService: InversionesService, private msgService: MessagesService, private actRoute: ActivatedRoute) {
        this.initForm()
        this.loadUsuarios()
        this.loadProductosDisponibles()
        this.loadInvoices()
    }

    ngOnInit() {
        this.isEdit = false
        this.isNew = true

        this.inversion = new Inversion()

        this.productosDisponibles = []
        this.productosSeleccionados = []

        this.prodsXInversion = []

        this.actRoute.params.subscribe(params => {
            if (params['id'] != undefined) {
                this.idInversion = params['id']

                this.inversionesService.getInversionById(this.idInversion).subscribe(
                    inversion => {

                        if (params['edit'] != undefined) {
                            this.isEdit = (params['edit'] == 'true')
                        }

                        this.inversion = inversion
                        this.factura = this.inversion.factura

                        this.inversionForm.get('factura').setValue(this.inversion.factura.consecutivo, { onlySelf: true })
                        this.inversionForm.get('usuario').setValue(this.inversion.usuario, { onlySelf: true })
                        this.inversionForm.get('fecha').setValue(this.inversion.fecha, { onlySelf: true })

                        let control = <FormArray>this.inversionForm.get('productosXInversion')

                        this.inversion.productosXInversions.forEach(inv => {
                            let proSel = this.productosDisponibles.find(p => p.codigo == inv.producto.codigo)
                            this.productosSeleccionados.push(proSel)
                            this.productosDisponibles.splice(this.productosDisponibles.indexOf(proSel), 1)
                            control.push(this.fb.group({
                                codigo: new FormControl(inv.producto.codigo),
                                nombre: new FormControl(inv.producto.nombre),
                                precioUnidad: new FormControl(inv.precioUnidadVenta, [Validators.required, Validators.min(1)]),
                                cantidad: new FormControl(inv.cantidad, [Validators.required, Validators.min(1)]),
                                valorVenta: new FormControl(inv.valorVenta)
                            }))
                            this.prodsXInversion = []
                            this.prodsXInversion.push(inv)
                            this.calculateValoresInversion()
                        })

                        if (!this.isEdit) {
                            this.inversionForm.disable()
                        } else {
                            this.inversionForm.enable()
                        }

                        this.isNew = false

                        this.subscribeFormChanges()

                    }
                )

            }
        })
    }

    private initForm() {
        this.inversionForm = this.fb.group({
            factura: new FormControl(null, [Validators.required]),
            usuario: new FormControl(null, [Validators.required]),
            fecha: new FormControl(null, [Validators.required]),
            productosXInversion: this.fb.array([])
        })
    }

    /**
     * Obsevable para cambios en el formulario
     */
    subscribeFormChanges() {
        let control = <FormArray>this.inversionForm.get('productosXInversion');
        if (control.controls.length > 0) {
            for (var i = 0; i < control.controls.length; i++) {
                control.controls[i].get('precioUnidad').valueChanges.subscribe(
                    val => {
                        this.calculateValoresInversion();
                    }
                );
                control.controls[i].get('cantidad').valueChanges.subscribe(
                    val => {
                        this.calculateValoresInversion();
                    }
                );
            }
        }
    }

    /**
     * Load the list of invoices without investment
     */
    private loadInvoices() {
        this.facturasDS = []
        this.facturasService.getInvoicesWithoutInvestment().subscribe(
            invoices => {
                this.facturasDS = invoices
            },
            error => {
                console.log(error)
            }
        )
    }

    /**
     * Carga el listado de Usuarios Vendedores Activos
     */
    private loadUsuarios() {
        this.usuarios = [];
        this.usuariosService.getAllUsuariosVendedoresActivos().subscribe(
            usuarios => {
                this.usuarios = usuarios;
            },
            error => {
                console.log(error);
            }
        )
    }

    /**
     * Carga el listado de Productos Activos
     */
    private loadProductosDisponibles() {
        this.productosService.getProductosActivos().subscribe(
            productos => {
                this.productosDisponibles = productos;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Compare del select de Usuarios
     * @param c1 
     * @param c2 
     */
    compareFnUsuarios(c1: Usuario, c2: Usuario): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Agrega un row al Form de la tabla de productos de la Factura
     * @param row 
     */
    private addRowToFormTotales(row: ProductoTable) {
        let control = <FormArray>this.inversionForm.get('productosXInversion');
        control.push(this.fb.group({
            codigo: new FormControl(row.codigo),
            nombre: new FormControl(row.producto),
            precioUnidad: new FormControl(row.precioUnidad, [Validators.required, Validators.min(1)]),
            cantidad: new FormControl(row.cantidad, [Validators.required, Validators.min(1)]),
            valorVenta: new FormControl(row.valorVenta)
        }));
        this.subscribeFormChanges();
    }

    /**
     * Calcula los valores totales de la Inversión
     */
    calculateValoresInversion() {
        this.inversion.total = 0;
        let control = <FormArray>this.inversionForm.get('productosXInversion');
        if (control.controls.length > 0) {
            for (var i = 0; i < control.controls.length; i++) {
                let newTotal = control.controls[i].get('precioUnidad').value * control.controls[i].get('cantidad').value;
                control.controls[i].get('valorVenta').setValue(newTotal);
                this.inversion.total += control.controls[i].get('valorVenta').value;
            }
        }
    }

    /**
     * Busca si ya existe un producto en la tabla de totales
     * @param codigo 
     */
    private searchProductOnFormTotales(codigo: string): boolean {
        let control = <FormArray>this.inversionForm.get('productosXInversion');
        for (var i = 0; i < control.controls.length; i++) {
            if (control.value[i].codigo == codigo) {
                return true;
            }
        }
        return false;
    }

    /**
     * Agrega un Producto a la Inversión
     * @param event 
     */
    addProduct(event) {
        let prodInvTable: ProductoTable = new ProductoTable();
        prodInvTable.codigo = event.items[0].codigo;
        prodInvTable.producto = event.items[0].nombre;
        prodInvTable.precioUnidad = event.items[0].precioUnidad;
        prodInvTable.valorVenta = prodInvTable.cantidad * prodInvTable.precioUnidad;
        this.addRowToFormTotales(prodInvTable);
        this.calculateValoresInversion();
    }

    /**
     * Asocia todos los productos a la Inversión
     * @param event 
     */
    addAllProducts(event) {
        if (event.items.length > 0) {
            for (var i = 0; i < event.items.length; i++) {
                let prodInvTable: ProductoTable = new ProductoTable();
                prodInvTable.codigo = event.items[i].codigo;
                prodInvTable.producto = event.items[i].nombre;
                prodInvTable.precioUnidad = event.items[i].precioUnidad;
                prodInvTable.valorVenta = prodInvTable.cantidad * prodInvTable.precioUnidad;
                if (!this.searchProductOnFormTotales(prodInvTable.codigo)) {
                    this.addRowToFormTotales(prodInvTable);
                }
            }
            this.calculateValoresInversion();
        }
    }

    /**
     * Elimina la relación de un producto a la Inversión
     * @param event 
     */
    removeProduct(event) {
        let control = <FormArray>this.inversionForm.get('productosXInversion');
        for (var i = 0; i < event.items.length; i++) {
            let index = event.items.indexOf(event.items[i]);
            if (index > -1) {
                control.removeAt(i);
            }
        }
        this.calculateValoresInversion();
    }

    /**
     * Elimina la relación de todos los productos asociados a la Inversión
     */
    removeAllProducts() {
        let control = <FormArray>this.inversionForm.get('productosXInversion');
        const length = control.length;
        for (var i = 0; i < length; i++) {
            control.removeAt(0);
        }
        this.calculateValoresInversion();
    }

    /**
     * Event triggered when the user select an invoice
     * @param event 
     */
    onSelectInvoice(event: TypeaheadMatch): void {
        this.factura = event.item
    }

    saveInversion(value: any, valid: boolean) {
        this.submitted = true
        if (valid) {
            if (this.isNew) {
                this.blockUI.start('Guardando Inversión')

                let inversion: Inversion = new Inversion()

                inversion.factura = this.factura
                inversion.usuario = value.usuario
                inversion.fecha = value.fecha
                inversion.total = this.inversion.total
                inversion.createdBy = this.securityService.getItem('user')

                let productos = <FormArray>this.inversionForm.get('productosXInversion')
                let productosXInversion: ProductoXInversion[] = []

                for (var i = 0; i < productos.length; i++) {
                    let proXInv: ProductoXInversion = new ProductoXInversion()
                    proXInv.cantidad = productos.value[i].cantidad
                    proXInv.precioUnidadVenta = productos.value[i].precioUnidad
                    proXInv.producto = this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo)
                    proXInv.valorVenta = productos.value[i].valorVenta
                    proXInv.createdBy = this.securityService.getItem('user')
                    productosXInversion.push(proXInv)
                }

                inversion.productosXInversions = productosXInversion

                this.inversionesService.saveInversion(inversion).subscribe(
                    inversion => {
                        this.inversion = inversion
                        this.inversion.productosXInversions.forEach(inv => {
                            this.prodsXInversion.push(inv)
                            this.calculateValoresInversion()
                        })
                        this.msgService.addMessage('La inversión fue guardada con éxito.')
                        this.isNew = false
                        this.isEdit = true
                        this.blockUI.stop()
                    },
                    error => {
                        this.msgService.addError("Error al guardar la inversión")
                        this.blockUI.stop()
                    }
                )
            } else {
                this.blockUI.start('Actualizando Inversión')

                this.inversion.factura = this.factura
                this.inversion.usuario = value.usuario
                this.inversion.fecha = value.fecha
                this.inversion.total = this.inversion.total
                this.inversion.modifiedBy = this.securityService.getItem('user')

                let productos = <FormArray>this.inversionForm.get('productosXInversion')

                for (var i = 0; i < productos.length; i++) {
                    let p = this.prodsXInversion.find(p => p.producto.codigo == productos.value[i].codigo)
                    if (p == undefined) {
                        this.prodsXInversion.push({
                            cantidad: productos.value[i].cantidad,
                            precioUnidadVenta: productos.value[i].precioUnidad,
                            producto: this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo),
                            valorVenta: productos.value[i].valorVenta,
                            createdBy: this.securityService.getItem('user')
                        })
                    } else {
                        this.prodsXInversion = this.prodsXInversion.map(pi => {

                            if (p.id === pi.id) {
                                pi.cantidad = productos.value[i].cantidad
                                pi.precioUnidadVenta = productos.value[i].precioUnidad
                                pi.producto = this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo)
                                pi.valorVenta = productos.value[i].valorVenta
                                pi.modifiedBy = this.securityService.getItem('user')
                            }

                            return pi
                        })
                    }
                }

                this.inversion.productosXInversions = this.prodsXInversion

                this.inversionesService.updateInversion(this.inversion).subscribe(
                    inversion => {
                        this.inversion = inversion
                        this.msgService.addMessage('La inversión fue actualizada con éxito.')
                        this.isNew = false
                        this.isEdit = true
                        this.blockUI.stop()
                    },
                    error => {
                        console.log(error)
                        this.msgService.addError("Error al actualizar la inversión")
                        this.blockUI.stop()
                    }
                )
            }
        }
    }

    /**
     * Retorna al listado de Inversiones
     */
    cancelar() {
        this.router.navigate(['/facturacion/inversiones'])
    }

    /**
     * Limpia el formulario
     */
    cleanForm() {
        this.submitted = false
        this.inversion = new Inversion()
        this.isEdit = false
        this.isNew = true
        this.initForm()
    }

}

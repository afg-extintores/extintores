import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppEndPoints } from 'src/app/app.endpoints';
import { Observable } from 'rxjs';
import { Inversion } from 'src/app/common/model/inversion';
import { Page } from 'src/app/common/model/page';

@Injectable()
export class InversionesService {

    /**
     * Constructor del Service de Inversiones
     * 
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Inversiones paginadas
     */
    public getAllInversiones(): Observable<Inversion[]> {
        return this.http.get<Inversion[]>(this.endpoints.inversiones);
    }

    /**
     * Obtiene el listado de Inversiones paginadas
     */
    public getAllInversionesPaginadas(values: Map<string, string>): Observable<Page> {
        let params = new HttpParams()
            .set('page', values.get('page'))
            .set('size', values.get('size'))
            .set('sort', values.get('sort'))
            ;
        return this.http.get<Page>(this.endpoints.inversiones + '/pageables', { params: params });
    }

    /**
     * Registra una inversion
     * @param inversion Inversion a registrar
     */
    public saveInversion(inversion: Inversion): Observable<Inversion> {
        return this.http.post<Inversion>(this.endpoints.inversiones, inversion);
    }

    /**
     * Actualiza una Inversión
     * @param inversion Inversion a actualizar
     */
    public updateInversion(inversion: Inversion): Observable<Inversion> {
        return this.http.put<Inversion>(this.endpoints.inversiones, inversion);
    }

    /**
     * Obtiene una inversion por Id
     * @param id Id de la inversion
     */
    public getInversionById(id: number): Observable<Inversion> {
        return this.http.get<Inversion>(this.endpoints.inversiones + '/' + id);
    }

}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/common/modules/shared/shared.module';
import { BlockUIModule } from 'ng-block-ui';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { InversionesRoutingModule } from './inversiones-routing.module';
import { InversionesListComponent } from './inversiones-list/inversiones-list.component';
import { InversionesFormComponent } from './inversiones-form/inversiones-form.component';
import { InversionesService } from './inversiones.service';

import { TableModule } from 'primeng/table';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { ClientesService } from 'src/app/admin/clientes/clientes.service';
import { ClientesDireccionesService } from 'src/app/admin/clientes/clientes-direcciones/clientes-direcciones.service';
import { UsuariosService } from 'src/app/admin/usuarios/usuarios.service';
import { ProductosService } from 'src/app/admin/productos/productos.service';
import { UtilService } from 'src/app/common/services/util/util.service';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FacturasService } from '../facturas/facturas.service';

/**
 * Módulo de administración de Remisiones
 */
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        InversionesRoutingModule,
        BlockUIModule,
        BsDatepickerModule,
        TableModule,
        TypeaheadModule,
        CurrencyMaskModule,
        ModalModule,
        TooltipModule.forRoot()
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [InversionesListComponent, InversionesFormComponent],
    providers: [
        UtilService,
        InversionesService,
        ClientesService,
        ClientesDireccionesService,
        UsuariosService,
        ProductosService,
        BsModalService,
        FacturasService
    ]
})
export class InversionesModule { }

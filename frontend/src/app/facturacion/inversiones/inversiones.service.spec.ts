import { TestBed } from '@angular/core/testing';

import { InversionesService } from './inversiones.service';

describe('InversionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InversionesService = TestBed.get(InversionesService);
    expect(service).toBeTruthy();
  });
});

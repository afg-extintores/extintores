import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppEndPoints } from 'src/app/app.endpoints';
import { Observable } from 'rxjs';
import { FacturaTipoPago } from 'src/app/common/model/factura-tipo-pago';

@Injectable()
export class FacturasTiposPagoService {

    /**
     * Service Tipos Pago Facturas
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Retorna el listado de tipos de pago de facturas
     */
    public getAllTiposPago(): Observable<FacturaTipoPago[]> {
        return this.http.get<FacturaTipoPago[]>(this.endpoints.facturas_tipos_pago);
    }
}

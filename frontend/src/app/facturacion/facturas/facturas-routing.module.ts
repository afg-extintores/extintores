import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacturasListComponent } from './facturas-list/facturas-list.component';
import { FacturasFormComponent } from './facturas-form/facturas-form.component';

const routes: Routes = [
    { path: '', component: FacturasListComponent },
    { path: 'factura', component: FacturasFormComponent },
    { path: 'factura/:id/:edit', component: FacturasFormComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FacturasRoutingModule { }
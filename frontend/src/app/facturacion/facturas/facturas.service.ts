import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AppEndPoints } from 'src/app/app.endpoints';
import { Observable } from 'rxjs';
import { Factura } from 'src/app/common/model/factura';
import { Page } from 'src/app/common/model/page';
import { StringResponse } from 'src/app/common/model/string-response';

/**
 * Service para el manejo de data de Facturas
 * 
 * @author Miguel Romero
 */
@Injectable()
export class FacturasService {

    /**
       * Constructor del Service de Facturas
       * 
       * @param http 
       * @param endpoints 
       */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Retorna el listado de todas las Facturas
     */
    public getAllFacturas(): Observable<Factura[]> {
        return this.http.get<Factura[]>(this.endpoints.facturas);
    }

    /**
     * Obtiene el listado de Facturas paginadas
     */
    public getAllFacturasPaginadas(values: Map<string, string>): Observable<Page> {
        let params = new HttpParams()
            .set('page', values.get('page'))
            .set('size', values.get('size'))
            .set('sort', values.get('sort'))
            ;
        return this.http.get<Page>(this.endpoints.facturas + '/pageables', { params: params });
    }

    /**
     * Return the list of invoices that doesn't have a investment yet
     */
    public getInvoicesWithoutInvestment() : Observable<Factura[]>{
        return this.http.get<Factura[]>(this.endpoints.facturas + '/no-investment')
    }

    /**
     * Registra un factura
     * @param factura Factura a registrar
     */
    public saveFactura(factura: Factura): Observable<Factura> {
        return this.http.post<Factura>(this.endpoints.facturas, factura);
    }

    /**
     * Actualiza un factura
     * @param factura Factura a actualizar
     */
    public updateFactura(factura: Factura): Observable<Factura> {
        return this.http.put<Factura>(this.endpoints.facturas, factura);
    }

    /**
     * Elimina una factura por Id
     * @param id Id de la factura
     */
    public deleteFactura(id: number): Observable<StringResponse> {
        return this.http.delete<StringResponse>(this.endpoints.facturas + '/' + id);
    }

    /**
     * Obtiene un Factura por Id
     * @param id Id del Factura
     */
    public getFacturaById(id: number): Observable<Factura> {
        return this.http.get<Factura>(this.endpoints.facturas + '/' + id);
    }

    /**
     * Actualiza el estado de la Factura
     * 
     * @param status Estado de la factura
     * @param idInvoice Id de la Factura
     */
    public changeStatusInvoice(status: number, idInvoice: number): Observable<boolean> {
        return this.http.get<boolean>(this.endpoints.facturas + '/update-status/' + status + '/' + idInvoice);
    }

    /**
     * Carga a S3 la imagen de la factura
     * @param image 
     */
    public uploadImage(image: File, facturaId: number): Observable<StringResponse> {
        const formData: FormData = new FormData();
        formData.append('image', image);
        formData.append('facturaId', String(facturaId));
        return this.http.post<StringResponse>(this.endpoints.facturas + '/upload', formData);
    }

    public generateInvoice(factura: any): Observable<any> {
        return this.http.post(this.endpoints.facturas + '/make-factura', factura, { observe: 'response', responseType: "blob" });
    }

    /**
     * Get an Invoice by consecutive
     * @param consecutive 
     */
    public getInvoiceByConsecutive(consecutive: number): Observable<Factura>{
        return this.http.get(this.endpoints.facturas + '/by-consecutive/' + consecutive)
    }
}

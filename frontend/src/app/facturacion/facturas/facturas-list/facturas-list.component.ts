import { Component, OnInit, TemplateRef } from '@angular/core';
import { Factura } from 'src/app/common/model/factura';
import { FacturasService } from '../facturas.service';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
    selector: 'afg-facturas-list',
    templateUrl: './facturas-list.component.html',
    styleUrls: ['./facturas-list.component.css']
})
export class FacturasListComponent implements OnInit {

    facturas: Factura[]; // Listado de Facturas
    idFactura: number; // Id de Factura
    cols: any[]; // Columnas de la tabla

    modalRef: BsModalRef;

    urlImageFactura: string;

    constructor(private facturasService: FacturasService, private router: Router, private modalService: BsModalService) {
        this.initCols();
        this.getAllFacturas();
        this.urlImageFactura = '';
    }

    ngOnInit() {
    }

    private initCols() {
        this.cols = [
            { field: 'consecutivo', header: 'Consecutivo', width: '7%' },
            { field: 'fechaFactura', header: 'Fecha Factura', width: '7%' },
            { field: 'cliente.nombre', header: 'Cliente', width: '7%' },
            { field: 'usuario.nombres', header: 'Usuario', width: '15%' },
            { field: 'total', header: 'Total', width: '10%' },
            { field: 'facturasEstado.nombre', header: 'Estado', width: '10%' }
        ];
    }

    private getAllFacturas() {
        this.facturas = [];
        this.facturasService.getAllFacturas().subscribe(
            facturas => {
                this.facturas = facturas;
            },
            error => {
                console.log(error);
            }
        );
    }

    goForm(item, edit) {
        this.router.navigate(['/facturacion/facturas/factura', item.id, edit]);
    }

    showImage(template: TemplateRef<any>, url: string){
        this.modalRef = this.modalService.show(template);
        this.urlImageFactura = url;
    }

    changeStatusInvoice(status: number, id: number) {
        this.facturasService.changeStatusInvoice(status, id).subscribe(
            res => { },
            error => { },
            () => {
                this.getAllFacturas();
            }
        );
    }
}

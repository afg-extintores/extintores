import { TestBed } from '@angular/core/testing';

import { FacturasTiposPagoService } from './facturas-tipos-pago.service';

describe('FacturasTiposPagoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturasTiposPagoService = TestBed.get(FacturasTiposPagoService);
    expect(service).toBeTruthy();
  });
});

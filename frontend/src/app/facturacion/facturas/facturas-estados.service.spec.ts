import { TestBed } from '@angular/core/testing';

import { FacturasEstadosService } from './facturas-estados.service';

describe('FacturasEstadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturasEstadosService = TestBed.get(FacturasEstadosService);
    expect(service).toBeTruthy();
  });
});

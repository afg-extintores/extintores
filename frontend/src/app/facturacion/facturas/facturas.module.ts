import { FacturasListComponent } from "./facturas-list/facturas-list.component";
import { FacturasFormComponent } from "./facturas-form/facturas-form.component";
import { UtilService } from "src/app/common/services/util/util.service";
import { FacturasService } from "./facturas.service";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "src/app/common/modules/shared/shared.module";
import { FacturasRoutingModule } from "./facturas-routing.module";
import { BlockUIModule } from "ng-block-ui";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { TableModule } from "primeng/table";
import { ClientesService } from "src/app/admin/clientes/clientes.service";
import { ClientesDireccionesService } from "src/app/admin/clientes/clientes-direcciones/clientes-direcciones.service";
import { UsuariosService } from "src/app/admin/usuarios/usuarios.service";
import { ProductosService } from "src/app/admin/productos/productos.service";
import { ProductoXRemisionService } from "../remisiones/producto-x-remision.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { RemisionesService } from "../remisiones/remisiones.service";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { FacturasEstadosService } from "./facturas-estados.service";
import { TypeaheadModule } from "ngx-bootstrap/typeahead";
import { FacturasTiposPagoService } from "./facturas-tipos-pago.service";
import { CurrencyMaskModule } from "ng2-currency-mask";
import { PickListModule } from 'primeng/picklist';
import { NgxDocViewerModule } from 'ngx-doc-viewer';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        FacturasRoutingModule,
        BlockUIModule,
        BsDatepickerModule,
        TableModule,
        TypeaheadModule,
        CurrencyMaskModule,
        TooltipModule.forRoot(),
        PickListModule,
        NgxDocViewerModule
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [FacturasListComponent, FacturasFormComponent],
    providers: [
        UtilService,
        FacturasService,
        FacturasEstadosService,
        FacturasTiposPagoService,
        ClientesService,
        ClientesDireccionesService,
        UsuariosService,
        ProductosService,
        ProductoXRemisionService,
        BsModalService,
        RemisionesService,
        UtilService
    ]
})
export class FacturasModule { }
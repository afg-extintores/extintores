import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppEndPoints } from 'src/app/app.endpoints';
import { Observable } from 'rxjs';
import { FacturaEstado } from 'src/app/common/model/factura-estado';

@Injectable()
export class FacturasEstadosService {

    /**
     * Service FacturasEstados
     * @param http 
     * @param endpoints 
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Retorna el listado de estados de las facturas
     */
    public getAllEstadosFacturas(): Observable<FacturaEstado[]> {
        return this.http.get<FacturaEstado[]>(this.endpoints.facturas_estado);
    }
}

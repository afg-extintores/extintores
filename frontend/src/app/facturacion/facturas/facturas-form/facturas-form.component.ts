import { Component, OnInit, TemplateRef } from '@angular/core';
import { Factura } from 'src/app/common/model/factura';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MessagesService } from 'src/app/common/components/messages/messages.service';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Producto } from 'src/app/common/model/producto';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametrosService } from 'src/app/common/services/util/parametros.service';
import { ClienteDireccion } from 'src/app/common/model/cliente-direccion';
import { Usuario } from 'src/app/common/model/usuario';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Cliente } from 'src/app/common/model/cliente';
import { ClientesDireccionesService } from 'src/app/admin/clientes/clientes-direcciones/clientes-direcciones.service';
import { ProductosService } from 'src/app/admin/productos/productos.service';
import { UsuariosService } from 'src/app/admin/usuarios/usuarios.service';
import { ClientesService } from 'src/app/admin/clientes/clientes.service';
import { Parametro } from 'src/app/common/model/parametro';
import { RemisionesService } from '../../remisiones/remisiones.service';
import { Remision } from 'src/app/common/model/remision';
import { FacturaEstado } from 'src/app/common/model/factura-estado';
import { FacturasEstadosService } from '../facturas-estados.service';
import { FacturaTipoPago } from 'src/app/common/model/factura-tipo-pago';
import { FacturasTiposPagoService } from '../facturas-tipos-pago.service';
import { ProductoTable } from 'src/app/common/model/producto-table';
import { SecurityService } from 'src/app/common/services/security/security.service';
import { ProductoXFactura } from 'src/app/common/model/producto-x-factura';
import { FacturasService } from '../facturas.service';
import { UtilService } from 'src/app/common/services/util/util.service';

@Component({
    selector: 'afg-facturas-form',
    templateUrl: './facturas-form.component.html',
    styleUrls: ['./facturas-form.component.css']
})
export class FacturasFormComponent implements OnInit {

    /** Object binding to info form */
    factura: Factura;
    /** Factura information form */
    facturaForm: FormGroup;
    /** Flag to validate if the form has been submitted  */
    submitted: boolean;
    /** Id of the factura */
    idFactura: number;

    isNew: boolean;
    isEdit: boolean;

    remisiones: Remision[];

    cliente: Cliente;
    clientesDS: Cliente[];

    direcciones: ClienteDireccion[];

    usuarios: Usuario[];
    estadosFactura: FacturaEstado[];
    tiposPago: FacturaTipoPago[];

    iva: number;

    @BlockUI() blockUI: NgBlockUI;
    modalRef: BsModalRef;

    productosDisponibles: Producto[];
    productosSeleccionados: Producto[];

    prodsXFactura: ProductoXFactura[];

    listFacturaTable: ProductoTable[];

    imageFactura: File;

    constructor(private fb: FormBuilder, private router: Router, private msgService: MessagesService,
        private localService: BsLocaleService, private paramsService: ParametrosService, private clientesDireccionesService: ClientesDireccionesService,
        private productosService: ProductosService, private usuariosService: UsuariosService, private clientesService: ClientesService,
        private remisionesService: RemisionesService, private facturasEstadosService: FacturasEstadosService, private facturasTiposPagoService: FacturasTiposPagoService,
        private securityService: SecurityService, private facturasService: FacturasService, private actRoute: ActivatedRoute,
        private modalService: BsModalService, private utilService: UtilService) {

        this.initForm();
        this.setLocale();
        this.loadIva();
        this.loadConsecutive();
        this.loadRemisiones();
        this.loadClientes();
        this.loadUsuarios();
        this.loadEstadosFactura();
        this.loadTiposPagoFactura();
        this.loadProductosDisponibles();
    }

    ngOnInit() {
        this.submitted = false;
        this.factura = new Factura();
        this.cliente = new Cliente();
        this.isNew = true;
        this.isEdit = false;
        this.remisiones = [];
        this.productosDisponibles = [];
        this.productosSeleccionados = [];
        this.listFacturaTable = [];

        this.actRoute.params.subscribe(params => {
            if (params['id'] != undefined) {
                this.idFactura = params['id'];

                this.facturasService.getFacturaById(this.idFactura).subscribe(
                    async factura => {
                        if (params['edit'] != undefined) {
                            this.isEdit = (params['edit'] == 'true');
                        }

                        this.factura = factura;

                        this.cliente = this.factura.cliente;
                        await this.getDireccionesByCliente(this.cliente.id);

                        this.facturaForm.get('consecutivo').setValue(this.factura.consecutivo, { onlySelf: true });
                        this.facturaForm.get('remision').setValue(this.factura.remision, { onlySelf: true });
                        this.facturaForm.get('fechaCarga').setValue(this.factura.fechaCarga, { onlySelf: true });
                        this.facturaForm.get('fechaFactura').setValue(this.factura.fechaFactura, { onlySelf: true });
                        this.facturaForm.get('cliente').setValue(this.cliente.nombre, { onlySelf: true });
                        this.facturaForm.get('clientesDirecciones').setValue(this.factura.clientesDirecciones, { onlySelf: true });
                        this.facturaForm.get('usuario').setValue(this.factura.usuario, { onlySelf: true });
                        this.facturaForm.get('facturasEstado').setValue(this.factura.facturasEstado, { onlySelf: true });
                        this.facturaForm.get('facturasTiposPago').setValue(this.factura.facturasTiposPago, { onlySelf: true });
                        this.facturaForm.get('diasCredito').setValue(this.factura.diasCredito, { onlySelf: true });
                        this.facturaForm.get('ordenCompra').setValue(this.factura.ordenCompra, { onlySelf: true });

                        this.prodsXFactura = [];
                        let control = <FormArray>this.facturaForm.get('productosXFactura');

                        this.factura.productosXFacturas.forEach(pr => {
                            let proSel = this.productosDisponibles.find(p => p.codigo == pr.producto.codigo);
                            this.productosSeleccionados.push(proSel);
                            this.productosDisponibles.splice(this.productosDisponibles.indexOf(proSel), 1);
                            control.push(this.fb.group({
                                codigo: new FormControl(pr.producto.codigo),
                                nombre: new FormControl(pr.producto.descripcion),
                                precioUnidad: new FormControl(pr.precioUnidadVenta, [Validators.required, Validators.min(1)]),
                                cantidad: new FormControl(pr.cantidad, [Validators.required, Validators.min(1)]),
                                valorVenta: new FormControl(pr.valorVenta)
                            }));
                            this.prodsXFactura.push(pr);
                            this.calculateValoresFactura();
                        });

                        if (!this.isEdit) {
                            this.facturaForm.disable();
                        } else {
                            this.facturaForm.enable();
                            this.facturaForm.get('consecutivo').disable();
                        }

                        this.isNew = false;

                        this.subscribeFormChanges();

                    }
                );
            }
        });

        this.subscribeFormChanges();
    }

    /**
     * Inicializa el formulario
     */
    private initForm() {
        this.facturaForm = this.fb.group({
            consecutivo: new FormControl(null, [Validators.required]),
            remision: new FormControl(null),
            fechaFactura: new FormControl(new Date(), [Validators.required]),
            fechaCarga: new FormControl(new Date(), [Validators.required]),
            cliente: new FormControl(null, [Validators.required]),
            clientesDirecciones: new FormControl(null, [Validators.required]),
            usuario: new FormControl(null, [Validators.required]),
            facturasEstado: new FormControl(null, [Validators.required]),
            facturasTiposPago: new FormControl(null, [Validators.required]),
            diasCredito: new FormControl(null),
            ordenCompra: new FormControl(null),
            productosXFactura: this.fb.array([])
        });
    }

    /**
     * Obsevable para cambios en el formulario
     */
    subscribeFormChanges() {
        let control = <FormArray>this.facturaForm.get('productosXFactura');
        if (control.controls.length > 0) {
            for (var i = 0; i < control.controls.length; i++) {
                control.controls[i].get('precioUnidad').valueChanges.subscribe(
                    val => {
                        this.calculateValoresFactura();
                    }
                );
                control.controls[i].get('cantidad').valueChanges.subscribe(
                    val => {
                        this.calculateValoresFactura();
                    }
                );
            }
        }
    }

    /**
     * Configura el Locale para el calendar
     */
    private setLocale() {
        this.localService.use('es');
    }

    /**
     * Carga el valor del parámetro del IVA
     */
    private loadIva() {
        this.paramsService.getValueParametroByName('IVA').subscribe(
            param => {
                this.iva = Number(param.value);
            }
        );
    }

    /**
     * Carga el valor del siguiente Consecutivo
     */
    private loadConsecutive() {
        this.paramsService.getValueParametroByName('FAC_CONS').subscribe(
            value => {
                this.facturaForm.get('consecutivo').setValue(value.value, { onlySelf: true });
            }
        );
    }

    private loadRemisiones() {
        this.remisionesService.getAllRemisionesNoFactura().subscribe(
            remisiones => {
                this.remisiones = remisiones;
            }
        );
    }

    /**
     * Actualiza el valor del consecutivo de Facturas
     * @param value Valor nuevo
     */
    async updateConsecutive(value: string) {
        let param = new Parametro();
        param.nombre = 'FAC_CONS';
        param.value = value;
        await this.paramsService.updateParametro(param).toPromise();
    }

    /**
     * Carga el listado de Clientes Activos
     */
    private loadClientes() {
        this.clientesDS = [];
        this.clientesService.getAllClientesActivos().subscribe(
            clientes => {
                this.clientesDS = clientes;
            }
        )
    }

    /**
     * Carga el listado de Usuarios Vendedores Activos
     */
    private loadUsuarios() {
        this.usuarios = [];
        this.usuariosService.getAllUsuariosVendedoresActivos().subscribe(
            usuarios => {
                this.usuarios = usuarios;
            },
            error => {
                console.log(error);
            }
        )
    }

    /**
     * Carga el listado de Estados de la Factura
     */
    private loadEstadosFactura() {
        this.estadosFactura = [];
        this.facturasEstadosService.getAllEstadosFacturas().subscribe(
            estados => {
                this.estadosFactura = estados;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Carga el listado de Tipos de Pago de la Factura
     */
    private loadTiposPagoFactura() {
        this.tiposPago = [];
        this.facturasTiposPagoService.getAllTiposPago().subscribe(
            tipos => {
                this.tiposPago = tipos;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Carga el listado de Productos Activos
     */
    private loadProductosDisponibles() {
        this.productosService.getProductosActivos().subscribe(
            productos => {
                this.productosDisponibles = productos;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Retorna el listado de direcciones por Cliente
     * @param idCliente Id del Cliente
     */
    private getDireccionesByCliente(idCliente: number) {
        this.direcciones = [];
        this.clientesDireccionesService.getDireccionesByCliente(idCliente).subscribe(
            direcciones => {
                this.direcciones = direcciones;
            },
            error => {
                console.log(error);
            }
        )
    }

    /**
     * Compare del select de Remisiones
     * @param c1 
     * @param c2 
     */
    compareFnRemisiones(c1: Remision, c2: Remision): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Compare del select de Direcciones
     * @param c1 
     * @param c2 
     */
    compareFnDirecciones(c1: ClienteDireccion, c2: ClienteDireccion): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Compare del select de Usuarios
     * @param c1 
     * @param c2 
     */
    compareFnUsuarios(c1: Usuario, c2: Usuario): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Compare del select de estados de la factura
     * 
     * @param c1 
     * @param c2 
     */
    compareFnEstadosFactura(c1: FacturaEstado, c2: FacturaEstado): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Compare del select de tipos de pago de la factura
     * 
     * @param c1 
     * @param c2 
     */
    compareFnTiposPagoFactura(c1: FacturaTipoPago, c2: FacturaTipoPago): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Selecciona un cliente
     * @param event 
     */
    onSelectCliente(event: TypeaheadMatch): void {
        this.cliente = event.item;
        this.getDireccionesByCliente(this.cliente.id);
    }

    /**
     * Calcula los valores totales de la Factura
     */
    calculateValoresFactura() {
        this.factura.subtotal = 0;
        this.factura.iva = 0;
        this.factura.total = 0;
        let control = <FormArray>this.facturaForm.get('productosXFactura');
        if (control.controls.length > 0) {
            for (var i = 0; i < control.controls.length; i++) {
                let newTotal = control.controls[i].get('precioUnidad').value * control.controls[i].get('cantidad').value;
                control.controls[i].get('valorVenta').setValue(newTotal);
                this.factura.subtotal += control.controls[i].get('valorVenta').value;
            }
            this.factura.iva = (this.factura.subtotal * this.iva) / 100;
            this.factura.total = this.factura.subtotal + this.factura.iva;
        }
    }

    /**
     * Agrega un row al Form de la tabla de productos de la Factura
     * @param row 
     */
    private addRowToFormTotales(row: ProductoTable) {
        let control = <FormArray>this.facturaForm.get('productosXFactura');
        control.push(this.fb.group({
            codigo: new FormControl(row.codigo),
            nombre: new FormControl(row.producto),
            precioUnidad: new FormControl(row.precioUnidad, [Validators.required, Validators.min(1)]),
            cantidad: new FormControl(row.cantidad, [Validators.required, Validators.min(1)]),
            valorVenta: new FormControl(row.valorVenta)
        }));
        this.subscribeFormChanges();
    }

    /**
     * Agrega un Producto a la Factura
     * @param event 
     */
    addProduct(event) {
        if (event.items.length > 0) {
            for (var i = 0; i < event.items.length; i++) {
                let prodFacTable: ProductoTable = new ProductoTable();
                prodFacTable.codigo = event.items[i].codigo;
                prodFacTable.producto = event.items[i].descripcion;
                prodFacTable.precioUnidad = event.items[i].precioUnidad;
                prodFacTable.valorVenta = prodFacTable.cantidad * prodFacTable.precioUnidad;
                this.addRowToFormTotales(prodFacTable);
                this.calculateValoresFactura();
            }
        }
    }

    /**
     * Elimina la relación de un producto a la Factura
     * @param event 
     */
    removeProduct(event) {
        let control = <FormArray>this.facturaForm.get('productosXFactura');
        for (var i = 0; i < event.items.length; i++) {
            for (var j = 0; j < control.controls.length; j++) {
                if (event.items[i].codigo == control.controls[j].value.codigo) {
                    control.removeAt(j);
                    continue;
                }
            }
        }
        this.calculateValoresFactura();
    }

    /**
     * Guarda o Actualiza una factura y sus productos
     * @param value Value del formulario
     * @param valid Status del formulario
     */
    saveFactura(value: any, valid: boolean) {
        this.submitted = true;
        if (valid) {
            if (this.isNew) {
                this.blockUI.start('Guardando Factura');

                let factura: Factura = new Factura();
                factura.consecutivo = value.consecutivo;
                factura.remision = value.remision;
                factura.fechaCarga = value.fechaCarga;
                factura.fechaFactura = value.fechaFactura;
                factura.cliente = this.cliente;
                factura.clientesDirecciones = value.clientesDirecciones;
                factura.usuario = value.usuario;
                factura.facturasEstado = value.facturasEstado;
                factura.facturasTiposPago = value.facturasTiposPago;
                factura.diasCredito = value.diasCredito;
                factura.ordenCompra = value.ordenCompra;
                factura.total = this.factura.total;
                factura.subtotal = this.factura.subtotal;
                factura.iva = this.factura.iva;
                factura.createdBy = this.securityService.getItem('user');

                let productos = <FormArray>this.facturaForm.get('productosXFactura');
                let productosXFactura: ProductoXFactura[] = [];

                for (var i = 0; i < productos.length; i++) {
                    let proXFac: ProductoXFactura = new ProductoXFactura();
                    proXFac.cantidad = productos.value[i].cantidad;
                    proXFac.precioUnidadVenta = productos.value[i].precioUnidad;
                    proXFac.producto = this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo);
                    proXFac.valorVenta = productos.value[i].valorVenta;
                    proXFac.createdBy = this.securityService.getItem('user');
                    productosXFactura.push(proXFac);
                }

                factura.productosXFacturas = productosXFactura;

                this.facturasService.saveFactura(factura).subscribe(
                    factura => {
                        this.updateConsecutive(String(factura.consecutivo + 1));
                        this.factura = factura;
                        this.msgService.addMessage('La factura fue guardada con éxito.');
                        this.isNew = false;
                        this.isEdit = true;
                        this.blockUI.stop();
                    },
                    error => {
                        this.msgService.addError("Error al guardar la factura");
                        this.blockUI.stop();
                    }
                );
            } else {
                this.blockUI.start('Actualizando Factura');

                this.factura.remision = value.remision;
                this.factura.fechaCarga = value.fechaCarga;
                this.factura.fechaFactura = value.fechaFactura;
                this.factura.cliente = this.cliente;
                this.factura.clientesDirecciones = value.clientesDirecciones;
                this.factura.usuario = value.usuario;
                this.factura.facturasEstado = value.facturasEstado;
                this.factura.facturasTiposPago = value.facturasTiposPago;
                this.factura.diasCredito = value.diasCredito;
                this.factura.ordenCompra = value.ordenCompra;
                this.factura.total = this.factura.total;
                this.factura.subtotal = this.factura.subtotal;
                this.factura.iva = this.factura.iva;
                this.factura.modifiedBy = this.securityService.getItem('user');

                let productos = <FormArray>this.facturaForm.get('productosXFactura');
                let prodsXFacturaUpdate: ProductoXFactura[] = [];

                for (var i = 0; i < productos.length; i++) {
                    let p = this.prodsXFactura.find(p => p.producto.codigo == productos.value[i].codigo);
                    if (p == undefined) {
                        prodsXFacturaUpdate.push({
                            cantidad: productos.value[i].cantidad,
                            precioUnidadVenta: productos.value[i].precioUnidad,
                            producto: this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo),
                            valorVenta: productos.value[i].valorVenta,
                            createdBy: this.securityService.getItem('user')
                        });
                    } else {
                        p.cantidad = productos.value[i].cantidad;
                        p.precioUnidadVenta = productos.value[i].precioUnidad;
                        p.producto = this.productosSeleccionados.find(p => p.codigo == productos.value[i].codigo);
                        p.valorVenta = productos.value[i].valorVenta;
                        p.modifiedBy = this.securityService.getItem('user');
                        prodsXFacturaUpdate.push(p);
                    }
                }

                this.prodsXFactura = prodsXFacturaUpdate;
                this.factura.productosXFacturas = this.prodsXFactura;

                this.facturasService.updateFactura(this.factura).subscribe(
                    factura => {
                        this.factura = factura;
                        this.msgService.addMessage('La factura fue actualizada con éxito.');
                        this.isNew = false;
                        this.isEdit = true;
                        this.blockUI.stop();
                    },
                    error => {
                        console.log(error);
                        this.msgService.addError("Error al actualizar la factura");
                        this.blockUI.stop();
                    }
                );
            }
        }
    }

    /**
     * Retorna al listado de Facturas
     */
    cancelar() {
        this.router.navigate(['/facturacion/facturas']);
    }

    /**
     * Limpia el formulario
     */
    cleanForm() {
        this.submitted = false;
        this.factura = new Factura();
        this.isEdit = false;
        this.isNew = true;
        this.initForm();
        this.setLocale();
        this.loadIva();
        this.loadConsecutive();
        this.loadClientes();
        this.loadUsuarios();
        this.loadEstadosFactura();
        this.loadTiposPagoFactura();
        this.loadProductosDisponibles();
        this.productosSeleccionados = [];
        this.msgService.close();
    }

    showModalUploadImage(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    onFileChanged(imageInput: any) {
        this.imageFactura = new File([imageInput.files[0]], this.factura.cliente.nombre + '.' + imageInput.files[0].name.split('.')[1], { type: imageInput.files[0].type });
    }

    uploadFile() {
        this.facturasService.uploadImage(this.imageFactura, this.factura.id).subscribe(
            res => {
                this.imageFactura = null;
                this.modalRef.hide();
                this.msgService.addMessage("Imagen factura cargada exitosamente");
            },
            error => {
                console.log(error);
                this.modalRef.hide();
                this.msgService.addError("Error al cargar la imagen de la factura");
            }
        );
    }

    generateInvoice() {
        this.blockUI.start('Generando Factura');

        let fechaTmp = new Date(this.factura.fechaFactura)
        let fecha = this.utilService.getMonthOnLetter(fechaTmp.getMonth()) + " " + fechaTmp.getDate() + " " + " DE " + fechaTmp.getFullYear()

        this.loadListFacturaTable();

        let facturaInfo = {
            fecha: fecha,
            nitEmpresa: this.factura.cliente.nit,
            nombreEmpresa: this.factura.cliente.nombre,
            direccion: this.factura.clientesDirecciones.direccion,
            telefono: this.factura.cliente.telefono,
            ordenCompra: this.factura.ordenCompra,
            tipoPago: this.factura.facturasTiposPago.nombre,
            diasCredito: this.factura.diasCredito != null ? this.factura.diasCredito : 0,
            nombreContacto: this.factura.cliente.nombreContacto,
            listFacturaTable: this.listFacturaTable,
            valorLetras: this.utilService.numeroALetras(this.factura.total).trim(),
            subtotal: this.utilService.currencyFormatNoSymbol(this.factura.subtotal),
            iva: this.utilService.currencyFormatNoSymbol(this.factura.iva),
            total: this.utilService.currencyFormatNoSymbol(this.factura.total)
        }

        this.facturasService.generateInvoice(facturaInfo).subscribe(
            result => {
                var file = new Blob([result.body], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                var fileURL = URL.createObjectURL(file);

                const a = document.createElement('a');
                document.body.appendChild(a);
                a.href = fileURL;
                a.download = 'Factura_' + this.factura.id + '_' + this.factura.fechaFactura + '_' + this.factura.cliente.nombre;
                a.click();

                this.blockUI.stop();
            },
            error => {
                console.log(error);
                this.blockUI.stop();
            }
        );
    }

    private loadListFacturaTable() {
        this.listFacturaTable = [];
        let control = <FormArray>this.facturaForm.get('productosXFactura');
        for (var i = 0; i < control.controls.length; i++) {
            const value = control.controls[i].value;
            this.listFacturaTable.push({
                codigo: value.codigo,
                producto: value.nombre,
                descripcion: value.nombre,
                precioUnidad: value.precioUnidad,
                cantidad: value.cantidad,
                valorVenta: value.valorVenta
            });
        }
    }

}

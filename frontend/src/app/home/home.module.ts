import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeCalendarDatesComponent } from './home-calendar-dates/home-calendar-dates.component';
import { CitasService } from '../citas/citas.service';
import { UtilService } from '../common/services/util/util.service';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

/**
* Módulo del Home de la aplicación
*
* @author Miguel Romero
*/
@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory
        })
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [HomeComponent, HomeCalendarDatesComponent],
    providers: [
        CitasService,
        UtilService
    ]
})
export class HomeModule { }

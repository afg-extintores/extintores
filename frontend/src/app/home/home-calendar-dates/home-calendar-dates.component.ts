import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { CitasService } from 'src/app/citas/citas.service';
import { UtilService } from 'src/app/common/services/util/util.service';
import { Subject } from 'rxjs';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarView,
    CalendarDateFormatter
} from 'angular-calendar';
import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours
} from 'date-fns';
import { CustomDateFormatterService } from 'src/app/common/services/util/custom-date-formatter.service';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    selector: 'afg-home-calendar-dates',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './home-calendar-dates.component.html',
    providers: [
        {
            provide: CalendarDateFormatter,
            useClass: CustomDateFormatterService
        }
    ],
    styleUrls: ['./home-calendar-dates.component.css']
})
export class HomeCalendarDatesComponent implements OnInit {

    view: CalendarView = CalendarView.Month;

    CalendarView = CalendarView;

    viewDate: Date = new Date();

    refresh: Subject<any> = new Subject();

    activeDayIsOpen: boolean = true;

    events: CalendarEvent[];

    locale: string = 'es';

    private startDate: Date;
    private endDate: Date;

    constructor(private citasService: CitasService, private utilService: UtilService) {
        this.buildInitialDates(new Date());
        this.loadEvents();
    }

    ngOnInit() {
        this.activeDayIsOpen = false;
    }

    actions: CalendarEventAction[] = [
        {
            label: '<i class="fa fa-fw fa-pencil"></i>',
            a11yLabel: 'Edit',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.handleEvent('Edited', event);
            }
        },
        {
            label: '<i class="fa fa-fw fa-times"></i>',
            a11yLabel: 'Delete',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.events = this.events.filter(iEvent => iEvent !== event);
                this.handleEvent('Deleted', event);
            }
        }
    ];

    private async loadEvents() {
        this.events = [];
        const dates = await this.citasService.getCitasByDateRange(this.startDate, this.endDate).toPromise();
        if (dates) {
            dates.forEach(d => {
                const time = d.fechaCita.toString().split(" ")[1]
                const ampm = new Date(d.fechaCita).getHours() >= 12 ? 'PM' : 'AM';
                this.events.push({
                    start: new Date(d.fechaCita),
                    end: addHours(new Date(d.fechaCita), 1),
                    title: d.cliente.nombre + ' - ' + d.clientesDirecciones.direccion + ' - ' + time + ' ' + ampm,
                    color: colors.blue,
                    allDay: false,
                    resizable: {
                        beforeStart: true,
                        afterEnd: true
                    },
                    draggable: true
                });
            });
        }
    }

    private buildInitialDates(date: Date) {
        const dates = this.utilService.getStartAndEndDateByMonth(date);
        this.startDate = dates['startDate'];
        this.endDate = dates['endDate'];
    }

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
            }
            this.viewDate = date;
        }
    }

    eventTimesChanged({
        event,
        newStart,
        newEnd
    }: CalendarEventTimesChangedEvent): void {
        this.events = this.events.map(iEvent => {
            if (iEvent === event) {
                return {
                    ...event,
                    start: newStart,
                    end: newEnd
                };
            }
            return iEvent;
        });
        this.handleEvent('Dropped or resized', event);
    }

    handleEvent(action: string, event: CalendarEvent): void {

    }

    setView(view: CalendarView) {
        this.view = view;
    }

    closeOpenMonthViewDay(viewDate: Date) {
        this.buildInitialDates(viewDate);
        this.loadEvents();
        this.activeDayIsOpen = false;
    }

}

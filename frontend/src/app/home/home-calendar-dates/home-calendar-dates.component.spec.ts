import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCalendarDatesComponent } from './home-calendar-dates.component';

describe('HomeCalendarDatesComponent', () => {
  let component: HomeCalendarDatesComponent;
  let fixture: ComponentFixture<HomeCalendarDatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCalendarDatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCalendarDatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

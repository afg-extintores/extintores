import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppEndPoints } from '../app.endpoints';
import { Observable } from 'rxjs';
import { Token } from '../common/model/token';

/**
 * Servicio para el login de usuarios
 * 
 * @author Miguel Romero
 */
@Injectable({
    providedIn: 'root',
})
export class LoginService {

    /**
     * Constructor del Servicio {@link LoginService}
     * 
     * @param http Servicio que consume los endpoints de la API
     */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Valida credenciales del usuario
     * 
     * @param login Login del usuairo
     * @param password Password del usuario
     */
    public login(login: string, password: string): Observable<Token> {
        let params = {
            login: login,
            password: password
        };
        return this.http.post<Token>(this.endpoints.login, params);
    }

}

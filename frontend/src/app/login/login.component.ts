import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { Login } from '../common/model/login';
import { SecurityService } from '../common/services/security/security.service';
import { MessagesService } from '../common/components/messages/messages.service';
import { NgBlockUI, BlockUI } from 'ng-block-ui';

/**
* Componente para el Login de usuarios a la aplicación
*
* @author Miguel Romero
*/
@Component({
    selector: 'afg-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    submitted: boolean;

    message: string;

    @BlockUI() blockUI: NgBlockUI;

    constructor(private fb: FormBuilder, private router: Router, private loginService: LoginService,
        private securityService: SecurityService, private msgService: MessagesService) {
        this.initForm();
    }

    ngOnInit() {
    }

    /**
     * Inicializa el formulario de login
     */
    private initForm() {
        this.loginForm = this.fb.group({
            login: new FormControl(null, [Validators.required]),
            password: new FormControl(null, [Validators.required])
        });
    }

    /**
     * Realiza la autenticación de usuario
     */
    login() {
        this.submitted = true;
        if (this.loginForm.valid) {

            this.blockUI.start('Validando Acceso Usuario');

            const login = new Login();
            login.login = this.loginForm.get('login').value;
            login.password = this.loginForm.get('password').value;

            this.loginService.login(login.login, login.password).subscribe(
                token => {
                    this.securityService.saveToken(token);
                    this.blockUI.stop();
                    this.router.navigateByUrl('/home');
                },
                error => {
                    console.log(error);
                    this.msgService.addError("Usuario y/o Password Invalidos");
                    this.blockUI.stop();
                }
            )
        }
    }

}

import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../services/security/security.service';

import * as $ from 'jquery';

@Component({
    selector: 'afg-template',
    templateUrl: './template.component.html',
    styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

    userLogged: string;
    rolName: string;

    constructor(private securityService: SecurityService) { }

    ngOnInit() {
        this.userLogged = this.securityService.getItem('user');
        this.rolName = this.securityService.getItem('rolName');

        $(document).ready(function () {

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

        });
    }

    logout() {
        this.securityService.logout();
    }

}

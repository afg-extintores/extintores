import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MessagesService {

    // Observables String sources
    /** Observable para el contenido del mensaje */
    private message: Subject<string> = new Subject<string>()
    /** Observable para el tipo de Mensaje */
    private type: Subject<string> = new Subject<string>()

    // Observables streams  
    /** Stream del Observable del contenido del mensaje */
    message$ = this.message.asObservable()
    /** Stream del Observable del tipo de mensaje */
    type$ = this.type.asObservable()

    // Eventos del Servicio
    /**
     * Agrega un mensaje de información de usuario
     * 
     * @param message Contenido del Mensaje
     */
    addMessage(message: string) {
        this.message.next(message)
        this.type.next('message')
    }

    /**
     * Agrega un error
     * 
     * @param message Contenido del mensaje de error
     */
    addError(message: string) {
        this.message.next(message)
        this.type.next('error')
    }

    /**
     * Cierra el mensaje
     */
    close() {
        this.message.next(null)
        this.type.next(null)
    }
}

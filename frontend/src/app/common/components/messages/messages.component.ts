import { Component, OnInit, Input } from '@angular/core';
import { MessagesService } from './messages.service';

@Component({
    selector: 'afg-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

    /** Tipo de Mensaje (Información o Error) */
    public type: string
    /** Contenido del Mensaje */
    public message: string

    /**
     * Constructor del Componente MessagesComponent
     * 
     * @param messageService Service para el manejo de los mensajes
     */
    constructor(private messageService: MessagesService) {

        this.messageService.type$.subscribe((type: string) => {
            this.type = type
        })

        this.messageService.message$.subscribe((message: string) => {
            this.message = message;
        })

    }

    /**
     * Inicializador del Componente
     */
    ngOnInit() {
    }

}

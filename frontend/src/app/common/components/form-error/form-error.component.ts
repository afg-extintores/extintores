import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'afg-form-error',
    templateUrl: './form-error.component.html',
    styleUrls: ['./form-error.component.css']
})
export class FormErrorComponent implements OnInit {

    @Input() message: string;

    constructor() {
    }

    ngOnInit() {
    }

}

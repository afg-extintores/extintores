import { Component, Input, ElementRef, ViewChild, AfterViewChecked, AfterContentInit, ContentChildren, QueryList, TemplateRef, EventEmitter, Output } from '@angular/core';
import { DomHandler } from 'primeng/components/dom/domhandler';
import { ObjectUtils } from 'primeng/components/utils/objectutils';
import { PrimeTemplate } from 'primeng/components/common/shared';

@Component({
    selector: 'afg-pick-list',
    templateUrl: './pick-list.component.html',
    styleUrls: ['./pick-list.component.css'],
    providers: [DomHandler, ObjectUtils]
})
export class PickListComponent implements AfterViewChecked, AfterContentInit {

    @Input() source: any[];
    @Input() target: any[];

    @Input() sourceHeader: string;
    @Input() targetHeader: string;

    @Input() sourcePlaceholder: string;
    @Input() targetPlaceholder: string;

    @Input() trackBy: Function = (index: number, item: any) => item;
    @Input() sourceTrackBy: Function;
    @Input() targetTrackBy: Function;

    @Input() metaKeySelection: boolean = true;
    @Input() disabled: boolean;
    @Input() enableAllButtons: boolean;

    @Output() onMoveToSource: EventEmitter<any> = new EventEmitter();
    @Output() onMoveAllToSource: EventEmitter<any> = new EventEmitter();
    @Output() onMoveAllToTarget: EventEmitter<any> = new EventEmitter();
    @Output() onMoveToTarget: EventEmitter<any> = new EventEmitter();

    @ViewChild('sourceSelect', {static: false}) selectViewSourceChild: ElementRef;
    @ViewChild('targetSelect', {static: false}) selectViewTargetChild: ElementRef;

    @ContentChildren(PrimeTemplate) templates: QueryList<any>;
    public itemTemplate: TemplateRef<any>;

    itemTouched: boolean;

    selectedItemsSource: any[] = [];
    selectedItemsTarget: any[] = [];

    constructor(public el: ElementRef, public domHandler: DomHandler, public objectUtils: ObjectUtils) { }

    ngAfterContentInit() {
        this.templates.forEach((item) => {
            switch (item.getType()) {
                case 'item':
                    this.itemTemplate = item.template;
                    break;

                default:
                    this.itemTemplate = item.template;
                    break;
            }
        });
    }

    ngAfterViewChecked() {
    }

    findIndexInList(item: any, list: any): number {
        let index: number = -1;
        if (list) {
            for (let i = 0; i < list.length; i++) {
                if (list[i] == item) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    findIndexInSelection(item: any, selectedItems: any[]): number {
        return this.findIndexInList(item, selectedItems);
    }

    onItemSelected(event, item: any, selectedItems: any[]) {
        let index = this.findIndexInSelection(item, selectedItems);
        let selected = (index != -1);
        let metaSelection = this.itemTouched ? false : this.metaKeySelection;
        if (metaSelection) {
            let metaKey = (event.metaKey || event.ctrlKey);
            if (selected && metaKey) {
                selectedItems.splice(index, 1);
            }
            else {
                if (!metaKey) {
                    selectedItems.length = 0;
                }
                selectedItems.push(item);
            }
        }
        else {
            if (selected)
                selectedItems.splice(index, 1);
            else
                selectedItems.push(item);
        }
        this.itemTouched = false;
    }

    moveRight() {
        if (this.selectedItemsSource && this.selectedItemsSource.length) {
            for (let i = 0; i < this.selectedItemsSource.length; i++) {
                let selectedItem = this.selectedItemsSource[i];
                if (this.findIndexInList(selectedItem, this.target) == -1) {
                    this.target.push(this.source.splice(this.findIndexInList(selectedItem, this.source), 1)[0]);
                }
            }
            this.onMoveToTarget.emit({
                items: this.selectedItemsSource
            });
            this.selectedItemsSource = [];
        }
    }

    moveAllRight() {
        if (this.source) {
            for (let i = 0; i < this.source.length; i++) {
                this.target.push(this.source[i]);
                this.selectedItemsSource.push(this.source[i]);
            }
            this.onMoveAllToTarget.emit({
                items: this.selectedItemsSource
            });
            this.source.splice(0, this.source.length);
            this.selectedItemsSource = [];
        }
    }

    moveLeft() {
        if (this.selectedItemsTarget && this.selectedItemsTarget.length) {
            for (let i = 0; i < this.selectedItemsTarget.length; i++) {
                let selectedItem = this.selectedItemsTarget[i];
                if (this.findIndexInList(selectedItem, this.source) == -1) {
                    this.source.push(this.target.splice(this.findIndexInList(selectedItem, this.target), 1)[0]);
                }
            }
            this.onMoveToSource.emit({
                items: this.selectedItemsTarget
            });
            this.selectedItemsTarget = [];
        }
    }

    moveAllLeft() {
        if (this.target) {
            for (let i = 0; i < this.target.length; i++) {
                this.source.push(this.target[i]);
                this.selectedItemsTarget.push(this.target[i]);
            }
            this.onMoveAllToSource.emit({
                items: this.selectedItemsTarget
            });
            this.target.splice(0, this.target.length);
            this.selectedItemsTarget = [];
        }
    }

}
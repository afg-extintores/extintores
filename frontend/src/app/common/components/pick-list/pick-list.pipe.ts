import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterByItemPickList',
    pure: false
})
export class FilterByItemPickListPipe implements PipeTransform {

    transform(value: any, args: string = ''): any {
        if (value.length > 0 && args != '') {
            return value.filter((item) => item.nombre.toLowerCase().includes(args.toLowerCase()))
        } else {
            return value;
        }
    }

}
import { Directive, Input, HostListener } from '@angular/core';

@Directive({
    selector: '[href]'
})
export class HrefDirective {

    @Input() href;

    /**
     * Constructor de la clase {@link HrefDirective}
     */
    constructor() { }

    @HostListener("click")
    public onClick(): boolean {
        if (this.href.length == 0 || this.href === '#')
            return false;
    }

}
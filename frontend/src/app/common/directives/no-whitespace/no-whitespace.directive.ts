import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
import { NoWhitespaceValidator } from '../../validators/no-whitespace.validator';

@Directive({
    selector: '[afgNoWhitespace]',
    providers: [{ provide: NG_VALIDATORS, useExisting: NoWhitespaceDirective, multi: true }]
})
export class NoWhitespaceDirective implements Validator {

    private valFn = NoWhitespaceValidator();

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }

}

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Token } from '../../model/token';

/**
* Servicio para utilidades de seguridad en la aplicación.
*
* @author Miguel Romero
*/
@Injectable({
    providedIn: 'root'
})
export class SecurityService implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
        if (this.getItem('token') != null) {
            return true;
        } else {
            this.router.navigate(['/'])
            return false;
        }
    }

    getItem(item: string): string {
        return sessionStorage.getItem(item)
    }

    saveToken(token: Token) {
        sessionStorage.setItem('token', token.accessToken);
        sessionStorage.setItem('user', token.names);
        sessionStorage.setItem('userId', String(token.userId));
        sessionStorage.setItem('rolId', String(token.rolId));
        sessionStorage.setItem('rolName', token.rolName);
    }

    cleanData() {
        sessionStorage.clear()
    }

    logout() {
        this.cleanData()
        this.router.navigateByUrl('/');
    }

}

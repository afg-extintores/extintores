import { Injectable } from '@angular/core';
import { AppEndPoints } from '../../../app.endpoints';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Parametro } from '../../model/parametro';

@Injectable({
    providedIn: 'root'
})
export class ParametrosService {

    /**
       * Constructor del Service de Servicios
       * 
       * @param http 
       * @param endpoints 
       */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el valor de un parámetro por su nombre
     * @param name Nombre del parámetro
     */
    public getValueParametroByName(name: string): Observable<Parametro> {
        return this.http.get<Parametro>(this.endpoints.parametros + '/by-name/' + name);
    }

    /**
     * Actualiza un Parametro
     * @param parametro Parametro a actualizar
     */
    public updateParametro(parametro: Parametro): Observable<Parametro> {
        return this.http.put<Parametro>(this.endpoints.parametros, parametro);
    }

}

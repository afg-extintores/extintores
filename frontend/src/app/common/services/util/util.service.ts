import { Injectable } from '@angular/core';

@Injectable()
export class UtilService {

    constructor() { }

    public getMonthOnLetter(mes: number): string {
        let meses = ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL',
            'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
        return meses[mes]
    }

    public currencyFormat(num: number): string {
        return "$ " + num.
            toFixed(2)
            .replace(".", ",")
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    }

    public currencyFormatNoSymbol(num: number): string {
        return "" + num.
            toFixed(2)
            .replace(".", ",")
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    }

    public getStartAndEndDateByMonth(date: Date) {
        let year = date.getFullYear();
        let month = date.getMonth();
        let dates = {
            "startDate": null,
            "endDate": null
        };
        if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) {
            dates['startDate'] = new Date(year, month, 1);
            dates['endDate'] = new Date(year, month, 31);
        } else if (month == 3 || month == 5 || month == 8 || month == 10) {
            dates['startDate'] = new Date(year, month, 1);
            dates['endDate'] = new Date(year, month, 30);
        } else if (month == 1) {
            if (this.leapYear(year)) {
                dates['startDate'] = new Date(year, month, 1);
                dates['endDate'] = new Date(year, month, 29);
            } else {
                dates['startDate'] = new Date(year, month, 1);
                dates['endDate'] = new Date(year, month, 28);
            }
        }
        return dates;
    }

    public leapYear(year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

    public numeroALetras(num: number): string {
        var data = {
            numero: num,
            enteros: Math.floor(num),
            centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
            letrasCentavos: "",
            letrasMonedaPlural: "",
            letrasMonedaSingular: ""
        };

        if (data.centavos > 0)
            data.letrasCentavos = "CON " + data.centavos + "/100";

        if (data.enteros == 0)
            return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
        if (data.enteros == 1)
            return this.Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;
        else
            return this.Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;

    }

    private Unidades(num) {
        switch (num) {
            case 1: return "UN";
            case 2: return "DOS";
            case 3: return "TRES";
            case 4: return "CUATRO";
            case 5: return "CINCO";
            case 6: return "SEIS";
            case 7: return "SIETE";
            case 8: return "OCHO";
            case 9: return "NUEVE";
        }

        return "";
    }

    private Decenas(num) {

        var decena = Math.floor(num / 10);
        var unidad = num - (decena * 10);

        switch (decena) {
            case 1:
                switch (unidad) {
                    case 0: return "DIEZ";
                    case 1: return "ONCE";
                    case 2: return "DOCE";
                    case 3: return "TRECE";
                    case 4: return "CATORCE";
                    case 5: return "QUINCE";
                    default: return "DIECI" + this.Unidades(unidad);
                }
            case 2:
                switch (unidad) {
                    case 0: return "VEINTE";
                    default: return "VEINTI" + this.Unidades(unidad);
                }
            case 3: return this.DecenasY("TREINTA", unidad);
            case 4: return this.DecenasY("CUARENTA", unidad);
            case 5: return this.DecenasY("CINCUENTA", unidad);
            case 6: return this.DecenasY("SESENTA", unidad);
            case 7: return this.DecenasY("SETENTA", unidad);
            case 8: return this.DecenasY("OCHENTA", unidad);
            case 9: return this.DecenasY("NOVENTA", unidad);
            case 0: return this.Unidades(unidad);
        }
    }//Unidades()

    private DecenasY(strSin, numUnidades) {
        if (numUnidades > 0)
            return strSin + " Y " + this.Unidades(numUnidades)

        return strSin;
    }//DecenasY()

    private Centenas(num) {

        var centenas = Math.floor(num / 100);
        var decenas = num - (centenas * 100);

        switch (centenas) {
            case 1:
                if (decenas > 0)
                    return "CIENTO " + this.Decenas(decenas);
                return "CIEN";
            case 2: return "DOSCIENTOS " + this.Decenas(decenas);
            case 3: return "TRESCIENTOS " + this.Decenas(decenas);
            case 4: return "CUATROCIENTOS " + this.Decenas(decenas);
            case 5: return "QUINIENTOS " + this.Decenas(decenas);
            case 6: return "SEISCIENTOS " + this.Decenas(decenas);
            case 7: return "SETECIENTOS " + this.Decenas(decenas);
            case 8: return "OCHOCIENTOS " + this.Decenas(decenas);
            case 9: return "NOVECIENTOS " + this.Decenas(decenas);
        }

        return this.Decenas(decenas);
    }//Centenas()

    private Seccion(num, divisor, strSingular, strPlural) {
        var cientos = Math.floor(num / divisor)
        var resto = num - (cientos * divisor)

        var letras = "";

        if (cientos > 0)
            if (cientos > 1)
                letras = this.Centenas(cientos) + " " + strPlural;
            else
                letras = strSingular;

        if (resto > 0)
            letras += "";

        return letras;
    }//Seccion()

    private Miles(num) {
        var divisor = 1000;
        var cientos = Math.floor(num / divisor)
        var resto = num - (cientos * divisor)

        var strMiles = this.Seccion(num, divisor, "UN MIL", "MIL");
        var strCentenas = this.Centenas(resto);

        if (strMiles == "")
            return strCentenas;

        return strMiles + " " + strCentenas;

        //return Seccion(num, divisor, "UN MIL", "MIL") + " " + Centenas(resto);
    }//Miles()

    private Millones(num) {
        var divisor = 1000000;
        var cientos = Math.floor(num / divisor)
        var resto = num - (cientos * divisor)

        var strMillones = this.Seccion(num, divisor, "UN MILLON", "MILLONES");
        var strMiles = this.Miles(resto);

        if (strMillones == "")
            return strMiles;

        return strMillones + " " + strMiles;

        //return Seccion(num, divisor, "UN MILLON", "MILLONES") + " " + Miles(resto);
    }//Millones()



}

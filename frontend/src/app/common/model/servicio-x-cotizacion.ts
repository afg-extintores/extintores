import { Base } from "./base";
import { Cotizacion } from "./cotizacion";
import { Servicio } from "./servicio";

/**
 * Clase que modela la entidad ServicioXCotizacion
 * 
 * @author Miguel Romero
 */
export class ServicioXCotizacion extends Base {

    constructor(
        public id?: number,
        public cantidad?: number,
        public precioUnidad?: number,
        public valorVenta?: number,
        public cotizacion?: Cotizacion,
        public servicio?: Servicio
    ) { super(); }

}
import { Base } from "./base";
import { Factura } from "./factura";
import { ProductoXInversion } from "./producto-x-inversion";
import { Usuario } from "./usuario";

/**
 * Clase que modela la entidad Inversion
 * 
 * @author Miguel Romero
 */
export class Inversion extends Base {

    constructor(
        public id?: number,
        public usuario?: Usuario,
        public fecha?: Date,
        public total?: number,
        public factura?: Factura,
        public productosXInversions?: ProductoXInversion[]
    ) { super(); }
}

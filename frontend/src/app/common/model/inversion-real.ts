import { Base } from "./base";
import { Factura } from "./factura";
import { ProductoXInversionReal } from "./producto-x-inversion-real";

/**
 * Clase que modela la entidad InversionReal
 * 
 * @author Miguel Romero
 */
export class InversionReal extends Base {

    constructor(
        public id?: number,
        public asesor?: string,
        public fecha?: Date,
        public total?: number,
        public factura?: Factura,
        public productosXInversionReals?: ProductoXInversionReal[]
    ) { super(); }
}

export class ProductoTable {
    constructor(
        public idProducto?: number,
        public codigo?: string,
        public producto?: string,
        public descripcion?: string,
        public precioUnidad?: number,
        public cantidad?: number,
        public valorVenta?: number
    ) {
        this.cantidad = 1;
    }
}
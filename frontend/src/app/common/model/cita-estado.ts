import { Base } from "./base";

/**
 * Clase que modela la entidad Cita
 * 
 * @author Miguel Romero
 */
export class CitaEstado extends Base {

    constructor(
        public id?: number,
        public nombre?: string
    ) { super(); }

}
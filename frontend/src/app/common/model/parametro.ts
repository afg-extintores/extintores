/**
 * Clase que modela la entidad Parametro
 * 
 * @author Miguel Romero
 */
export class Parametro {

    constructor(
        public nombre?: string,
        public value?: string
    ) { }

}

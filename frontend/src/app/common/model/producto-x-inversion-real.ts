import { Base } from "./base";
import { Factura } from "./factura";
import { InversionReal } from "./inversion-real";
import { Producto } from "./producto";

/**
 * Clase que modela la entidad ProductoXInversion
 * 
 * @author Miguel Romero
 */
export class ProductoXInversionReal extends Base {

    constructor(
        public id?: number,
        public cantidad?: number,
        public precioUnidadVenta?: number,
        public valorVenta?: number,
        public inversionesReales?: InversionReal,
        public producto?: Producto
    ) { super(); }
}

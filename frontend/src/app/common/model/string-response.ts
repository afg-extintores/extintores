/**
 * Clase que modela la respuesta en string
 * 
 * @author Miguel Romero
 */
export class StringResponse {
    constructor(
        public response?: string
    ) { }
}
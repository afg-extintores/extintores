import { Base } from "./base";
import { Factura } from "./factura";
import { Producto } from "./producto";

/**
 * Clase que modela la entidad ProductoXFactura
 * 
 * @author Miguel Romero
 */
export class ProductoXFactura extends Base {

    constructor(
        public id?: number,
        public cantidad?: number,
        public precioUnidadVenta?: number,
        public valorVenta?: number,
        public factura?: Factura,
        public producto?: Producto
    ) { super(); }
}

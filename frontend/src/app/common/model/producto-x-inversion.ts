import { Base } from "./base";
import { Factura } from "./factura";
import { Inversion } from "./inversion";
import { Producto } from "./producto";

/**
 * Clase que modela la entidad ProductoXInversion
 * 
 * @author Miguel Romero
 */
export class ProductoXInversion extends Base {

    constructor(
        public id?: number,
        public cantidad?: number,
        public precioUnidadVenta?: number,
        public valorVenta?: number,
        public inversion?: Inversion,
        public producto?: Producto
    ) { super(); }
}

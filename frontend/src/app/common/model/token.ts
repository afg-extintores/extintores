import { Rol } from "./rol";

/**
 * Clase que modela la respuesta del Login Exitoso
 * 
 * @author Miguel Romero
 */
export class Token {
    constructor(
        public names?: string,
        public userId?: number,
        public rolId?: number,
        public rolName?: string,
        public accessToken?: string
    ) { }
}
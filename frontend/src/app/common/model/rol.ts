import { Base } from "./base";

/**
 * Clase que modela la entidad Rol
 * 
 * @author Miguel Romero
 */
export class Rol extends Base {

    constructor(
        public id?: number,
        public nombre?: string
    ) { super(); }
}

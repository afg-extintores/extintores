export class CotizacionTable {

    constructor(
        public id_servicio?: number,
        public codigo?: string,
        public servicio?: string,
        public precio_unidad?: number,
        public cantidad?: number,
        public valor_venta?: number
    ) {
        this.cantidad = 1;
    }

}
import { Base } from "./base";
import { Cliente } from "./cliente";
import { Usuario } from "./usuario";
import { CitaEstado } from "./cita-estado";
import { ClienteDireccion } from "./cliente-direccion";

/**
 * Clase que modela la entidad Cita
 * 
 * @author Miguel Romero
 */
export class Cita extends Base {

    constructor(
        public id?: number,
        public fechaCita?: Date,
        public fechaLlamadaCita?: Date,
        public observaciones?: string,
        public quienLlamo?: Usuario,
        public quienFue?: Usuario,
        public cliente?: Cliente,
        public clientesDirecciones?: ClienteDireccion,
        public estado?: CitaEstado
    ) { super(); }

}

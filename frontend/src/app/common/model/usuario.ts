import { Base } from "./base";
import { Rol } from "./rol";

/**
 * Clase que modela la entidad Usuario
 * 
 * @author Miguel Romero
 */
export class Usuario extends Base {

    constructor(
        public id?: number,
        public acceso?: boolean,
        public activo?: boolean,
        public apellidos?: string,
        public correo?: string,
        public documento?: string,
        public nombres?: string,
        public password?: string,
        public vendedor?: boolean,
        public rol?: Rol
    ) { super(); }
}

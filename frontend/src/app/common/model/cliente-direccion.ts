import { Base } from "./base";
import { Cliente } from "./cliente";

/**
 * Clase que modela la entidad ClienteDireccion
 * 
 * @author Miguel Romero
 */
export class ClienteDireccion extends Base {

    constructor(
        public id?: number,
        public direccion?: string,
        public cliente?: Cliente
    ) { super(); }

}

import { Base } from "./base";
import { Cliente } from "./cliente";
import { ClienteDireccion } from "./cliente-direccion";
import { Usuario } from "./usuario";
import { ProductoXRemision } from "./producto_x_remision";

/**
 * Clase que modela la entidad Remision
 * 
 * @author Miguel Romero
 */
export class Remision extends Base {

    constructor(
        public id?: number,
        public consecutivo?: number,
        public fechaRemision?: Date,
        public iva?: number,
        public observaciones?: string,
        public total?: number,
        public subtotal?: number,
        public cliente?: Cliente,
        public clientesDirecciones?: ClienteDireccion,
        public usuario?: Usuario,
        public productosXRemisions?: ProductoXRemision[]
    )
    { super(); };

}
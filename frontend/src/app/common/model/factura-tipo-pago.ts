import { Base } from "./base";

/**
 * Clase que modela la entidad FacturaTipoPago
 * 
 * @author Miguel Romero
 */
export class FacturaTipoPago extends Base {

    constructor(
        public id?: number,
        public nombre?: string
    ) { super(); }
}

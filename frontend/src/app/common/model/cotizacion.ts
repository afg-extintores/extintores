import { Base } from "./base";
import { Cliente } from "./cliente";
import { ClienteDireccion } from "./cliente-direccion";
import { Usuario } from "./usuario";

/**
 * Clase que modela la entidad Cotizacion
 * 
 * @author Miguel Romero
 */
export class Cotizacion extends Base {

    constructor(
        public id?: number,
        public consecutivo?: string,
        public entrega?: string,
        public fecha?: Date,
        public formaPago?: string,
        public incluirDocumentos?: boolean,
        public iva?: number,
        public nota?: string,
        public oferta?: string,
        public subtotal?: number,
        public total?: number,
        public cliente?: Cliente,
        public clienteDireccion?: ClienteDireccion,
        public usuario?: Usuario
    ) {
        super();
        this.iva = 0
        this.subtotal = 0
        this.total = 0
    }

}
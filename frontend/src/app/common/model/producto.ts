import { Base } from "./base";

/**
 * Clase que modela la entidad Producto
 * 
 * @author Miguel Romero
 */
export class Producto extends Base {

    constructor(
        public id?: number,
        public activo?: boolean,
        public codigo?: string,
        public descripcion?: string,
        public nombre?: string,
        public precioUnidad?: number
    ) { super(); }
}

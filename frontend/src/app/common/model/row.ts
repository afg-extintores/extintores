export class Row{
    constructor(
        public key?: string,
        public header?: string,
        public type?: string
    ){};
}
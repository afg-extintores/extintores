import { Base } from "./base";
import { Remision } from "./remision";
import { Producto } from "./producto";

/**
 * Clase que modela la entidad ProductoXRemision
 * 
 * @author Miguel Romero
 */
export class ProductoXRemision extends Base {

    constructor(
        public id?: number,
        public cantidad?: number,
        public precioUnidadVenta?: number,
        public valorVenta?: number,
        public remision?: Remision,
        public producto?: Producto
    ) { super(); }

}
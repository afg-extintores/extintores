/**
* Clase para mapear la paginación de los listados
*
* @author Miguel Romero
*/
export class Page{

    constructor(
        public content?: any[],
        public last?: boolean,
        public totalElements?: number,
        public totalPages?: number,
        public size?: number,
        public number?: number,
        public sort?: string,
        public first?: boolean,
        public numberOfElements?: number
    ){}

}
/**
 * Clase que modela el formulario de Login
 * 
 * @author Miguel Romero
 */
export class Login {
    constructor(
        public login?: string,
        public password?: string
    ) { }
}

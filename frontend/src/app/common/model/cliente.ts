import { Base } from "./base";
import { Cita } from "./cita";
import { ClienteDireccion } from "./cliente-direccion";
import { Usuario } from "./usuario";

/**
 * Clase que modela la entidad Cliente
 * 
 * @author Miguel Romero
 */
export class Cliente extends Base {

    constructor(
        public id?: number,
        public activo?: boolean,
        public correo?: string,
        public ourClient?: boolean,
        public nit?: string,
        public nombre?: string,
        public nombreContacto?: string,
        public telefono?: string,
        public usuario?: Usuario,
        public citas?: Cita[],
        public clientesDirecciones?: ClienteDireccion[]
    ){ super(); }
}

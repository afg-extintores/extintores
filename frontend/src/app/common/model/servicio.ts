import { Base } from "./base";

/**
 * Clase que modela la entidad Servicio
 * 
 * @author Miguel Romero
 */
export class Servicio extends Base {

    constructor(
        public id?: number,
        public codigo?: string,
        public descripcion?: string,
        public precioUnidad?: number
    ) { super(); }
}
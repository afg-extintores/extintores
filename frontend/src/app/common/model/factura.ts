import { Base } from "./base";
import { Cliente } from "./cliente";
import { ClienteDireccion } from "./cliente-direccion";
import { FacturaEstado } from "./factura-estado";
import { FacturaTipoPago } from "./factura-tipo-pago";
import { Inversion } from "./inversion";
import { InversionReal } from "./inversion-real";
import { ProductoXFactura } from "./producto-x-factura";
import { Usuario } from "./usuario";
import { Remision } from "./remision";

/**
 * Clase que modela la entidad Factura
 * 
 * @author Miguel Romero
 */
export class Factura extends Base {

    constructor(
        public id?: number,
        public consecutivo?: number,
        public diasCredito?: number,
        public fechaCarga?: Date,
        public fechaFactura?: Date,
        public ica?: number,
        public iva?: number,
        public ordenCompra?: string,
        public subtotal?: number,
        public total?: number,
        public remision?: Remision,
        public usuario?: Usuario,
        public cliente?: Cliente,
        public clientesDirecciones?: ClienteDireccion,
        public facturasEstado?: FacturaEstado,
        public facturasTiposPago?: FacturaTipoPago,
        public inversiones?: Inversion[],
        public inversionesReales?: InversionReal[],
        public productosXFacturas?: ProductoXFactura[]
    ) { super(); }
}

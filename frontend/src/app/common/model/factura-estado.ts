import { Base } from "./base";

/**
 * Clase que modela la entidad FacturaEstado
 * 
 * @author Miguel Romero
 */
export class FacturaEstado extends Base {

    constructor(
        public id?: number,
        public nombre?: string
    ) { super(); }
}

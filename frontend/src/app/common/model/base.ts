/**
 * Clase que modela los atributos generales de todas las clases.
 * 
 * @author Miguel Romero
 */
export class Base {

    /** Usuario que crea el registro */
    public createdBy?: string;
    /** Fecha de creación del registro */
    public createdDate?: Date;
    /** Usuario que realiza la ultima modificación al registro */
    public modifiedBy?: string;
    /** Fecha de la ultima modificación al registro */
    public modifiedDate?: Date;

    /**
     * Constructor de la Clase {@link Base}
     */
    constructor() { }


}

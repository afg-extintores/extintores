import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from "@angular/common/http";
import { SecurityService } from "../services/security/security.service";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(private securityService: SecurityService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(catchError(err => {
            if (!request.url.endsWith('login') && err.status === 401) {
                this.securityService.logout();
                location.reload(true);
            }

            //const error = err.error.message || err.statusText;
            //console.log(err);
            return throwError(err);
        }))
    }
}
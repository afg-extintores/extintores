import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from 'rxjs/operators';
import { SecurityService } from "../services/security/security.service";

/**
* Interceptor para anexar el token en el header de cada request que se haga al servidor.
* Valida las respuestas de las peticiones
*
* @author Miguel Romero
*/
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private securityService: SecurityService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!request.url.endsWith('/login')) {
            request = request.clone({
                headers: request.headers
                    .set('Authorization', 'Bearer ' + this.securityService.getItem('token'))
            });
        }
        return next.handle(request);
    }


}
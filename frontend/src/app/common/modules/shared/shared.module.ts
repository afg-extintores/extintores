import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesComponent } from '../../components/messages/messages.component';
import { MessagesService } from '../../components/messages/messages.service';
import { FormErrorComponent } from '../../components/form-error/form-error.component';

import { BlockUIModule } from 'ng-block-ui';
import { HrefDirective } from '../../directives/href/href.directive';
import { TableModule } from 'primeng/table';
import { PickListComponent } from '../../components/pick-list/pick-list.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { FilterByItemPickListPipe } from '../../components/pick-list/pick-list.pipe';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

/**
* Modulo de utilidades compartido en la aplicación
*
* @author Miguel Romero
*/
@NgModule({
    declarations: [
        MessagesComponent,
        FormErrorComponent,
        PickListComponent,
        FilterByItemPickListPipe,
        HrefDirective
    ],
    imports: [
        CommonModule,
        BlockUIModule,
        CurrencyMaskModule,
        TableModule,
        TooltipModule.forRoot()
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    providers: [MessagesService],
    exports: [
        MessagesComponent,
        FormErrorComponent,
        PickListComponent,
        HrefDirective
    ]
})
export class SharedModule { }

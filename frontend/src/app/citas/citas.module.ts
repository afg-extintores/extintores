import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { BlockUIModule } from 'ng-block-ui';
import { TableModule } from 'primeng/table';
import { SharedModule } from '../common/modules/shared/shared.module';
import { CitasRoutingModule } from './citas-routing.module';
import { CitasListComponent } from './citas-list/citas-list.component';
import { CitasFormComponent } from './citas-form/citas-form.component';
import { CitasService } from './citas.service';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ClientesService } from '../admin/clientes/clientes.service';
import { ClientesDireccionesService } from '../admin/clientes/clientes-direcciones/clientes-direcciones.service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { CalendarModule } from 'primeng/calendar';
import { TimepickerModule, TimepickerConfig, TimepickerActions } from 'ngx-bootstrap/timepicker';
import { CitasEstadosService } from './citas-estados/citas-estados.service';
import { UsuariosService } from '../admin/usuarios/usuarios.service';
import { UtilService } from '../common/services/util/util.service';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

/**
* Modulo de administración de Citas
*
* @author Miguel Romero
*/
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        CitasRoutingModule,
        BlockUIModule,
        TableModule,
        BsDatepickerModule,
        TypeaheadModule,
        CalendarModule,
        TimepickerModule,
        TooltipModule.forRoot()
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    declarations: [CitasListComponent, CitasFormComponent],
    providers: [
        CitasService, 
        ClientesService, 
        ClientesDireccionesService, 
        CitasEstadosService,
        UsuariosService,
        UtilService,
        TimepickerConfig, 
        TimepickerActions]
})
export class CitasModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppEndPoints } from 'src/app/app.endpoints';
import { Observable } from 'rxjs';
import { CitaEstado } from 'src/app/common/model/cita-estado';

@Injectable()
export class CitasEstadosService {

    /**
      * Constructor del Service de Citas
      * 
      * @param http 
      * @param endpoints 
      */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Retorna el listado de estados de las citas
     */
    public getAllCitasEstados(): Observable<CitaEstado[]> {
        return this.http.get<CitaEstado[]>(this.endpoints.citas_estados);
    }
}

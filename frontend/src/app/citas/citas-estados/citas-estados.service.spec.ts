import { TestBed } from '@angular/core/testing';

import { CitasEstadosService } from './citas-estados.service';

describe('CitasEstadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CitasEstadosService = TestBed.get(CitasEstadosService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Cita } from 'src/app/common/model/cita';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { Cliente } from 'src/app/common/model/cliente';
import { ClienteDireccion } from 'src/app/common/model/cliente-direccion';
import { ClientesService } from 'src/app/admin/clientes/clientes.service';
import { ClientesDireccionesService } from 'src/app/admin/clientes/clientes-direcciones/clientes-direcciones.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/public_api';
import { CitaEstado } from 'src/app/common/model/cita-estado';
import { CitasEstadosService } from '../citas-estados/citas-estados.service';
import { Usuario } from 'src/app/common/model/usuario';
import { UsuariosService } from 'src/app/admin/usuarios/usuarios.service';
import { CitasService } from '../citas.service';
import { MessagesService } from 'src/app/common/components/messages/messages.service';
import { UtilService } from 'src/app/common/services/util/util.service';
import { TimepickerConfig } from 'ngx-bootstrap/timepicker';
import { SecurityService } from 'src/app/common/services/security/security.service';
import { Router, ActivatedRoute } from '@angular/router';

/**
 * Configuración Timepicker
 */
export function getTimepickerConfig(): TimepickerConfig {
    return Object.assign(new TimepickerConfig(), {
        hourStep: 1,
        minuteStep: 5,
        showMeridian: true,
        readonlyInput: false,
        mousewheel: true,
        showMinutes: true,
        showSeconds: false
    });
}

/**
* Componente del formulario de las citas
*
* @author Miguel Romero
*/
@Component({
    selector: 'afg-citas-form',
    templateUrl: './citas-form.component.html',
    styleUrls: ['./citas-form.component.css'],
    providers: [{ provide: TimepickerConfig, useFactory: getTimepickerConfig }]
})
export class CitasFormComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;

    cita: Cita;
    idCita: number;
    citaForm: FormGroup;
    submitted: boolean;

    isNew: boolean;
    isEdit: boolean;

    cliente: Cliente;
    clientesDS: Cliente[];

    direcciones: ClienteDireccion[];
    usuarios: Usuario[];
    estados: CitaEstado[];

    minDate: Date = new Date();

    constructor(private fb: FormBuilder, private clientesService: ClientesService, private clientesDireccionesService: ClientesDireccionesService,
        private citasEstadosService: CitasEstadosService, private usuariosService: UsuariosService, private citasService: CitasService,
        private msgService: MessagesService, private utilService: UtilService, private securityService: SecurityService,
        private router: Router, private actRoute: ActivatedRoute) {
        this.initForm();
        this.loadClientes();
        this.loadEstados();
        this.loadUsuarios();
    }

    ngOnInit() {
        this.submitted = false;

        this.cita = new Cita();

        this.isNew = true;

        this.actRoute.params.subscribe(params => {
            if (params['id'] != undefined) {
                this.idCita = params['id'];
                this.citasService.getCitaById(this.idCita).subscribe(
                    async cita => {

                        if (params['edit'] != undefined) {
                            this.isEdit = (params['edit'] == 'true');
                        }

                        this.cita = cita;
                        this.cita.id = this.idCita;

                        this.cliente = this.cita.cliente;
                        await this.getDireccionesByCliente(this.cliente.id);

                        this.citaForm.get('cliente').setValue(this.cliente.nombre, { onlySelf: true });
                        this.citaForm.get('clientesDirecciones').setValue(this.cita.clientesDirecciones, { onlySelf: true });
                        this.citaForm.get('quienLlamo').setValue(this.cita.quienLlamo, { onlySelf: true });
                        this.citaForm.get('fechaCita').setValue(this.cita.fechaCita, { onlySelf: true });
                        this.citaForm.get('timeCita').setValue(this.cita.fechaCita, { onlySelf: true });
                        this.citaForm.get('estado').setValue(this.cita.estado, { onlySelf: true });
                        this.citaForm.get('observaciones').setValue(this.cita.observaciones, { onlySelf: true });

                        if (!this.isEdit) {
                            this.citaForm.disable();
                        } else {
                            this.citaForm.enable();
                        }

                        this.isNew = false;
                    },
                    error => {
                        console.log(error);
                    }
                );
            }
        });
    }

    private initForm() {
        this.citaForm = this.fb.group({
            cliente: new FormControl(null, [Validators.required]),
            clientesDirecciones: new FormControl(null, [Validators.required]),
            fechaLlamadaCita: new FormControl(null),
            fechaCita: new FormControl(null, [Validators.required]),
            timeCita: new FormControl(new Date()),
            quienLlamo: new FormControl(null, [Validators.required]),
            quienFue: new FormControl(null),
            observaciones: new FormControl(null),
            estado: new FormControl(null, [Validators.required])
        });
    }

    /**
     * Carga el listado de Clientes Activos
     */
    private loadClientes() {
        this.clientesDS = [];
        this.clientesService.getAllClientesActivos().subscribe(
            clientes => {
                this.clientesDS = clientes;
            }
        )
    }

    /**
     * Retorna el listado de direcciones por Cliente
     * @param idCliente Id del Cliente
     */
    private getDireccionesByCliente(idCliente: number) {
        this.direcciones = [];
        this.clientesDireccionesService.getDireccionesByCliente(idCliente).subscribe(
            direcciones => {
                this.direcciones = direcciones;
            },
            error => {
                console.log(error);
            }
        )
    }

    /**
     * Carga el listado de usuarios activos.
     */
    private loadUsuarios() {
        this.usuarios = [];
        this.usuariosService.getAllUsuariosActivos().subscribe(
            usuarios => {
                this.usuarios = usuarios;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Carga el listado de estados de las citas
     */
    private loadEstados() {
        this.estados = [];
        this.citasEstadosService.getAllCitasEstados().subscribe(
            estados => {
                this.estados = estados;
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * Selecciona un cliente
     * @param event 
     */
    onSelectCliente(event: TypeaheadMatch): void {
        this.cliente = event.item;
        this.getDireccionesByCliente(this.cliente.id);
    }

    /**
     * Compare del select de Direcciones
     * @param c1 
     * @param c2 
     */
    compareFnDirecciones(c1: ClienteDireccion, c2: ClienteDireccion): boolean {
        return c1 && c2 ? c1.id === c2.id : c1 === c2;
    }

    /**
     * Compare del select de Usuarios
     * @param u1 
     * @param u2 
     */
    compareFnQuienLlamo(u1: Usuario, u2: Usuario): boolean {
        return u1 && u2 ? u1.id === u2.id : u1 === u2;
    }

    /**
     * Compare del select de Estado
     * @param e1 
     * @param e2 
     */
    compareFnEstados(e1: CitaEstado, e2: CitaEstado): boolean {
        return e1 && e2 ? e1.id === e2.id : e1 === e2;
    }

    /**
     * Construye la Fecha de la Cita
     * @param fecha 
     */
    private getDateFormatCita(fecha: Date): Date {
        let day = fecha.getDate();
        let month = fecha.getMonth();
        let year = fecha.getFullYear();
        let hour = fecha.getHours();
        let minutes = fecha.getMinutes();
        let date = new Date(year, month, day);
        date.setHours(hour);
        date.setMinutes(minutes);
        return date;
    }

    /**
     * Formatea la fecha para editarla
     * @param fecha 
     * @param hora 
     */
    private formatDateToEdit(fecha: Date, hora: Date): Date {
        let day = fecha.getDate();
        let month = fecha.getMonth();
        let year = fecha.getFullYear();

        let hour = hora.getHours();
        let minutes = hora.getMinutes();

        let date = new Date(year, month, day);
        date.setHours(hour);
        date.setMinutes(minutes);
        return date;
    }

    /**
     * Registra o Actualiza una Cita
     */
    saveCita(value: any, valid: boolean) {
        this.submitted = true;
        if (valid) {
            if (this.isNew) {

                this.blockUI.start('Guardando Cita');

                let cita: Cita = new Cita();
                cita.cliente = this.cliente
                cita.clientesDirecciones = value.clientesDirecciones;
                cita.quienLlamo = value.quienLlamo;

                let fechaCita: Date = value.fechaCita;
                let horaCita: Date = value.timeCita;

                fechaCita.setHours(horaCita.getHours());
                fechaCita.setMinutes(horaCita.getMinutes());

                cita.fechaCita = this.getDateFormatCita(fechaCita);
                cita.estado = value.estado;
                cita.observaciones = value.observaciones;
                cita.createdBy = this.securityService.getItem('usuario');

                this.citasService.saveCita(cita).subscribe(
                    cita => {
                        this.isEdit = true;
                        this.isNew = false;
                        this.cita = cita;
                        this.msgService.addMessage("Cita agregada exitosamente.");
                        this.blockUI.stop();
                    },
                    error => {
                        console.log(error);
                        this.msgService.addError("Error al registrar la Cita.");
                        this.blockUI.stop();
                    }
                );

            } else {
                this.blockUI.start('Actualizando Cita');

                this.cita.cliente = this.cliente;
                this.cita.clientesDirecciones = value.clientesDirecciones;
                this.cita.quienLlamo = value.quienLlamo;

                let fechaCita: Date = new Date(value.fechaCita);
                let horaCita: Date = new Date(value.timeCita);

                fechaCita.setHours(horaCita.getHours());
                fechaCita.setMinutes(horaCita.getMinutes());

                this.cita.fechaCita = this.formatDateToEdit(this.getDateFormatCita(fechaCita), this.getDateFormatCita(horaCita));
                this.cita.fechaLlamadaCita = this.cita.fechaCita;
                this.cita.estado = value.estado;
                this.cita.observaciones = value.observaciones;
                this.cita.modifiedBy = this.securityService.getItem('usuario');

                this.citasService.updateCita(this.cita).subscribe(
                    cita => {
                        this.isEdit = true;
                        this.isNew = false;
                        this.cita = cita;
                        this.msgService.addMessage("Cita actualizada exitosamente.");
                        this.blockUI.stop();
                    },
                    error => {
                        console.log(error);
                        this.msgService.addError("Error al actualizar la Cita.");
                        this.blockUI.stop();
                    }
                );
            }
        }
    }

    /**
     * Retorna al listado de citas
     */
    cancelar() {
        this.router.navigate(['/citas']);
    }

    /**
     * Limpia el formulario de Citas
     */
    cleanForm() {
        this.submitted = false;
        this.cita = new Cita();
        this.initForm();
        this.loadClientes();
        this.loadEstados();
        this.loadUsuarios();
        this.isEdit = false;
        this.isNew = true;
        this.msgService.close();
    }

}

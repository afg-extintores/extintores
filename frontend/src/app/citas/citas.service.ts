import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppEndPoints } from '../app.endpoints';
import { Observable } from 'rxjs';
import { Cita } from '../common/model/cita';
import { Page } from '../common/model/page';
import { StringResponse } from '../common/model/string-response';

/**
 * Service para el manejo de data de las citas
 * 
 * @author Miguel Romero
 */
@Injectable()
export class CitasService {

    /**
    * Constructor del Service de Citas
    * 
    * @param http 
    * @param endpoints 
    */
    constructor(private http: HttpClient, private endpoints: AppEndPoints) { }

    /**
     * Obtiene el listado de Citas
     */
    public getAllCitas(): Observable<Cita[]> {
        return this.http.get<Cita[]>(this.endpoints.citas);
    }

    /**
     * Obtiene el listado de citas paginadas
     */
    public getAllCitasPaginadas(values: Map<string, string>): Observable<Page> {
        let params = new HttpParams()
            .set('page', values.get('page'))
            .set('size', values.get('size'))
            .set('sort', values.get('sort'))
            ;
        return this.http.get<Page>(this.endpoints.citas + '/pageables', { params: params });
    }

    /**
     * Registra un Cita
     * @param cita Cita a registrar
     */
    public saveCita(cita: Cita): Observable<Cita> {
        return this.http.post<Cita>(this.endpoints.citas, cita);
    }

    /**
     * Actualiza un Cita
     * @param cita Cita a actualizar
     */
    public updateCita(cita: Cita): Observable<Cita> {
        return this.http.put<Cita>(this.endpoints.citas, cita);
    }

    /**
     * Elimina un cita por Id
     * @param id Id de la Cita
     */
    public deleteCita(id: number): Observable<StringResponse> {
        return this.http.delete<StringResponse>(this.endpoints.citas + '/' + id);
    }

    /**
     * Obtiene una cita por Id
     * @param id Id de la cita
     */
    public getCitaById(id: number): Observable<Cita> {
        return this.http.get<Cita>(this.endpoints.citas + '/' + id);
    }

    /**
     * Retorna el listado de citas en un rango de fechas
     * 
     * @param startDate Fecha inicial
     * @param endDate Fecha final
     */
    public getCitasByDateRange(startDate: Date, endDate: Date): Observable<Cita[]> {
        let params = new HttpParams()
            .set('startDate', startDate + '')
            .set('endDate', endDate + '')
            ;
        return this.http.get<Cita[]>(this.endpoints.citas + '/by-date-range', { params: params })
    }
}

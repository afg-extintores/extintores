import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CitasListComponent } from './citas-list/citas-list.component';
import { CitasFormComponent } from './citas-form/citas-form.component';

const routes: Routes = [
    { path: '', component: CitasListComponent },
    { path: 'cita', component: CitasFormComponent },
    { path: 'cita/:id/:edit', component: CitasFormComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CitasRoutingModule { }
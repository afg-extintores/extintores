import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CitasService } from '../citas.service';
import { Cita } from 'src/app/common/model/cita';

/**
* Componente del listado de citas
*
* @author Miguel Romero
*/
@Component({
    selector: 'afg-citas-list',
    templateUrl: './citas-list.component.html',
    styleUrls: ['./citas-list.component.css']
})
export class CitasListComponent implements OnInit {

    citas: Cita[];
    idCita: number;

    cols: any[]; // Columnas de la tabla

    constructor(private router: Router, private citasService: CitasService) {
    }

    ngOnInit() {
        this.initCols();
        this.getAllCitas();
    }

    private initCols() {
        this.cols = [
            { field: 'cliente.nombre', header: 'Cliente', width: '15%' },
            { field: 'clientesDirecciones.direccion', header: 'Direcciones', width: '15%' },
            { field: 'fechaCita', header: 'Fecha Cita', width: '15%' },
            { field: 'quienLlamo.nombres', header: 'Quién Llamo', width: '15%' },
            { field: 'estado.nombre', header: 'Estado', width: '15%' }
        ];
    }

    /**
     * Retorna todas las citas registradas
     */
    private getAllCitas() {
        this.citasService.getAllCitas().subscribe(
            citas => {
                this.citas = citas;
            },
            error => {
                console.log(error);
            }
        );
    }

    goForm(item, edit) {
        this.router.navigate(['/citas/cita', item.id, edit]);
    }

}

#!/bin/bash

## Start the QA Environment
docker-compose -f docker-compose.qa.yml down
docker-compose -f docker-compose.qa.yml build
docker-compose -f docker-compose.qa.yml up -d
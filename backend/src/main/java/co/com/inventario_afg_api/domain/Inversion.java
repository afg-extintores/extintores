package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla INVERSIONES
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "inversiones")
public class Inversion extends BaseEntity implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la inversión */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false)
	private Integer id;

	/** Fecha de la Inversión */
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	/** Valor Total de la Inversión */
	@Column(precision = 10, scale = 2)
	private BigDecimal total;

	/** Id de la Factura */
	@ManyToOne
	@JoinColumn(name = "id_factura", nullable = false)
	private Factura factura;

	/** Asesor que registra la inversión */
	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable = false)
	private Usuario usuario;

	/** Listado de Relaciones Productos x Inversión */
	@OneToMany(mappedBy = "inversion", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<ProductoXInversion> productosXInversions;
 
	/**
	 * Agrega una relación producto x inversión al listado
	 * 
	 * @param productosXInversion
	 * @return
	 */
	public ProductoXInversion addProductosXInversion(ProductoXInversion productosXInversion) {
		getProductosXInversions().add(productosXInversion);
		productosXInversion.setInversion(this);
		return productosXInversion;
	}

	/**
	 * Elimina una relación producto x inversión al listado
	 * 
	 * @param productosXInversion
	 * @return
	 */
	public ProductoXInversion removeProductosXInversion(ProductoXInversion productosXInversion) {
		getProductosXInversions().remove(productosXInversion);
		productosXInversion.setInversion(null);
		return productosXInversion;
	}

}
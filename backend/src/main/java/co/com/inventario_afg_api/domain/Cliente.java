package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla CLIENTES
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "clientes")
public class Cliente extends BaseEntity implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id del cliente */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false)
	private Integer id;

	/** Estado del cliente */
	@Column(nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean activo;

	/** Correo del cliente */
	@Column(length = 150)
	private String correo;

	/** Indica si el cliente ya es un cliente */
	@Column(name = "our_client", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean ourClient;

	/** Nit del Cliente */
	@Column(nullable = false, length = 50)
	private String nit;

	/** Nombre del Cliente - Empresa */
	@Column(nullable = false, length = 150)
	private String nombre;

	/** Nombre del Contacto */
	@Column(name = "nombre_contacto", nullable = false, length = 150)
	private String nombreContacto;

	/** Telefono del Cliente */
	@Column(length = 75)
	private String telefono;

	/** Id del usuario propietario del cliente */
	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable = false)
	private Usuario usuario;

	/** Listado de Citas Asociadas al Cliente */
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Cita> citas;

	/** Listado de Direcciones asociadas al cliente */
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<ClienteDireccion> clientesDirecciones;

	/** Listado de Facturas asociadas al cliente */
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Factura> facturas;

}
package co.com.inventario_afg_api.domain.payloads;

public class JwtAuthenticationResponse {
	
	private String tokenType = "Bearer";
	private String accessToken;
	private int userId;
	private String names;
	private int rolId;
	private String rolName;

	public JwtAuthenticationResponse(String accessToken, int userId, String names, int rolId, String rolName) {
		this.accessToken = accessToken;
		this.userId = userId;
		this.names = names;
		this.rolId = rolId;
		this.rolName = rolName;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public int getRolId() {
		return rolId;
	}

	public void setRolId(int rolId) {
		this.rolId = rolId;
	}

	public String getRolName() {
		return rolName;
	}

	public void setRolName(String rolName) {
		this.rolName = rolName;
	}
}

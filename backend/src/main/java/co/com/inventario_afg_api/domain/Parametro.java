package co.com.inventario_afg_api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla PARAMETROS
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="parametros")
public class Parametro implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Nombre del parametro*/
	@Id
	@Column(updatable=false, unique=true, nullable=false, length=10)
	private String nombre;

	/** Valor del parametro */
	@Column(nullable=false, length=50)
	private String value;
	
}
package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla INVERSIONES_REALES
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="inversiones_reales")
public class InversionReal extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la inversión real */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Asesor que registra la inversión real */
	@Column(length=100)
	private String asesor;

	/** Fecha de la Inversión real */
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	/** Valor Total de la Inversión real */
	@Column(precision=10, scale=2)
	private BigDecimal total;

	/** Id de la Factura */
	@ManyToOne
	@JoinColumn(name="id_factura", nullable=false)
	private Factura factura;

	/** Listado de Relaciones Productos x Inversión Real */
	@OneToMany(mappedBy="inversionesReales")
	private List<ProductoXInversionReal> productosXInversionReals;

	/**
	 * Agrega una relación producto x inversión real al listado
	 * @param productosXInversion
	 * @return
	 */
	public ProductoXInversionReal addProductosXInversionReal(ProductoXInversionReal productosXInversionReal) {
		getProductosXInversionReals().add(productosXInversionReal);
		productosXInversionReal.setInversionesReales(this);
		return productosXInversionReal;
	}

	/**
	 * Elimina una relación producto x inversión real al listado
	 * @param productosXInversion
	 * @return
	 */
	public ProductoXInversionReal removeProductosXInversionReal(ProductoXInversionReal productosXInversionReal) {
		getProductosXInversionReals().remove(productosXInversionReal);
		productosXInversionReal.setInversionesReales(null);
		return productosXInversionReal;
	}
	
}
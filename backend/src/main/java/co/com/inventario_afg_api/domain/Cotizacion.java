package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla COTIZACIONES
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="cotizaciones")
public class Cotizacion extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la cotización */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Consecutivo de la cotización */
	@Column(nullable=false, length=10)
	private String consecutivo;
	
	/** Tiempo de entrega de la cotización */
	@Column(length=25)
	private String entrega;

	/** Fecha de la cotización */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date fecha;

	/** Forma de pago de la cotización */
	@Column(name="forma_pago", length=40)
	private String formaPago;

	/** Flag que marca si la cotización anexa documentos */
	@Column(name="incluir_documentos", nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean incluirDocumentos;

	/** IVA de la cotización */
	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal iva;

	/** Nota de la cotización */
	@Column(length=250)
	private String nota;

	/** Oferta de la cotización */
	@Column(length=25)
	private String oferta;

	/** Subtotal de la cotización */
	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal subtotal;

	/** Total de la cotización */
	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal total;
	
	/** Id del cliente al que se le envía la cotización */
	@ManyToOne
	@JoinColumn(name="id_cliente", nullable=false)
	private Cliente cliente;
	
	/** Id de la dirección que se asocia en la cotización */
	@ManyToOne
	@JoinColumn(name="id_cliente_direccion", nullable=false)
	private ClienteDireccion clienteDireccion;
	
	/** Id del usuario que genera la cotización */
	@ManyToOne
	@JoinColumn(name="id_usuario", nullable=false)
	private Usuario usuario;

}
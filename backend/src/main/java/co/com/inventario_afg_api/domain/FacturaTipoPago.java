package co.com.inventario_afg_api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla FACTURAS_TIPOS_PAGO
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="facturas_tipos_pago")
public class FacturaTipoPago extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id del Tipo de Pago de la Factura */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Nombre del Tipo de Pago de la factura */
	@Column(nullable=false, length=45)
	private String nombre;

}
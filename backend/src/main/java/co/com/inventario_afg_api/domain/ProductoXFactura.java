package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla PRODUCTOS_X_FACTURA
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="productos_x_factura")
public class ProductoXFactura extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la relación producto - factura */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Cantidad de productos */
	@Column(nullable=false)
	private Integer cantidad;

	/** Precio de venta del producto */
	@Column(name="precio_unidad_venta", nullable=false, precision=10, scale=2)
	private BigDecimal precioUnidadVenta;

	/** Valor de venta del producto */
	@Column(name="valor_venta", nullable=false, precision=10, scale=2)
	private BigDecimal valorVenta;

	/** Id de la Factura */
	@ManyToOne
	@JoinColumn(name="id_factura", nullable=false)
	@JsonBackReference
	private Factura factura;

	/** Id del Producto */
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private Producto producto;

}
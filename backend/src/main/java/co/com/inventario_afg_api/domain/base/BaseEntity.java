package co.com.inventario_afg_api.domain.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * <p>
 * Clase Base para las entidades que mapean las tablas de la aplicacion
 * 
 * @author Miguel Romero
 */
@Data
@MappedSuperclass
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BaseEntity implements Serializable{

	/** serialVersionUID */
	private static final long serialVersionUID = -5131224583080224416L;
	
	/** Usuario que crea el registro */
	@Column(name = "CREATEDBY")
	protected String createdBy;
	
	/** Fecha de creación del registro */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATEDDATE")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone="America/Bogota")
	protected Date createdDate;
	
	/** Usuario que realiza la ultima modificación al registro */
	@Column(name = "MODIFIEDBY")
	protected String modifiedBy;
	
	/** Fecha de la ultima modificación al registro */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIEDDATE")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone="America/Bogota")
	protected Date modifiedDate;
	
	/**
	 * Se ejecuta antes de persistir la entidad 
	 */
	@PrePersist
	public void onPrePersist(){
		setCreatedDate(new Date());
	}
	
	/**
	 * Se ejecuta antes de Actualizar la entidad 
	 */
	@PreUpdate
	public void onPreUpdate(){
		setModifiedDate(new Date());
	}

}

package co.com.inventario_afg_api.domain.dto.reportes;

import java.util.List;

public class FacturaInfo {

	private String fecha;
	private String nitEmpresa;
	private String nombreEmpresa;
	private String telefono;
	private String direccion;
	private String tipoPago;
	private String nombreContacto;
	private String diasCredito;
	private String ordenCompra;
	private List<FacturaTable> listFacturaTable;
	private String valorLetras;
	private String subtotal;
	private String iva;
	private String total;

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNitEmpresa() {
		return nitEmpresa;
	}

	public void setNitEmpresa(String nitEmpresa) {
		this.nitEmpresa = nitEmpresa;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getDiasCredito() {
		return diasCredito;
	}

	public void setDiasCredito(String diasCredito) {
		this.diasCredito = diasCredito;
	}

	public String getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public List<FacturaTable> getListFacturaTable() {
		return listFacturaTable;
	}

	public void setListFacturaTable(List<FacturaTable> listFacturaTable) {
		this.listFacturaTable = listFacturaTable;
	}

	public String getValorLetras() {
		return valorLetras;
	}

	public void setValorLetras(String valorLetras) {
		this.valorLetras = valorLetras;
	}

	public String getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

}

package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import co.com.inventario_afg_api.utils.api.dates.CustomDateTimeISODeserializer;
import co.com.inventario_afg_api.utils.api.dates.CustomDateTimeISOSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla CITAS
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "citas")
public class Cita extends BaseEntity implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la cita */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false)
	private Integer id;

	/** Fecha de la Cita */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_cita", nullable = false)
	@JsonSerialize(using = CustomDateTimeISOSerializer.class)
	@JsonDeserialize(using = CustomDateTimeISODeserializer.class)
	private Date fechaCita;

	/** Fecha de la Llamada para la Cita */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_llamada_cita")
	@JsonSerialize(using = CustomDateTimeISOSerializer.class)
	@JsonDeserialize(using = CustomDateTimeISODeserializer.class)
	private Date fechaLlamadaCita;

	/** Observaciones respecto a la llamada */
	@Lob
	@Column(name = "observaciones", columnDefinition = "LONGTEXT")
	private String observaciones;

	/** Id del usuario de la persona que llamo para sacar la cita */
	@ManyToOne
	@JoinColumn(name = "quien_llamo", nullable = false)
	private Usuario quienLlamo;

	/** Id del usuario de la persona que llamo para sacar la cita */
	@ManyToOne
	@JoinColumn(name = "quien_fue")
	private Usuario quienFue;

	/** Id del cliente */
	@ManyToOne
	@JoinColumn(name = "id_cliente", nullable = false)
	private Cliente cliente;
	
	/** Id de la Dirección del Cliente */
	@ManyToOne
	@JoinColumn(name = "id_cliente_direccion", nullable = false)
	private ClienteDireccion clientesDirecciones;

	/** Id del estado de la cita */
	@ManyToOne
	@JoinColumn(name = "id_estado", nullable = false)
	private CitaEstado estado;

}
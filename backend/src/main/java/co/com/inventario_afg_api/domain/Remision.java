package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla REMISIONES
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "remisiones")
public class Remision extends BaseEntity implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la Remisión */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	private Integer id;

	/** Consecutivo de la Factura */
	@Column(nullable = false)
	private int consecutivo;

	/** Fecha de la remisión */
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_remision")
	private Date fechaRemision;

	/** Valor del iva de la remisión */
	private BigDecimal iva;

	/** Observaciones en la remisión */
	private String observaciones;

	/** Valor subtotal de la remisión */
	private BigDecimal subtotal;

	/** Valor total de la remisión */
	private BigDecimal total;

	/** Url de la imagen de la remisión */
	private String url;

	/** Nombre del archivo cargado en S3 */
	@Column(name = "key_s3")
	private String keyS3;

	/** Flag que indica si ya se generó una factura para la remisión */
	@Column(nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean factura;

	/** Id del cliente asociado a la remisión */
	@ManyToOne
	@JoinColumn(name = "id_cliente", nullable = false)
	private Cliente cliente;

	/** Id de la Dirección del Cliente */
	@ManyToOne
	@JoinColumn(name = "id_cliente_direccion", nullable = false)
	private ClienteDireccion clientesDirecciones;

	/** Id del usuario dueño de la remisión */
	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable = false)
	private Usuario usuario;

	// bi-directional many-to-one association to ProductosXRemision
	@OneToMany(mappedBy = "remision", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<ProductoXRemision> productosXRemisions;

}
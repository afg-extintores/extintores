package co.com.inventario_afg_api.domain.dto.reportes;

/**
 * POJO para la impresi{on de la factura
 * 
 * @author Miguel Romero
 */
public class FacturaTable {

	private String codigo;
	private String producto;
	private String descripcion;
	private Long precioUnidad;
	private String cantidad;
	private Long valorVenta;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(Long precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public Long getValorVenta() {
		return valorVenta;
	}

	public void setValorVenta(Long valorVenta) {
		this.valorVenta = valorVenta;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "FacturaTable [codigo=" + codigo + ", producto=" + producto + ", descripcion=" + descripcion
				+ ", precioUnidad=" + precioUnidad + ", cantidad=" + cantidad + ", valorVenta=" + valorVenta + "]";
	}

}

package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * <p>
 * Entity que mapea la tabla PRODUCTOS_X_REMISION
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "productos_x_remision")
public class ProductoXRemision extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;
	
	/** Id de la relación productos x remisión */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false)
	private Integer id;

	/** Cantidad de productos x remisión */
	private int cantidad;
	
	/** Precio de venta del producto */
	@Column(name="precio_unidad_venta")
	private BigDecimal precioUnidadVenta;

	/** Valor de venta del producto */
	@Column(name="valor_venta")
	private BigDecimal valorVenta;

	/** Id de la remisión */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_remision", nullable=false)
	@JsonBackReference
	private Remision remision;
	
	/** Id del Producto */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_producto", nullable=false)
	private Producto producto;

}
package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla FACTURAS
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "facturas")
public class Factura extends BaseEntity implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la Factura */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false)
	private Integer id;

	/** Consecutivo de la Factura */
	@Column(nullable = false)
	private Integer consecutivo;

	/** Días de Credito para el pago de la factura */
	@Column(name = "dias_credito")
	private Integer diasCredito;

	/** Fecha de Carga */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_carga", nullable = false)
	private Date fechaCarga;

	/** Fecha de la Factura */
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_factura", nullable = false)
	private Date fechaFactura;
	
	/** Url de la imagen de la factura */
	private String url;

	/** Nombre del archivo cargado en S3 */
	@Column(name = "key_s3")
	private String keyS3;

	/** Valor del ica de la factura */
	@Column(precision = 10, scale = 2)
	private BigDecimal ica;

	/** Valor del iva de la factura */
	@Column(precision = 10, scale = 2)
	private BigDecimal iva;

	/** Orden de Compra de la Factura */
	@Column(name = "orden_compra", length = 30)
	private String ordenCompra;

	/** Valor del subtotal de la factura */
	@Column(precision = 10, scale = 2)
	private BigDecimal subtotal;

	/** Valor total de la factura */
	@Column(precision = 10, scale = 2)
	private BigDecimal total;
	
	/**
	 * Id de la remisión para generar la factura
	 */
	@OneToOne
	@JoinColumn(name = "id_remision", nullable = true)
	private Remision remision;

	/** Id del cliente asociado a la Factura */
	@ManyToOne
	@JoinColumn(name = "id_cliente", nullable = false)
	private Cliente cliente;

	/** Id de la Dirección del Cliente */
	@ManyToOne
	@JoinColumn(name = "id_cliente_direccion", nullable = false)
	private ClienteDireccion clientesDirecciones;

	/** Id del estado de la Factura */
	@ManyToOne
	@JoinColumn(name = "id_estado", nullable = false)
	private FacturaEstado facturasEstado;

	/** Id del Tipo de pago de la factura */
	@ManyToOne
	@JoinColumn(name = "id_tipo_pago", nullable = false)
	private FacturaTipoPago facturasTiposPago;
	
	/** Id del usuario vendedor de la factura */
	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable = false)
	private Usuario usuario;

	/** Listado de Inversiones asociadas a la Factura */
	@OneToMany(mappedBy = "factura", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Inversion> inversiones;

	/** Listado de Inversiones Reales asociadas a la Factura */
	@OneToMany(mappedBy = "factura", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<InversionReal> inversionesReales;

	/** Listado de Relaciones asociadas por producto a la Factura */
	@OneToMany(mappedBy = "factura", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<ProductoXFactura> productosXFacturas;

}
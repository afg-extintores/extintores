package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla CIUDADES
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="ciudades")
public class Ciudad implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la ciudad */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Nombre de la ciudad */
	@Column(nullable=false, length=45)
	private String nombre;

}
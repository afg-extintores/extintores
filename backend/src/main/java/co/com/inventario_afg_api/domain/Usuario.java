package co.com.inventario_afg_api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla USUARIOS
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="usuarios")
public class Usuario extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id del Usuario */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Indica si el usuario tiene acceso a la plataforma */
	@Column(nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean acceso;
	
	/** Estado del usuario */
	@Column(nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean activo;

	/** Apellidos del Usuario */
	@Column(nullable=false, length=45)
	private String apellidos;

	/** Correo del usuario */
	@Column(nullable=false, length=100)
	private String correo;

	/** Documento del usuario */
	@Column(nullable=false, length=100)
	private String documento;

	/** Nombres del Usuario */
	@Column(nullable=false, length=45)
	private String nombres;

	/** Password de acceso del usuario */
	@Column(nullable=false, length=100)
	private String password;
	
	/** Indica si el usuario es vendedor de facturas */
	@Column(nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean vendedor;

	/** Id del rol asociado al usuario */
	@ManyToOne
	@JoinColumn(name="id_rol", nullable=false)
	private Rol rol;

}
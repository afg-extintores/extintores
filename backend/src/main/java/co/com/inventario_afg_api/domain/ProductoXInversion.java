package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * <p>
 * Entity que mapea la tabla PRODUCTOS_X_INVERSION
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="productos_x_inversion")
public class ProductoXInversion extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la relación producto - inversión */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Cantidad de productos */
	@Column(nullable=false)
	private Integer cantidad;

	/** Precio unitario de venta del producto */
	@Column(name="precio_unidad_venta", nullable=false, precision=10, scale=2)
	private BigDecimal precioUnidadVenta;

	/** Valor de venta del producto */
	@Column(name="valor_venta", nullable=false, precision=10, scale=2)
	private BigDecimal valorVenta;

	/** Id de la inversión */
	@ManyToOne
	@JoinColumn(name="id_inversion", nullable=false)
	@JsonBackReference
	private Inversion inversion;

	/** Id del producto */
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private Producto producto;

}
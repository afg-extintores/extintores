package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla PRODUCTOS
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="productos")
public class Producto extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id del producto */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Estado del producto */
	@Column(nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean activo;

	/** Codigo del producto */
	@Column(nullable=false, length=20)
	private String codigo;
	
	/** Descripcion del producto */
	@Lob
	@Column(name = "descripcion", columnDefinition = "LONGTEXT")
	private String descripcion;

	/** Nombre del producto */
	@Column(nullable=false, length=150)
	private String nombre;

	/** Precio unitario del producto */
	@Column(name="precio_unidad", nullable=false, precision=10, scale=2)
	private BigDecimal precioUnidad;

}
package co.com.inventario_afg_api.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import co.com.inventario_afg_api.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Entity que mapea la tabla CLIENTES_DIRECCIONES
 * 
 * @author Miguel Romero
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name="clientes_direcciones")
public class ClienteDireccion extends BaseEntity implements Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** Id de la Dirección del cliente */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(updatable=false, unique=true, nullable=false)
	private Integer id;

	/** Direccion del Cliente */
	@Column(nullable=false, length=100)
	private String direccion;

	/** Id del cliente */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_cliente", nullable=false)
	@JsonBackReference
	private Cliente cliente;

	/** Listado de Facturas donde la dirección ha sido asociada */
	@OneToMany(mappedBy="clientesDirecciones", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Factura> facturas;

}
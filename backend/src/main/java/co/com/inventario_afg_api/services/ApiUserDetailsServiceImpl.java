package co.com.inventario_afg_api.services;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.config.security.JWTUser;
import co.com.inventario_afg_api.domain.Usuario;
import co.com.inventario_afg_api.repositories.UsuarioRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Servicio que obtiene el usuario para configurarlo en el contexto de seguridad
 * de la API
 * 
 * @author Miguel Romero
 */
@Service
public class ApiUserDetailsServiceImpl implements UserDetailsService {

	/**
	 * Atributo log : Logger - Permite registrar los eventos de lo que sucede en el
	 * servicio
	 */
	@Loggable
	private Logger log;

	/**
	 * Atributo seUsuarioService : SeUsuarioService - Inyectar acceso a los
	 * servicios de la entidad SeUsuario
	 */
	@Autowired
	private UsuarioRepository usuarioRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		log.info("loadUserByUsername");
		Usuario usuario = null;
		try {						
			if (login.contains("@")) {
				usuario = this.usuarioRepository.findByCorreo(login);
			} else {
				usuario = this.usuarioRepository.findByDocumento(login);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		if (usuario == null) {
			throw new UsernameNotFoundException(String.format("No user found with login '%s'.", login));
		} else if(usuario.isAcceso()) {
			return JWTUser.create(usuario);
		} else {
			throw new UsernameNotFoundException(String.format("No user found with login '%s'.", login));
		}
	}
	
	@Transactional
    public UserDetails loadUserById(Integer id) {
		Usuario usuario = this.usuarioRepository.findById(id).orElseThrow(
            () -> new UsernameNotFoundException("User not found with id : " + id)
        );

		return JWTUser.create(usuario);
    }

}

package co.com.inventario_afg_api.services.usuario;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import co.com.inventario_afg_api.domain.Usuario;

public interface IUsuarioService {
	
	List<Usuario> listAllUsuarios() throws Exception;
	
	Page<Usuario> listAll(Map<String, String> params) throws Exception;
	
	Usuario saveUser(Usuario user) throws Exception;
	
	Usuario updateUser(Usuario user) throws Exception;
	
	void deleteUser(Integer id) throws Exception;
	
	Usuario findUserById(Integer id) throws Exception;
	
	List<Usuario> listUsuariosVendedoresActivos() throws Exception;
	
	List<Usuario> listUsuariosActivos() throws Exception;
	
	boolean changePassword(Usuario user) throws Exception;
	
	Usuario getUsuarioByCorreo(String correo) throws Exception;
	
	Usuario getUsuarioByDocumento(String documento) throws Exception;
	
	List<Usuario> getListUsuariosByNombre(String nombre) throws Exception;

}

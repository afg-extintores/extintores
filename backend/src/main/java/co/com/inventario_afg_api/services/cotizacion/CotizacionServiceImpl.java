package co.com.inventario_afg_api.services.cotizacion;

import java.io.File;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Cotizacion;
import co.com.inventario_afg_api.repositories.CotizacionRepository;
import co.com.inventario_afg_api.services.ReportsService;
import co.com.inventario_afg_api.services.aws.AwsS3Client;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "cotizacionService")
public class CotizacionServiceImpl implements ICotizacionService {
	
	/** 
	 * Atributo reporte : String - Nombre del reporte para generar el PDF de la Cotización
	 */
	private String reporte = "formatos/afg_cotizacion.jrxml";
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;
	
	/** Repositorio de acceso a la data de {@link Cotizacion} */
	@Autowired
	private CotizacionRepository cotizacionRepository;
	
	/** Servicio para la generación de Reportes */
	@Autowired 
	private ReportsService reportsService;
	
	/** 
	 * Atributo awsS3 : AwsS3Client - Client S3 to upload files 
	 */
	@Autowired
	private AwsS3Client awsS3;

	@Override
	@Transactional(readOnly = true)
	public Page<Cotizacion> listCotizaciones(Pageable pageable) throws Exception {
		log.info("listCotizaciones");
		return this.cotizacionRepository.findAll(pageable);
	}

	@Override
	@Transactional
	public Cotizacion saveCotizacion(Cotizacion cotizacion) throws Exception {
		log.info("saveCotizacion");
		return this.cotizacionRepository.save(cotizacion);
	}

	@Override
	@Transactional
	public Cotizacion updateCotizacion(Cotizacion cotizacion) throws Exception {
		log.info("updateCotizacion");
		return this.cotizacionRepository.save(cotizacion);
	}

	@Override
	@Transactional
	public StringResponse deleteCotizacionById(Integer id) throws Exception {
		log.info("deleteCotizacionById");
		try {
			this.cotizacionRepository.deleteById(id);
		} catch (Exception e) {
			return null;
		}
		return new StringResponse("Cotización eliminada exitosamente");
	}

	@Override
	@Transactional(readOnly = true)
	public Cotizacion getCotizacionById(Integer id) throws Exception {
		log.info("getCotizacionById");
		return this.cotizacionRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public String generateCotizacion(Integer idCotizacion) throws Exception {
		log.info("generateCotizacion");
		File report = null;
		String url = "";
		try {
			// report = this.reportsService.generatePDF(fileName, this.reporte, new
			// HashMap<String, Object>());
			// url = this.awsS3.uploadFile(report, "quotes", fileName);
		} catch (Exception e) {
			log.info(e.getMessage());
			return null;
		}
		return null;
	}

}

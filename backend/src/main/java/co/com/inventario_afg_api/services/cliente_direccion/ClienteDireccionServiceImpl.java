package co.com.inventario_afg_api.services.cliente_direccion;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.ClienteDireccion;
import co.com.inventario_afg_api.repositories.ClienteDireccionRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "clienteDireccionService")
public class ClienteDireccionServiceImpl implements IClienteDireccionService {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;
	
	@Autowired
	private ClienteDireccionRepository clienteDireccionRepository;

	@Override
	@Transactional(readOnly = true)
	public List<ClienteDireccion> listClientesDirecciones() throws Exception {
		log.info("listClientesDirecciones");
		return this.clienteDireccionRepository.findAll();
	}

	@Override
	@Transactional
	public ClienteDireccion saveClienteDireccion(ClienteDireccion clienteDireccion) throws Exception {
		log.info("saveClienteDireccion");
		return this.clienteDireccionRepository.save(clienteDireccion);
	}

	@Override
	@Transactional
	public ClienteDireccion updateClienteDireccion(ClienteDireccion clienteDireccion) throws Exception {
		log.info("updateClienteDireccion");
		return this.clienteDireccionRepository.save(clienteDireccion);
	}

	@Override
	@Transactional
	public void deleteClienteDireccionById(Integer id) throws Exception {
		log.info("deleteClienteDireccionById");
		this.clienteDireccionRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public ClienteDireccion getClienteDireccionById(Integer id) throws Exception {
		log.info("getClienteDireccionById");
		return this.clienteDireccionRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<ClienteDireccion> getListClienteDireccionByIdCliente(Integer idCliente) throws Exception {
		log.info("getListClienteDireccionByIdCliente");
		return this.clienteDireccionRepository.getDireccionesByCliente(idCliente);
	}

}

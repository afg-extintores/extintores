package co.com.inventario_afg_api.services.ciudad;

import java.util.List;

import co.com.inventario_afg_api.domain.Ciudad;

public interface ICiudadService {
	
	List<Ciudad> listAll() throws Exception;
	
	Ciudad getCiudadById(Integer id) throws Exception;

}

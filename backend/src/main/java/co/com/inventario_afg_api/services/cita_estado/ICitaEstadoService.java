package co.com.inventario_afg_api.services.cita_estado;

import java.util.List;

import co.com.inventario_afg_api.domain.CitaEstado;

public interface ICitaEstadoService {
	
	List<CitaEstado> listAll() throws Exception;

}

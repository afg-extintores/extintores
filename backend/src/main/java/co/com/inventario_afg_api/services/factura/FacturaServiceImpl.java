package co.com.inventario_afg_api.services.factura;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Factura;
import co.com.inventario_afg_api.domain.dto.reportes.FacturaInfo;
import co.com.inventario_afg_api.domain.dto.reportes.FacturaTable;
import co.com.inventario_afg_api.repositories.FacturaRepository;
import co.com.inventario_afg_api.services.aws.AwsS3Client;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "facturaService")
public class FacturaServiceImpl implements IFacturaService {

	private final String pathFacturas = "facturas";

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private FacturaRepository facturaRepository;

	@Autowired
	private AwsS3Client awsS3;
	
	@Autowired
	private ApplicationContext appContext;

	@Override
	@Transactional(readOnly = true)
	public List<Factura> listAllFacturas() throws Exception {
		log.info("listAllFacturas");
		List<Factura> list = new ArrayList<>();
		this.facturaRepository.findAll().forEach(fac -> {
			list.add(fac);
		});
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Factura> listFacturas(Pageable pageable) throws Exception {
		log.info("listFacturas");
		return this.facturaRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> getInvoicesWithoutInvestment() throws Exception {
		log.info("getInvoicesWithoutInvestment");
		log.info(this.facturaRepository.getInvoicesWithoutInvestment().size() + "");
		return this.facturaRepository.getInvoicesWithoutInvestment();
	}

	@Override
	@Transactional
	public Factura saveFactura(Factura factura) throws Exception {
		log.info("saveFactura");
		return this.facturaRepository.save(factura);
	}

	@Override
	@Transactional
	public Factura updateFactura(Factura factura) throws Exception {
		log.info("updateFactura");
		return this.facturaRepository.save(factura);
	}

	@Override
	@Transactional
	public void deleteFactura(Integer id) throws Exception {
		log.info("deleteFactura");
		this.facturaRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Factura getFacturaById(Integer id) throws Exception {
		log.info("getFacturaById");
		return this.facturaRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public Integer getMaxConsecutivoFactura() throws Exception {
		log.info("getMaxConsecutivoFactura");
		return this.facturaRepository.getMaxConsecutivo();
	}

	@Override
	@Transactional
	public byte[] generateFactura(FacturaInfo factura, HttpServletResponse response) throws Exception {
		log.info("generateFactura");

		try {

			String fileName = "formatos/FORMATO_DE_FACTURACION.xlsx";
			
			InputStream excelFile = appContext.getResource("classpath:/"+fileName).getInputStream();

			byte[] fileResponse = null;

			Workbook libro = new XSSFWorkbook(excelFile);
			Sheet hoja = libro.getSheetAt(libro.getFirstVisibleTab());

			hoja.getRow(0).getCell(2).setCellValue(factura.getFecha());
			hoja.getRow(0).getCell(3).setCellValue(factura.getNitEmpresa());
			hoja.getRow(1).getCell(2).setCellValue(factura.getNombreEmpresa());
			hoja.getRow(1).getCell(3).setCellValue(factura.getTelefono());
			hoja.getRow(2).getCell(2).setCellValue(factura.getDireccion());
			hoja.getRow(2).getCell(3).setCellValue(factura.getOrdenCompra());
			hoja.getRow(3).getCell(2).setCellValue(factura.getTipoPago().toUpperCase());
			hoja.getRow(3).getCell(3)
					.setCellValue(factura.getDiasCredito().equals("0") ? "" : factura.getDiasCredito());
			hoja.getRow(4).getCell(2).setCellValue("      ATT: " + factura.getNombreContacto());

			int rowCount = 8;
			int maxRow = 36;
			int limit = 45;

			for (FacturaTable item : factura.getListFacturaTable()) {
				if (rowCount < maxRow) {					
					Long totalItem = item.getPrecioUnidad() * Long.parseLong(item.getCantidad());

					if (hoja.getRow(rowCount).getCell(0) != null) {
						hoja.getRow(rowCount).getCell(0).setCellValue(item.getCantidad());
					} else {
						hoja.getRow(rowCount).createCell(0).setCellValue(item.getCantidad());
					}

					if (hoja.getRow(rowCount).getCell(3) != null) {
						hoja.getRow(rowCount).getCell(3).setCellValue(item.getPrecioUnidad());
					} else {
						hoja.getRow(rowCount).createCell(3).setCellValue(item.getPrecioUnidad());
					}

					if (hoja.getRow(rowCount).getCell(4) != null) {
						hoja.getRow(rowCount).getCell(4).setCellValue(totalItem);
					} else {
						hoja.getRow(rowCount).createCell(4).setCellValue(totalItem);
					}

					List<String> descriptionItems = splitByWord(item.getDescripcion(), limit);
					for (String description : descriptionItems) {
						hoja.getRow(rowCount).createCell(2).setCellValue(description);
						rowCount++;
					}
				}
				rowCount++;
			}

			hoja.getRow(37).getCell(2).setCellValue(factura.getValorLetras() + "PESOS MCTE");

			hoja.getRow(36).getCell(4).setCellValue(factura.getSubtotal());
			hoja.getRow(37).getCell(4).setCellValue(factura.getIva());
			hoja.getRow(38).getCell(4).setCellValue(factura.getTotal());

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				libro.write(bos);
			} finally {
				bos.close();
			}

			fileResponse = bos.toByteArray();

			libro.close();
			excelFile.close();

			return fileResponse;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public boolean changeStatusInvoice(Integer status, Integer id) throws Exception {
		log.info("changeStatusInvoice");
		if (this.facturaRepository.changeStatusInvoice(status, id) > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	@Transactional
	public String uploadFactura(File file, Integer facturaId) throws Exception {
		log.info("uploadFactura");

		Factura f = this.facturaRepository.findById(facturaId).get();

		if (f.getKeyS3() != null) {
			this.awsS3.deleteFileFromBucket(f.getKeyS3());
		}

		String name = facturaId.toString() + "_" + f.getFechaFactura() + "_" + file.getName();
		String url = this.awsS3.uploadFile(file, this.pathFacturas, name);

		f.setUrl(url);
		f.setKeyS3(this.pathFacturas + "/" + name);

		this.facturaRepository.save(f);

		return url;
	}

	private List<String> splitByWord(String text, int limit) {
		String[] words = text.split(" ");
		List<String> items = new ArrayList<>();
		StringBuilder phrase = new StringBuilder();
		for (String word : words) {
			if (phrase.length() > limit) {
				items.add(phrase.toString().toUpperCase());
				phrase = new StringBuilder();
				phrase.append(word);
				phrase.append(" ");
			} else {
				phrase.append(word);
				phrase.append(" ");
			}

		}
		items.add(phrase.toString().toUpperCase());
		return items;
	}

	@Override
	@Transactional(readOnly = true)
	public Factura getFacturaByConsecutive(Integer consecutive) throws Exception {
		log.info("getFacturaByConsecutive");
		return this.facturaRepository.findByConsecutivo(consecutive);
	}

}

package co.com.inventario_afg_api.services.cotizacion;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.inventario_afg_api.domain.Cotizacion;
import co.com.inventario_afg_api.utils.api.StringResponse;

public interface ICotizacionService {
	
	Page<Cotizacion> listCotizaciones(Pageable pageable) throws Exception;
	
	Cotizacion saveCotizacion(Cotizacion cotizacion) throws Exception;
	
	Cotizacion updateCotizacion(Cotizacion cotizacion) throws Exception;
	
	StringResponse deleteCotizacionById(Integer id) throws Exception;
	
	Cotizacion getCotizacionById(Integer id) throws Exception;
	
	String generateCotizacion(Integer idCotizacion) throws Exception;

}

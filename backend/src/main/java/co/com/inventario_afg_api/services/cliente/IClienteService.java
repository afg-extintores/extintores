package co.com.inventario_afg_api.services.cliente;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.inventario_afg_api.domain.Cliente;

public interface IClienteService {
	
	List<Cliente> listAll() throws Exception;
	
	Page<Cliente> listAllPaginados(Pageable pageable) throws Exception;
	
	Cliente saveCliente(Cliente cliente) throws Exception;
	
	Cliente updateCliente(Cliente cliente) throws Exception;
	
	void deleteCliente(Integer id) throws Exception;
	
	Cliente getClienteById(Integer id) throws Exception;
	
	Cliente getClienteByNombre(String nombre) throws Exception;
	
	Cliente getClienteByNit(String nit) throws Exception;
	
	Cliente getClienteByCorreo(String correo) throws Exception;
	
	List<Cliente> getListClientesByNombre(String nombre) throws Exception;
	
	List<Cliente> getListClientesByNombreActivos(String nombre) throws Exception;
	
	List<Cliente> getListClientesActivos() throws Exception;

	List<Cliente> findAllClientsNameOrdered() throws Exception;

}

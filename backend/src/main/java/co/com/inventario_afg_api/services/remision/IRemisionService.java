package co.com.inventario_afg_api.services.remision;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import co.com.inventario_afg_api.domain.Remision;

public interface IRemisionService {

	List<Remision> listAllRemisiones() throws Exception;
	
	Page<Remision> listAll(Map<String, String> params) throws Exception;

	Remision saveRemision(Remision remision) throws Exception;

	Remision updateRemision(Remision remision) throws Exception;

	void deleteRemision(Integer id) throws Exception;

	Remision findRemisionById(Integer id) throws Exception;

	Integer getMaxConsecutiveRemision() throws Exception;
	
	String uploadRemision(File file, Integer remisionId) throws Exception;
	
	List<Remision> getRemisionesNoFactura() throws Exception;

}

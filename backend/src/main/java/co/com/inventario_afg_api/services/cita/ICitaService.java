package co.com.inventario_afg_api.services.cita;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.inventario_afg_api.domain.Cita;

public interface ICitaService {

	List<Cita> listAll() throws Exception;

	Page<Cita> listAllPaginados(Pageable pageable) throws Exception;

	Cita saveCita(Cita cliente) throws Exception;

	Cita updateCita(Cita cliente) throws Exception;

	void deleteCita(Integer id) throws Exception;

	Cita getCitaById(Integer id) throws Exception;
	
	List<Cita> getCitasByRangeDates(Date startDate, Date endDate) throws Exception;

}

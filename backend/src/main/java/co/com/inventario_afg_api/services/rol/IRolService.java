package co.com.inventario_afg_api.services.rol;

import java.util.List;

import co.com.inventario_afg_api.domain.Rol;

public interface IRolService {
	
	List<Rol> listRols() throws Exception;
	
	Rol saveRol(Rol rol) throws Exception;
	
	Rol updateRol(Rol rol) throws Exception;
	
	void deleteRolById(Integer id) throws Exception;
	
	Rol getRolById(Integer id) throws Exception;

}

package co.com.inventario_afg_api.services.producto_x_remision;

import java.util.List;

import co.com.inventario_afg_api.domain.ProductoXRemision;

public interface IProductoXRemisionService {
	
	List<ProductoXRemision> listAll() throws Exception;
	
	ProductoXRemision saveProductoXRemision(ProductoXRemision productoXRemision) throws Exception;
	
	ProductoXRemision updateProductosXRemision(ProductoXRemision productoXRemision) throws Exception;
	
	void deleteProductosXRemision(Integer id) throws Exception;
	
	ProductoXRemision findProductosXRemisionById(Integer id) throws Exception;

}

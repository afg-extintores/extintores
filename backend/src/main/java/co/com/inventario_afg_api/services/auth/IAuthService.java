package co.com.inventario_afg_api.services.auth;

import co.com.inventario_afg_api.domain.payloads.JwtAuthenticationResponse;
import co.com.inventario_afg_api.domain.payloads.LoginRequest;

public interface IAuthService {
	
	JwtAuthenticationResponse login(LoginRequest request) throws Exception;

}

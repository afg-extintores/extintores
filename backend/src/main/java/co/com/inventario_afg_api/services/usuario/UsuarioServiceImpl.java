package co.com.inventario_afg_api.services.usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Usuario;
import co.com.inventario_afg_api.repositories.UsuarioRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "usuarioService")
public class UsuarioServiceImpl implements IUsuarioService {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PasswordEncoder pe;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> listAllUsuarios() throws Exception {
		log.info("listAllUsuarios");
		List<Usuario> list = new ArrayList<Usuario>();
		this.usuarioRepository.findAll().forEach(user -> {
			list.add(user);
		});
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Usuario> listAll(Map<String, String> params) throws Exception {
		log.info("listAll");
		Order order = null;
		String[] paramSort = params.get("sort").split(",");
		if(paramSort[1].equals("ASC")){
			order = new Order(Direction.ASC, paramSort[0]);
		}else{
			order = new Order(Direction.DESC, paramSort[0]);
		}
		Pageable pageable = new PageRequest(Integer.parseInt(params.get("page")), Integer.parseInt(params.get("size")),
				new Sort(order));
		return this.usuarioRepository.findAll(pageable);
	}

	@Override
	@Transactional
	public Usuario saveUser(Usuario user) throws Exception {
		log.info("saveUser");
		Usuario usuario = null;
		try {
			user.setPassword(pe.encode("12345"));
			usuario = this.usuarioRepository.save(user);
		} catch(Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return usuario;
	}

	@Override
	@Transactional
	public Usuario updateUser(Usuario user) throws Exception {
		log.info("updateUser");
		Usuario usuario = null;
		try {
			usuario = this.usuarioRepository.save(user);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return usuario;
	}

	@Override
	@Transactional
	public void deleteUser(Integer id) throws Exception {
		log.info("deleteUser");
		try {
			this.usuarioRepository.deleteById(id);
		} catch(Exception e) {
			log.error(e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findUserById(Integer id) throws Exception {
		log.info("findUserById");
		return this.usuarioRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> listUsuariosVendedoresActivos() throws Exception {
		log.info("listUsuariosVendedoresActivos");
		return this.usuarioRepository.findUsuariosVendedoresActivos();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> listUsuariosActivos() throws Exception {
		log.info("listUsuariosActivos");
		return this.usuarioRepository.findUsuariosActivos();
	}

	@Override
	@Transactional
	public boolean changePassword(Usuario user) throws Exception {
		log.info("changePassword");
		boolean response = false;
		Usuario aux = null;
		try {
			aux = this.usuarioRepository.findById(user.getId()).get();
			aux.setPassword(pe.encode(user.getPassword()));
			if (this.usuarioRepository.save(aux) != null) {
				response = true;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return response;
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario getUsuarioByCorreo(String correo) throws Exception {
		log.info("getUsuarioByCorreo");
		return this.usuarioRepository.findByCorreo(correo);
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario getUsuarioByDocumento(String documento) throws Exception {
		log.info("getUsuarioByDocumento");
		return this.usuarioRepository.findByDocumento(documento);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getListUsuariosByNombre(String nombre) throws Exception {
		log.info("getListUsuariosByNombre");
		return this.usuarioRepository.findUserByLikeNombre(nombre);
	}

}

package co.com.inventario_afg_api.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.inventario_afg_api.utils.log.Loggable;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 * <p>
 * Servicio para el manejo de la generación de reportes
 * 
 * @author Miguel Romero
 */
@Service
public class ReportsService {

	@Autowired
	private DataSource ds;

	/**
	 * Atributo log : Logger - Permite registrar los eventos de lo que sucede en el
	 * servicio
	 */
	@Loggable
	private Logger log;

	public File generatePDF(String fileName, String reporte, HashMap<String, Object> params) throws Exception {
		log.info("generatePDF");
		String reportPath = this.getPathReport(reporte);
		JasperReport jasperReport = JasperCompileManager.compileReport(reportPath);
		JasperPrint print = JasperFillManager.fillReport(jasperReport, params, this.ds.getConnection());
		OutputStream output = new FileOutputStream(new File(fileName + ".pdf"));
		JasperExportManager.exportReportToPdfStream(print, output);
        return null;
	}

	/**
	 * <p>
	 * Obtiene el path del reporte a generar
	 * 
	 * @param fileName
	 *            Nombre del archivo
	 * @return
	 * @throws Exception
	 */
	private String getPathReport(String fileName) throws Exception {
		log.info("getPathReport");
		ClassLoader classLoader = new ReportsService().getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		return file.getPath();
	}

}

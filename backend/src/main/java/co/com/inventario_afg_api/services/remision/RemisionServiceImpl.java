/**
 * 
 */
package co.com.inventario_afg_api.services.remision;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Remision;
import co.com.inventario_afg_api.repositories.RemisionRepository;
import co.com.inventario_afg_api.services.aws.AwsS3Client;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "remisionService")
public class RemisionServiceImpl implements IRemisionService {

	private final String pathRemisiones = "remisiones";

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private RemisionRepository remisionRepository;

	@Autowired
	private AwsS3Client awsS3;

	@Override
	@Transactional(readOnly = true)
	public List<Remision> listAllRemisiones() throws Exception {
		log.info("listAllRemisiones");
		List<Remision> list = new ArrayList<>();
		this.remisionRepository.findAll().forEach(rem -> {
			list.add(rem);
		});
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Remision> listAll(Map<String, String> params) throws Exception {
		log.info("listAll");
		Order order = null;
		String[] paramSort = params.get("sort").split(",");
		if (paramSort[1].equals("ASC")) {
			order = new Order(Direction.ASC, paramSort[0]);
		} else {
			order = new Order(Direction.DESC, paramSort[0]);
		}
		Pageable pageable = new PageRequest(Integer.parseInt(params.get("page")), Integer.parseInt(params.get("size")),
				new Sort(order));
		return this.remisionRepository.findAll(pageable);
	}

	@Override
	@Transactional
	public Remision saveRemision(Remision remision) throws Exception {
		log.info("saveRemision");
		Remision r = null;
		try {
			r = this.remisionRepository.save(remision);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return r;
	}

	@Override
	@Transactional
	public Remision updateRemision(Remision remision) throws Exception {
		log.info("updateRemision");
		Remision r = null;
		try {
			r = this.remisionRepository.save(remision);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return r;
	}

	@Override
	@Transactional
	public void deleteRemision(Integer id) throws Exception {
		log.info("deleteRemision");
		try {
			this.remisionRepository.deleteById(id);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Remision findRemisionById(Integer id) throws Exception {
		log.info("findRemisionById");
		return this.remisionRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public Integer getMaxConsecutiveRemision() throws Exception {
		log.info("getMaxConsecutiveRemision");
		return this.remisionRepository.getMaxConsecutivo();
	}

	@Override
	@Transactional
	public String uploadRemision(File file, Integer remisionId) throws Exception {
		log.info("uploadRemision");

		Remision r = this.remisionRepository.findById(remisionId).get();

		if (r.getKeyS3() != null) {
			this.awsS3.deleteFileFromBucket(r.getKeyS3());
		}

		String name = remisionId.toString() + "_" + r.getFechaRemision() + "_" + file.getName();
		String url = this.awsS3.uploadFile(file, this.pathRemisiones, name);

		r.setUrl(url);
		r.setKeyS3(this.pathRemisiones + "/" + name);

		this.remisionRepository.save(r);

		return url;
	}

	@Override
	public List<Remision> getRemisionesNoFactura() throws Exception {
		log.info("getRemisionesNoFactura");
		return this.remisionRepository.getRemisionesNoFactura();
	}

}
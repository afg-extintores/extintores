package co.com.inventario_afg_api.services.cita_estado;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.CitaEstado;
import co.com.inventario_afg_api.repositories.CitaEstadoRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "citaEstadoService")
public class CitaEstadoServiceImpl implements ICitaEstadoService {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;
	
	@Autowired
	private CitaEstadoRepository citaEstadoRepository;

	@Override
	@Transactional(readOnly = true)
	public List<CitaEstado> listAll() throws Exception {
		log.info("listAll");
		return this.citaEstadoRepository.findAll();
	}

}

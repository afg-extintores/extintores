package co.com.inventario_afg_api.services.producto;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.inventario_afg_api.domain.Producto;

public interface IProductoService {
	
	List<Producto> listAll() throws Exception;
	
	Page<Producto> listAllPaginados(Pageable pageable) throws Exception;
	
	List<Producto> listProductosActivos()throws Exception;
	
	Producto saveProducto(Producto cliente) throws Exception;
	
	Producto updateProducto(Producto cliente) throws Exception;
	
	void deleteProducto(Integer id) throws Exception;
	
	Producto getProductoById(Integer id) throws Exception;
	
	Producto getProductoByCodigo(String codigo) throws Exception;

	List<Producto> findAllProductsNameOrdered() throws Exception;

}

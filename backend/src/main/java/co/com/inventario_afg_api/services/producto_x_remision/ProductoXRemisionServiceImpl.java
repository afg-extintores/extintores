/**
 * 
 */
package co.com.inventario_afg_api.services.producto_x_remision;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.ProductoXRemision;
import co.com.inventario_afg_api.repositories.ProductoXRemisionRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "productoXRemisionService")
public class ProductoXRemisionServiceImpl implements IProductoXRemisionService {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private ProductoXRemisionRepository productoXRemisionRepository;

	@Override
	@Transactional(readOnly = true)
	public List<ProductoXRemision> listAll() throws Exception {
		log.info("listAll");
		return this.productoXRemisionRepository.findAll();
	}

	@Override
	@Transactional
	public ProductoXRemision saveProductoXRemision(ProductoXRemision productoXRemision) throws Exception {
		log.info("saveProductoXRemision");
		ProductoXRemision pxr = null;
		try {
			pxr = this.productoXRemisionRepository.save(productoXRemision);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return pxr;
	}

	@Override
	@Transactional
	public ProductoXRemision updateProductosXRemision(ProductoXRemision productoXRemision) throws Exception {
		log.info("updateProductosXRemision");
		ProductoXRemision pxr = null;
		try {
			pxr = this.productoXRemisionRepository.save(productoXRemision);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return pxr;
	}

	@Override
	@Transactional
	public void deleteProductosXRemision(Integer id) throws Exception {
		log.info("deleteProductosXRemision");
		try {
			this.productoXRemisionRepository.deleteById(id);
		} catch(Exception e){
			log.error(e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public ProductoXRemision findProductosXRemisionById(Integer id) throws Exception {
		log.info("findProductosXRemisionById");
		return this.productoXRemisionRepository.findById(id).get();
	}

}

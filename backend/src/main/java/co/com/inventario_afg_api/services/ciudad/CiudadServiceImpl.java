package co.com.inventario_afg_api.services.ciudad;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Ciudad;
import co.com.inventario_afg_api.repositories.CiudadRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "ciudadService")
public class CiudadServiceImpl implements ICiudadService {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;
	
	@Autowired
	private CiudadRepository ciudadRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Ciudad> listAll() throws Exception {
		log.info("listAll");
		return this.ciudadRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Ciudad getCiudadById(Integer id) throws Exception {
		log.info("getCiudadById");
		return this.ciudadRepository.findById(id).get();
	}

}

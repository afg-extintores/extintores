package co.com.inventario_afg_api.services.inversion;

import co.com.inventario_afg_api.domain.Inversion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IInversionService {

    List<Inversion> listAllInversiones() throws Exception;

    Page<Inversion> listInversiones(Pageable pageable) throws Exception;

    Inversion saveInversion(Inversion inversion) throws Exception;

    Inversion updateInversion(Inversion inversion) throws Exception;

    void deleteInversion(Integer id) throws Exception;

    Inversion getInversionById(Integer id) throws Exception;

}

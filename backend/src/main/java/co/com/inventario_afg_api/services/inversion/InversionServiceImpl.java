package co.com.inventario_afg_api.services.inversion;

import co.com.inventario_afg_api.domain.Inversion;
import co.com.inventario_afg_api.repositories.InversionRepository;
import co.com.inventario_afg_api.utils.log.Loggable;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("inversionService")
public class InversionServiceImpl implements IInversionService {

    @Loggable
    private Logger log;

    /** Repositorio de acceso a la data de {@link Inversion} */
    @Autowired
    private InversionRepository inversionRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Inversion> listAllInversiones() throws Exception {
        log.info("listAllInversiones");
        List<Inversion> list = new ArrayList<>();
        this.inversionRepository.findAll().forEach(inv -> {
            list.add(inv);
        });
        return list;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Inversion> listInversiones(Pageable pageable) throws Exception {
        log.info("listInversiones");
        return this.inversionRepository.findAll(pageable);
    }

    @Override
    @Transactional
    public Inversion saveInversion(Inversion inversion) throws Exception {
        log.info("saveInversion");
        return this.inversionRepository.save(inversion);
    }

    @Override
    @Transactional
    public Inversion updateInversion(Inversion inversion) throws Exception {
        log.info("updateInversion");
        return this.inversionRepository.save(inversion);
    }

    @Override
    @Transactional
    public void deleteInversion(Integer id) throws Exception {
        log.info("deleteInversion");
        this.inversionRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Inversion getInversionById(Integer id) throws Exception {
        log.info("getInversionById");
        return this.inversionRepository.findById(id).get();
    }
}

package co.com.inventario_afg_api.services.rol;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Rol;
import co.com.inventario_afg_api.repositories.RolRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "rolService")
public class RolServiceImpl implements IRolService {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private RolRepository rolRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Rol> listRols() throws Exception {
		log.info("listRols");
		return this.rolRepository.findAll();
	}

	@Override
	@Transactional
	public Rol saveRol(Rol rol) throws Exception {
		log.info("saveRol");
		return this.rolRepository.save(rol);
	}

	@Override
	@Transactional
	public Rol updateRol(Rol rol) throws Exception {
		log.info("updateRol");
		return this.rolRepository.save(rol);
	}

	@Override
	@Transactional
	public void deleteRolById(Integer id) throws Exception {
		log.info("deleteRolById");
		this.rolRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Rol getRolById(Integer id) throws Exception {
		log.info("getRolById");
		return this.rolRepository.findById(id).get();
	}

}

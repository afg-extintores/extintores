package co.com.inventario_afg_api.services.auth;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import co.com.inventario_afg_api.config.security.JwtTokenProvider;
import co.com.inventario_afg_api.domain.Usuario;
import co.com.inventario_afg_api.domain.payloads.JwtAuthenticationResponse;
import co.com.inventario_afg_api.domain.payloads.LoginRequest;
import co.com.inventario_afg_api.repositories.UsuarioRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "authService")
public class AuthServiceImpl implements IAuthService {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private AuthenticationManager authenticationManager;

	/** Repositorio de {@link Usuario} */
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Override
	public JwtAuthenticationResponse login(LoginRequest request) throws Exception {
		log.info("login");
		Authentication authentication = null;
		Usuario usuario = null;
		final String token;
		try {
			// Perform the authentication
			authentication = this.authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword()));

			SecurityContextHolder.getContext().setAuthentication(authentication);

			token = this.tokenProvider.generateToken(authentication);

			if (request.getLogin().contains("@")) {
				usuario = this.usuarioRepository.findByCorreo(request.getLogin());
			} else {
				usuario = this.usuarioRepository.findByDocumento(request.getLogin());
			}
			if (!usuario.isActivo() || !usuario.isAcceso()) {
				return null;
			}
		} catch (Exception e) {
			log.info(e.getMessage());
			return null;
		}
		return new JwtAuthenticationResponse(token, usuario.getId(),
				usuario.getNombres() + " " + usuario.getApellidos(), usuario.getRol().getId(),
				usuario.getRol().getNombre());
	}

}

package co.com.inventario_afg_api.services.cliente_direccion;

import java.util.List;

import co.com.inventario_afg_api.domain.ClienteDireccion;

public interface IClienteDireccionService {
	
	List<ClienteDireccion> listClientesDirecciones() throws Exception;
	
	ClienteDireccion saveClienteDireccion(ClienteDireccion clienteDireccion) throws Exception;
	
	ClienteDireccion updateClienteDireccion(ClienteDireccion clienteDireccion) throws Exception;
	
	void deleteClienteDireccionById(Integer id) throws Exception;
	
	ClienteDireccion getClienteDireccionById(Integer id) throws Exception;
	
	List<ClienteDireccion> getListClienteDireccionByIdCliente(Integer idCliente) throws Exception;

}

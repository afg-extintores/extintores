package co.com.inventario_afg_api.services.factura;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.inventario_afg_api.domain.Factura;
import co.com.inventario_afg_api.domain.dto.reportes.FacturaInfo;

public interface IFacturaService {

	List<Factura> listAllFacturas() throws Exception;

	Page<Factura> listFacturas(Pageable pageable) throws Exception;
	
	List<Factura> getInvoicesWithoutInvestment() throws Exception;

	Factura saveFactura(Factura factura) throws Exception;

	Factura updateFactura(Factura factura) throws Exception;

	void deleteFactura(Integer id) throws Exception;

	Factura getFacturaById(Integer id) throws Exception;

	Integer getMaxConsecutivoFactura() throws Exception;

	byte[] generateFactura(FacturaInfo factura, HttpServletResponse response) throws Exception;

	boolean changeStatusInvoice(Integer status, Integer id) throws Exception;

	String uploadFactura(File file, Integer facturaId) throws Exception;

	Factura getFacturaByConsecutive(Integer consecutive) throws Exception;

}
package co.com.inventario_afg_api.services.cita;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Cita;
import co.com.inventario_afg_api.repositories.CitaRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "citaService")
public class CitaServiceImpl implements ICitaService {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private CitaRepository citaRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Cita> listAll() throws Exception {
		log.info("listAll");
		List<Cita> list = new ArrayList<Cita>();
		this.citaRepository.findAll().forEach(cit -> {
			list.add(cit);
		});
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Cita> listAllPaginados(Pageable pageable) throws Exception {
		log.info("listAllPaginados");
		return this.citaRepository.findAll(pageable);
	}

	@Override
	@Transactional
	public Cita saveCita(Cita cita) throws Exception {
		log.info("saveCita");
		return this.citaRepository.save(cita);
	}

	@Override
	@Transactional
	public Cita updateCita(Cita cita) throws Exception {
		log.info("updateCita");
		return this.citaRepository.save(cita);
	}

	@Override
	@Transactional
	public void deleteCita(Integer id) throws Exception {
		log.info("deleteCita");
		this.citaRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Cita getCitaById(Integer id) throws Exception {
		log.info("getCitaById");
		return this.citaRepository.findById(id).get();
	}

	@Override
	public List<Cita> getCitasByRangeDates(Date startDate, Date endDate) throws Exception {
		log.info("getCitasByRangeDates");
		return this.citaRepository.findAllDatesByRange(startDate, endDate);
	}

}

package co.com.inventario_afg_api.services.cliente;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Cliente;
import co.com.inventario_afg_api.repositories.ClienteRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "clienteService")
public class ClienteServiceImpl implements IClienteService {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> listAll() throws Exception {
		log.info("listAll");
		List<Cliente> list = new ArrayList<Cliente>();
		this.clienteRepository.findAll().forEach(cli -> {
			list.add(cli);
		});
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Cliente> listAllPaginados(Pageable pageable) throws Exception {
		log.info("listAll");
		return this.clienteRepository.findAll(pageable);
	}

	@Override
	@Transactional
	public Cliente saveCliente(Cliente cliente) throws Exception {
		log.info("saveCliente");
		Cliente client = null;
		try {
			client = this.clienteRepository.save(cliente);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return client;
	}

	@Override
	@Transactional
	public Cliente updateCliente(Cliente cliente) throws Exception {
		log.info("updateCliente");
		Cliente client = null;
		try {
			client = this.clienteRepository.save(cliente);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return client;
	}

	@Override
	@Transactional
	public void deleteCliente(Integer id) throws Exception {
		log.info("deleteCliente");
		try {
			this.clienteRepository.deleteById(id);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente getClienteById(Integer id) throws Exception {
		log.info("getClienteById");
		return this.clienteRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente getClienteByNombre(String nombre) throws Exception {
		log.info("getClienteByNombre");
		return this.clienteRepository.findByNombre(nombre);
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente getClienteByNit(String nit) throws Exception {
		log.info("getClienteByNit");
		return this.clienteRepository.findByNit(nit);
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente getClienteByCorreo(String correo) throws Exception {
		log.info("getClienteByCorreo");
		return this.clienteRepository.findByCorreo(correo);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> getListClientesByNombre(String nombre) throws Exception {
		log.info("getListClientesByNombre");
		return this.clienteRepository.findClientByLikeNombre(nombre);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> getListClientesByNombreActivos(String nombre) throws Exception {
		log.info("getListClientesByNombreActivos");
		return this.clienteRepository.findClientByLikeNombreActivos(nombre);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> getListClientesActivos() throws Exception {
		log.info("getListClientesActivos");
		return this.clienteRepository.findClientesActivos();
	}

	@Override
	public List<Cliente> findAllClientsNameOrdered() throws Exception {
		log.info("findAllClientsNameOrdered");
		return this.clienteRepository.findAllClientsNameOrdered();
	}

}

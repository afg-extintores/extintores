package co.com.inventario_afg_api.services.producto;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.Producto;
import co.com.inventario_afg_api.repositories.ProductoRepository;
import co.com.inventario_afg_api.utils.log.Loggable;

@Service(value = "productoService")
public class ProductoServiceImpl implements IProductoService {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;
	
	@Autowired
	private ProductoRepository productoRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Producto> listAll() throws Exception {
		log.info("listAll");
		List<Producto> list = new ArrayList<>();
		this.productoRepository.findAll().forEach(pro -> {
			list.add(pro);
		});
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Producto> listAllPaginados(Pageable pageable) throws Exception {
		log.info("listAllPaginados");
		return this.productoRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Producto> listProductosActivos() throws Exception {
		log.info("listProductosActivos");
		return this.productoRepository.findByActivo(true);
	}

	@Override
	@Transactional
	public Producto saveProducto(Producto producto) throws Exception {
		log.info("saveProducto");
		Producto product = null;
		try {
			product = this.productoRepository.save(producto);
		}  catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return product;
	}

	@Override
	@Transactional
	public Producto updateProducto(Producto producto) throws Exception {
		log.info("updateProducto");
		Producto product = null;
		try {
			product = this.productoRepository.save(producto);
		}  catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return product;
	}

	@Override
	@Transactional
	public void deleteProducto(Integer id) throws Exception {
		log.info("deleteProducto");
		this.productoRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Producto getProductoById(Integer id) throws Exception {
		log.info("getProductoById");
		return this.productoRepository.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public Producto getProductoByCodigo(String codigo) throws Exception {
		log.info("getProductoByCodigo");
		return this.productoRepository.findByCodigo(codigo);
	}

	@Override
	public List<Producto> findAllProductsNameOrdered() throws Exception {
		log.info("findAllProductsNameOrdered");
		return this.productoRepository.findAllProductsNameOrdered();
	}

}

package co.com.inventario_afg_api.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.InversionReal;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link InversionReal}
 * 
 * @author Miguel Romero
 */
@Repository
public interface InversionRealRepository extends PagingAndSortingRepository<InversionReal, Integer> {

}

package co.com.inventario_afg_api.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Cotizacion;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Cotizacion}
 * 
 * @author Miguel Romero
 */
@Repository
public interface CotizacionRepository extends PagingAndSortingRepository<Cotizacion, Integer> {

}

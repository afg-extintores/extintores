package co.com.inventario_afg_api.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Inversion;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Inversion}
 * 
 * @author Miguel Romero
 */
@Repository
public interface InversionRepository extends PagingAndSortingRepository<Inversion, Integer> {

}

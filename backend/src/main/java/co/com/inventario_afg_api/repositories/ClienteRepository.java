package co.com.inventario_afg_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Cliente;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Cliente}
 * 
 * @author Miguel Romero
 */
@Repository
public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Integer> {

	/**
	 * <p>
	 * Busca un Cliente por Nombre
	 * 
	 * @param nombre Nombre del Cliente
	 * @return {@code Cliente} Cliente encontrado por Nombre
	 * @throws Exception
	 */
	public Cliente findByNombre(String nombre) throws Exception;
	
	/**
	 * <p>
	 * Busca un Cliente por NIT
	 * 
	 * @param nit Nit del Cliente
	 * @return {@code Cliente} Cliente encontrado por NIT
	 * @throws Exception
	 */
	public Cliente findByNit(String nit) throws Exception;
	
	/**
	 * <p>
	 * Busca un Cliente por Correo
	 * @param correo Correo del Cliente
	 * @return {@code Correo} Cliente encontrado por Correo
	 * @throws Exception
	 */
	public Cliente findByCorreo(String correo) throws Exception;
	
	/**
	 * <p>
	 * Retorna los clientes que coincidan con la busqueda del nombre
	 *  
	 * @param nombre Nombre de los clientes
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT c FROM Cliente c WHERE c.nombre LIKE %:nombre%")
	public List<Cliente> findClientByLikeNombre(@Param("nombre") String nombre) throws Exception;
	
	/**
	 * <p>
	 * Retorna los clientes activos que coincidan con la busqueda del nombre
	 *  
	 * @param nombre Nombre de los clientes
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT c FROM Cliente c WHERE c.nombre LIKE %:nombre% and c.activo = true")
	public List<Cliente> findClientByLikeNombreActivos(@Param("nombre") String nombre) throws Exception;
	
	/**
	 * <p>
	 * Retorna los clientes activos
	 *  
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT c FROM Cliente c WHERE c.activo = true")
	public List<Cliente> findClientesActivos() throws Exception;

	/**
	 * <p>Returns list of clients ordered by name asc</p>
	 *
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT c FROM Cliente c ORDER BY c.nombre ASC")
	public List<Cliente> findAllClientsNameOrdered() throws Exception;
	
}

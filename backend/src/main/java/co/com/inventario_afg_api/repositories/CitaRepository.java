package co.com.inventario_afg_api.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Cita;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Cita}
 * 
 * @author Miguel Romero
 */
@Repository
public interface CitaRepository extends PagingAndSortingRepository<Cita, Integer> {

	@Query("select c from Cita c where c.fechaCita >= :startDate and c.fechaCita <= :endDate")
	List<Cita> findAllDatesByRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate) throws Exception;

}

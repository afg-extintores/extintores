package co.com.inventario_afg_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Factura;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Factura}
 * 
 * @author Miguel Romero
 */
@Repository
public interface FacturaRepository extends PagingAndSortingRepository<Factura, Integer> {

	@Query(nativeQuery = true, value = "SELECT COALESCE(MAX(consecutivo),0) AS consecutivo FROM facturas")
	public Integer getMaxConsecutivo() throws Exception;

	@Modifying
	@Query("update Factura f set f.facturasEstado.id = :status where f.id = :id")
	public Integer changeStatusInvoice(@Param("status") Integer status, @Param("id") Integer id) throws Exception;

	public Factura findByConsecutivo(Integer consecutivo);

	@Query(nativeQuery = true, value = "select f.* from facturas f, clientes c\n"
			+ "where f.id_cliente = c.id and f.id not in (select id_factura from inversiones)")
	public List<Factura> getInvoicesWithoutInvestment() throws Exception;

}

package co.com.inventario_afg_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.inventario_afg_api.domain.ProductoXFactura;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link ProductoXFactura}
 * 
 * @author Miguel Romero
 */
@Repository
public interface ProductoXFacturaRepository extends JpaRepository<ProductoXFactura, Integer> {

	/**
	 * <p>
	 * Retorna el listado de productos asociados por Factura
	 * 
	 * @param facturaId
	 *            Id de la Factura
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT pf FROM ProductoXFactura pf WHERE pf.factura.id = :facturaId")
	public List<ProductoXFactura> getProductoXFacturaByFactura(@Param("facturaId") Integer facturaId) throws Exception;

	/**
	 * <p>
	 * Elimina los productos asociados a una factura
	 * 
	 * @param factura
	 * @return
	 */
	@Modifying
	@Transactional
	@Query(value = "delete from ProductoXFactura pf where pf.factura.id = :facturaId")
	public void deleteByFactura(@Param("facturaId") Integer facturaId);

}

package co.com.inventario_afg_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Usuario;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Usuario}
 * 
 * @author Miguel Romero
 */
@Repository
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Integer> {
	
	/**
	 * Obtiene un usuario por su documento
	 * 
	 * @param documento Documento del usuario
	 * @return
	 * @throws Exception
	 */
	public Usuario findByDocumento(String documento) throws Exception;

	/**
	 * Obtiene un usuario por su correo
	 * 
	 * @param correo Correo del usuario
	 * @return Usuario encontrado por su correo
	 * @throws Exception
	 */
	public Usuario findByCorreo(String correo) throws Exception;
	
	/**
	 * Obtiene el listado de usuarios vendedores activos
	 * 
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT u FROM Usuario u WHERE u.vendedor = true and u.activo = true")
	public List<Usuario> findUsuariosVendedoresActivos() throws Exception;
	
	/**
	 * <p>
	 * Retorna los usuarios activos
	 * 
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT u FROM Usuario u WHERE u.activo = true")
	public List<Usuario> findUsuariosActivos() throws Exception;
	
	/**
	 * <p>
	 * Retorna los usuarios que coincidan con la busqueda del nombre
	 *  
	 * @param nombre Nombre de los usuarios
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT u FROM Usuario u WHERE u.nombres LIKE %:nombre%")
	public List<Usuario> findUserByLikeNombre(@Param("nombre") String nombre) throws Exception;
	
}

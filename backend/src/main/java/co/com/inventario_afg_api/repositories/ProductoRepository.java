package co.com.inventario_afg_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Producto;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Producto}
 * 
 * @author Miguel Romero
 */
@Repository
public interface ProductoRepository extends PagingAndSortingRepository<Producto, Integer> {
	
	/**
	 * Retorna el listado de productos activos
	 * 
	 * @param activo Estado del producto
	 * @return
	 * @throws Exception
	 */
	List<Producto> findByActivo(boolean activo) throws Exception;
	
	/**
	 * Retorna un producto por código
	 * 
	 * @param codigo Código del producto
	 * @return
	 * @throws Exception
	 */
	Producto findByCodigo(String codigo) throws Exception;

	/**
	 * <p>Returns list of products ordered by name</p>
	 *
	 * @return
	 * @throws Exception
	 */
	@Query("SELECT p FROM Producto p ORDER BY p.nombre ASC")
	List<Producto> findAllProductsNameOrdered() throws Exception;
	
}

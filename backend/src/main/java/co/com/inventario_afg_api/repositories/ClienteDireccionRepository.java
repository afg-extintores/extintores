package co.com.inventario_afg_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.ClienteDireccion;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link ClienteDireccion}
 * 
 * @author Miguel Romero
 */
@Repository
public interface ClienteDireccionRepository extends JpaRepository<ClienteDireccion, Integer> {

	/**
	 * <p>
	 * Obtiene el listado de direcciones asociadas a un cliente
	 * 
	 * @param idCliente Id del Cliente
	 * @return 
	 * @throws Exception
	 */
	@Query(value = "SELECT cd FROM ClienteDireccion cd WHERE cd.cliente.id = :idCliente")
	public List<ClienteDireccion> getDireccionesByCliente(@Param("idCliente") Integer idCliente)throws Exception;
	
}

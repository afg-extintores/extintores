package co.com.inventario_afg_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.inventario_afg_api.domain.CitaEstado;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link CitaEstado}
 * 
 * @author Miguel Romero
 */
public interface CitaEstadoRepository extends JpaRepository<CitaEstado, Integer> {

}

package co.com.inventario_afg_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Remision;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Remision}
 * 
 * @author Miguel Romero
 */
@Repository
public interface RemisionRepository extends PagingAndSortingRepository<Remision, Integer> {
	
	@Query(nativeQuery = true, value = "SELECT COALESCE(MAX(consecutivo),0) AS consecutivo FROM remisiones")	
	public Integer getMaxConsecutivo() throws Exception;
	
	@Query(value = "SELECT r FROM Remision r WHERE r.factura = false")
	public List<Remision> getRemisionesNoFactura()throws Exception;

}

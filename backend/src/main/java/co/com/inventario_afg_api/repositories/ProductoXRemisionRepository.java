package co.com.inventario_afg_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.ProductoXRemision;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link ProductoXRemision}
 * 
 * @author Miguel Romero
 */
@Repository
public interface ProductoXRemisionRepository extends JpaRepository<ProductoXRemision, Integer> {

}

package co.com.inventario_afg_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.FacturaTipoPago;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link FacturaTipoPago}
 * 
 * @author Miguel Romero
 */
@Repository
public interface FacturaTipoPagoRepository extends JpaRepository<FacturaTipoPago, Integer> {

}

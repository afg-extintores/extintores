package co.com.inventario_afg_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.inventario_afg_api.domain.Ciudad;

/**
 * <p>
 * Repositorio para el acceso y manejo de los datos de {@link Ciudad}
 * 
 * @author Miguel Romero
 */
@Repository
public interface CiudadRepository extends JpaRepository<Ciudad, Integer> {

}

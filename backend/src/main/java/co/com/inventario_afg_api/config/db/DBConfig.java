package co.com.inventario_afg_api.config.db;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * <p>
 * Clase de Configuración de recursos de la DB
 * 
 * @author Miguel Romero
 */
@Configuration
public class DBConfig {

	/**
	 * <p>
	 * Bean que retorna el datasource de la aplicación
	 * 
	 * @return
	 */
	@ConfigurationProperties(prefix = "spring.datasource")
	@Bean
	public DataSource dataSource() {
		return new DriverManagerDataSource();
	}

}

package co.com.inventario_afg_api.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import co.com.inventario_afg_api.utils.log.Loggable;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Loggable
	private Logger log;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {

		// Se invoca cuando el usuario intenta acceder a un recurso REST seguro
		// sin proveer ninguna credencial.
		// En la respuesta se envía solo un estado 401 Unauthorized ya que no
		// existe una pagina de Login a la cual redireccionar
		log.info(authException.getMessage());
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Access Denied");

	}

}

package co.com.inventario_afg_api.config.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import co.com.inventario_afg_api.domain.Usuario;

public class JWTUser implements UserDetails {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String email;
	private String documento;
	private boolean activo;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;

	/**
	 * Metodo constructor de la clase JWTUser.java
	 */
	public JWTUser(Integer id, String name, String email, String documento, boolean activo, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.documento = documento;
		this.activo = activo;
		this.password = password;
		this.authorities = authorities;
	}

	public static JWTUser create(Usuario usuario) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(usuario.getRol().getNombre()));

		return new JWTUser(usuario.getId(), usuario.getNombres().concat(" ").concat(usuario.getApellidos()),
				usuario.getCorreo(), usuario.getDocumento(), usuario.isActivo(), usuario.getPassword(), authorities);
	}

	/**
	 * Metodo que retorna el atributo serialVersionUID
	 * 
	 * @return : serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Metodo que retorna el atributo id
	 * 
	 * @return : id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Metodo que retorna el atributo name
	 * 
	 * @return : name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Metodo que retorna el atributo email
	 * 
	 * @return : email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Metodo que retorna el atributo documento
	 * 
	 * @return : documento
	 */
	public String getDocumento() {
		return documento;
	}

	/**
	 * Metodo que retorna el atributo activo
	 * 
	 * @return : activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#getPassword()
	 */
	@Override
	public String getPassword() {
		return this.password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#getUsername()
	 */
	@Override
	public String getUsername() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired
	 * ()
	 */
	@Override
	public boolean isAccountNonExpired() {
		return isActivo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked(
	 * )
	 */
	@Override
	public boolean isAccountNonLocked() {
		return isActivo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isCredentialsNonExpired()
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		return isActivo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return isActivo();
	}

}

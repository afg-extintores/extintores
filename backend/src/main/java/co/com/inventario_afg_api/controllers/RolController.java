package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Rol;
import co.com.inventario_afg_api.services.rol.IRolService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Rol}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_ROLES)
public class RolController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Servicio de acceso a la data de {@link Rol} */
	@Autowired
	private IRolService rolService;

	/**
	 * <p>
	 * Retorna el listado de Rols
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Rol>> listRols() throws Exception {
		log.info("listRols");
		List<Rol> list = new ArrayList<>();
		try {
			list = this.rolService.listRols();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Rol>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una rol
	 * 
	 * @param rol
	 *            Rol a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Rol> saveRol(@RequestBody Rol rol) throws Exception {
		log.info("saveRol");
		Rol rolTmp = null;
		try {
			rolTmp = this.rolService.saveRol(rol);
			if (rolTmp != null) {
				return new ResponseEntity<>(rolTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una rol
	 * 
	 * @param rol
	 *            Rol a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Rol> updateRol(@RequestBody Rol rol) throws Exception {
		log.info("updateRol");
		Rol rolTmp = null;
		try {
			rolTmp = this.rolService.updateRol(rol);
			if (rolTmp != null) {
				return new ResponseEntity<>(rolTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una rol por id
	 * 
	 * @param id Id de la rol
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteRolById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteRolById");		
		try {
			this.rolService.deleteRolById(id);
			return new ResponseEntity<>("Rol Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una rol por id
	 * 
	 * @param id Id de la rol
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Rol> getRolById(@PathVariable("id") Integer id) throws Exception {
		log.info("getRolById");
		Rol rol = null;
		try {
			rol = this.rolService.getRolById(id);
			if (rol != null) {
				return new ResponseEntity<>(rol, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

package co.com.inventario_afg_api.controllers.exceptions;

/**
 * <p>
 * Modelo de Respuesta de las Excepciones
 * 
 * @author Miguel Romero
 */
public class ErrorResponse {
	
	/** Atributo errorCode : String - Codigo del error asociado a la excepcion */
	private String errorCode;
	
	/** Atributo message : String - Mensaje de respuesta asociado a la excepcion */
	private String message;
	
	/**
	* Metodo constructor de la clase ErrorResponse.java
	*/
	public ErrorResponse(){
		
	}
	
	/**
	* Metodo constructor de la clase ErrorResponse.java
	* @param errorCode : String
	* @param message : String
	*/
	public ErrorResponse(String errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.message = message;
	}

	/**
	* Metodo que retorna el atributo errorCode
	* @return errorCode : String
	*/
	public String getErrorCode() {
		return errorCode;
	}

	/**
	* Metodo que modifica el atributo errorCode
	* @param errorCode : String
	*/
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	* Metodo que retorna el atributo message
	* @return message : String
	*/
	public String getMessage() {
		return message;
	}

	/**
	* Metodo que modifica el atributo message
	* @param message : String
	*/
	public void setMessage(String message) {
		this.message = message;
	}

	
}

package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Producto;
import co.com.inventario_afg_api.services.producto.IProductoService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Producto}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_PRODUCTOS)
public class ProductoController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Servicio de acceso a la data de {@link Producto} */
	@Autowired
	private IProductoService productoService;

	/**
	 * Retorna el listado de productos
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listProductos() throws Exception {
		log.info("listProductos");
		List<Producto> list = new ArrayList<Producto>();
		try {
			list = this.productoService.findAllProductsNameOrdered();
			if (!list.isEmpty()) {
				return new ResponseEntity<>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Productos paginados
	 * 
	 * @param pageable Parámetros de paginación
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/pageables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Producto>> listProductosPaginados(Pageable pageable) throws Exception {
		log.info("listProductosPaginados");
		Page<Producto> pageProduct;
		try {
			pageProduct = this.productoService.listAllPaginados(pageable);
			if (pageProduct.hasContent()) {
				return new ResponseEntity<Page<Producto>>(pageProduct, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Retorna el listado de productos activos
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/activos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listProductosActivos() throws Exception {
		log.info("listProductosActivos");
		List<Producto> list = new ArrayList<Producto>();
		try {
			list = this.productoService.listProductosActivos();
			if (!list.isEmpty()) {
				return new ResponseEntity<>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una producto
	 * 
	 * @param producto Producto a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> saveProducto(@RequestBody Producto producto) throws Exception {
		log.info("saveProducto");
		Producto productoTmp = null;
		try {
			productoTmp = this.productoService.saveProducto(producto);
			if (productoTmp != null) {
				return new ResponseEntity<>(productoTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una producto
	 * 
	 * @param producto Producto a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> updateProducto(@RequestBody Producto producto) throws Exception {
		log.info("updateProducto");
		Producto productoTmp = null;
		try {
			productoTmp = this.productoService.updateProducto(producto);
			if (productoTmp != null) {
				return new ResponseEntity<>(productoTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina una producto por id
	 * 
	 * @param id Id de la producto
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteProductoById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteProductoById");
		try {
			this.productoService.deleteProducto(id);
			return new ResponseEntity<>(new StringResponse("Producto Eliminada Exitosamente"), HttpStatus.OK);
		} catch (DataIntegrityViolationException cve) {
			throw new DataIntegrityViolationException("", cve);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una producto por id
	 * 
	 * @param id Id de la producto
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> getProductoById(@PathVariable("id") Integer id) throws Exception {
		log.info("getProductoById");
		Producto producto = null;
		try {
			producto = this.productoService.getProductoById(id);
			if (producto != null) {
				return new ResponseEntity<>(producto, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca un producto por codigo
	 * 
	 * @param id Id de la producto
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/by-codigo/{codigo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> getProductoByCodigo(@PathVariable("codigo") String codigo) throws Exception {
		log.info("getProductoByCodigo");
		Producto producto = null;
		try {
			producto = this.productoService.getProductoByCodigo(codigo);
			if (producto != null) {
				return new ResponseEntity<>(producto, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import co.com.inventario_afg_api.domain.Factura;
import co.com.inventario_afg_api.domain.dto.reportes.FacturaInfo;
import co.com.inventario_afg_api.services.factura.IFacturaService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.FileUtils;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Factura}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_FACTURAS)
public class FacturaController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private IFacturaService facturaService;

	@Value("${afg.path-reportes}")
	String excelFilePath;

	@Autowired
	private FileUtils fileUtils;

	/**
	 * Retorna todas las facturas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Factura>> listFacturas() throws Exception {
		log.info("listFacturas");
		List<Factura> list = new ArrayList<>();
		try {
			list = this.facturaService.listAllFacturas();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Factura>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Facturas paginadas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/pageables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Factura>> listFacturasPageables(Pageable pageable) throws Exception {
		log.info("listFacturasPageables");
		Page<Factura> pageInvoice;
		try {
			pageInvoice = this.facturaService.listFacturas(pageable);
			if (pageInvoice.hasContent()) {
				return new ResponseEntity<Page<Factura>>(pageInvoice, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Return the list of invoices that doesn't have a investment yet
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/no-investment", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Factura>> listFacturasWithoutInvestment() throws Exception {
		log.info("listFacturasWithoutInvestment");
		List<Factura> invoices;
		try {
			invoices = this.facturaService.getInvoicesWithoutInvestment();
			if (invoices.size() > 0) {
				return new ResponseEntity<List<Factura>>(invoices, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una factura
	 * 
	 * @param factura Factura a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Factura> saveFactura(@RequestBody Factura factura) throws Exception {
		log.info("saveFactura");
		Factura facturaTmp = null;
		try {
			facturaTmp = this.facturaService.saveFactura(factura);

			if (facturaTmp != null) {
				return new ResponseEntity<>(facturaTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una factura
	 * 
	 * @param factura Factura a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Factura> updateFactura(@RequestBody Factura factura) throws Exception {
		log.info("updateFactura");
		Factura facturaTmp = null;
		try {
			facturaTmp = this.facturaService.updateFactura(factura);

			if (facturaTmp != null) {
				return new ResponseEntity<>(facturaTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina una factura por id
	 * 
	 * @param id Id de la factura
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteFacturaById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteFacturaById");
		try {
			this.facturaService.deleteFactura(id);
			return new ResponseEntity<>("Factura Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una factura por id
	 * 
	 * @param id Id de la factura
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Factura> getFacturaById(@PathVariable("id") Integer id) throws Exception {
		log.info("getFacturaById");
		Factura factura = null;
		try {
			factura = this.facturaService.getFacturaById(id);

			if (factura != null) {
				return new ResponseEntity<>(factura, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el número máximo del consecutivo de facturas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/max-consecutivo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> getMaxConsecutivoFactura() throws Exception {
		log.info("getMaxConsecutivoFactura");
		try {
			return new ResponseEntity<>(this.facturaService.getMaxConsecutivoFactura() + 1, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Actualiza el estado de la Factura
	 * 
	 * @param status
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/update-status/{status}/{id}")
	public ResponseEntity<Boolean> changeStatusInvoice(@PathVariable("status") Integer status,
			@PathVariable("id") Integer id) throws Exception {
		log.info("changeStatusInvoice");
		try {
			return new ResponseEntity<Boolean>(this.facturaService.changeStatusInvoice(status, id), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Carga la imagen de la factura a S3
	 * 
	 * @param image
	 * @param facturaId
	 * @return
	 * @throws Exception
	 */
	@PostMapping(path = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> uploadImageFactura(@RequestPart("image") MultipartFile image,
			@RequestPart("facturaId") String facturaId) throws Exception {
		log.info("uploadImageFactura");
		try {
			String url = this.facturaService.uploadFactura(this.fileUtils.multipartToFileConverter(image),
					Integer.valueOf(facturaId));
			return ResponseEntity.ok().body(new StringResponse(url));
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Genera la factura para imprimir
	 * 
	 * @param factura
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@PostMapping(path = "/make-factura", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public @ResponseBody byte[] generateFactura(@RequestBody FacturaInfo factura, HttpServletResponse response)
			throws Exception {
		log.info("generateFactura");
		byte[] res = this.facturaService.generateFactura(factura, response);
		return res;
	}

	/**
	 * Retorna la factura por el consecutivo
	 * 
	 * @param consecutive
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/by-consecutive/{consecutive}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Factura> getFacturaByConsecutive(@PathVariable("consecutive") Integer consecutive)
			throws Exception {
		log.info("getFacturaByConsecutive");
		try {
			return new ResponseEntity<Factura>(this.facturaService.getFacturaByConsecutive(consecutive), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

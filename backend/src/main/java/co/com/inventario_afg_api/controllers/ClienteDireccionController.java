package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.ClienteDireccion;
import co.com.inventario_afg_api.services.cliente_direccion.IClienteDireccionService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de
 * {@link ClienteDireccion}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_CLIENTES_DIRECCIONES)
public class ClienteDireccionController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Servicio de acceso a la data de {@link ClienteDireccion} */
	@Autowired
	private IClienteDireccionService clienteDireccionService;

	/**
	 * <p>
	 * Retorna el listado de Direcciones de los Clientes
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ClienteDireccion>> listClientesDirecciones() throws Exception {
		log.info("listClientesDirecciones");
		List<ClienteDireccion> list = new ArrayList<>();
		try {
			list = this.clienteDireccionService.listClientesDirecciones();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<ClienteDireccion>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una dirección de un cliente
	 * 
	 * @param clienteDireccion Dirección del cliente a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ClienteDireccion> saveClienteDireccion(@RequestBody ClienteDireccion clienteDireccion)
			throws Exception {
		log.info("saveClienteDireccion");
		ClienteDireccion clienteDireccionTmp = null;
		try {
			clienteDireccionTmp = this.clienteDireccionService.saveClienteDireccion(clienteDireccion);
			if (clienteDireccionTmp != null) {
				return new ResponseEntity<>(clienteDireccionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Asocia las direcciones a un cliente
	 * 
	 * @param list
	 * @return
	 * @throws Exception
	 */
	@PostMapping(path = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> saveListDireccionesXCliente(@RequestBody List<ClienteDireccion> list)
			throws Exception {
		log.info("saveListDirecciones");
		try {
			for (ClienteDireccion cd : list) {
				this.clienteDireccionService.saveClienteDireccion(cd);
			}
			return new ResponseEntity<>(new StringResponse("Direcciones asociados al cliente correctamente"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una dirección de un cliente
	 * 
	 * @param clienteDireccion Dirección de un cliente a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ClienteDireccion> updateClienteDireccion(@RequestBody ClienteDireccion clienteDireccion)
			throws Exception {
		log.info("updateClienteDireccion");
		ClienteDireccion clienteDireccionTmp = null;
		try {
			clienteDireccionTmp = this.clienteDireccionService.updateClienteDireccion(clienteDireccion);
			if (clienteDireccionTmp != null) {
				return new ResponseEntity<>(clienteDireccionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina una dirección de un cliente por id
	 * 
	 * @param id Id de la dirección del cliente
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteClienteDireccionById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteClienteDireccionById");
		try {
			this.clienteDireccionService.deleteClienteDireccionById(id);
			return new ResponseEntity<>(new StringResponse("Dirección del Cliente Eliminada Exitosamente"),
					HttpStatus.OK);
		} catch (DataIntegrityViolationException cve) {
			throw new DataIntegrityViolationException("", cve);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una dirección de cliente por id
	 * 
	 * @param id Id de la dirección del cliente
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ClienteDireccion> getClienteDireccionById(@PathVariable("id") Integer id) throws Exception {
		log.info("getClienteDireccionById");
		ClienteDireccion clienteDireccion = null;
		try {
			clienteDireccion = this.clienteDireccionService.getClienteDireccionById(id);
			if (clienteDireccion != null) {
				return new ResponseEntity<>(clienteDireccion, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Direccions por Cliente
	 * 
	 * @param idCliente Id del Cliente
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/by-client/{idCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ClienteDireccion>> getListClienteDireccionByIdCliente(
			@PathVariable("idCliente") Integer idCliente) throws Exception {
		log.info("getListClienteDireccionByIdCliente");
		List<ClienteDireccion> list = new ArrayList<>();
		try {
			list = this.clienteDireccionService.getListClienteDireccionByIdCliente(idCliente);
			if (!list.isEmpty()) {
				return new ResponseEntity<List<ClienteDireccion>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

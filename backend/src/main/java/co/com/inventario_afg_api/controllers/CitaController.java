package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Cita;
import co.com.inventario_afg_api.services.cita.ICitaService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Cita}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_CITAS)
public class CitaController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Servicio de acceso a la data de {@link Cita} */
	@Autowired
	private ICitaService citaService;

	/**
	 * <p>
	 * Retorna el listado de Citas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cita>> listCitas(Pageable pageable) throws Exception {
		log.info("listCitas");
		List<Cita> list = new ArrayList<Cita>();
		try {
			list = this.citaService.listAll();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Cita>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Citas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/pageables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Cita>> listCitasPaginadas(Pageable pageable) throws Exception {
		log.info("listCitasPaginadas");
		Page<Cita> pageCitas;
		try {
			pageCitas = this.citaService.listAllPaginados(pageable);
			if (pageCitas.hasContent()) {
				return new ResponseEntity<Page<Cita>>(pageCitas, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una cita
	 * 
	 * @param cita Cita a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cita> saveCita(@RequestBody Cita cita) throws Exception {
		log.info("saveCita");
		Cita citaTmp = null;
		try {
			cita.setFechaLlamadaCita(new Date());
			citaTmp = this.citaService.saveCita(cita);
			if (citaTmp != null) {
				return new ResponseEntity<>(citaTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una cita
	 * 
	 * @param cita Cita a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cita> updateCita(@RequestBody Cita cita) throws Exception {
		log.info("updateCita");
		Cita citaTmp = null;
		try {
			citaTmp = this.citaService.updateCita(cita);
			if (citaTmp != null) {
				return new ResponseEntity<>(citaTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina una cita por id
	 * 
	 * @param id Id de la cita
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteCitaById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteCitaById");
		try {
			this.citaService.deleteCita(id);
			return new ResponseEntity<>("Cita Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una cita por id
	 * 
	 * @param id Id de la cita
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cita> getCitaById(@PathVariable("id") Integer id) throws Exception {
		log.info("getCitaById");
		Cita cita = null;
		try {
			cita = this.citaService.getCitaById(id);
			if (cita != null) {
				return new ResponseEntity<>(cita, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Retorna el listado de citas en un rango de fechas
	 * 
	 * @param startDate Fecha inicial
	 * @param endDate   Fecha final
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/by-date-range", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cita>> getCitasByRangeDate(
			@RequestParam(required = true, name = "startDate") Date startDate,
			@RequestParam(required = true, name = "endDate") Date endDate) throws Exception {
		log.info("getCitasByRangeDate");
		List<Cita> list = null;
		try {
			System.out.println(startDate);
			System.out.println(endDate);
			list = this.citaService.getCitasByRangeDates(startDate, endDate);
			if (!list.isEmpty()) {
				return new ResponseEntity<>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

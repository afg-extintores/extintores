package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Cliente;
import co.com.inventario_afg_api.services.cliente.IClienteService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Cliente}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_CLIENTES)
public class ClienteController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Servicio de acceso a la data de {@link Cliente} */
	@Autowired
	private IClienteService clienteService;

	/**
	 * <p>
	 * Retorna el listado de Clientes
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> listClientes() throws Exception {
		log.info("listClientes");
		List<Cliente> clientes = new ArrayList<Cliente>();
		try {
			clientes = this.clienteService.findAllClientsNameOrdered();
			if (!clientes.isEmpty()) {
				return new ResponseEntity<List<Cliente>>(clientes, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Clientes paginados
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/pageables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Cliente>> listClientes(Pageable pageable) throws Exception {
		log.info("listClientes");
		Page<Cliente> pageClient;
		try {
			pageClient = this.clienteService.listAllPaginados(pageable);
			if (pageClient.hasContent()) {
				return new ResponseEntity<Page<Cliente>>(pageClient, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda un cliente
	 * 
	 * @param cliente Cliente a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente) throws Exception {
		log.info("saveCliente");
		Cliente clienteTmp = null;
		try {
			clienteTmp = this.clienteService.saveCliente(cliente);
			if (clienteTmp != null) {
				return new ResponseEntity<>(clienteTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza un cliente
	 * 
	 * @param cliente Cliente a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> updateCliente(@RequestBody Cliente cliente) throws Exception {
		log.info("updateCliente");
		Cliente clienteTmp = null;
		try {
			clienteTmp = this.clienteService.updateCliente(cliente);
			if (clienteTmp != null) {
				return new ResponseEntity<>(clienteTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina un cliente por id
	 * 
	 * @param id Id del cliente
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteClienteById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteClienteById");
		try {
			this.clienteService.deleteCliente(id);
			return new ResponseEntity<>(new StringResponse("Cliente Eliminado Exitosamente"), HttpStatus.OK);
		} catch (DataIntegrityViolationException cve) {
			throw new DataIntegrityViolationException("", cve);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una cliente por id
	 * 
	 * @param id Id del cliente
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> getClienteById(@PathVariable("id") Integer id) throws Exception {
		log.info("getClienteById");
		Cliente cliente = null;
		try {
			cliente = this.clienteService.getClienteById(id);
			if (cliente != null) {
				return new ResponseEntity<>(cliente, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca un cliente por Nombre
	 * 
	 * @param nombre Nombre del Cliente
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/find-by-name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> getClienteByNombre(@PathVariable("name") String nombre) throws Exception {
		log.info("getClienteByNombre");
		Cliente cliente = null;
		try {
			cliente = this.clienteService.getClienteByNombre(nombre);
			if (cliente != null) {
				return new ResponseEntity<>(cliente, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca un cliente por NIT
	 * 
	 * @param nit NIT del Cliente
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/find-by-nit/{nit}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> getClienteByNit(@PathVariable("nit") String nit) throws Exception {
		log.info("getClienteByNit");
		Cliente cliente = null;
		try {
			cliente = this.clienteService.getClienteByNit(nit);
			if (cliente != null) {
				return new ResponseEntity<>(cliente, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca un cliente por Correo
	 * 
	 * @param correo Correo del Cliente
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/find-by-correo/{correo:.+}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> getClienteByCorreo(@PathVariable("correo") String correo) throws Exception {
		log.info("getClienteByCorreo");
		Cliente cliente = null;
		try {
			cliente = this.clienteService.getClienteByCorreo(correo);
			if (cliente != null) {
				return new ResponseEntity<>(cliente, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna los clientes que coincidan con la busqueda del nombre
	 * 
	 * @param nombre Nombre del cliente
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/find-like-nombre/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> getListClientesByNombre(@PathVariable("nombre") String nombre)
			throws Exception {
		log.info("getListClientesByNombre");
		List<Cliente> list = null;
		try {
			list = this.clienteService.getListClientesByNombre(nombre);
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Cliente>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/find-like-nombre-activos/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> getListClientesByNombreActivos(@PathVariable("nombre") String nombre)
			throws Exception {
		log.info("getListClientesByNombreActivos");
		List<Cliente> list = null;
		try {
			list = this.clienteService.getListClientesByNombreActivos(nombre);
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Cliente>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/activos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> getListClientesActivos() throws Exception {
		log.info("getListClientesActivos");
		List<Cliente> list = null;
		try {
			list = this.clienteService.getListClientesActivos();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Cliente>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

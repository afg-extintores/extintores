package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.FacturaTipoPago;
import co.com.inventario_afg_api.repositories.FacturaTipoPagoRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link FacturaTipoPago}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_FACTURAS_TIPOS_PAGO)
public class FacturaTipoPagoController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Repositorio de acceso a la data de {@link FacturaTipoPago} */
	@Autowired
	private FacturaTipoPagoRepository facturaTipoPagoRepository;

	/**
	 * <p>
	 * Retorna el listado de FacturaTipoPagoTipoPagos
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<FacturaTipoPago>> listFacturaTipoPagoTipoPagos() throws Exception {
		log.info("listFacturaTipoPagoTipoPagos");
		List<FacturaTipoPago> list = new ArrayList<>();
		try {
			list = this.facturaTipoPagoRepository.findAll();

			if (!list.isEmpty()) {
				return new ResponseEntity<List<FacturaTipoPago>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una facturaTipoPago
	 * 
	 * @param facturaTipoPago
	 *            FacturaTipoPago a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FacturaTipoPago> saveFacturaTipoPago(@RequestBody FacturaTipoPago facturaTipoPago) throws Exception {
		log.info("saveFacturaTipoPago");
		FacturaTipoPago facturaTipoPagoTmp = null;
		try {
			facturaTipoPagoTmp = this.facturaTipoPagoRepository.save(facturaTipoPago);

			if (facturaTipoPagoTmp != null) {
				return new ResponseEntity<>(facturaTipoPagoTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una facturaTipoPago
	 * 
	 * @param facturaTipoPago
	 *            FacturaTipoPago a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FacturaTipoPago> updateFacturaTipoPago(@RequestBody FacturaTipoPago facturaTipoPago) throws Exception {
		log.info("updateFacturaTipoPago");
		FacturaTipoPago facturaTipoPagoTmp = null;
		try {
			facturaTipoPagoTmp = this.facturaTipoPagoRepository.save(facturaTipoPago);

			if (facturaTipoPagoTmp != null) {
				return new ResponseEntity<>(facturaTipoPagoTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una facturaTipoPago por id
	 * 
	 * @param id Id de la facturaTipoPago
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteFacturaTipoPagoById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteFacturaTipoPagoById");		
		try {
			this.facturaTipoPagoRepository.deleteById(id);
			return new ResponseEntity<>("FacturaTipoPago Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una facturaTipoPago por id
	 * 
	 * @param id Id de la facturaTipoPago
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FacturaTipoPago> getFacturaTipoPagoById(@PathVariable("id") Integer id) throws Exception {
		log.info("getFacturaTipoPagoById");
		FacturaTipoPago facturaTipoPago = null;
		try {
			facturaTipoPago = this.facturaTipoPagoRepository.getOne(id);

			if (facturaTipoPago != null) {
				return new ResponseEntity<>(facturaTipoPago, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

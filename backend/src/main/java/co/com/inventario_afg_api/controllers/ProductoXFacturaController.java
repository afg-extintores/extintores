package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.ProductoXFactura;
import co.com.inventario_afg_api.repositories.ProductoXFacturaRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link ProductoXFactura}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_PRODUCTOS_X_FACTURA)
public class ProductoXFacturaController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Repositorio de acceso a la data de {@link ProductoXFactura} */
	@Autowired
	private ProductoXFacturaRepository productoXFacturaRepository;

	/**
	 * <p>
	 * Retorna el listado de ProductoXFacturas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductoXFactura>> listProductoXFacturas() throws Exception {
		log.info("listProductoXFacturas");
		List<ProductoXFactura> list = new ArrayList<>();
		try {
			list = this.productoXFacturaRepository.findAll();

			if (!list.isEmpty()) {
				return new ResponseEntity<List<ProductoXFactura>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una productoXFactura
	 * 
	 * @param productoXFactura
	 *            ProductoXFactura a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductoXFactura> saveProductoXFactura(@RequestBody ProductoXFactura productoXFactura) throws Exception {
		log.info("saveProductoXFactura");
		ProductoXFactura productoXFacturaTmp = null;
		try {
			productoXFacturaTmp = this.productoXFacturaRepository.save(productoXFactura);

			if (productoXFacturaTmp != null) {
				return new ResponseEntity<>(productoXFacturaTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una productoXFactura
	 * 
	 * @param productoXFactura
	 *            ProductoXFactura a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductoXFactura> updateProductoXFactura(@RequestBody ProductoXFactura productoXFactura) throws Exception {
		log.info("updateProductoXFactura");
		ProductoXFactura productoXFacturaTmp = null;
		try {
			productoXFacturaTmp = this.productoXFacturaRepository.save(productoXFactura);

			if (productoXFacturaTmp != null) {
				return new ResponseEntity<>(productoXFacturaTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una productoXFactura por id
	 * 
	 * @param id Id de la productoXFactura
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteProductoXFacturaById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteProductoXFacturaById");		
		try {
			this.productoXFacturaRepository.deleteById(id);
			return new ResponseEntity<>(new StringResponse("ProductoXFactura Eliminada Exitosamente"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una productoXFactura por id
	 * 
	 * @param id Id de la productoXFactura
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductoXFactura> getProductoXFacturaById(@PathVariable("id") Integer id) throws Exception {
		log.info("getProductoXFacturaById");
		ProductoXFactura productoXFactura = null;
		try {
			productoXFactura = this.productoXFacturaRepository.getOne(id);

			if (productoXFactura != null) {
				return new ResponseEntity<>(productoXFactura, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Retorna el listado de ProductoXFacturas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path="/by-factura/{facturaId}", produces = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<List<ProductoXFactura>> listProductoXFacturasByFactura(@PathVariable("facturaId") Integer facturaId) throws Exception {
		log.info("listProductoXFacturasByFactura");
		List<ProductoXFactura> list = new ArrayList<>();
		try {
			list = this.productoXFacturaRepository.getProductoXFacturaByFactura(facturaId);

			if (!list.isEmpty()) {
				return new ResponseEntity<List<ProductoXFactura>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina los productoXFactura por factura
	 * 
	 * @param factura Factura
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/by-factura/{facturaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteProductoXFacturaByFactura(@PathVariable("facturaId") Integer facturaId) throws Exception {
		log.info("deleteProductoXFacturaByFactura");
		try {
			this.productoXFacturaRepository.deleteByFactura(facturaId);
			return new ResponseEntity<>(new StringResponse("Productos de la Factura Eliminados Exitosamente"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

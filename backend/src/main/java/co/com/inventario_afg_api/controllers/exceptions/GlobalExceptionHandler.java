package co.com.inventario_afg_api.controllers.exceptions;

import java.sql.SQLException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandlerException(Exception e) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
		error.setMessage(e.getCause().getLocalizedMessage() == null
				? (e.getCause().getMessage() == null ? (e.getMessage() == null ? "Error server" : e.getMessage())
						: e.getCause().getMessage())
				: e.getCause().getLocalizedMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ErrorResponse> exceptionHandlerDataIntegrityException(DataIntegrityViolationException e) {
		ErrorResponse error = new ErrorResponse();
		if (e.getMostSpecificCause() instanceof SQLException) {
			SQLException sqle = (SQLException) e.getMostSpecificCause();
			error.setErrorCode(String.valueOf(sqle.getErrorCode()));
			error.setMessage(sqle.getMessage());
		} else {
			error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
			error.setMessage(e.getCause().getLocalizedMessage());
		}
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}

package co.com.inventario_afg_api.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Cotizacion;
import co.com.inventario_afg_api.services.cotizacion.ICotizacionService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Cotizacion}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_COTIZACIONES)
public class CotizacionController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private ICotizacionService cotizacionService;

	/**
	 * <p>
	 * Retorna el listado de Cotizaciones
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Cotizacion>> listCotizaciones(Pageable pageable) throws Exception {
		log.info("listCotizaciones");
		Page<Cotizacion> pageQuote;
		try {
			pageQuote = this.cotizacionService.listCotizaciones(pageable);
			if (pageQuote.hasContent()) {
				return new ResponseEntity<Page<Cotizacion>>(pageQuote, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una cotización
	 * 
	 * @param cotizacion Cotizacion a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cotizacion> saveCotizacion(@RequestBody Cotizacion cotizacion) throws Exception {
		log.info("saveCotizacion");
		Cotizacion cotizacionTmp = null;
		try {
			cotizacionTmp = this.cotizacionService.saveCotizacion(cotizacion);
			if (cotizacionTmp != null) {
				return new ResponseEntity<>(cotizacionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una Cotizacion
	 * 
	 * @param cotizacion Cotizacion a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cotizacion> updateCotizacion(@RequestBody Cotizacion cotizacion) throws Exception {
		log.info("updateCotizacion");
		Cotizacion cotizacionTmp = null;
		try {
			cotizacionTmp = this.cotizacionService.updateCotizacion(cotizacion);
			if (cotizacionTmp != null) {
				return new ResponseEntity<>(cotizacionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina una Cotizacion por id
	 * 
	 * @param id Id de la Cotizacion
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteCotizacionById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteCotizacionById");
		try {
			return new ResponseEntity<>(this.cotizacionService.deleteCotizacionById(id), HttpStatus.OK);
		} catch (DataIntegrityViolationException cve) {
			throw new DataIntegrityViolationException("", cve);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una Cotizacion por id
	 * 
	 * @param id Id de la Cotizacion
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cotizacion> getCotizacionById(@PathVariable("id") Integer id) throws Exception {
		log.info("getCotizacionById");
		Cotizacion cotizacion = null;
		try {
			cotizacion = this.cotizacionService.getCotizacionById(id);
			if (cotizacion != null) {
				return new ResponseEntity<>(cotizacion, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/generate-pdf", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> generateCotizacion(@RequestParam("id_cotizacion") Integer idCotizacion)
			throws Exception {
		log.info("generateCotizacion");
		return new ResponseEntity<>(new StringResponse(""), HttpStatus.OK);
	}

}

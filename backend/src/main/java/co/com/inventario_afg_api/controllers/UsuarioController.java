package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Usuario;
import co.com.inventario_afg_api.services.usuario.IUsuarioService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Usuario}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_USUARIOS)
public class UsuarioController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private IUsuarioService usuarioService;

	/**
	 * Retorna el listado de usuarios
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Usuario>> listUsuarios() throws Exception {
		log.info("listUsuarios");
		List<Usuario> list = new ArrayList<Usuario>();
		try {
			list = this.usuarioService.listAllUsuarios();
			if (!list.isEmpty()) {
				return new ResponseEntity<>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Retorna el listado de usuarios paginados
	 * 
	 * @param requestParams Parámetros de paginación y búsqueda
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/pageables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Usuario>> listUsuariosPaginados(@RequestParam Map<String, String> requestParams)
			throws Exception {
		log.info("listUsuariosPaginados");
		Page<Usuario> pageUser;
		try {
			pageUser = this.usuarioService.listAll(requestParams);
			if (pageUser.hasContent()) {
				return new ResponseEntity<Page<Usuario>>(pageUser, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una usuario
	 * 
	 * @param usuario Usuario a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> saveUsuario(@RequestBody Usuario usuario) throws Exception {
		log.info("saveUsuario");
		Usuario usuarioTmp = null;
		try {
			usuarioTmp = this.usuarioService.saveUser(usuario);
			if (usuarioTmp != null) {
				return new ResponseEntity<>(usuarioTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una usuario
	 * 
	 * @param usuario Usuario a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> updateUsuario(@RequestBody Usuario usuario) throws Exception {
		log.info("updateUsuario");
		Usuario usuarioTmp = null;
		try {
			usuarioTmp = this.usuarioService.updateUser(usuario);
			if (usuarioTmp != null) {
				return new ResponseEntity<>(usuarioTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina una usuario por id
	 * 
	 * @param id Id de la usuario
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteUsuarioById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteUsuarioById");
		try {
			this.usuarioService.deleteUser(id);
			return new ResponseEntity<>(new StringResponse("Usuario Eliminado Exitosamente"), HttpStatus.OK);
		} catch (DataIntegrityViolationException cve) {
			throw new DataIntegrityViolationException("", cve);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una usuario por id
	 * 
	 * @param id Id de la usuario
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> getUsuarioById(@PathVariable("id") Integer id) throws Exception {
		log.info("getUsuarioById");
		Usuario usuario = null;
		try {
			usuario = this.usuarioService.findUserById(id);
			if (usuario != null) {
				return new ResponseEntity<>(usuario, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Usuarios
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/vendedores-activos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Usuario>> listUsuariosVendedoresActivos() throws Exception {
		log.info("listUsuariosVendedoresActivos");
		List<Usuario> list = new ArrayList<>();
		try {
			list = this.usuarioService.listUsuariosVendedoresActivos();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Usuarios activos
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/activos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Usuario>> listUsuariosActivos() throws Exception {
		log.info("listUsuariosActivos");
		List<Usuario> list = new ArrayList<>();
		try {
			list = this.usuarioService.listUsuariosActivos();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza el password del usuario
	 * 
	 * @param usuario Usuario con la información del cambio de password
	 * @return
	 * @throws Exception
	 */
	@PostMapping(path = "/change-pass", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> changePassword(@RequestBody Usuario usuario) throws Exception {
		log.info("changePassword");
		boolean response = false;
		try {
			if (this.usuarioService.changePassword(usuario)) {
				response = true;
			}
		} catch (Exception e) {
			return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}

	/**
	 * <p>
	 * Busca una usuario por correo
	 * 
	 * @param correo Correo del usuario
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/by-correo/{correo:.+}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> getUsuarioByCorreo(@PathVariable("correo") String correo) throws Exception {
		log.info("getUsuarioByCorreo");
		Usuario usuario = null;
		try {
			usuario = this.usuarioService.getUsuarioByCorreo(correo);
			if (usuario != null) {
				return new ResponseEntity<>(usuario, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una usuario por documento
	 * 
	 * @param documento Documento del usuario
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/by-documento/{documento}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> getUsuarioByDocumento(@PathVariable("documento") String documento) throws Exception {
		log.info("getUsuarioByDocumento");
		Usuario usuario = null;
		try {
			usuario = this.usuarioService.getUsuarioByDocumento(documento);
			if (usuario != null) {
				return new ResponseEntity<>(usuario, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna los usuarios que coincidan con la busqueda del nombre
	 * 
	 * @param nombre Nombre del usuario
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/find-like-nombre/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Usuario>> getListUsuariosByNombre(@PathVariable("nombre") String nombre)
			throws Exception {
		log.info("getListUsuariosByNombre");
		List<Usuario> list = null;
		try {
			list = this.usuarioService.getListUsuariosByNombre(nombre);
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Usuario>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

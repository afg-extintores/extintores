package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.FacturaEstado;
import co.com.inventario_afg_api.repositories.FacturaEstadoRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link FacturaEstado}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_FACTURAS_ESTADO)
public class FacturaEstadoController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Repositorio de acceso a la data de {@link FacturaEstado} */
	@Autowired
	private FacturaEstadoRepository facturaEstadoRepository;

	/**
	 * <p>
	 * Retorna el listado de FacturaEstadoEstados
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<FacturaEstado>> listFacturaEstadoEstados() throws Exception {
		log.info("listFacturaEstadoEstados");
		List<FacturaEstado> list = new ArrayList<>();
		try {
			list = this.facturaEstadoRepository.findAll();

			if (!list.isEmpty()) {
				return new ResponseEntity<List<FacturaEstado>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una facturaEstado
	 * 
	 * @param facturaEstado
	 *            FacturaEstado a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FacturaEstado> saveFacturaEstado(@RequestBody FacturaEstado facturaEstado) throws Exception {
		log.info("saveFacturaEstado");
		FacturaEstado facturaEstadoTmp = null;
		try {
			facturaEstadoTmp = this.facturaEstadoRepository.save(facturaEstado);

			if (facturaEstadoTmp != null) {
				return new ResponseEntity<>(facturaEstadoTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una facturaEstado
	 * 
	 * @param facturaEstado
	 *            FacturaEstado a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FacturaEstado> updateFacturaEstado(@RequestBody FacturaEstado facturaEstado) throws Exception {
		log.info("updateFacturaEstado");
		FacturaEstado facturaEstadoTmp = null;
		try {
			facturaEstadoTmp = this.facturaEstadoRepository.save(facturaEstado);

			if (facturaEstadoTmp != null) {
				return new ResponseEntity<>(facturaEstadoTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una facturaEstado por id
	 * 
	 * @param id Id de la facturaEstado
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteFacturaEstadoById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteFacturaEstadoById");		
		try {
			this.facturaEstadoRepository.deleteById(id);
			return new ResponseEntity<>("FacturaEstado Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una facturaEstado por id
	 * 
	 * @param id Id de la facturaEstado
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FacturaEstado> getFacturaEstadoById(@PathVariable("id") Integer id) throws Exception {
		log.info("getFacturaEstadoById");
		FacturaEstado facturaEstado = null;
		try {
			facturaEstado = this.facturaEstadoRepository.getOne(id);

			if (facturaEstado != null) {
				return new ResponseEntity<>(facturaEstado, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.CitaEstado;
import co.com.inventario_afg_api.services.cita_estado.ICitaEstadoService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link CitaEstado}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_CITAS_ESTADOS)
public class CitaEstadoController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Servicio de acceso a la data de {@link CitaEstado} */
	@Autowired
	private ICitaEstadoService citaEstadoService;
	
	/**
	 * <p>
	 * Retorna el listado de Estados de las Citas
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CitaEstado>> listCitasEstados() throws Exception {
		log.info("listCitasEstados");
		List<CitaEstado> list = new ArrayList<>();
		try {
			list = this.citaEstadoService.listAll();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<CitaEstado>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

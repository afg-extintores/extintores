package co.com.inventario_afg_api.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.payloads.JwtAuthenticationResponse;
import co.com.inventario_afg_api.domain.payloads.LoginRequest;
import co.com.inventario_afg_api.services.auth.IAuthService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de la Autenticación de la API
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_AUTH)
public class AuthController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private IAuthService authService;

	/**
	 * <p>
	 * Valida las credenciales de usuario en el proceso de Login
	 * 
	 * @param request
	 * @return
	 */
	@PostMapping(path = ApiEndpoints.API_AUTH_LOGIN)
	public ResponseEntity<JwtAuthenticationResponse> login(@RequestBody @Valid LoginRequest request) {
		log.info("login");
		JwtAuthenticationResponse response = null;
		try {
			response = this.authService.login(request);
			if (response == null) {
				return new ResponseEntity<JwtAuthenticationResponse>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			log.info(e.getMessage());
			return new ResponseEntity<JwtAuthenticationResponse>(HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<JwtAuthenticationResponse>(response, HttpStatus.OK);
	}

}

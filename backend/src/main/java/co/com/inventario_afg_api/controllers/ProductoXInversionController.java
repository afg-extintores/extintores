package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.ProductoXInversion;
import co.com.inventario_afg_api.repositories.ProductoXInversionRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link ProductoXInversion}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_PRODUCTOS_X_INVERSION)
public class ProductoXInversionController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Repositorio de acceso a la data de {@link ProductoXInversion} */
	@Autowired
	private ProductoXInversionRepository productoXInversionRepository;

	/**
	 * <p>
	 * Retorna el listado de ProductoXInversions
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping
	public ResponseEntity<List<ProductoXInversion>> listProductoXInversions() throws Exception {
		log.info("listProductoXInversions");
		List<ProductoXInversion> list = new ArrayList<>();
		try {
			list = this.productoXInversionRepository.findAll();

			if (!list.isEmpty()) {
				return new ResponseEntity<List<ProductoXInversion>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una productoXInversion
	 * 
	 * @param productoXInversion
	 *            ProductoXInversion a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping
	public ResponseEntity<ProductoXInversion> saveProductoXInversion(@RequestBody ProductoXInversion productoXInversion) throws Exception {
		log.info("saveProductoXInversion");
		ProductoXInversion productoXInversionTmp = null;
		try {
			productoXInversionTmp = this.productoXInversionRepository.save(productoXInversion);

			if (productoXInversionTmp != null) {
				return new ResponseEntity<>(productoXInversionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una productoXInversion
	 * 
	 * @param productoXInversion
	 *            ProductoXInversion a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping
	public ResponseEntity<ProductoXInversion> updateProductoXInversion(@RequestBody ProductoXInversion productoXInversion) throws Exception {
		log.info("updateProductoXInversion");
		ProductoXInversion productoXInversionTmp = null;
		try {
			productoXInversionTmp = this.productoXInversionRepository.save(productoXInversion);

			if (productoXInversionTmp != null) {
				return new ResponseEntity<>(productoXInversionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una productoXInversion por id
	 * 
	 * @param id Id de la productoXInversion
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<String> deleteProductoXInversionById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteProductoXInversionById");		
		try {
			this.productoXInversionRepository.deleteById(id);
			return new ResponseEntity<>("ProductoXInversion Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una productoXInversion por id
	 * 
	 * @param id Id de la productoXInversion
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}")
	public ResponseEntity<ProductoXInversion> getProductoXInversionById(@PathVariable("id") Integer id) throws Exception {
		log.info("getProductoXInversionById");
		ProductoXInversion productoXInversion = null;
		try {
			productoXInversion = this.productoXInversionRepository.getOne(id);

			if (productoXInversion != null) {
				return new ResponseEntity<>(productoXInversion, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

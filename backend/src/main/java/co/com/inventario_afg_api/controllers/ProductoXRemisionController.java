package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.ProductoXRemision;
import co.com.inventario_afg_api.services.producto_x_remision.IProductoXRemisionService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de
 * {@link ProductoXRemision}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_PRODUCTOS_X_REMISION)
public class ProductoXRemisionController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private IProductoXRemisionService productoXRemisionService;

	/**
	 * <p>
	 * Retorna el listado de Productos x Remision
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductoXRemision>> listProductoXRemision() throws Exception {
		log.info("listProductoXRemision");
		List<ProductoXRemision> list = new ArrayList<>();
		try {
			list = this.productoXRemisionService.listAll();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<ProductoXRemision>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda un producto x remision
	 * 
	 * @param productoXRemision Producto x Remision a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductoXRemision> saveProductoXRemision(@RequestBody ProductoXRemision productoXRemision)
			throws Exception {
		log.info("saveProductoXRemision");
		ProductoXRemision productoXRemisionTmp = null;
		try {
			productoXRemisionTmp = this.productoXRemisionService.saveProductoXRemision(productoXRemision);
			if (productoXRemisionTmp != null) {
				return new ResponseEntity<>(productoXRemisionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> saveListProductoXRemision(@RequestBody List<ProductoXRemision> list)
			throws Exception {
		log.info("saveListProductoXRemision");
		try {
			for (ProductoXRemision pr : list) {
				this.productoXRemisionService.saveProductoXRemision(pr);
			}
			return new ResponseEntity<>(new StringResponse("Productos asociados a la remisión correctamente"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza un producto x remision
	 * 
	 * @param productoXRemision Producto x Remision a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductoXRemision> updateProductoXRemision(@RequestBody ProductoXRemision productoXRemision)
			throws Exception {
		log.info("updateProductoXRemision");
		ProductoXRemision productoXRemisionTmp = null;
		try {
			productoXRemisionTmp = this.productoXRemisionService.saveProductoXRemision(productoXRemision);
			if (productoXRemisionTmp != null) {
				return new ResponseEntity<>(productoXRemisionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina un producto_x_remision por id
	 * 
	 * @param id Id del producto_x_remision
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteProductoXRemisionById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteProductoXRemisionById");
		try {
			this.productoXRemisionService.deleteProductosXRemision(id);
			return new ResponseEntity<>("ProductoXRemision Eliminado Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca un producto_x_remision por id
	 * 
	 * @param id Id del producto_x_remision
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProductoXRemision> getProductoXRemisionById(@PathVariable("id") Integer id) throws Exception {
		log.info("getProductoXRemisionById");
		ProductoXRemision productoXRemision = null;
		try {
			productoXRemision = this.productoXRemisionService.findProductosXRemisionById(id);
			if (productoXRemision != null) {
				return new ResponseEntity<>(productoXRemision, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

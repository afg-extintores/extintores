package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import co.com.inventario_afg_api.domain.Remision;
import co.com.inventario_afg_api.services.remision.IRemisionService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.api.FileUtils;
import co.com.inventario_afg_api.utils.api.StringResponse;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Remision}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_REMISIONES)
public class RemisionController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private FileUtils fileUtils;

	@Autowired
	private IRemisionService remisionService;

	/**
	 * Retorna el listado de remisiones
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Remision>> listRemisiones() throws Exception {
		log.info("listRemisiones");
		List<Remision> list = new ArrayList<Remision>();
		try {
			list = this.remisionService.listAllRemisiones();
			if (!list.isEmpty()) {
				return new ResponseEntity<>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Remisiones
	 * 
	 * @param requestParams Parámetros de paginación y búsqueda
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/pageables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Remision>> listRemisionesPaginadas(@RequestParam Map<String, String> requestParams)
			throws Exception {
		log.info("listRemisionesPageables");
		Page<Remision> pageRemision;
		try {
			pageRemision = this.remisionService.listAll(requestParams);
			if (pageRemision.hasContent()) {
				return new ResponseEntity<Page<Remision>>(pageRemision, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Retorna el listado de remisiones que aun no est{an asociadas a una Factura
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/no-factura", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Remision>> listRemisionesNoFactura() throws Exception {
		log.info("listRemisionesNoFactura");
		try {
			List<Remision> list = this.remisionService.getRemisionesNoFactura();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Remision>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una remision
	 * 
	 * @param remision Remision a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Remision> saveRemision(@RequestBody Remision remision) throws Exception {
		log.info("saveRemision");
		Remision remisionTmp = null;
		try {
			remisionTmp = this.remisionService.saveRemision(remision);
			if (remisionTmp != null) {
				return new ResponseEntity<>(remisionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una remision
	 * 
	 * @param remision Remision a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Remision> updateRemision(@RequestBody Remision remision) throws Exception {
		log.info("updateRemision");
		Remision remisionTmp = null;
		try {
			remisionTmp = this.remisionService.saveRemision(remision);
			if (remisionTmp != null) {
				return new ResponseEntity<>(remisionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Elimina una remision por id
	 * 
	 * @param id Id de la remision
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> deleteRemisionById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteRemisionById");
		try {
			this.remisionService.deleteRemision(id);
			return new ResponseEntity<>(new StringResponse("Remision Eliminada Exitosamente"), HttpStatus.OK);
		} catch (DataIntegrityViolationException cve) {
			throw new DataIntegrityViolationException("", cve);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Busca una remision por id
	 * 
	 * @param id Id de la remision
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Remision> getRemisionById(@PathVariable("id") Integer id) throws Exception {
		log.info("getRemisionById");
		Remision remision = null;
		try {
			remision = this.remisionService.findRemisionById(id);
			if (remision != null) {
				return new ResponseEntity<>(remision, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/max-consecutive", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> getMaxConsecutiveRemisiones() throws Exception {
		log.info("getMaxConsecutiveRemisiones");
		try {
			return new ResponseEntity<>(this.remisionService.getMaxConsecutiveRemision(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StringResponse> uploadImageRemision(@RequestPart("image") MultipartFile image,
			@RequestPart("remisionId") String remisionId) throws Exception {
		log.info("uploadImageRemision");
		try {
			String url = this.remisionService.uploadRemision(this.fileUtils.multipartToFileConverter(image),
					Integer.valueOf(remisionId));
			return ResponseEntity.ok().body(new StringResponse(url));
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

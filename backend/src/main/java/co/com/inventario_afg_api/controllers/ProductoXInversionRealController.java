package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.ProductoXInversionReal;
import co.com.inventario_afg_api.repositories.ProductoXInversionRealRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link ProductoXInversionReal}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_PRODUCTOS_X_INVERSION_REAL)
public class ProductoXInversionRealController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Repositorio de acceso a la data de {@link ProductoXInversionReal} */
	@Autowired
	private ProductoXInversionRealRepository productoXInversionRealRepository;

	/**
	 * <p>
	 * Retorna el listado de ProductoXInversionReals
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping
	public ResponseEntity<List<ProductoXInversionReal>> listProductoXInversionReals() throws Exception {
		log.info("listProductoXInversionReals");
		List<ProductoXInversionReal> list = new ArrayList<>();
		try {
			list = this.productoXInversionRealRepository.findAll();

			if (!list.isEmpty()) {
				return new ResponseEntity<List<ProductoXInversionReal>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una productoXInversionReal
	 * 
	 * @param productoXInversionReal
	 *            ProductoXInversionReal a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping
	public ResponseEntity<ProductoXInversionReal> saveProductoXInversionReal(@RequestBody ProductoXInversionReal productoXInversionReal) throws Exception {
		log.info("saveProductoXInversionReal");
		ProductoXInversionReal productoXInversionRealTmp = null;
		try {
			productoXInversionRealTmp = this.productoXInversionRealRepository.save(productoXInversionReal);

			if (productoXInversionRealTmp != null) {
				return new ResponseEntity<>(productoXInversionRealTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una productoXInversionReal
	 * 
	 * @param productoXInversionReal
	 *            ProductoXInversionReal a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping
	public ResponseEntity<ProductoXInversionReal> updateProductoXInversionReal(@RequestBody ProductoXInversionReal productoXInversionReal) throws Exception {
		log.info("updateProductoXInversionReal");
		ProductoXInversionReal productoXInversionRealTmp = null;
		try {
			productoXInversionRealTmp = this.productoXInversionRealRepository.save(productoXInversionReal);

			if (productoXInversionRealTmp != null) {
				return new ResponseEntity<>(productoXInversionRealTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una productoXInversionReal por id
	 * 
	 * @param id Id de la productoXInversionReal
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<String> deleteProductoXInversionRealById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteProductoXInversionRealById");		
		try {
			this.productoXInversionRealRepository.deleteById(id);
			return new ResponseEntity<>("ProductoXInversionReal Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una productoXInversionReal por id
	 * 
	 * @param id Id de la productoXInversionReal
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}")
	public ResponseEntity<ProductoXInversionReal> getProductoXInversionRealById(@PathVariable("id") Integer id) throws Exception {
		log.info("getProductoXInversionRealById");
		ProductoXInversionReal productoXInversionReal = null;
		try {
			productoXInversionReal = this.productoXInversionRealRepository.getOne(id);

			if (productoXInversionReal != null) {
				return new ResponseEntity<>(productoXInversionReal, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

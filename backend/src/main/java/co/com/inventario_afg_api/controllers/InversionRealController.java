package co.com.inventario_afg_api.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.InversionReal;
import co.com.inventario_afg_api.repositories.InversionRealRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link InversionReal}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_INVERSIONES_REALES)
public class InversionRealController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Repositorio de acceso a la data de {@link InversionReal} */
	@Autowired
	private InversionRealRepository inversionRealRepository;

	/**
	 * <p>
	 * Retorna el listado de InversionReals
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping
	public ResponseEntity<Page<InversionReal>> listInversionReals(Pageable pageable) throws Exception {
		log.info("listInversionReals");
		Page<InversionReal> pageInversionReal;
		try {
			pageInversionReal = this.inversionRealRepository.findAll(pageable);

			if (pageInversionReal.hasContent()) {
				return new ResponseEntity<Page<InversionReal>>(pageInversionReal, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una inversionReal
	 * 
	 * @param inversionReal
	 *            InversionReal a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping
	public ResponseEntity<InversionReal> saveInversionReal(@RequestBody InversionReal inversionReal) throws Exception {
		log.info("saveInversionReal");
		InversionReal inversionRealTmp = null;
		try {
			inversionRealTmp = this.inversionRealRepository.save(inversionReal);

			if (inversionRealTmp != null) {
				return new ResponseEntity<>(inversionRealTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una inversionReal
	 * 
	 * @param inversionReal
	 *            InversionReal a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping
	public ResponseEntity<InversionReal> updateInversionReal(@RequestBody InversionReal inversionReal) throws Exception {
		log.info("updateInversionReal");
		InversionReal inversionRealTmp = null;
		try {
			inversionRealTmp = this.inversionRealRepository.save(inversionReal);

			if (inversionRealTmp != null) {
				return new ResponseEntity<>(inversionRealTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una inversionReal por id
	 * 
	 * @param id Id de la inversionReal
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<String> deleteInversionRealById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteInversionRealById");		
		try {
			this.inversionRealRepository.deleteById(id);
			return new ResponseEntity<>("InversionReal Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una inversionReal por id
	 * 
	 * @param id Id de la inversionReal
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}")
	public ResponseEntity<InversionReal> getInversionRealById(@PathVariable("id") Integer id) throws Exception {
		log.info("getInversionRealById");
		InversionReal inversionReal = null;
		try {
			inversionReal = this.inversionRealRepository.findById(id).get();

			if (inversionReal != null) {
				return new ResponseEntity<>(inversionReal, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}

package co.com.inventario_afg_api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
	
	@Autowired
	private PasswordEncoder pe;
	
	@GetMapping(path = "/index")
	public String index() throws Exception{
		System.out.println(pe.encode("12345"));
		return "Index API Inventario AFG - " + pe.encode("12345");
	}

}

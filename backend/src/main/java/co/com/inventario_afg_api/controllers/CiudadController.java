package co.com.inventario_afg_api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Ciudad;
import co.com.inventario_afg_api.services.ciudad.ICiudadService;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Ciudad}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_CIUDADES)
public class CiudadController {
	
	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Servicio de acceso a la data de {@link Ciudad} */
	@Autowired
	private ICiudadService ciudadService;
	
	/**
	 * <p>
	 * Retorna el listado de Ciudades
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Ciudad>> listCiudades() throws Exception {
		log.info("listCiudades");
		List<Ciudad> list = new ArrayList<>();
		try {
			list = this.ciudadService.listAll();
			if (!list.isEmpty()) {
				return new ResponseEntity<List<Ciudad>>(list, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una ciudad por su Id
	 * 
	 * @param id Id de la ciudad
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Ciudad> getCiudadById(@PathVariable("id") Integer id) throws Exception {
		log.info("getCiudadById");
		Ciudad city = null;
		try {
			city = this.ciudadService.getCiudadById(id);
			if (city != null) {
				return new ResponseEntity<>(city, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

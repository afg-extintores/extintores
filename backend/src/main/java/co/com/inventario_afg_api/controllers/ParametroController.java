package co.com.inventario_afg_api.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Parametro;
import co.com.inventario_afg_api.repositories.ParametroRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Parametro}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_PARAMETROS)
public class ParametroController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	/** Repositorio de acceso a la data de {@link Parametro} */
	@Autowired
	private ParametroRepository parametroRepository;

	/**
	 * <p>
	 * Busca un parámetro por su nombre
	 * 
	 * @param nombre
	 *            Nombre del parámetro
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/by-name/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Parametro> getParametroByName(@PathVariable("nombre") String nombre) throws Exception {
		log.info("getParametroByName");
		Parametro param = null;
		try {
			param = this.parametroRepository.findById(nombre).get();

			if (param != null) {
				return new ResponseEntity<>(param, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza el valor de un parámetro
	 * 
	 * @param parametro
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Parametro> updateParametroValue(@RequestBody Parametro parametro) throws Exception {
		log.info("updateParametroValue");
		Parametro param = null;
		try {
			param = this.parametroRepository.save(parametro);

			if (param != null) {
				return new ResponseEntity<>(param, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

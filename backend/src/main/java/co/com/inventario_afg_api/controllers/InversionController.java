package co.com.inventario_afg_api.controllers;

import co.com.inventario_afg_api.services.inversion.IInversionService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.inventario_afg_api.domain.Inversion;
import co.com.inventario_afg_api.repositories.InversionRepository;
import co.com.inventario_afg_api.utils.api.ApiEndpoints;
import co.com.inventario_afg_api.utils.log.Loggable;

import java.util.List;

/**
 * <p>
 * Controller de los servicios para la administración de {@link Inversion}
 * 
 * @author Miguel Romero
 */
@RestController
@RequestMapping(path = ApiEndpoints.API_INVERSIONES)
public class InversionController {

	/** Log de la Aplicación */
	@Loggable
	private Logger log;

	@Autowired
	private IInversionService inversionService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Inversion>> listInversiones() {
		log.info("listInversiones");
		List<Inversion> list;
		try {
			list = this.inversionService.listAllInversiones();
			if (!list.isEmpty()) {
				return new ResponseEntity<>(list, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Retorna el listado de Inversions
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/pageables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Inversion>> listInversionsPageables(Pageable pageable) throws Exception {
		log.info("listInversionsPageables");
		Page<Inversion> pageInversion;
		try {
			pageInversion = this.inversionService.listInversiones(pageable);
			if (pageInversion.hasContent()) {
				return new ResponseEntity<Page<Inversion>>(pageInversion, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Guarda una inversion
	 * 
	 * @param inversion
	 *            Inversion a guardar
	 * @return
	 * @throws Exception
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Inversion> saveInversion(@RequestBody Inversion inversion) throws Exception {
		log.info("saveInversion");
		Inversion inversionTmp = null;
		try {
			inversionTmp = this.inversionService.saveInversion(inversion);
			if (inversionTmp != null) {
				return new ResponseEntity<>(inversionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * <p>
	 * Actualiza una inversion
	 * 
	 * @param inversion
	 *            Inversion a actualizar
	 * @return
	 * @throws Exception
	 */
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Inversion> updateInversion(@RequestBody Inversion inversion) throws Exception {
		log.info("updateInversion");
		Inversion inversionTmp = null;
		try {
			inversionTmp = this.inversionService.updateInversion(inversion);
			if (inversionTmp != null) {
				return new ResponseEntity<>(inversionTmp, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Elimina una inversion por id
	 * 
	 * @param id Id de la inversion
	 * @return
	 * @throws Exception
	 */
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteInversionById(@PathVariable("id") Integer id) throws Exception {
		log.info("deleteInversionById");		
		try {
			this.inversionService.deleteInversion(id);
			return new ResponseEntity<>("Inversion Eliminada Exitosamente", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * <p>
	 * Busca una inversion por id
	 * 
	 * @param id Id de la inversion
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Inversion> getInversionById(@PathVariable("id") Integer id) throws Exception {
		log.info("getInversionById");
		Inversion inversion = null;
		try {
			inversion = this.inversionService.getInversionById(id);
			if (inversion != null) {
				return new ResponseEntity<>(inversion, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}

package co.com.inventario_afg_api.utils.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import co.com.inventario_afg_api.utils.log.Loggable;

@Component
public class FileUtils {
	
	@Loggable
	private Logger log;
	
	public File multipartToFileConverter(MultipartFile multipart) throws IOException {
		File convFile = new File(multipart.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(multipart.getBytes());
	    fos.close(); 
	    return convFile;
	}

}

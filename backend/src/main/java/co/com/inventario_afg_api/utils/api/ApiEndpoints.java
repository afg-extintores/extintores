package co.com.inventario_afg_api.utils.api;

/**
 * <p>
 * Configuración de los Endpoints de la API
 * 
 * @author Miguel Romero
 */
public class ApiEndpoints {
	
	/** Constante con el nombre de la API */
	public static final String API_NAME = "/api";
	
	/** Constante con la versión de la API */
	public static final String API_VERSION = ApiEndpoints.API_NAME + "/v1";
	
	/** Constante de Endpoint Autenticacion general de sistema */
	public static final String API_AUTH = ApiEndpoints.API_VERSION + "/auth";
	/** Constante de Endpoint Autenticacion Logueo en el sistema */
	public static final String API_AUTH_LOGIN = "/login";
	/** Constante Endpoint Autenticacion Salir del sistema */
	public static final String API_AUTH_LOGOUT = "/logout";
	
	/** Constante Endpoints Citas */
	public static final String API_CITAS = ApiEndpoints.API_VERSION + "/citas";
	
	/** Constante Endpoints Citas */
	public static final String API_CITAS_ESTADOS = ApiEndpoints.API_VERSION + "/citas-estados";
	
	/** Constante Endpoints Clientes */
	public static final String API_CLIENTES = ApiEndpoints.API_VERSION + "/clientes";
	
	/** Constante Endpoints Ciudades */
	public static final String API_CIUDADES = ApiEndpoints.API_VERSION + "/ciudades";
	
	/** Constante Endpoints Cliente Dirección */
	public static final String API_CLIENTES_DIRECCIONES = ApiEndpoints.API_VERSION + "/clientes-direcciones";
	
	/** Constante Endpoints Cotizaciones */
	public static final String API_COTIZACIONES = ApiEndpoints.API_VERSION + "/cotizaciones";
	
	/** Constante Endpoints Remisiones */
	public static final String API_REMISIONES = ApiEndpoints.API_VERSION + "/remisiones";
	/** Constante Endpoints Productos x Remision */
	public static final String API_PRODUCTOS_X_REMISION = ApiEndpoints.API_VERSION + "/productos-x-remision";
	
	/** Constante Endpoints Facturas */
	public static final String API_FACTURAS = ApiEndpoints.API_VERSION + "/facturas";
	public static final String API_FACTURAS_ESTADO = ApiEndpoints.API_VERSION + "/facturas-estados";
	public static final String API_FACTURAS_TIPOS_PAGO = ApiEndpoints.API_VERSION + "/facturas-tipos-pago";

	/** Constante Endpoints Inversiones */
	public static final String API_INVERSIONES = ApiEndpoints.API_VERSION + "/inversiones";
	
	/** Constante Endpoints Inversiones Reales */
	public static final String API_INVERSIONES_REALES = ApiEndpoints.API_VERSION + "/inversiones-reales";
	
	/** Constante Endpoints Parametros */
	public static final String API_PARAMETROS = ApiEndpoints.API_VERSION + "/parametros";
	
	/** Constante Endpoints Productos */
	public static final String API_PRODUCTOS = ApiEndpoints.API_VERSION + "/productos";
	
	/** Constante Endpoints Productos x Factura */
	public static final String API_PRODUCTOS_X_FACTURA = ApiEndpoints.API_VERSION + "/productos-x-factura";
	
	/** Constante Endpoints Productos x Inversión */
	public static final String API_PRODUCTOS_X_INVERSION = ApiEndpoints.API_VERSION + "/productos-x-inversion";
	
	/** Constante Endpoints Productos x Inversión Real */
	public static final String API_PRODUCTOS_X_INVERSION_REAL = ApiEndpoints.API_VERSION + "/productos-x-inversion-real";
	
	/** Constante Endpoints Roles */
	public static final String API_ROLES = ApiEndpoints.API_VERSION + "/roles";
	
	/** Constante Endpoints Servicios */
	public static final String API_SERVICIOS = ApiEndpoints.API_VERSION + "/servicios";
	
	/** Constante Endpoints Servicios X Cotización */
	public static final String API_SERVICIOS_X_COTIZACION = ApiEndpoints.API_VERSION + "/servicios-x-cotizacion";
	
	/** Constante Endpoints Usuarios */
	public static final String API_USUARIOS = ApiEndpoints.API_VERSION + "/usuarios";
	
}

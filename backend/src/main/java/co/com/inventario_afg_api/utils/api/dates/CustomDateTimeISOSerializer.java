package co.com.inventario_afg_api.utils.api.dates;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class CustomDateTimeISOSerializer extends JsonSerializer<Date> {

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException, JsonProcessingException {

		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

		String formattedDate = outputFormat.format(value);
		gen.writeString(formattedDate);

	}

}

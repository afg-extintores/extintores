package co.com.inventario_afg_api.utils.api.dates;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class CustomDateTimeSerializer extends JsonSerializer<Date> {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	@Override
	public void serialize(Date fecha, JsonGenerator gen, SerializerProvider serializers)
			throws IOException, JsonProcessingException {

		String formattedDate = dateFormat.format(fecha);
		gen.writeString(formattedDate);

	}

}

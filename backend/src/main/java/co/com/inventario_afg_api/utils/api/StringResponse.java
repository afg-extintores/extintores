package co.com.inventario_afg_api.utils.api;

/**
 * <p>
 * Configuración de luna respuesta como String
 * 
 * @author Miguel Romero
 */
public class StringResponse {

	/** Atributo response - String de la respuesta */
	private String response;

	/**
	* Metodo constructor de la clase StringResponse.java
	*/
	public StringResponse(String s) {
		this.response = s;
	}

	/**
	 * Metodo que retorna el atributo response
	 * @return : response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * Metodo que modifica el atributo response
	 * @param : response
	 */
	public void setResponse(String response) {
		this.response = response;
	}

}

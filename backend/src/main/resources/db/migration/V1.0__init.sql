# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Base de datos: inventario_afg
# Tiempo de Generación: 2019-08-24 03:48:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla citas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `citas`;

CREATE TABLE `citas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la cita',
  `id_cliente` int(10) unsigned NOT NULL COMMENT 'Id del cliente',
  `id_cliente_direccion` int(10) unsigned DEFAULT NULL COMMENT 'Id de la dirección del cliente',
  `fecha_llamada_cita` datetime DEFAULT NULL COMMENT 'Fecha de la Llamada para la Cita',
  `fecha_cita` datetime DEFAULT NULL COMMENT 'Fecha de la Cita',
  `quien_llamo` int(10) unsigned NOT NULL COMMENT 'Id del usuario de la persona que llamo para sacar la cita',
  `quien_fue` int(10) unsigned DEFAULT NULL COMMENT 'Id del usuario de la persona que fue a la cita',
  `observaciones` text COMMENT 'Observaciones respecto a la llamada',
  `id_estado` int(10) unsigned DEFAULT NULL COMMENT 'Id del estado de la cita',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_citas_clientes_idx` (`id_cliente`),
  KEY `fk_citas_usuario_llamo_idx` (`quien_llamo`),
  KEY `fk_citas_usuario_fue_idx` (`quien_fue`),
  KEY `fk_citas_estado_idx` (`id_estado`),
  KEY `fk_citas_direc_cli_idx` (`id_cliente_direccion`),
  CONSTRAINT `fk_citas_clientes` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_citas_direc_cli` FOREIGN KEY (`id_cliente_direccion`) REFERENCES `clientes_direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_citas_estado` FOREIGN KEY (`id_estado`) REFERENCES `citas_estados` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_citas_usuario_fue` FOREIGN KEY (`quien_fue`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_citas_usuario_llamo` FOREIGN KEY (`quien_llamo`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de las citas';



# Volcado de tabla citas_estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `citas_estados`;

CREATE TABLE `citas_estados` (
  `id` int(10) unsigned NOT NULL COMMENT 'Id del estado de la cita',
  `nombre` varchar(30) DEFAULT NULL COMMENT 'Nombre del estado de la cita',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena los estados de las citas';



# Volcado de tabla ciudades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ciudades`;

CREATE TABLE `ciudades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la ciudad',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre de la Ciudad',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena las ciudades de trabajo de AFG';



# Volcado de tabla clientes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del cliente',
  `nombre` varchar(150) NOT NULL COMMENT 'Nombre del Cliente - Empresa',
  `nombre_contacto` varchar(150) NOT NULL COMMENT 'Nombre del Contacto',
  `nit` varchar(50) NOT NULL COMMENT 'Nit del Cliente',
  `telefono` varchar(75) DEFAULT NULL COMMENT 'Telefono del Cliente',
  `correo` varchar(150) DEFAULT NULL COMMENT 'Correo del cliente',
  `our_client` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indica si el cliente ya es un cliente',
  `activo` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Estado del cliente',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`),
  UNIQUE KEY `nombre_contacto_UNIQUE` (`nombre_contacto`),
  UNIQUE KEY `nit_UNIQUE` (`nit`),
  UNIQUE KEY `correo_UNIQUE` (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de los Clientes';

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;

INSERT INTO `clientes` (`id`, `nombre`, `nombre_contacto`, `nit`, `telefono`, `correo`, `our_client`, `activo`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,'Cliente1','Nombre Cliente1','102030','7410258','cliente1@gmail.com',1,1,'Admin Admin','2019-03-03 20:39:18',NULL,NULL);

/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla clientes_direcciones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientes_direcciones`;

CREATE TABLE `clientes_direcciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la Dirección del cliente',
  `id_cliente` int(10) unsigned NOT NULL COMMENT 'Id del cliente',
  `direccion` varchar(100) NOT NULL COMMENT 'Direccion del Cliente',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `direccion_UNIQUE` (`direccion`),
  KEY `fk_cliente_clidireccion_idx` (`id_cliente`),
  CONSTRAINT `fk_cliente_clidireccion` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información d elas direcciones del cliente';

LOCK TABLES `clientes_direcciones` WRITE;
/*!40000 ALTER TABLE `clientes_direcciones` DISABLE KEYS */;

INSERT INTO `clientes_direcciones` (`id`, `id_cliente`, `direccion`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,1,'Calle 100 Of 101','Admin Admin','2019-03-03 20:39:59',NULL,NULL);

/*!40000 ALTER TABLE `clientes_direcciones` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla cotizaciones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cotizaciones`;

CREATE TABLE `cotizaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la cotización',
  `id_cliente` int(10) unsigned NOT NULL COMMENT 'Id del cliente al que se le envía la cotización',
  `id_cliente_direccion` int(10) unsigned NOT NULL COMMENT 'Id de la dirección del cliente',
  `id_usuario` int(10) unsigned NOT NULL COMMENT 'Id del usuario que genera la cotización',
  `fecha` date NOT NULL COMMENT 'Fecha de la cotización',
  `consecutivo` varchar(10) NOT NULL COMMENT 'Consecutivo de la cotización',
  `nota` varchar(250) DEFAULT NULL COMMENT 'Nota de la cotización',
  `subtotal` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Subtotal de la cotización',
  `iva` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'IVA de la cotización',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Total de la cotización',
  `forma_pago` varchar(40) DEFAULT NULL COMMENT 'Forma de pago de la cotización',
  `oferta` varchar(25) DEFAULT NULL COMMENT 'Oferta de la cotización',
  `entrega` varchar(25) DEFAULT NULL COMMENT 'Tiempo de entrega de la cotización',
  `incluir_documentos` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flag que marca si la cotización anexa documentos',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Id del usuario que genera la cotización',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `consecutivo_UNIQUE` (`consecutivo`),
  KEY `fk_cotizaciones_clientes_idx` (`id_cliente`),
  KEY `fk_cotizaciones_clidireccion_idx` (`id_cliente_direccion`),
  KEY `fk_cotizaciones_usuarios_idx` (`id_usuario`),
  CONSTRAINT `fk_cotizaciones_clidireccion` FOREIGN KEY (`id_cliente_direccion`) REFERENCES `clientes_direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotizaciones_clientes` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotizaciones_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de las cotizaciones generadas';



# Volcado de tabla divipola
# ------------------------------------------------------------

DROP TABLE IF EXISTS `divipola`;

CREATE TABLE `divipola` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la Divipola',
  `codigo_depto` int(11) NOT NULL COMMENT 'Código del Departamento',
  `codigo_mipio` int(11) NOT NULL COMMENT 'Código del Municipio',
  `departamento` varchar(75) NOT NULL COMMENT 'Nombre del Departamento',
  `municipio` varchar(150) NOT NULL COMMENT 'Nombre del Municipio',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de la Divipola de Colombia';

LOCK TABLES `divipola` WRITE;
/*!40000 ALTER TABLE `divipola` DISABLE KEYS */;

INSERT INTO `divipola` (`id`, `codigo_depto`, `codigo_mipio`, `departamento`, `municipio`)
VALUES
	(1,5,5002,'Antioquia','Abejorral'),
	(2,54,54003,'Norte de Santander','Ábrego'),
	(3,5,5004,'Antioquia','Abriaquí'),
	(4,50,50006,'Meta','Acacías'),
	(5,27,27006,'Chocó','Acandí'),
	(6,41,41006,'Huila','Acevedo'),
	(7,13,13006,'Bolívar','Achí'),
	(8,41,41013,'Huila','Agrado'),
	(9,20,20011,'Cesar','Aguachica'),
	(10,68,68013,'Santander','Aguada'),
	(11,17,17013,'Caldas','Aguadas'),
	(12,85,85010,'Casanare','Aguazul'),
	(13,41,41016,'Huila','Aipe'),
	(14,25,25019,'Cundinamarca','Albán'),
	(15,52,52019,'Nariño','Albán'),
	(16,18,18029,'Caquetá','Albania'),
	(17,44,44035,'La Guajira','Albania'),
	(18,68,68020,'Santander','Albania'),
	(19,76,76020,'Valle del Cauca','Alcalá'),
	(20,52,52022,'Nariño','Aldana'),
	(21,5,5021,'Antioquia','Alejandría'),
	(22,47,47030,'Magdalena','Algarrobo'),
	(23,41,41020,'Huila','Algeciras'),
	(24,19,19022,'Cauca','Almaguer'),
	(25,15,15022,'Boyacá','Almeida'),
	(26,73,73024,'Tolima','Alpujarra'),
	(27,41,41026,'Huila','Altamira'),
	(28,73,73026,'Tolima','Alvarado'),
	(29,5,5030,'Antioquia','Amagá'),
	(30,5,5031,'Antioquia','Amalfi'),
	(31,73,73030,'Tolima','Ambalema'),
	(32,25,25035,'Cundinamarca','Anapoima'),
	(33,52,52036,'Nariño','Ancuyá'),
	(34,76,76036,'Valle del Cauca','Andalucía'),
	(35,5,5034,'Antioquia','Andes'),
	(36,5,5036,'Antioquia','Angelópolis'),
	(37,5,5038,'Antioquia','Angostura'),
	(38,25,25040,'Cundinamarca','Anolaima'),
	(39,5,5040,'Antioquia','Anorí'),
	(40,17,17042,'Caldas','Anserma'),
	(41,76,76041,'Valle del Cauca','Ansermanuevo'),
	(42,5,5044,'Antioquia','Anzá'),
	(43,73,73043,'Tolima','Anzoátegui'),
	(44,5,5045,'Antioquia','Apartadó'),
	(45,66,66045,'Risaralda','Apía'),
	(46,25,25599,'Cundinamarca','Apulo'),
	(47,15,15047,'Boyacá','Aquitania'),
	(48,47,47053,'Magdalena','Aracataca'),
	(49,17,17050,'Caldas','Aranzazu'),
	(50,68,68051,'Santander','Aratoca'),
	(51,81,81001,'Arauca','Arauca'),
	(52,81,81065,'Arauca','Arauquita'),
	(53,25,25053,'Cundinamarca','Arbeláez'),
	(54,52,52051,'Nariño','Arboleda'),
	(55,54,54051,'Norte de Santander','Arboledas'),
	(56,5,5051,'Antioquia','Arboletes'),
	(57,15,15051,'Boyacá','Arcabuco'),
	(58,13,13042,'Bolívar','Arenal'),
	(59,5,5055,'Antioquia','Argelia'),
	(60,19,19050,'Cauca','Argelia'),
	(61,76,76054,'Valle del Cauca','Argelia'),
	(62,47,47058,'Magdalena','Ariguaní'),
	(63,13,13052,'Bolívar','Arjona'),
	(64,5,5059,'Antioquia','Armenia'),
	(65,63,63001,'Quindio','Armenia'),
	(66,13,13062,'Bolívar','Arroyohondo'),
	(67,20,20032,'Cesar','Astrea'),
	(68,73,73067,'Tolima','Ataco'),
	(69,27,27050,'Chocó','Atrato'),
	(70,23,23068,'Córdoba','Ayapel'),
	(71,27,27073,'Chocó','Bagadó'),
	(72,19,19075,'Cauca','Balboa'),
	(73,66,66075,'Risaralda','Balboa'),
	(74,8,8078,'Atlántico','Baranoa'),
	(75,41,41078,'Huila','Baraya'),
	(76,52,52079,'Nariño','Barbacoas'),
	(77,5,5079,'Antioquia','Barbosa'),
	(78,68,68077,'Santander','Barbosa'),
	(79,68,68079,'Santander','Barichara'),
	(80,68,68081,'Santander','Barrancabermeja'),
	(81,44,44078,'La Guajira','Barrancas'),
	(82,8,8001,'Atlántico','Barranquilla'),
	(83,20,20045,'Cesar','Becerril'),
	(84,17,17088,'Caldas','Belalcázar'),
	(85,15,15087,'Boyacá','Belén'),
	(86,52,52083,'Nariño','Belén'),
	(87,5,5088,'Antioquia','Bello'),
	(88,5,5086,'Antioquia','Belmira'),
	(89,25,25086,'Cundinamarca','Beltrán'),
	(90,15,15090,'Boyacá','Berbeo'),
	(91,5,5091,'Antioquia','Betania'),
	(92,15,15092,'Boyacá','Betéitiva'),
	(93,5,5093,'Antioquia','Betulia'),
	(94,68,68092,'Santander','Betulia'),
	(95,25,25095,'Cundinamarca','Bituima'),
	(96,15,15097,'Boyacá','Boavita'),
	(97,54,54099,'Norte de Santander','Bochalema'),
	(98,25,25099,'Cundinamarca','Bojacá'),
	(99,27,27099,'Chocó','Bojayá'),
	(100,19,19100,'Cauca','Bolívar'),
	(101,68,68101,'Santander','Bolívar'),
	(102,76,76100,'Valle del Cauca','Bolívar'),
	(103,20,20060,'Cesar','Bosconia'),
	(104,15,15104,'Boyacá','Boyacá'),
	(105,5,5107,'Antioquia','Briceño'),
	(106,15,15106,'Boyacá','Briceño'),
	(107,68,68001,'Santander','Bucaramanga'),
	(108,54,54109,'Norte de Santander','Bucarasica'),
	(109,76,76109,'Valle del Cauca','Buenaventura'),
	(110,15,15109,'Boyacá','Buenavista'),
	(111,23,23079,'Córdoba','Buenavista'),
	(112,63,63111,'Quindio','Buenavista'),
	(113,70,70110,'Sucre','Buenavista'),
	(114,52,52110,'Nariño','Buesaco'),
	(115,76,76113,'Valle del Cauca','Bugalagrande'),
	(116,5,5113,'Antioquia','Buriticá'),
	(117,15,15114,'Boyacá','Busbanzá'),
	(118,25,25120,'Cundinamarca','Cabrera'),
	(119,68,68121,'Santander','Cabrera'),
	(120,50,50124,'Meta','Cabuyaro'),
	(121,94,94886,'Guainía','Cacahual'),
	(122,5,5120,'Antioquia','Cáceres'),
	(123,25,25123,'Cundinamarca','Cachipay'),
	(124,54,54128,'Norte de Santander','Cáchira'),
	(125,54,54125,'Norte de Santander','Cácota'),
	(126,5,5125,'Antioquia','Caicedo'),
	(127,76,76122,'Valle del Cauca','Caicedonia'),
	(128,70,70124,'Sucre','Caimito'),
	(129,73,73124,'Tolima','Cajamarca'),
	(130,19,19130,'Cauca','Cajibío'),
	(131,25,25126,'Cundinamarca','Cajicá'),
	(132,13,13140,'Bolívar','Calamar'),
	(133,95,95015,'Guaviare','Calamar'),
	(134,63,63130,'Quindio','Calarcá'),
	(135,5,5129,'Antioquia','Caldas'),
	(136,15,15131,'Boyacá','Caldas'),
	(137,19,19137,'Cauca','Caldono'),
	(138,76,76001,'Valle del Cauca','Cali'),
	(139,68,68132,'Santander','California'),
	(140,76,76126,'Valle del Cauca','Calima'),
	(141,19,19142,'Cauca','Caloto'),
	(142,5,5134,'Antioquia','Campamento'),
	(143,41,41132,'Huila','Campoalegre'),
	(144,15,15135,'Boyacá','Campohermoso'),
	(145,23,23090,'Córdoba','Canalete'),
	(146,5,5138,'Antioquia','Cañasgordas'),
	(147,8,8141,'Atlántico','Candelaria'),
	(148,76,76130,'Valle del Cauca','Candelaria'),
	(149,13,13160,'Bolívar','Cantagallo'),
	(150,25,25148,'Cundinamarca','Caparrapí'),
	(151,68,68147,'Santander','Capitanejo'),
	(152,25,25151,'Cundinamarca','Cáqueza'),
	(153,5,5142,'Antioquia','Caracolí'),
	(154,5,5145,'Antioquia','Caramanta'),
	(155,68,68152,'Santander','Carcasí'),
	(156,5,5147,'Antioquia','Carepa'),
	(157,5,5150,'Antioquia','Carolina'),
	(158,76,76147,'Valle del Cauca','Cartago'),
	(159,97,97161,'Vaupés','Carurú'),
	(160,73,73152,'Tolima','Casabianca'),
	(161,5,5154,'Antioquia','Caucasia'),
	(162,68,68160,'Santander','Cepitá'),
	(163,23,23162,'Córdoba','Cereté'),
	(164,15,15162,'Boyacá','Cerinza'),
	(165,68,68162,'Santander','Cerrito'),
	(166,27,27160,'Chocó','Cértegui'),
	(167,52,52240,'Nariño','Chachagüí'),
	(168,25,25168,'Cundinamarca','Chaguaní'),
	(169,70,70230,'Sucre','Chalán'),
	(170,85,85015,'Casanare','Chámeza'),
	(171,73,73168,'Tolima','Chaparral'),
	(172,68,68167,'Santander','Charalá'),
	(173,68,68169,'Santander','Charta'),
	(174,25,25175,'Cundinamarca','Chía'),
	(175,5,5172,'Antioquia','Chigorodó'),
	(176,68,68176,'Santander','Chima'),
	(177,23,23168,'Córdoba','Chimá'),
	(178,20,20175,'Cesar','Chimichagua'),
	(179,54,54172,'Norte de Santander','Chinácota'),
	(180,15,15172,'Boyacá','Chinavita'),
	(181,17,17174,'Caldas','Chinchiná'),
	(182,23,23182,'Córdoba','Chinú'),
	(183,25,25178,'Cundinamarca','Chipaque'),
	(184,68,68179,'Santander','Chipatá'),
	(185,15,15176,'Boyacá','Chiquinquirá'),
	(186,15,15232,'Boyacá','Chíquiza'),
	(187,20,20178,'Cesar','Chiriguaná'),
	(188,15,15180,'Boyacá','Chiscas'),
	(189,15,15183,'Boyacá','Chita'),
	(190,54,54174,'Norte de Santander','Chitagá'),
	(191,15,15185,'Boyacá','Chitaraque'),
	(192,15,15187,'Boyacá','Chivatá'),
	(193,47,47170,'Magdalena','Chivolo'),
	(194,15,15236,'Boyacá','Chivor'),
	(195,25,25181,'Cundinamarca','Choachí'),
	(196,25,25183,'Cundinamarca','Chocontá'),
	(197,13,13188,'Bolívar','Cicuco'),
	(198,47,47189,'Magdalena','Ciénaga'),
	(199,15,15189,'Boyacá','Ciénega'),
	(200,68,68190,'Santander','Cimitarra'),
	(201,63,63190,'Quindio','Circasia'),
	(202,5,5190,'Antioquia','Cisneros'),
	(203,13,13222,'Bolívar','Clemencia'),
	(204,5,5197,'Antioquia','Cocorná'),
	(205,73,73200,'Tolima','Coello'),
	(206,25,25200,'Cundinamarca','Cogua'),
	(207,41,41206,'Huila','Colombia'),
	(208,52,52203,'Nariño','Colón'),
	(209,86,86219,'Putumayo','Colón'),
	(210,70,70204,'Sucre','Coloso'),
	(211,15,15204,'Boyacá','Cómbita'),
	(212,5,5206,'Antioquia','Concepción'),
	(213,68,68207,'Santander','Concepción'),
	(214,5,5209,'Antioquia','Concordia'),
	(215,47,47205,'Magdalena','Concordia'),
	(216,27,27205,'Chocó','Condoto'),
	(217,68,68209,'Santander','Confines'),
	(218,52,52207,'Nariño','Consacá'),
	(219,52,52210,'Nariño','Contadero'),
	(220,68,68211,'Santander','Contratación'),
	(221,54,54206,'Norte de Santander','Convención'),
	(222,5,5212,'Antioquia','Copacabana'),
	(223,15,15212,'Boyacá','Coper'),
	(224,13,13212,'Bolívar','Córdoba'),
	(225,52,52215,'Nariño','Córdoba'),
	(226,63,63212,'Quindio','Córdoba'),
	(227,19,19212,'Cauca','Corinto'),
	(228,68,68217,'Santander','Coromoro'),
	(229,70,70215,'Sucre','Corozal'),
	(230,15,15215,'Boyacá','Corrales'),
	(231,25,25214,'Cundinamarca','Cota'),
	(232,23,23300,'Córdoba','Cotorra'),
	(233,15,15218,'Boyacá','Covarachía'),
	(234,70,70221,'Sucre','Coveñas'),
	(235,73,73217,'Tolima','Coyaima'),
	(236,52,52224,'Nariño','Cuaspúd'),
	(237,15,15223,'Boyacá','Cubará'),
	(238,15,15224,'Boyacá','Cucaita'),
	(239,25,25224,'Cundinamarca','Cucunubá'),
	(240,54,54001,'Norte de Santander','Cúcuta'),
	(241,54,54223,'Norte de Santander','Cucutilla'),
	(242,15,15226,'Boyacá','Cuítiva'),
	(243,50,50226,'Meta','Cumaral'),
	(244,99,99773,'Vichada','Cumaribo'),
	(245,52,52227,'Nariño','Cumbal'),
	(246,52,52233,'Nariño','Cumbitara'),
	(247,73,73226,'Tolima','Cunday'),
	(248,18,18205,'Caquetá','Curillo'),
	(249,68,68229,'Santander','Curití'),
	(250,20,20228,'Cesar','Curumaní'),
	(251,5,5234,'Antioquia','Dabeiba'),
	(252,76,76233,'Valle del Cauca','Dagua'),
	(253,44,44090,'La Guajira','Dibulla'),
	(254,44,44098,'La Guajira','Distracción'),
	(255,73,73236,'Tolima','Dolores'),
	(256,5,5237,'Antioquia','Donmatías'),
	(257,66,66170,'Risaralda','Dosquebradas'),
	(258,15,15238,'Boyacá','Duitama'),
	(259,54,54239,'Norte de Santander','Durania'),
	(260,5,5240,'Antioquia','Ebéjico'),
	(261,20,20238,'Cesar','El Copey'),
	(262,18,18247,'Caquetá','El Doncello'),
	(263,50,50270,'Meta','El Dorado'),
	(264,76,76250,'Valle del Cauca','El Dovio'),
	(265,41,41244,'Huila','Elías'),
	(266,68,68264,'Santander','Encino'),
	(267,68,68266,'Santander','Enciso'),
	(268,5,5264,'Antioquia','Entrerríos'),
	(269,5,5266,'Antioquia','Envigado'),
	(270,73,73268,'Tolima','Espinal'),
	(271,25,25269,'Cundinamarca','Facatativá'),
	(272,73,73270,'Tolima','Falan'),
	(273,17,17272,'Caldas','Filadelfia'),
	(274,63,63272,'Quindio','Filandia'),
	(275,15,15272,'Boyacá','Firavitoba'),
	(276,73,73275,'Tolima','Flandes'),
	(277,18,18001,'Caquetá','Florencia'),
	(278,19,19290,'Cauca','Florencia'),
	(279,15,15276,'Boyacá','Floresta'),
	(280,68,68271,'Santander','Florián'),
	(281,76,76275,'Valle del Cauca','Florida'),
	(282,68,68276,'Santander','Floridablanca'),
	(283,25,25279,'Cundinamarca','Fómeque'),
	(284,44,44279,'La Guajira','Fonseca'),
	(285,81,81300,'Arauca','Fortul'),
	(286,25,25281,'Cundinamarca','Fosca'),
	(287,5,5282,'Antioquia','Fredonia'),
	(288,73,73283,'Tolima','Fresno'),
	(289,5,5284,'Antioquia','Frontino'),
	(290,47,47288,'Magdalena','Fundación'),
	(291,52,52287,'Nariño','Funes'),
	(292,25,25286,'Cundinamarca','Funza'),
	(293,25,25288,'Cundinamarca','Fúquene'),
	(294,25,25290,'Cundinamarca','Fusagasugá'),
	(295,25,25293,'Cundinamarca','Gachalá'),
	(296,25,25295,'Cundinamarca','Gachancipá'),
	(297,15,15248,'Boyacá','El Espino'),
	(298,15,15293,'Boyacá','Gachantivá'),
	(299,25,25297,'Cundinamarca','Gachetá'),
	(300,68,68296,'Santander','Galán'),
	(301,8,8296,'Atlántico','Galapa'),
	(302,70,70235,'Sucre','Galeras'),
	(303,25,25299,'Cundinamarca','Gama'),
	(304,20,20295,'Cesar','Gamarra'),
	(305,68,68298,'Santander','Gámbita'),
	(306,15,15296,'Boyacá','Gámeza'),
	(307,15,15299,'Boyacá','Garagoa'),
	(308,41,41298,'Huila','Garzón'),
	(309,63,63302,'Quindio','Génova'),
	(310,41,41306,'Huila','Gigante'),
	(311,76,76306,'Valle del Cauca','Ginebra'),
	(312,5,5306,'Antioquia','Giraldo'),
	(313,25,25307,'Cundinamarca','Girardot'),
	(314,5,5308,'Antioquia','Girardota'),
	(315,68,68307,'Santander','Girón'),
	(316,20,20310,'Cesar','González'),
	(317,54,54313,'Norte de Santander','Gramalote'),
	(318,5,5313,'Antioquia','Granada'),
	(319,25,25312,'Cundinamarca','Granada'),
	(320,50,50313,'Meta','Granada'),
	(321,68,68318,'Santander','Guaca'),
	(322,15,15317,'Boyacá','Guacamayas'),
	(323,76,76318,'Valle del Cauca','Guacarí'),
	(324,19,19300,'Cauca','Guachené'),
	(325,25,25317,'Cundinamarca','Guachetá'),
	(326,52,52317,'Nariño','Guachucal'),
	(327,5,5315,'Antioquia','Guadalupe'),
	(328,41,41319,'Huila','Guadalupe'),
	(329,68,68320,'Santander','Guadalupe'),
	(330,25,25320,'Cundinamarca','Guaduas'),
	(331,52,52320,'Nariño','Guaitarilla'),
	(332,52,52323,'Nariño','Gualmatán'),
	(333,47,47318,'Magdalena','Guamal'),
	(334,50,50318,'Meta','Guamal'),
	(335,73,73319,'Tolima','Guamo'),
	(336,19,19318,'Cauca','Guapí'),
	(337,68,68322,'Santander','Guapotá'),
	(338,70,70265,'Sucre','Guaranda'),
	(339,5,5318,'Antioquia','Guarne'),
	(340,25,25322,'Cundinamarca','Guasca'),
	(341,5,5321,'Antioquia','Guatapé'),
	(342,25,25324,'Cundinamarca','Guataquí'),
	(343,25,25326,'Cundinamarca','Guatavita'),
	(344,15,15322,'Boyacá','Guateque'),
	(345,66,66318,'Risaralda','Guática'),
	(346,68,68324,'Santander','Guavatá'),
	(347,25,25335,'Cundinamarca','Guayabetal'),
	(348,15,15325,'Boyacá','Guayatá'),
	(349,68,68327,'Santander','Güepsa'),
	(350,15,15332,'Boyacá','Güicán'),
	(351,25,25339,'Cundinamarca','Gutiérrez'),
	(352,54,54344,'Norte de Santander','Hacarí'),
	(353,68,68344,'Santander','Hato'),
	(354,44,44378,'La Guajira','Hatonuevo'),
	(355,5,5347,'Antioquia','Heliconia'),
	(356,54,54347,'Norte de Santander','Herrán'),
	(357,73,73347,'Tolima','Herveo'),
	(358,5,5353,'Antioquia','Hispania'),
	(359,41,41349,'Huila','Hobo'),
	(360,73,73349,'Tolima','Honda'),
	(361,73,73001,'Tolima','Ibagué'),
	(362,73,73352,'Tolima','Icononzo'),
	(363,52,52352,'Nariño','Iles'),
	(364,52,52354,'Nariño','Imués'),
	(365,94,94001,'Guainía','Inírida'),
	(366,19,19355,'Cauca','Inzá'),
	(367,52,52356,'Nariño','Ipiales'),
	(368,41,41357,'Huila','Íquira'),
	(369,41,41359,'Huila','Isnos'),
	(370,27,27361,'Chocó','Istmina'),
	(371,5,5360,'Antioquia','Itagüí'),
	(372,5,5361,'Antioquia','Ituango'),
	(373,15,15362,'Boyacá','Iza'),
	(374,19,19364,'Cauca','Jambaló'),
	(375,76,76364,'Valle del Cauca','Jamundí'),
	(376,5,5364,'Antioquia','Jardín'),
	(377,15,15367,'Boyacá','Jenesano'),
	(378,5,5368,'Antioquia','Jericó'),
	(379,15,15368,'Boyacá','Jericó'),
	(380,25,25368,'Cundinamarca','Jerusalén'),
	(381,68,68370,'Santander','Jordán'),
	(382,25,25372,'Cundinamarca','Junín'),
	(383,27,27372,'Chocó','Juradó'),
	(384,68,68397,'Santander','La Paz'),
	(385,91,91407,'Amazonas','La Pedrera'),
	(386,25,25398,'Cundinamarca','La Peña'),
	(387,5,5390,'Antioquia','La Pintada'),
	(388,41,41396,'Huila','La Plata'),
	(389,54,54398,'Norte de Santander','La Playa'),
	(390,99,99524,'Vichada','La Primavera'),
	(391,85,85136,'Casanare','La Salina'),
	(392,19,19392,'Cauca','La Sierra'),
	(393,63,63401,'Quindio','La Tebaida'),
	(394,52,52390,'Nariño','La Tola'),
	(395,5,5400,'Antioquia','La Unión'),
	(396,52,52399,'Nariño','La Unión'),
	(397,54,54377,'Norte de Santander','Labateca'),
	(398,15,15377,'Boyacá','Labranzagrande'),
	(399,68,68385,'Santander','Landázuri'),
	(400,68,68406,'Santander','Lebrija'),
	(401,52,52405,'Nariño','Leiva'),
	(402,50,50400,'Meta','Lejanías'),
	(403,25,25407,'Cundinamarca','Lenguazaque'),
	(404,73,73408,'Tolima','Lérida'),
	(405,91,91001,'Amazonas','Leticia'),
	(406,73,73411,'Tolima','Líbano'),
	(407,5,5411,'Antioquia','Liborina'),
	(408,52,52411,'Nariño','Linares'),
	(409,27,27413,'Chocó','Lloró'),
	(410,23,23417,'Córdoba','Lorica'),
	(411,54,54418,'Norte de Santander','Lourdes'),
	(412,8,8421,'Atlántico','Luruaco'),
	(413,15,15425,'Boyacá','Macanal'),
	(414,68,68425,'Santander','Macaravita'),
	(415,5,5425,'Antioquia','Maceo'),
	(416,25,25426,'Cundinamarca','Machetá'),
	(417,25,25430,'Cundinamarca','Madrid'),
	(418,13,13430,'Bolívar','Magangué'),
	(419,52,52427,'Nariño','Magüí'),
	(420,13,13433,'Bolívar','Mahates'),
	(421,44,44430,'La Guajira','Maicao'),
	(422,70,70429,'Sucre','Majagual'),
	(423,68,68432,'Santander','Málaga'),
	(424,8,8433,'Atlántico','Malambo'),
	(425,52,52435,'Nariño','Mallama'),
	(426,8,8436,'Atlántico','Manatí'),
	(427,44,44560,'La Guajira','Manaure'),
	(428,85,85139,'Casanare','Maní'),
	(429,17,17001,'Caldas','Manizales'),
	(430,25,25436,'Cundinamarca','Manta'),
	(431,17,17433,'Caldas','Manzanares'),
	(432,50,50325,'Meta','Mapiripán'),
	(433,94,94663,'Guainía','Mapiripana'),
	(434,13,13440,'Bolívar','Margarita'),
	(435,5,5440,'Antioquia','Marinilla'),
	(436,15,15442,'Boyacá','Maripí'),
	(437,17,17442,'Caldas','Marmato'),
	(438,17,17444,'Caldas','Marquetalia'),
	(439,66,66440,'Risaralda','Marsella'),
	(440,17,17446,'Caldas','Marulanda'),
	(441,68,68444,'Santander','Matanza'),
	(442,5,5001,'Antioquia','Medellín'),
	(443,25,25438,'Cundinamarca','Medina'),
	(444,73,73449,'Tolima','Melgar'),
	(445,19,19450,'Cauca','Mercaderes'),
	(446,50,50330,'Meta','Mesetas'),
	(447,18,18460,'Caquetá','Milán'),
	(448,15,15455,'Boyacá','Miraflores'),
	(449,95,95200,'Guaviare','Miraflores'),
	(450,19,19455,'Cauca','Miranda'),
	(451,66,66456,'Risaralda','Mistrató'),
	(452,97,97001,'Vaupés','Mitú'),
	(453,86,86001,'Putumayo','Mocoa'),
	(454,68,68464,'Santander','Mogotes'),
	(455,68,68468,'Santander','Molagavita'),
	(456,23,23464,'Córdoba','Momil'),
	(457,13,13468,'Bolívar','Mompós'),
	(458,15,15464,'Boyacá','Mongua'),
	(459,15,15466,'Boyacá','Monguí'),
	(460,15,15469,'Boyacá','Moniquirá'),
	(461,23,23500,'Córdoba','Moñitos'),
	(462,5,5467,'Antioquia','Montebello'),
	(463,13,13458,'Bolívar','Montecristo'),
	(464,23,23466,'Córdoba','Montelíbano'),
	(465,63,63470,'Quindio','Montenegro'),
	(466,23,23001,'Córdoba','Montería'),
	(467,85,85162,'Casanare','Monterrey'),
	(468,13,13473,'Bolívar','Morales'),
	(469,19,19473,'Cauca','Morales'),
	(470,18,18479,'Caquetá','Morelia'),
	(471,94,94888,'Guainía','Morichal'),
	(472,70,70473,'Sucre','Morroa'),
	(473,25,25473,'Cundinamarca','Mosquera'),
	(474,52,52473,'Nariño','Mosquera'),
	(475,15,15476,'Boyacá','Motavita'),
	(476,73,73461,'Tolima','Murillo'),
	(477,5,5475,'Antioquia','Murindó'),
	(478,5,5480,'Antioquia','Mutatá'),
	(479,54,54480,'Norte de Santander','Mutiscua'),
	(480,15,15480,'Boyacá','Muzo'),
	(481,5,5483,'Antioquia','Nariño'),
	(482,25,25483,'Cundinamarca','Nariño'),
	(483,52,52480,'Nariño','Nariño'),
	(484,41,41483,'Huila','Nátaga'),
	(485,73,73483,'Tolima','Natagaima'),
	(486,5,5495,'Antioquia','Nechí'),
	(487,5,5490,'Antioquia','Necoclí'),
	(488,17,17486,'Caldas','Neira'),
	(489,41,41001,'Huila','Neiva'),
	(490,25,25486,'Cundinamarca','Nemocón'),
	(491,25,25488,'Cundinamarca','Nilo'),
	(492,25,25489,'Cundinamarca','Nimaima'),
	(493,15,15491,'Boyacá','Nobsa'),
	(494,25,25491,'Cundinamarca','Nocaima'),
	(495,17,17495,'Caldas','Norcasia'),
	(496,13,13490,'Bolívar','Norosí'),
	(497,27,27491,'Chocó','Nóvita'),
	(498,85,85225,'Casanare','Nunchía'),
	(499,27,27495,'Chocó','Nuquí'),
	(500,76,76497,'Valle del Cauca','Obando'),
	(501,68,68498,'Santander','Ocamonte'),
	(502,54,54498,'Norte de Santander','Ocaña'),
	(503,68,68500,'Santander','Oiba'),
	(504,15,15500,'Boyacá','Oicatá'),
	(505,5,5501,'Antioquia','Olaya'),
	(506,68,68502,'Santander','Onzaga'),
	(507,41,41503,'Huila','Oporapa'),
	(508,86,86320,'Putumayo','Orito'),
	(509,85,85230,'Casanare','Orocué'),
	(510,73,73504,'Tolima','Ortega'),
	(511,52,52506,'Nariño','Ospina'),
	(512,15,15507,'Boyacá','Otanche'),
	(513,70,70508,'Sucre','Ovejas'),
	(514,15,15511,'Boyacá','Pachavita'),
	(515,25,25513,'Cundinamarca','Pacho'),
	(516,97,97511,'Vaupés','Pacoa'),
	(517,17,17513,'Caldas','Pácora'),
	(518,19,19513,'Cauca','Padilla'),
	(519,15,15514,'Boyacá','Páez'),
	(520,19,19517,'Cauca','Páez'),
	(521,41,41518,'Huila','Paicol'),
	(522,20,20517,'Cesar','Pailitas'),
	(523,25,25518,'Cundinamarca','Paime'),
	(524,15,15516,'Boyacá','Paipa'),
	(525,15,15518,'Boyacá','Pajarito'),
	(526,41,41524,'Huila','Palermo'),
	(527,17,17524,'Caldas','Palestina'),
	(528,41,41530,'Huila','Palestina'),
	(529,68,68522,'Santander','Palmar'),
	(530,76,76520,'Valle del Cauca','Palmira'),
	(531,70,70523,'Sucre','Palmito'),
	(532,73,73520,'Tolima','Palocabildo'),
	(533,54,54518,'Norte de Santander','Pamplona'),
	(534,54,54520,'Norte de Santander','Pamplonita'),
	(535,25,25524,'Cundinamarca','Pandi'),
	(536,15,15522,'Boyacá','Panqueba'),
	(537,97,97777,'Vaupés','Papunaua'),
	(538,68,68533,'Santander','Páramo'),
	(539,25,25530,'Cundinamarca','Paratebueno'),
	(540,25,25535,'Cundinamarca','Pasca'),
	(541,52,52001,'Nariño','Pasto'),
	(542,19,19532,'Cauca','Patía'),
	(543,15,15531,'Boyacá','Pauna'),
	(544,15,15533,'Boyacá','Paya'),
	(545,47,47541,'Magdalena','Pedraza'),
	(546,20,20550,'Cesar','Pelaya'),
	(547,5,5541,'Antioquia','Peñol'),
	(548,17,17541,'Caldas','Pensilvania'),
	(549,5,5543,'Antioquia','Peque'),
	(550,66,66001,'Risaralda','Pereira'),
	(551,15,15542,'Boyacá','Pesca'),
	(552,19,19533,'Cauca','Piamonte'),
	(553,68,68547,'Santander','Piedecuesta'),
	(554,73,73547,'Tolima','Piedras'),
	(555,19,19548,'Cauca','Piendamó'),
	(556,63,63548,'Quindio','Pijao'),
	(557,68,68549,'Santander','Pinchote'),
	(558,13,13549,'Bolívar','Pinillos'),
	(559,8,8549,'Atlántico','Piojó'),
	(560,15,15550,'Boyacá','Pisba'),
	(561,41,41548,'Huila','Pital'),
	(562,41,41551,'Huila','Pitalito'),
	(563,47,47551,'Magdalena','Pivijay'),
	(564,73,73555,'Tolima','Planadas'),
	(565,47,47555,'Magdalena','Plato'),
	(566,52,52540,'Nariño','Policarpa'),
	(567,8,8558,'Atlántico','Polonuevo'),
	(568,8,8560,'Atlántico','Ponedera'),
	(569,19,19001,'Cauca','Popayán'),
	(570,85,85263,'Casanare','Pore'),
	(571,52,52560,'Nariño','Potosí'),
	(572,76,76563,'Valle del Cauca','Pradera'),
	(573,73,73563,'Tolima','Prado'),
	(574,88,88564,'Archipiélago de San Andrés Providencia y Santa Catalina','Providencia'),
	(575,52,52565,'Nariño','Providencia'),
	(576,5,5576,'Antioquia','Pueblorrico'),
	(577,47,47570,'Magdalena','Puebloviejo'),
	(578,52,52573,'Nariño','Puerres'),
	(579,25,25580,'Cundinamarca','Pulí'),
	(580,52,52585,'Nariño','Pupiales'),
	(581,19,19585,'Cauca','Puracé'),
	(582,73,73585,'Tolima','Purificación'),
	(583,25,25592,'Cundinamarca','Quebradanegra'),
	(584,25,25594,'Cundinamarca','Quetame'),
	(585,27,27001,'Chocó','Quibdó'),
	(586,63,63594,'Quindio','Quimbaya'),
	(587,66,66594,'Risaralda','Quinchía'),
	(588,15,15580,'Boyacá','Quípama'),
	(589,25,25596,'Cundinamarca','Quipile'),
	(590,54,54599,'Norte de Santander','Ragonvalia'),
	(591,15,15599,'Boyacá','Ramiriquí'),
	(592,15,15600,'Boyacá','Ráquira'),
	(593,85,85279,'Casanare','Recetor'),
	(594,13,13580,'Bolívar','Regidor'),
	(595,5,5604,'Antioquia','Remedios'),
	(596,47,47605,'Magdalena','Remolino'),
	(597,8,8606,'Atlántico','Repelón'),
	(598,50,50606,'Meta','Restrepo'),
	(599,76,76606,'Valle del Cauca','Restrepo'),
	(600,5,5607,'Antioquia','Retiro'),
	(601,25,25612,'Cundinamarca','Ricaurte'),
	(602,52,52612,'Nariño','Ricaurte'),
	(603,73,73616,'Tolima','Rioblanco'),
	(604,91,91540,'Amazonas','Puerto Nariño'),
	(605,68,68573,'Santander','Puerto Parra'),
	(606,18,18592,'Caquetá','Puerto Rico'),
	(607,50,50590,'Meta','Puerto Rico'),
	(608,81,81591,'Arauca','Puerto Rondón'),
	(609,25,25572,'Cundinamarca','Puerto Salgar'),
	(610,91,91669,'Amazonas','Puerto Santander'),
	(611,54,54553,'Norte de Santander','Puerto Santander'),
	(612,19,19573,'Cauca','Puerto Tejada'),
	(613,5,5591,'Antioquia','Puerto Triunfo'),
	(614,68,68575,'Santander','Puerto Wilches'),
	(615,23,23586,'Córdoba','Purísima de la Concepción'),
	(616,76,76616,'Valle del Cauca','Riofrío'),
	(617,44,44001,'La Guajira','Riohacha'),
	(618,5,5615,'Antioquia','Rionegro'),
	(619,68,68615,'Santander','Rionegro'),
	(620,17,17614,'Caldas','Riosucio'),
	(621,27,27615,'Chocó','Riosucio'),
	(622,17,17616,'Caldas','Risaralda'),
	(623,41,41615,'Huila','Rivera'),
	(624,76,76622,'Valle del Cauca','Roldanillo'),
	(625,73,73622,'Tolima','Roncesvalles'),
	(626,15,15621,'Boyacá','Rondón'),
	(627,19,19622,'Cauca','Rosas'),
	(628,73,73624,'Tolima','Rovira'),
	(629,8,8634,'Atlántico','Sabanagrande'),
	(630,5,5628,'Antioquia','Sabanalarga'),
	(631,8,8638,'Atlántico','Sabanalarga'),
	(632,85,85300,'Casanare','Sabanalarga'),
	(633,5,5631,'Antioquia','Sabaneta'),
	(634,15,15632,'Boyacá','Saboyá'),
	(635,85,85315,'Casanare','Sácama'),
	(636,15,15638,'Boyacá','Sáchica'),
	(637,23,23660,'Córdoba','Sahagún'),
	(638,41,41660,'Huila','Saladoblanco'),
	(639,17,17653,'Caldas','Salamina'),
	(640,47,47675,'Magdalena','Salamina'),
	(641,54,54660,'Norte de Santander','Salazar'),
	(642,73,73671,'Tolima','Saldaña'),
	(643,63,63690,'Quindio','Salento'),
	(644,5,5642,'Antioquia','Salgar'),
	(645,15,15646,'Boyacá','Samacá'),
	(646,17,17662,'Caldas','Samaná'),
	(647,52,52678,'Nariño','Samaniego'),
	(648,70,70670,'Sucre','Sampués'),
	(649,23,23672,'Córdoba','San Antero'),
	(650,73,73675,'Tolima','San Antonio'),
	(651,25,25645,'Cundinamarca','San Antonio del Tequendama'),
	(652,68,68673,'Santander','San Benito'),
	(653,70,70678,'Sucre','San Benito abad'),
	(654,25,25649,'Cundinamarca','San Bernardo'),
	(655,52,52685,'Nariño','San Bernardo'),
	(656,23,23675,'Córdoba','San Bernardo del Viento'),
	(657,54,54670,'Norte de Santander','San Calixto'),
	(658,5,5649,'Antioquia','San Carlos'),
	(659,23,23678,'Córdoba','San Carlos'),
	(660,20,20750,'Cesar','San Diego'),
	(661,17,17665,'Caldas','San José'),
	(662,5,5658,'Antioquia','San José la Montaña'),
	(663,68,68684,'Santander','San José de Miranda'),
	(664,15,15664,'Boyacá','San José de Pare'),
	(665,23,23682,'Córdoba','San José de Uré'),
	(666,18,18610,'Caquetá','San José del Fragua'),
	(667,95,95001,'Guaviare','San José del Guaviare'),
	(668,27,27660,'Chocó','San José del Palmar'),
	(669,50,50683,'Meta','San Juan de Arama'),
	(670,70,70702,'Sucre','San Juan de Betulia'),
	(671,25,25662,'Cundinamarca','San Juan de Rioseco'),
	(672,5,5659,'Antioquia','San Juan de Urabá'),
	(673,44,44650,'La Guajira','San Juan del Cesar'),
	(674,13,13657,'Bolívar','San Juan Nepomuceno'),
	(675,50,50686,'Meta','San Juanito'),
	(676,52,52687,'Nariño','San Lorenzo'),
	(677,50,50223,'Meta','San Luis de Cubarral'),
	(678,15,15667,'Boyacá','San Luis de Gaceno'),
	(679,70,70717,'Sucre','San Pedro'),
	(680,52,52683,'Nariño','Sandoná'),
	(681,52,52699,'Nariño','Santacruz'),
	(682,15,15686,'Boyacá','Santana'),
	(683,54,54680,'Norte de Santander','Santiago'),
	(684,86,86760,'Putumayo','Santiago'),
	(685,66,66687,'Risaralda','Santuario'),
	(686,52,52720,'Nariño','Sapuyes'),
	(687,81,81736,'Arauca','Saravena'),
	(688,54,54720,'Norte de Santander','Sardinata'),
	(689,25,25718,'Cundinamarca','Sasaima'),
	(690,15,15720,'Boyacá','Sativanorte'),
	(691,15,15723,'Boyacá','Sativasur'),
	(692,5,5736,'Antioquia','Segovia'),
	(693,25,25736,'Cundinamarca','Sesquilé'),
	(694,76,76736,'Valle del Cauca','Sevilla'),
	(695,8,8685,'Atlántico','Santo Tomás'),
	(696,5,5690,'Antioquia','Santo Domingo'),
	(697,70,70820,'Sucre','Santiago de Tolú'),
	(698,19,19698,'Cauca','Santander de Quilichao'),
	(699,15,15696,'Boyacá','Santa Sofía'),
	(700,99,99624,'Vichada','Santa Rosalía'),
	(701,47,47707,'Magdalena','Santa Ana'),
	(702,47,47703,'Magdalena','San Zenón'),
	(703,5,5674,'Antioquia','San Vicente Ferrer'),
	(704,18,18753,'Caquetá','San Vicente del Caguán'),
	(705,68,68689,'Santander','San Vicente de Chucurí'),
	(706,47,47692,'Magdalena','San Sebastián de Buenavista'),
	(707,73,73443,'Tolima','San Sebastián de Mariquita'),
	(708,15,15740,'Boyacá','Siachoque'),
	(709,25,25740,'Cundinamarca','Sibaté'),
	(710,86,86749,'Putumayo','Sibundoy'),
	(711,54,54743,'Norte de Santander','Silos'),
	(712,25,25743,'Cundinamarca','Silvania'),
	(713,19,19743,'Cauca','Silvia'),
	(714,68,68745,'Santander','Simacota'),
	(715,25,25745,'Cundinamarca','Simijaca'),
	(716,13,13744,'Bolívar','Simití'),
	(717,70,70001,'Sucre','Sincelejo'),
	(718,27,27745,'Chocó','Sipí'),
	(719,47,47745,'Magdalena','Sitionuevo'),
	(720,25,25754,'Cundinamarca','Soacha'),
	(721,15,15753,'Boyacá','Soatá'),
	(722,15,15757,'Boyacá','Socha'),
	(723,68,68755,'Santander','Socorro'),
	(724,15,15755,'Boyacá','Socotá'),
	(725,15,15759,'Boyacá','Sogamoso'),
	(726,18,18756,'Caquetá','Solano'),
	(727,8,8758,'Atlántico','Soledad'),
	(728,18,18785,'Caquetá','Solita'),
	(729,15,15761,'Boyacá','Somondoco'),
	(730,5,5756,'Antioquia','Sonsón'),
	(731,5,5761,'Antioquia','Sopetrán'),
	(732,13,13760,'Bolívar','Soplaviento'),
	(733,25,25758,'Cundinamarca','Sopó'),
	(734,15,15762,'Boyacá','Sora'),
	(735,15,15764,'Boyacá','Soracá'),
	(736,15,15763,'Boyacá','Sotaquirá'),
	(737,19,19760,'Cauca','Sotara'),
	(738,68,68770,'Santander','Suaita'),
	(739,8,8770,'Atlántico','Suan'),
	(740,19,19780,'Cauca','Suárez'),
	(741,73,73770,'Tolima','Suárez'),
	(742,41,41770,'Huila','Suaza'),
	(743,25,25769,'Cundinamarca','Subachoque'),
	(744,19,19785,'Cauca','Sucre'),
	(745,68,68773,'Santander','Sucre'),
	(746,70,70771,'Sucre','Sucre'),
	(747,25,25772,'Cundinamarca','Suesca'),
	(748,25,25777,'Cundinamarca','Supatá'),
	(749,17,17777,'Caldas','Supía'),
	(750,68,68780,'Santander','Suratá'),
	(751,25,25779,'Cundinamarca','Susa'),
	(752,15,15774,'Boyacá','Susacón'),
	(753,15,15776,'Boyacá','Sutamarchán'),
	(754,25,25781,'Cundinamarca','Sutatausa'),
	(755,15,15778,'Boyacá','Sutatenza'),
	(756,25,25785,'Cundinamarca','Tabio'),
	(757,27,27787,'Chocó','Tadó'),
	(758,20,20787,'Cesar','Tamalameque'),
	(759,85,85400,'Casanare','Támara'),
	(760,81,81794,'Arauca','Tame'),
	(761,5,5789,'Antioquia','Támesis'),
	(762,52,52786,'Nariño','Taminango'),
	(763,52,52788,'Nariño','Tangua'),
	(764,97,97666,'Vaupés','Taraira'),
	(765,91,91798,'Amazonas','Tarapacá'),
	(766,5,5790,'Antioquia','Tarazá'),
	(767,41,41791,'Huila','Tarqui'),
	(768,5,5792,'Antioquia','Tarso'),
	(769,15,15790,'Boyacá','Tasco'),
	(770,85,85410,'Casanare','Tauramena'),
	(771,25,25793,'Cundinamarca','Tausa'),
	(772,41,41799,'Huila','Tello'),
	(773,25,25797,'Cundinamarca','Tena'),
	(774,47,47798,'Magdalena','Tenerife'),
	(775,25,25799,'Cundinamarca','Tenjo'),
	(776,15,15798,'Boyacá','Tenza'),
	(777,54,54800,'Norte de Santander','Teorama'),
	(778,41,41801,'Huila','Teruel'),
	(779,41,41797,'Huila','Tesalia'),
	(780,25,25805,'Cundinamarca','Tibacuy'),
	(781,15,15804,'Boyacá','Tibaná'),
	(782,15,15806,'Boyacá','Tibasosa'),
	(783,25,25807,'Cundinamarca','Tibirita'),
	(784,54,54810,'Norte de Santander','Tibú'),
	(785,23,23807,'Córdoba','Tierralta'),
	(786,41,41807,'Huila','Timaná'),
	(787,19,19807,'Cauca','Timbío'),
	(788,19,19809,'Cauca','Timbiquí'),
	(789,15,15808,'Boyacá','Tinjacá'),
	(790,15,15810,'Boyacá','Tipacoque'),
	(791,13,13810,'Bolívar','Tiquisio'),
	(792,5,5809,'Antioquia','Titiribí'),
	(793,15,15814,'Boyacá','Toca'),
	(794,25,25815,'Cundinamarca','Tocaima'),
	(795,25,25817,'Cundinamarca','Tocancipá'),
	(796,15,15816,'Boyacá','Togüí'),
	(797,5,5819,'Antioquia','Toledo'),
	(798,54,54820,'Norte de Santander','Toledo'),
	(799,68,68820,'Santander','Tona'),
	(800,15,15820,'Boyacá','Tópaga'),
	(801,25,25823,'Cundinamarca','Topaipí'),
	(802,19,19821,'Cauca','Toribío'),
	(803,76,76823,'Valle del Cauca','Toro'),
	(804,15,15822,'Boyacá','Tota'),
	(805,19,19824,'Cauca','Totoró'),
	(806,85,85430,'Casanare','Trinidad'),
	(807,76,76828,'Valle del Cauca','Trujillo'),
	(808,8,8832,'Atlántico','Tubará'),
	(809,23,23815,'Córdoba','Tuchín'),
	(810,76,76834,'Valle del Cauca','Tuluá'),
	(811,15,15001,'Boyacá','Tunja'),
	(812,15,15832,'Boyacá','Tununguá'),
	(813,52,52838,'Nariño','Túquerres'),
	(814,13,13836,'Bolívar','Turbaco'),
	(815,13,13838,'Bolívar','Turbaná'),
	(816,5,5837,'Antioquia','Turbo'),
	(817,15,15835,'Boyacá','Turmequé'),
	(818,15,15837,'Boyacá','Tuta'),
	(819,15,15839,'Boyacá','Tutazá'),
	(820,25,25839,'Cundinamarca','Ubalá'),
	(821,25,25841,'Cundinamarca','Ubaque'),
	(822,76,76845,'Valle del Cauca','Ulloa'),
	(823,15,15842,'Boyacá','Úmbita'),
	(824,25,25845,'Cundinamarca','Une'),
	(825,27,27800,'Chocó','Unguía'),
	(826,5,5842,'Antioquia','Uramita'),
	(827,50,50370,'Meta','Uribe'),
	(828,44,44847,'La Guajira','Uribia'),
	(829,5,5847,'Antioquia','Urrao'),
	(830,44,44855,'La Guajira','Urumita'),
	(831,8,8849,'Atlántico','Usiacurí'),
	(832,25,25851,'Cundinamarca','Útica'),
	(833,5,5854,'Antioquia','Valdivia'),
	(834,23,23855,'Córdoba','Valencia'),
	(835,20,20001,'Cesar','Valledupar'),
	(836,5,5856,'Antioquia','Valparaíso'),
	(837,18,18860,'Caquetá','Valparaíso'),
	(838,5,5858,'Antioquia','Vegachí'),
	(839,68,68861,'Santander','Vélez'),
	(840,73,73861,'Tolima','Venadillo'),
	(841,5,5861,'Antioquia','Venecia'),
	(842,25,25506,'Cundinamarca','Venecia'),
	(843,15,15861,'Boyacá','Ventaquemada'),
	(844,25,25862,'Cundinamarca','Vergara'),
	(845,76,76863,'Valle del Cauca','Versalles'),
	(846,68,68867,'Santander','Vetas'),
	(847,25,25867,'Cundinamarca','Vianí'),
	(848,17,17867,'Caldas','Victoria'),
	(849,76,76869,'Valle del Cauca','Vijes'),
	(850,19,19845,'Cauca','Villa rica'),
	(851,86,86885,'Putumayo','Villagarzón'),
	(852,25,25871,'Cundinamarca','Villagómez'),
	(853,73,73870,'Tolima','Villahermosa'),
	(854,17,17873,'Caldas','Villamaría'),
	(855,13,13873,'Bolívar','Villanueva'),
	(856,85,85440,'Casanare','Villanueva'),
	(857,44,44874,'La Guajira','Villanueva'),
	(858,68,68872,'Santander','Villanueva'),
	(859,25,25873,'Cundinamarca','Villapinzón'),
	(860,73,73873,'Tolima','Villarrica'),
	(861,50,50001,'Meta','Villavicencio'),
	(862,41,41872,'Huila','Villavieja'),
	(863,5,5873,'Antioquia','Vigía del Fuerte'),
	(864,86,86865,'Putumayo','Valle del Guamuez'),
	(865,73,73854,'Tolima','Valle de San Juan'),
	(866,68,68855,'Santander','Valle de San José'),
	(867,27,27810,'Chocó','Unión Panamericana'),
	(868,25,25875,'Cundinamarca','Villeta'),
	(869,25,25878,'Cundinamarca','Viotá'),
	(870,15,15879,'Boyacá','Viracachá'),
	(871,50,50711,'Meta','Vistahermosa'),
	(872,17,17877,'Caldas','Viterbo'),
	(873,25,25885,'Cundinamarca','Yacopí'),
	(874,52,52885,'Nariño','Yacuanquer'),
	(875,41,41885,'Huila','Yaguará'),
	(876,5,5885,'Antioquia','Yalí'),
	(877,5,5887,'Antioquia','Yarumal'),
	(878,97,97889,'Vaupés','Yavaraté'),
	(879,5,5890,'Antioquia','Yolombó'),
	(880,5,5893,'Antioquia','Yondó'),
	(881,85,85001,'Casanare','Yopal'),
	(882,76,76890,'Valle del Cauca','Yotoco'),
	(883,76,76892,'Valle del Cauca','Yumbo'),
	(884,13,13894,'Bolívar','Zambrano'),
	(885,68,68895,'Santander','Zapatoca'),
	(886,47,47960,'Magdalena','Zapayán'),
	(887,5,5895,'Antioquia','Zaragoza'),
	(888,76,76895,'Valle del Cauca','Zarzal'),
	(889,15,15897,'Boyacá','Zetaquira'),
	(890,25,25898,'Cundinamarca','Zipacón'),
	(891,25,25899,'Cundinamarca','Zipaquirá'),
	(892,20,20013,'Cesar','Agustín Codazzi'),
	(893,13,13030,'Bolívar','Altos del Rosario'),
	(894,27,27075,'Chocó','Bahía Solano'),
	(895,27,27077,'Chocó','Bajo Baudó'),
	(896,50,50110,'Meta','Barranca de Upía'),
	(897,13,13074,'Bolívar','Barranco de Loba'),
	(898,94,94343,'Guainía','Barranco Minas'),
	(899,18,18094,'Caquetá','Belén de los Andaquíes'),
	(900,66,66088,'Risaralda','Belén de Umbría'),
	(901,11,11001,'Bogotá d C.','Bogotá D.C.'),
	(902,19,19110,'Cauca','Buenos Aires'),
	(903,8,8137,'Atlántico','Campo de la Cruz'),
	(904,73,73148,'Tolima','Carmen de Apicalá'),
	(905,25,25154,'Cundinamarca','Carmen de Carupa'),
	(906,27,27150,'Chocó','Carmen del Darién'),
	(907,13,13001,'Bolívar','Cartagena de Indias'),
	(908,18,18150,'Caquetá','Cartagena del Chairá'),
	(909,50,50150,'Meta','Castilla la Nueva'),
	(910,47,47161,'Magdalena','Cerro de San Antonio'),
	(911,23,23189,'Córdoba','Ciénaga de Oro'),
	(912,5,5101,'Antioquia','Ciudad Bolívar'),
	(913,81,81220,'Arauca','Cravo Norte'),
	(914,25,25394,'Cundinamarca','La Palma'),
	(915,52,52520,'Nariño','Francisco Pizarro'),
	(916,13,13300,'Bolívar','Hatillo de Loba'),
	(917,23,23350,'Córdoba','La Apartada'),
	(918,68,68377,'Santander','La Belleza'),
	(919,15,15380,'Boyacá','La Capilla'),
	(920,76,76670,'Valle del Cauca','San Pedro'),
	(921,5,5665,'Antioquia','San Pedro de Urabá'),
	(922,25,25843,'Cundinamarca','Villa de de Ubaté'),
	(923,8,8675,'Atlántico','Santa Lucía'),
	(924,50,50287,'Meta','Fuente de Oro'),
	(925,68,68682,'Santander','San Joaquín'),
	(926,68,68705,'Santander','Santa Bárbara'),
	(927,52,52696,'Nariño','Santa Bárbara'),
	(928,5,5310,'Antioquia','Gómez Plata'),
	(929,17,17388,'Caldas','La Merced'),
	(930,20,20443,'Cesar','Manaure Balcón del Cesar'),
	(931,52,52490,'Nariño','Olaya Herrera'),
	(932,68,68524,'Santander','Palmas del Socorro'),
	(933,15,15537,'Boyacá','Paz de Río'),
	(934,23,23555,'Córdoba','Planeta Rica'),
	(935,76,76111,'Valle del Cauca','Guadalajara de Buga'),
	(936,27,27425,'Chocó','Medio Atrato'),
	(937,41,41668,'Huila','San Agustín'),
	(938,88,88001,'Archipiélago de San Andrés Providencia y Santa Catalina','San Andrés'),
	(939,23,23670,'Córdoba','San Andrés de Sotavento'),
	(940,13,13647,'Bolívar','San Estanislao'),
	(941,15,15673,'Boyacá','San Mateo'),
	(942,25,25328,'Cundinamarca','Guayabal de Síquima'),
	(943,8,8372,'Atlántico','Juan de Acosta'),
	(944,27,27250,'Chocó','El Litoral San Juan'),
	(945,52,52258,'Nariño','El Tablón de Gómez'),
	(946,5,5376,'Antioquia','La Ceja'),
	(947,20,20383,'Cesar','La Gloria'),
	(948,68,68655,'Santander','Sabana de Torres'),
	(949,47,47980,'Magdalena','Zona Bananera'),
	(950,54,54874,'Norte de Santander','Villa del Rosario'),
	(951,15,15407,'Boyacá','Villa de Leyva'),
	(952,85,85125,'Casanare','Hato Corozal'),
	(953,68,68368,'Santander','Jesús María'),
	(954,41,41378,'Huila','La Argentina'),
	(955,25,25377,'Cundinamarca','La Calera'),
	(956,66,66383,'Risaralda','La Celia'),
	(957,52,52378,'Nariño','La Cruz'),
	(958,52,52385,'Nariño','La Llanada'),
	(959,19,19418,'Cauca','López de Micay'),
	(960,8,8520,'Atlántico','Palmar de Varela'),
	(961,23,23570,'Córdoba','Pueblo Nuevo'),
	(962,66,66572,'Risaralda','Pueblo Rico'),
	(963,91,91536,'Amazonas','Puerto Arica'),
	(964,15,15572,'Boyacá','Puerto Boyacá'),
	(965,13,13688,'Bolívar','Santa Rosa del Sur'),
	(966,15,15690,'Boyacá','Santa María'),
	(967,91,91405,'Amazonas','La Chorrera'),
	(968,25,25653,'Cundinamarca','San Cayetano'),
	(969,17,17380,'Caldas','La Dorada'),
	(970,76,76377,'Valle del Cauca','La Cumbre'),
	(971,25,25386,'Cundinamarca','La Mesa'),
	(972,15,15403,'Boyacá','La Uvita'),
	(973,5,5579,'Antioquia','Puerto Berrío'),
	(974,94,94884,'Guainía','Puerto Colombia'),
	(975,70,70713,'Sucre','San Onofre'),
	(976,19,19693,'Cauca','San Sebastián'),
	(977,54,54871,'Norte de Santander','Villa Caro'),
	(978,47,47720,'Magdalena','Santa Bárbara de Pinto'),
	(979,54,54385,'Norte de Santander','La Esperanza'),
	(980,5,5380,'Antioquia','La Estrella'),
	(981,52,52381,'Nariño','La Florida'),
	(982,20,20400,'Cesar','La Jagua de Ibirico'),
	(983,18,18410,'Caquetá','La Montañita'),
	(984,76,76400,'Valle del Cauca','La Unión'),
	(985,76,76403,'Valle del Cauca','La Victoria'),
	(986,66,66400,'Risaralda','La Virginia'),
	(987,52,52418,'Nariño','Los Andes'),
	(988,27,27430,'Chocó','Medio Baudó'),
	(989,91,91460,'Amazonas','Mirití Paraná'),
	(990,91,91530,'Amazonas','Puerto Alegría'),
	(991,23,23574,'Córdoba','Puerto Escondido'),
	(992,86,86571,'Putumayo','Puerto Guzmán'),
	(993,5,5585,'Antioquia','Puerto Nare'),
	(994,52,52621,'Nariño','Roberto Payán'),
	(995,68,68669,'Santander','San Andrés'),
	(996,5,5647,'Antioquia','San Andrés de Cuerquía'),
	(997,86,86755,'Putumayo','San Francisco'),
	(998,5,5660,'Antioquia','San Luis'),
	(999,70,70708,'Sucre','San Marcos'),
	(1000,5,5664,'Antioquia','San Pedro los Milagros'),
	(1001,70,70823,'Sucre','Tolú Viejo'),
	(1002,19,19701,'Cauca','Santa Rosa'),
	(1003,13,13683,'Bolívar','Santa Rosa'),
	(1004,94,94885,'Guainía','La Guadalupe'),
	(1005,44,44420,'La Guajira','La Jagua del Pilar'),
	(1006,50,50350,'Meta','La Macarena'),
	(1007,70,70418,'Sucre','Los Palmitos'),
	(1008,27,27450,'Chocó','Medio San Juan'),
	(1009,85,85250,'Casanare','Paz de Ariporo'),
	(1010,47,47545,'Magdalena','Pijiño del Carmen'),
	(1011,20,20570,'Cesar','Pueblo Bello'),
	(1012,68,68572,'Santander','Puente Nacional'),
	(1013,86,86569,'Putumayo','Puerto Caicedo'),
	(1014,99,99001,'Vichada','Puerto Carreño'),
	(1015,86,86573,'Putumayo','Puerto Leguízamo'),
	(1016,23,23580,'Córdoba','Puerto Libertador'),
	(1017,50,50577,'Meta','Puerto Lleras'),
	(1018,50,50573,'Meta','Puerto López'),
	(1019,94,94883,'Guainía','San Felipe'),
	(1020,86,86757,'Putumayo','San Miguel'),
	(1021,13,13670,'Bolívar','San Pablo'),
	(1022,52,52693,'Nariño','San Pablo'),
	(1023,23,23686,'Córdoba','San Pelayo'),
	(1024,5,5670,'Antioquia','San Roque'),
	(1025,68,68720,'Santander','Santa Helena del Opón'),
	(1026,20,20621,'Cesar','La Paz'),
	(1027,70,70400,'Sucre','La Unión'),
	(1028,19,19397,'Cauca','La Vega'),
	(1029,25,25402,'Cundinamarca','La Vega'),
	(1030,15,15401,'Boyacá','La Victoria'),
	(1031,86,86568,'Putumayo','Puerto Asís'),
	(1032,8,8573,'Atlántico','Puerto Colombia'),
	(1033,52,52835,'Nariño','San Andrés de Tumaco'),
	(1034,5,5652,'Antioquia','San Francisco'),
	(1035,91,91430,'Amazonas','La Victoria'),
	(1036,23,23419,'Córdoba','Los Córdobas'),
	(1037,54,54405,'Norte de Santander','Los Patios'),
	(1038,68,68418,'Santander','Los Santos'),
	(1039,13,13442,'Bolívar','María la Baja'),
	(1040,15,15494,'Boyacá','Nuevo Colón'),
	(1041,94,94887,'Guainía','Pana Pana'),
	(1042,15,15660,'Boyacá','San Eduardo'),
	(1043,85,85325,'Casanare','San Luis de Palenque'),
	(1044,15,15681,'Boyacá','San Pablo de Borbur'),
	(1045,15,15693,'Boyacá','Santa Rosa de Viterbo'),
	(1046,5,5042,'Antioquia','Santa Fé de Antioquia'),
	(1047,13,13673,'Bolívar','Santa Catalina'),
	(1048,5,5679,'Antioquia','Santa Bárbara'),
	(1049,47,47460,'Magdalena','Nueva Granada'),
	(1050,50,50450,'Meta','Puerto Concordia'),
	(1051,50,50568,'Meta','Puerto Gaitán'),
	(1052,47,47660,'Magdalena','Sabanas de San Ángel'),
	(1053,13,13650,'Bolívar','San Fernando'),
	(1054,13,13654,'Bolívar','San Jacinto'),
	(1055,13,13655,'Bolívar','San Jacinto del Cauca'),
	(1056,5,5656,'Antioquia','San Jerónimo'),
	(1057,73,73678,'Tolima','San Luis'),
	(1058,70,70742,'Sucre','San Luis de Sincé'),
	(1059,5,5667,'Antioquia','San Rafael'),
	(1060,47,47001,'Magdalena','Santa Marta'),
	(1061,73,73686,'Tolima','Santa Isabel'),
	(1062,20,20710,'Cesar','San Alberto'),
	(1063,54,54673,'Norte de Santander','San Cayetano'),
	(1064,50,50680,'Meta','San Carlos de Guaroa'),
	(1065,13,13620,'Bolívar','San Cristóbal'),
	(1066,25,25658,'Cundinamarca','San Francisco'),
	(1067,68,68679,'Santander','San Gil'),
	(1068,20,20770,'Cesar','San Martín'),
	(1069,50,50689,'Meta','San Martín'),
	(1070,13,13667,'Bolívar','San Martín de Loba'),
	(1071,68,68686,'Santander','San Miguel'),
	(1072,15,15676,'Boyacá','San Miguel de Sema'),
	(1073,52,52694,'Nariño','San Pedro de Cartago'),
	(1074,5,5686,'Antioquia','Santa Rosa de Osos'),
	(1075,66,66682,'Risaralda','Santa Rosa de Cabal'),
	(1076,41,41676,'Huila','Santa María'),
	(1077,25,25001,'Cundinamarca','Agua de Dios'),
	(1078,27,27025,'Chocó','Alto Baudó'),
	(1079,76,76243,'Valle del Cauca','El Águila'),
	(1080,5,5250,'Antioquia','El Bagre'),
	(1081,47,47245,'Magdalena','El Banco'),
	(1082,76,76246,'Valle del Cauca','El Cairo'),
	(1083,50,50245,'Meta','El Calvario'),
	(1084,27,27135,'Chocó','El Cantón San Pablo'),
	(1085,54,54245,'Norte de Santander','El Carmen'),
	(1086,27,27245,'Chocó','El Carmen de Atrato'),
	(1087,13,13244,'Bolívar','El Carmen de Bolívar'),
	(1088,68,68235,'Santander','El Carmen de Chucurí'),
	(1089,5,5148,'Antioquia','El Carmen de Viboral'),
	(1090,50,50251,'Meta','El Castillo'),
	(1091,76,76248,'Valle del Cauca','El Cerrito'),
	(1092,52,52250,'Nariño','El Charco'),
	(1093,15,15244,'Boyacá','El Cocuy'),
	(1094,25,25245,'Cundinamarca','El Colegio'),
	(1095,91,91263,'Amazonas','El Encanto'),
	(1096,68,68245,'Santander','El Guacamayo'),
	(1097,13,13248,'Bolívar','El Guamo'),
	(1098,44,44110,'La Guajira','El Molino'),
	(1099,20,20250,'Cesar','El Paso'),
	(1100,18,18256,'Caquetá','El Paujíl'),
	(1101,52,52254,'Nariño','El Peñol'),
	(1102,13,13268,'Bolívar','El Peñón'),
	(1103,68,68250,'Santander','El Peñón'),
	(1104,25,25258,'Cundinamarca','El Peñón'),
	(1105,47,47258,'Magdalena','El Piñón'),
	(1106,68,68255,'Santander','El Playón'),
	(1107,47,47268,'Magdalena','El Retén'),
	(1108,95,95025,'Guaviare','El Retorno'),
	(1109,52,52256,'Nariño','El Rosario'),
	(1110,70,70233,'Sucre','El Roble'),
	(1111,19,19256,'Cauca','El Tambo'),
	(1112,52,52260,'Nariño','El Tambo'),
	(1113,54,54261,'Norte de Santander','El Zulia'),
	(1114,25,25260,'Cundinamarca','El Rosal'),
	(1115,5,5697,'Antioquia','El Santuario'),
	(1116,54,54250,'Norte de Santander','El Tarra'),
	(1117,27,27580,'Chocó','Río Iró'),
	(1118,13,13600,'Bolívar','Río Viejo'),
	(1119,20,20614,'Cesar','Río de Oro'),
	(1120,27,27600,'Chocó','Río Quito'),
	(1121,13,13780,'Bolívar','Talaigua Nuevo'),
	(1122,73,73055,'Tolima','Armero ');

/*!40000 ALTER TABLE `divipola` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla facturas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `facturas`;

CREATE TABLE `facturas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la Factura',
  `id_remision` int(10) unsigned DEFAULT NULL COMMENT 'Id de la remisión para generar la factura',
  `consecutivo` int(11) NOT NULL COMMENT 'Consecutivo de la Factura',
  `fecha_factura` date NOT NULL COMMENT 'Fecha de la Factura',
  `fecha_carga` datetime NOT NULL COMMENT 'Fecha de Carga',
  `id_usuario` int(10) unsigned NOT NULL COMMENT 'Id del usuario vendedor de la factura',
  `id_estado` int(10) unsigned NOT NULL COMMENT 'Id del estado de la Factura',
  `id_tipo_pago` int(10) unsigned NOT NULL COMMENT 'Id del Tipo de pago de la factura',
  `dias_credito` int(11) DEFAULT NULL COMMENT 'Días de Credito para el pago de la factura',
  `orden_compra` varchar(30) DEFAULT NULL COMMENT 'Orden de Compra de la Factura',
  `id_cliente` int(10) unsigned NOT NULL COMMENT 'Id del cliente asociado a la Factura',
  `id_cliente_direccion` int(10) unsigned NOT NULL COMMENT 'Id de la Dirección del Cliente',
  `url` varchar(255) DEFAULT NULL COMMENT 'Url de la imagen de la factura',
  `key_s3` varchar(255) DEFAULT NULL COMMENT 'Nombre del archivo cargado en S3',
  `total` decimal(10,2) DEFAULT NULL COMMENT 'Valor total de la factura',
  `subtotal` decimal(10,2) DEFAULT NULL COMMENT 'Valor del subtotal de la factura',
  `iva` decimal(10,2) DEFAULT NULL COMMENT 'Valor del iva de la factura',
  `ica` decimal(10,2) DEFAULT NULL COMMENT 'Valor del ica de la factura',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `consecutivo_UNIQUE` (`consecutivo`),
  KEY `fk_factura_cliente_idx` (`id_cliente`),
  KEY `fk_factura_cliente_direccion_idx` (`id_cliente_direccion`),
  KEY `fk_factura_estado_idx` (`id_estado`),
  KEY `fk_factura_tipo_pago_idx` (`id_tipo_pago`),
  KEY `fk_factura_usuario` (`id_usuario`),
  KEY `fk_factura_remision_idx` (`id_remision`),
  CONSTRAINT `fk_factura_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_factura_cliente_direccion` FOREIGN KEY (`id_cliente_direccion`) REFERENCES `clientes_direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_factura_estado` FOREIGN KEY (`id_estado`) REFERENCES `facturas_estados` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_factura_remision` FOREIGN KEY (`id_remision`) REFERENCES `remisiones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_factura_tipo_pago` FOREIGN KEY (`id_tipo_pago`) REFERENCES `facturas_tipos_pago` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_factura_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de las facturas generadas';

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;

INSERT INTO `facturas` (`id`, `id_remision`, `consecutivo`, `fecha_factura`, `fecha_carga`, `id_usuario`, `id_estado`, `id_tipo_pago`, `dias_credito`, `orden_compra`, `id_cliente`, `id_cliente_direccion`, `url`, `key_s3`, `total`, `subtotal`, `iva`, `ica`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,NULL,1,'2019-04-15','2019-04-17 09:15:26',1,2,1,NULL,'3245w5',1,1,'https://s3.us-east-1.amazonaws.com/afg-uploads/facturas/1_2019-04-17_Cliente1.png','facturas/1_2019-04-17_Cliente1.png',178500.00,150000.00,28500.00,NULL,'Admin Admin','2019-04-17 09:15:45','Admin Admin','2019-08-02 12:46:06');

/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla facturas_estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `facturas_estados`;

CREATE TABLE `facturas_estados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del estado de la factura',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del estado de la factura',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de los estados de la Factura';

LOCK TABLES `facturas_estados` WRITE;
/*!40000 ALTER TABLE `facturas_estados` DISABLE KEYS */;

INSERT INTO `facturas_estados` (`id`, `nombre`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,'Pagada',NULL,NULL,NULL,NULL),
	(2,'Por Cobrar',NULL,NULL,NULL,NULL),
	(3,'Anulada',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `facturas_estados` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla facturas_tipos_pago
# ------------------------------------------------------------

DROP TABLE IF EXISTS `facturas_tipos_pago`;

CREATE TABLE `facturas_tipos_pago` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del Tipo de Pago de la Factura',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del Tipo de Pago de la factura',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de los Tipos de Pago de una Factura';

LOCK TABLES `facturas_tipos_pago` WRITE;
/*!40000 ALTER TABLE `facturas_tipos_pago` DISABLE KEYS */;

INSERT INTO `facturas_tipos_pago` (`id`, `nombre`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,'Contado',NULL,NULL,NULL,NULL),
	(2,'Crédito',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `facturas_tipos_pago` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla inversiones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inversiones`;

CREATE TABLE `inversiones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la inversión',
  `id_factura` int(10) unsigned NOT NULL COMMENT 'Id de la Factura',
  `id_usuario` int(10) unsigned NOT NULL COMMENT 'Id del asesor que registra la inversión',
  `fecha` datetime DEFAULT NULL COMMENT 'Fecha de la Inversión',
  `total` decimal(10,2) DEFAULT NULL COMMENT 'Valor Total de la Inversión',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_inversion_factura_idx` (`id_factura`),
  KEY `fk_inversion_usuario_idx` (`id_usuario`),
  CONSTRAINT `fk_inversion_factura` FOREIGN KEY (`id_factura`) REFERENCES `facturas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inversion_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de las inversiones';

LOCK TABLES `inversiones` WRITE;
/*!40000 ALTER TABLE `inversiones` DISABLE KEYS */;

INSERT INTO `inversiones` (`id`, `id_factura`, `id_usuario`, `fecha`, `total`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(14,1,1,'2019-08-02 05:00:00',300000.00,'Admin Admin','2019-07-31 19:22:56','Admin Admin','2019-08-01 21:44:14');

/*!40000 ALTER TABLE `inversiones` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla inversiones_reales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inversiones_reales`;

CREATE TABLE `inversiones_reales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la inversión real',
  `id_factura` int(10) unsigned NOT NULL COMMENT 'Id de la Factura',
  `asesor` varchar(100) DEFAULT NULL COMMENT 'Asesor que registra la inversión real',
  `fecha` datetime DEFAULT NULL COMMENT 'Fecha de la Inversión real',
  `total` decimal(10,2) DEFAULT NULL COMMENT 'Valor Total de la Inversión real',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_inversion_real_factura_idx` (`id_factura`),
  CONSTRAINT `fk_inversion_real_factura` FOREIGN KEY (`id_factura`) REFERENCES `facturas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de las inversiones reales';



# Volcado de tabla parametros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parametros`;

CREATE TABLE `parametros` (
  `nombre` varchar(10) NOT NULL COMMENT 'Nombre del parámetro',
  `value` varchar(250) NOT NULL COMMENT 'Valor del parámetro',
  PRIMARY KEY (`nombre`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Parametros de aplicación';

LOCK TABLES `parametros` WRITE;
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;

INSERT INTO `parametros` (`nombre`, `value`)
VALUES
	('COT_CONS','1'),
	('COT_DOCS','Ficha técnica,Ficha temática,Proceso de carga y mantenimiento,Hojas de seguridad,Certificación de garantía'),
	('COT_NOTA','Los extintores se entregan pintados, señalizados e instalados para cumplir normatividad.'),
	('FAC_CONS','2'),
	('IVA','19'),
	('REM_CONS','2');

/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla perfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perfiles`;

CREATE TABLE `perfiles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del Perfil',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del Perfil',
  `descripcion` varchar(200) DEFAULT NULL COMMENT 'Descripción del Perfil',
  `activo` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indica si el perfil está activo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información básica de los perfiles de usuario';

LOCK TABLES `perfiles` WRITE;
/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;

INSERT INTO `perfiles` (`id`, `nombre`, `descripcion`, `activo`)
VALUES
	(1,'Administrador','Administrador del Sistema',1),
	(2,'Vendedor','Vendedor de Productos',1),
	(3,'Comprador','Comprador de Productos',1),
	(4,'Ambos','Vende y Compra Productos',1);

/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla productos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del producto',
  `nombre` varchar(255) NOT NULL COMMENT 'Nombre del producto',
  `codigo` varchar(20) NOT NULL COMMENT 'Codigo del producto',
  `precio_unidad` decimal(10,2) NOT NULL COMMENT 'Precio unitario del producto',
  `activo` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Estado del producto',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de los productos';

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;

INSERT INTO `productos` (`id`, `nombre`, `codigo`, `precio_unidad`, `activo`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,'Extintor ABC','EXTABC',150000.00,1,'Admin Admin','2019-03-03 20:43:20',NULL,NULL);

/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla productos_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos_images`;

CREATE TABLE `productos_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la imagen del producto',
  `id_producto` int(11) unsigned NOT NULL COMMENT 'Id del Producto dueño de la imagen',
  `url_imagen` varchar(250) NOT NULL COMMENT 'URL de la imagen subida a AWS S3',
  `created_by` varchar(100) NOT NULL COMMENT 'Usuario que realiza el cargue de la imagen',
  `created_at` datetime NOT NULL COMMENT 'Fecha de cargue de la imagen',
  `updated_by` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `updated_at` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `url_imagen_UNIQUE` (`url_imagen`),
  KEY `INX_producto` (`id_producto`),
  CONSTRAINT `fk_productos_images` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='almacena la información de las imagenes de los productos';



# Volcado de tabla productos_presentacion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos_presentacion`;

CREATE TABLE `productos_presentacion` (
  `id` int(11) unsigned NOT NULL COMMENT 'Id de la presentación del producto',
  `nombre` varchar(45) NOT NULL COMMENT 'Presentacion del producto',
  `activo` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indica si la presentacion del producto esta activa',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena las formas de presentacion del producto';

LOCK TABLES `productos_presentacion` WRITE;
/*!40000 ALTER TABLE `productos_presentacion` DISABLE KEYS */;

INSERT INTO `productos_presentacion` (`id`, `nombre`, `activo`)
VALUES
	(1,'ATADO',1),
	(2,'DOCENA',1),
	(3,'KILO',1),
	(4,'BULTO',1);

/*!40000 ALTER TABLE `productos_presentacion` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla productos_x_factura
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos_x_factura`;

CREATE TABLE `productos_x_factura` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la relación producto - factura',
  `id_factura` int(10) unsigned NOT NULL COMMENT 'Id de la Factura',
  `id_producto` int(10) unsigned NOT NULL COMMENT 'Id del producto',
  `cantidad` int(11) NOT NULL COMMENT 'Cantidad de productos',
  `precio_unidad_venta` decimal(10,2) NOT NULL COMMENT 'Precio de venta del producto',
  `valor_venta` decimal(10,2) NOT NULL COMMENT 'Valor de venta del producto',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_profac_factura_idx` (`id_factura`),
  KEY `fk_profac_producto_idx` (`id_producto`),
  CONSTRAINT `fk_profac_factura` FOREIGN KEY (`id_factura`) REFERENCES `facturas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profac_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena las relaciones entre productos y factura';

LOCK TABLES `productos_x_factura` WRITE;
/*!40000 ALTER TABLE `productos_x_factura` DISABLE KEYS */;

INSERT INTO `productos_x_factura` (`id`, `id_factura`, `id_producto`, `cantidad`, `precio_unidad_venta`, `valor_venta`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,1,1,1,150000.00,150000.00,'Admin Admin','2019-04-17 09:15:45','Admin Admin','2019-08-02 12:46:06');

/*!40000 ALTER TABLE `productos_x_factura` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla productos_x_inversion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos_x_inversion`;

CREATE TABLE `productos_x_inversion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la relación producto - inversión',
  `id_inversion` int(10) unsigned NOT NULL COMMENT 'Id de la inversión',
  `id_producto` int(10) unsigned NOT NULL COMMENT 'Id del producto',
  `cantidad` int(11) NOT NULL COMMENT 'Cantidad de productos',
  `precio_unidad_venta` decimal(10,2) NOT NULL COMMENT 'Precio unitario de venta del producto',
  `valor_venta` decimal(10,2) NOT NULL COMMENT 'Valor de venta del producto',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_proinv_inversion_idx` (`id_inversion`),
  KEY `fk_proinv_producto_idx` (`id_producto`),
  CONSTRAINT `fk_proinv_inversion` FOREIGN KEY (`id_inversion`) REFERENCES `inversiones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proinv_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de productos x inversion';

LOCK TABLES `productos_x_inversion` WRITE;
/*!40000 ALTER TABLE `productos_x_inversion` DISABLE KEYS */;

INSERT INTO `productos_x_inversion` (`id`, `id_inversion`, `id_producto`, `cantidad`, `precio_unidad_venta`, `valor_venta`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(21,14,1,1,300000.00,300000.00,'Admin Admin','2019-07-31 19:22:56','Admin Admin','2019-08-01 21:44:14');

/*!40000 ALTER TABLE `productos_x_inversion` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla productos_x_inversion_real
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos_x_inversion_real`;

CREATE TABLE `productos_x_inversion_real` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la relación producto - inversión real',
  `id_inversion_real` int(10) unsigned NOT NULL COMMENT 'Id de la inversión real',
  `id_producto` int(10) unsigned NOT NULL COMMENT 'Id del producto',
  `cantidad` int(11) NOT NULL COMMENT 'Cantidad de productos',
  `precio_unidad_venta` decimal(10,2) NOT NULL COMMENT 'Precio unitario de venta del producto',
  `valor_venta` decimal(10,2) NOT NULL COMMENT 'Valor de venta del producto',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_proinv_inversion_real_idx` (`id_inversion_real`),
  KEY `fk_proinv_producto_idx` (`id_producto`),
  CONSTRAINT `fk_proinv_inversion_real` FOREIGN KEY (`id_inversion_real`) REFERENCES `inversiones_reales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proinv_producto_real` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de productos x inversion real';



# Volcado de tabla productos_x_remision
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productos_x_remision`;

CREATE TABLE `productos_x_remision` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la relación productos x remisión',
  `id_remision` int(10) unsigned NOT NULL COMMENT 'Id de la remisión',
  `id_producto` int(10) unsigned NOT NULL COMMENT 'Id del producto',
  `cantidad` int(11) NOT NULL COMMENT 'Cantidad de productos x remisión',
  `precio_unidad_venta` decimal(10,2) NOT NULL COMMENT 'Precio de venta del producto',
  `valor_venta` decimal(10,2) NOT NULL COMMENT 'Valor de venta del producto',
  `createdby` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_pro_x_rem_remision_idx` (`id_remision`),
  KEY `fk_pro_x_rem_producto_idx` (`id_producto`),
  CONSTRAINT `fk_pro_x_rem_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pro_x_rem_remision` FOREIGN KEY (`id_remision`) REFERENCES `remisiones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena las relaciones de productos x remisión';

LOCK TABLES `productos_x_remision` WRITE;
/*!40000 ALTER TABLE `productos_x_remision` DISABLE KEYS */;

INSERT INTO `productos_x_remision` (`id`, `id_remision`, `id_producto`, `cantidad`, `precio_unidad_venta`, `valor_venta`, `createdby`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,1,1,1,150000.00,150000.00,NULL,'2019-03-03 20:47:11',NULL,NULL);

/*!40000 ALTER TABLE `productos_x_remision` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla remisiones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `remisiones`;

CREATE TABLE `remisiones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la remisión',
  `consecutivo` int(11) NOT NULL COMMENT 'Consecutivo de la remisión',
  `fecha_remision` date NOT NULL COMMENT 'Fecha de la remisión',
  `id_cliente` int(10) unsigned NOT NULL COMMENT 'Id del cliente al que se le crea la remisión',
  `id_cliente_direccion` int(10) unsigned NOT NULL COMMENT 'Id de la dirección del cliente de la remisión',
  `id_usuario` int(10) unsigned NOT NULL COMMENT 'Id del usuario dueño de la remisión',
  `observaciones` varchar(255) DEFAULT NULL COMMENT 'Observaciones acerca de la remisión',
  `url` varchar(255) DEFAULT NULL COMMENT 'Url de la imagen de la remisión',
  `key_s3` varchar(45) DEFAULT NULL COMMENT 'Nombre del archivo cargado en S3',
  `total` decimal(10,2) DEFAULT NULL COMMENT 'Valor total de la remisión',
  `subtotal` decimal(10,2) DEFAULT NULL COMMENT 'Valor subtotal de la remision',
  `iva` decimal(10,2) DEFAULT NULL COMMENT 'IVA de la remisión',
  `factura` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flag que indica si ya se generó una factura para la remisión',
  `createdby` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` varchar(255) DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `consecutivo_UNIQUE` (`consecutivo`),
  KEY `fk_remision_cliente_idx` (`id_cliente`),
  KEY `fk_remision_cliente_direccion_idx` (`id_cliente_direccion`),
  KEY `fk_remision_usuario_idx` (`id_usuario`),
  CONSTRAINT `fk_remision_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remision_cliente_direccion` FOREIGN KEY (`id_cliente_direccion`) REFERENCES `clientes_direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remision_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena las remisiones';

LOCK TABLES `remisiones` WRITE;
/*!40000 ALTER TABLE `remisiones` DISABLE KEYS */;

INSERT INTO `remisiones` (`id`, `consecutivo`, `fecha_remision`, `id_cliente`, `id_cliente_direccion`, `id_usuario`, `observaciones`, `url`, `key_s3`, `total`, `subtotal`, `iva`, `factura`, `createdby`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,1,'2019-03-03',1,1,1,'Pruebas','https://s3.us-east-1.amazonaws.com/afg-uploads/remisiones/1_awsecs.png','remisiones/1_awsecs.png',178500.00,150000.00,28500.00,0,NULL,'2019-03-03 20:47:11',NULL,'2019-04-17 09:21:40.252');

/*!40000 ALTER TABLE `remisiones` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del Rol',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del Rol',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de los Roles de Usuario';

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `nombre`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,'Administrador','Admin','2018-05-05 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla unidades_medida
# ------------------------------------------------------------

DROP TABLE IF EXISTS `unidades_medida`;

CREATE TABLE `unidades_medida` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id de la unidad de Medida',
  `nombre` varchar(30) NOT NULL COMMENT 'Nombre de la Unidad de Medida',
  `sigla` varchar(7) NOT NULL COMMENT 'Sigla de la Unidad de Medida',
  `activa` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indica si la unidad de medida está activa',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`),
  UNIQUE KEY `sigla_UNIQUE` (`sigla`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Almacena la información de las unidades de medida de los pr';

LOCK TABLES `unidades_medida` WRITE;
/*!40000 ALTER TABLE `unidades_medida` DISABLE KEYS */;

INSERT INTO `unidades_medida` (`id`, `nombre`, `sigla`, `activa`)
VALUES
	(1,'KILO','Kg',1);

/*!40000 ALTER TABLE `unidades_medida` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla usuarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id del Usuario',
  `nombres` varchar(45) NOT NULL COMMENT 'Nombres del Usuario',
  `apellidos` varchar(45) NOT NULL COMMENT 'Apellidos del Usuario',
  `correo` varchar(100) NOT NULL COMMENT 'Correo del usuario',
  `documento` varchar(100) NOT NULL COMMENT 'Documento del Usuario',
  `password` varchar(100) NOT NULL COMMENT 'Password de acceso del usuario',
  `id_rol` int(10) unsigned NOT NULL COMMENT 'Id del rol asociado al usuario',
  `activo` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Estado del usuario',
  `vendedor` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indica si el usuario es vendedor de facturas',
  `acceso` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indica si el usuario tiene acceso a la plataforma',
  `createdBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que crea el registro',
  `createdDate` datetime DEFAULT NULL COMMENT 'Fecha de creación del registro',
  `modifiedBy` varchar(100) DEFAULT NULL COMMENT 'Usuario que realiza la ultima modificación al registro',
  `modifiedDate` datetime DEFAULT NULL COMMENT 'Fecha de la ultima modificación al registro',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `correo_UNIQUE` (`correo`),
  UNIQUE KEY `login_UNIQUE` (`documento`),
  KEY `fk_usuario_rol_idx` (`id_rol`),
  KEY `inx_vendedor` (`vendedor`),
  CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='almacena la Información de los Usuarios';

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;

INSERT INTO `usuarios` (`id`, `nombres`, `apellidos`, `correo`, `documento`, `password`, `id_rol`, `activo`, `vendedor`, `acceso`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`)
VALUES
	(1,'Admin','Admin','admin@gmail.com','123456789','{bcrypt}$2a$10$Rmsx/EoCurumoBIZUDimZOC/1LebZ.Q7Z73eemFgtwMDQkQSm7bVm',1,1,1,1,'Admin','2019-03-03 20:40:46',NULL,NULL);

/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
